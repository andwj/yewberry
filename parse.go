// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

// A "node" is a token which has been analysed and given an ND_XXX
// kind and/or extra information in the Info field.  The code in
// this file is primarily concerned with syntax, it is *the* place
// where special syntax is decoded into a simpler node tree.

import "fmt"

const (
	ND_INVALID TokenKind = 20 + iota

	ND_DefVar    // name | value.  NOTE: ND_Let is used within functions
	ND_DefFunc   // name | params | body.  Info.return_ty is return type (nil if not given)
	ND_DefMethod // name | params | body.  params[0] has the receiver type
	ND_Params    // each child is a TOK_Name, Info.ty has its type (nil if not given)
	ND_Lambda    // params | body.
	ND_Native    // represents #native syntax, .Str is the target name.

	ND_Void    // evaluates to {void}
	ND_Literal // int/float/str/enum literals, Info.lit has decoded value
	ND_RawTag  // used for identifiers beginning with backquote

	ND_Local   // Str is name, Info.lvar is variable info
	ND_Upvar   // Str is name, Info.lvar is variable info
	ND_Global  // Str is name, Info.gdef is global definition

	ND_Array     // children are the array elements
	ND_Map       // children are ND_MapPair
	ND_MapPair   // first child is key, second is value
	ND_Set       // children are the set elements
	ND_Tuple     // children are field values or ND_TuplePair
	ND_TuplePair // first child is name, second is value
	ND_Object    // children are field values or ND_TuplePair. Info has type
	ND_Union     // child is datum value, Info has tag index
	ND_String    // children are characters or strings

	ND_FunCall     // func | zero or more parameters.
	ND_MethodCall  // name | obj | parameters.  Info.lit is function

	ND_Block    // sequence of elements, at least one
	ND_Let      // var | value | body
	ND_Skip     // child is value.  Str is label
	ND_If       // condition | then body | else body
	ND_While    // condition | body

	ND_Cast      // child is value, Info.ty is new type, or nil for simple casts
	ND_TagConv   // child is value, Info.ty is new type (int or str)
	ND_ClassName // child is value
	ND_Wrap      // child is value
	ND_Unwrap    // child is value
	ND_Flatten   // child is value
	ND_Fmt       // child is value, Info.fmt_spec is format spec.

	ND_SetVar   // var | value
	ND_SetField // obj | value.  Str is field name
	ND_GetField // obj           Str is field name

	ND_IsTag       // value | tag names...
	ND_RefEqual    // two children, L and R values

	ND_Match       // first is expr, rest are ND_MatchRule nodes
	ND_MatchRule   // pattern | body | where-cond (optional)
	ND_MatchAll    // represents the '_' match-all pattern
	ND_MatchConsts // one or more constant values
	ND_MatchTags   // one or more tag names
	ND_MatchUnion  // tag name | pattern for datum
)

// NodeInfo provides extra information for each Token (i.e. node)
// in the code tree.
type NodeInfo struct {
	// the type for ND_Array, ND_Map, ND_Union, ND_Cast (etc).
	// for arrays and maps it is generally NIL unless the type was
	// explicitly specified, though it gets decided eventually.
	//
	// for ND_DefMethod, this is the type which gets the method
	// (for prelude it can be different than the type of "self").
	ty *Type

	// for ND_Literal and ND_Lambda
	lit *Value

	// for ND_Let and ND_DefVar, indicates the "let-mut" form was used
	mutable bool

	// tag for ND_Union constructor, ND_GetField, and children of ND_MatchTags
	tag_id int

	// for ND_DefFunc, ND_DefMethod, ND_Lambda: the return type (void if absent)
	return_ty *Type

	// for function/method definitions: any generic types used
	gen_types map[string]*Type

	// for ND_Fmt: the decoded format string like "%3d"
	fmt_spec *Formatter

	// for match against arrays (etc), -1 means "..." at start, +1 means at end.
	open int

	// marks tail-call positions (for function/method calls)
	tail bool

	// for ND_MethodCall, true when it was an operator
	is_operator bool

	// for ND_Local and ND_Let
	lvar *LocalVar

	// for ND_Global
	gdef *GlobalDef

	// for ND_Skip and ND_Block
	junction *JunctionInfo

	// for ND_MatchRule, all the bindings created in patterns
	group *LocalGroup
}

func NewNode(kind TokenKind, num_children int) *Token {
	nd := new(Token)
	nd.Kind = kind
	nd.Info = NewNodeInfo()
	nd.Children = make([]*Token, num_children)
	return nd
}

func NewNodeInfo() *NodeInfo {
	nd := new(NodeInfo)
	return nd
}

func (nd *Token) InitInfo() {
	if nd.Info == nil {
		nd.Info = NewNodeInfo()
	}

	if nd.Children != nil {
		for _, child := range nd.Children {
			child.InitInfo()
		}
	}
}

func (t *Token) String2() string {
	// this handles the ND_XXX node types defined in the file

	switch t.Kind {
	case ND_DefVar: return "ND_DefVar"
	case ND_DefFunc: return "ND_DefFunc"
	case ND_DefMethod: return "ND_DefMethod"
	case ND_Params: return "ND_Params"
	case ND_Lambda: return "ND_Lambda"
	case ND_Native: return "ND_Native"

	case ND_Void: return "ND_Void"
	case ND_Literal: return "ND_Literal '" + t.Str + "'"
	case ND_RawTag: return "ND_RawTag " + t.Str

	case ND_Local: return "ND_Local '" + t.Str + "'"
	case ND_Upvar: return "ND_Upvar '" + t.Str + "'"
	case ND_Global: return "ND_Global '" + t.Str + "'"

	case ND_Array: return "ND_Array"
	case ND_Set: return "ND_Set"
	case ND_Map: return "ND_Map"
	case ND_MapPair: return "ND_MapPair"
	case ND_Tuple: return "ND_Tuple"
	case ND_TuplePair: return "ND_TuplePair"
	case ND_Object: return "ND_Object"
	case ND_Union: return "ND_Union"
	case ND_String: return "ND_String"

	case ND_FunCall: return "ND_FunCall"
	case ND_MethodCall: return "ND_MethodCall " + t.Str

	case ND_Block: return "ND_Block"
	case ND_Skip: return "ND_Skip " + t.Str
	case ND_If: return "ND_If"
	case ND_While: return "ND_While"

	case ND_Let: return "ND_Let"
	case ND_Cast: return "ND_Cast"
	case ND_TagConv: return "ND_TagConv"
	case ND_ClassName: return "ND_ClassName"
	case ND_Wrap: return "ND_Wrap"
	case ND_Unwrap: return "ND_Unwrap"
	case ND_Flatten: return "ND_Flatten"
	case ND_Fmt: return "ND_Fmt"
	case ND_SetVar: return "ND_SetVar"
	case ND_SetField: return "ND_SetField '" + t.Str + "'"
	case ND_GetField: return "ND_GetField '" + t.Str + "'"

	case ND_IsTag: return "ND_IsTag"
	case ND_RefEqual: return "ND_RefEqual"

	case ND_Match: return "ND_Match"
	case ND_MatchRule: return "ND_MatchRule"
	case ND_MatchAll: return "ND_MatchAll '_'"
	case ND_MatchConsts: return "ND_MatchConsts"
	case ND_MatchTags: return "ND_MatchTags"
	case ND_MatchUnion: return "ND_MatchUnion"

	default:
		return "!!!INVALID!!!"
	}
}

func (nd *Token) InfoString() string {
	if nd.Info == nil {
		return ""
	}

	s := ""

	switch nd.Kind {
	case ND_Union:
		s = fmt.Sprintf(" tag:%d", nd.Info.tag_id)
	case ND_DefVar, ND_Let:
		if nd.Info.mutable {
			s = "mutable"
		}
	}

	// show type information, but not too long
	if nd.Info.ty != nil {
		ts := nd.Info.ty.String()
		if len(ts) > 32 {
			ts = ts[0:30] + "..."
		}

		if s != "" {
			s += " "
		}
		s += "ty:" + ts
	}

	return s
}

// DumpTokens is a debugging aid, displays each high-level token.
func DumpTokens(lex *Lexer) {
	for {
		t := lex.Scan()

		t.Dump(0)

		if t.Kind == TOK_EOF {
			break
		}
	}
}

func (t *Token) Dump(level int) {
	Print("line %4d: %*s%s %s %s\n", t.LineNum, level, "",
		t.String(), t.InfoString(), t.Cat.String())

	if t.Children != nil {
		for _, child := range t.Children {
			child.Dump(level + 2)
		}
	}
}

//----------------------------------------------------------------------

func ParseElement(t *Token) cmError {
	return ParseElement2(t, nil)
}

func ParseElement2(t *Token, lit_type *Type) cmError {
	// lit_type is non-NIL when we are parsing something, primarily
	// literals in {}, and we KNOW exactly what type it should be.
	// this allows data structures to omit some type indicators.
	//
	// Note that parents are still generally responsible for doing a
	// type check on their children -- that's why we don't pass lit_type
	// to ParseInt(), plus the deduce code always catches a mismatch.

	ErrorSetToken(t)

	// ensure node and all children have an Info struct
	t.InitInfo()

	switch t.Kind {
	case TOK_Name:
		return ParseName(t, lit_type)

	case TOK_Int:
		return ParseInt(t)

	case TOK_Float:
		return ParseFloat(t)

	case TOK_Char:
		return ParseChar(t)

	case TOK_String:
		return ParseString(t)

	case TOK_Expr:
		return ParseExpr(t)

	case TOK_Access:
		return ParseAccess(t)

	case TOK_Data:
		return ParseDataStruct(t, lit_type)

	default:
		PostError("syntax error, got: %s", t.String())
		return FAILED
	}
}

func ParseInt(t *Token) cmError {
	t.Info.lit = new(Value)

	err := t.Info.lit.DecodeInt(t.Str)
	if err != nil {
		PostError("%s", err.Error())
		return FAILED
	}

	t.Kind = ND_Literal
	t.Info.ty = t.Info.lit.ty

	return OKAY
}

func ParseFloat(t *Token) cmError {
	t.Info.lit = new(Value)

	err := t.Info.lit.DecodeFloat(t.Str)
	if err != nil {
		PostError("%s", err.Error())
		return FAILED
	}

	t.Kind = ND_Literal
	t.Info.ty = t.Info.lit.ty

	return OKAY
}

func ParseChar(t *Token) cmError {
	t.Info.lit = new(Value)

	err := t.Info.lit.DecodeChar(t.Str)
	if err != nil {
		PostError("%s", err.Error())
		return FAILED
	}

	t.Kind = ND_Literal
	t.Info.ty = t.Info.lit.ty

	return OKAY
}

func ParseString(t *Token) cmError {
	t.Info.lit = new(Value)

	err := t.Info.lit.DecodeString(t.Str)
	if err != nil {
		PostError("%s", err.Error())
		return FAILED
	}

	t.Kind = ND_Literal
	t.Info.ty = t.Info.lit.ty

	return OKAY
}

func ParseDataStruct(t *Token, lit_type *Type) cmError {
	children := t.Children

	if len(children) > 0 {
		head := children[0]

		// handle indicator of type, either a vague indicator like
		// "array" and "map" (where full type will be deduced later)
		// or an explicit type.

		switch {
		case head.Match("array"):
			t.Children = t.Children[1:]
			return ParseArrayLiteral(t, lit_type)

		case head.Match("byte-vec"):
			t.Children = t.Children[1:]
			return ParseArrayLiteral(t, bytevec_type)

		case head.Match("map"):
			t.Children = t.Children[1:]
			return ParseMapLiteral(t, lit_type)

		case head.Match("set"):
			t.Children = t.Children[1:]
			return ParseSetLiteral(t, lit_type)

		case head.Match("str"):
			t.Children = t.Children[1:]
			return ParseStringBuilder(t)

		case head.Match("no"):
			return ParseNoLiteral(t)

		case head.Match("wrap"):
			return ParseWrap(t)

//??		case head.Match("res"), head.Match("error"):
//??			return ParseResLiteral(t)
		}

		if IsTypeSpec(head) {
			data_type, err2 := ParseType(head)
			if err2 != OKAY {
				return FAILED
			}

			// do a basic compatibility check
			// (the deduce code will perform a full check)
			if lit_type != nil {
				t1 := lit_type
				t2 := data_type

				if t1.base != t2.base {
					PostError("type mismatch in literal: wanted %s, got %s",
						lit_type.String(), data_type.String())
					return FAILED
				}
			}

			lit_type = data_type

			t.Children = t.Children[1:]
		}
	}

	// without type information it will be a simple tuple.
	// [ though later on it may be deduced into an object literal ]
	if lit_type == nil {
		return ParseTupleLiteral(t, "tuple", nil)
	}

	type2 := lit_type

	switch type2.base {
	case TYP_Array, TYP_ByteVec:
		return ParseArrayLiteral(t, lit_type)

	case TYP_Map:
		return ParseMapLiteral(t, lit_type)

	case TYP_Set:
		return ParseSetLiteral(t, lit_type)

	case TYP_Tuple:
		return ParseTupleLiteral(t, "tuple", lit_type)

	case TYP_Class:
		return ParseTupleLiteral(t, "object", lit_type)

	case TYP_Union:
		return ParseUnionLiteral(t, lit_type)

	case TYP_Enum:
		return ParseEnumLiteral(t, lit_type)

	case TYP_Int, TYP_Float, TYP_String:
		if lit_type.custom != "" {
			return ParseCustomLiteral(t, lit_type)
		}

	case TYP_Void:
		if len(t.Children) == 0 {
			t.Kind = ND_Void
			t.Info.ty = void_type
			t.Children = nil
			return OKAY
		}
		PostError("wrong syntax for void literal")
		return FAILED
	}

	PostError("cannot use {} syntax with type: %s", lit_type.String())
	return FAILED
}

func ParseCustomLiteral(t *Token, lit_type *Type) cmError {
	if len(t.Children) < 1 {
		PostError("custom literal is missing value")
		return FAILED
	}
	if len(t.Children) > 1 {
		PostError("custom literal has extra rubbish at end")
		return FAILED
	}

	t_exp := t.Children[0]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	if t_exp.Kind == ND_Literal {
		if t_exp.Info.lit.ty.base != lit_type.base {
			PostError("type mismatch in custom literal: wanted %s, got %s",
				lit_type.base.String(), t_exp.Info.lit.ty.base.String())
			return FAILED
		}

		t.Replace(t_exp)

		t.Info.lit.ty = lit_type
		t.Info.ty = lit_type

	} else {
		t.Kind = ND_Cast
		t.Info.ty = lit_type

		t.Children = make([]*Token, 0)
		t.Add(t_exp)
	}

	return OKAY
}

func ParseArrayLiteral(t *Token, arr_type *Type) cmError {
	children := t.Children
	t.Children = make([]*Token, 0)

	t.Kind = ND_Array

	var elem_type *Type

	if arr_type != nil {
		t.Info.ty = arr_type

		if arr_type.base == TYP_Array {
			elem_type = arr_type.sub
		} else if arr_type.base == TYP_ByteVec {
			elem_type = int_type
		} else {
			PostError("array literal with non-array type: %s", arr_type.String())
			return FAILED
		}
	}

	for _, elem := range children {
		if ParseElement2(elem, elem_type) != OKAY {
			return FAILED
		}

		t.Add(elem)
	}

	return OKAY
}

func ParseTupleLiteral(t *Token, what string, tup_type *Type) cmError {
	// Note: this code handles object literals too (TYP_Class)

	children := t.Children

	if len(children) == 0 {
		PostError("empty data structure")
		return FAILED
	}

	t.Kind = ND_Tuple
	t.Info.ty = tup_type
	t.Children = make([]*Token, 0)

	if what == "object" {
		t.Kind = ND_Object
	}

	// a trailing '...' indicates that not all fields are present,
	// and remaining ones should get their default value.
	if len(children) > 0 && children[len(children)-1].Match("...") {
		t.Info.open = 1
		children = children[0 : len(children)-1]
	}

	// check for explicit fields
	has_fields := false
	for _, child := range children {
		if child.IsNamedField() {
			has_fields = true
			break
		}
	}

	if has_fields {
		seen_fields := make(map[string]bool)

		count := len(children)
		if count%2 != 0 {
			PostError("%s literal: missing/malformed fields", what)
			return FAILED
		}
		count /= 2

		// check number of fields (when type is explicit)
		if tup_type != nil && count > len(tup_type.param) {
			PostError("too many fields in %s literal: wanted %d, got %d",
				what, len(tup_type.param), count)
			return FAILED
		}

		for i := 0; i < len(children); i += 2 {
			t_field := children[i]
			t_val := children[i+1]

			if !t_field.IsNamedField() {
				PostError("%s literal: expected field name, got %s",
					what, t_field.String())
				return FAILED
			}
			if seen_fields[t_field.Str] {
				PostError("%s literal: duplicate field %s", what, t_field.Str)
				return FAILED
			}
			if t_val.IsNamedField() {
				PostError("%s literal: missing value for %s", what, t_field.Str)
				return FAILED
			}

			seen_fields[t_field.Str] = true

			var field_type *Type

			if tup_type != nil {
				field_idx := i / 2
				if what == "object" {
					field_idx = tup_type.FindParam(t_field.Str)
				}
				if field_idx >= 0 {
					field_type = tup_type.param[field_idx].ty
				}
			}

			if ParseElement2(t_val, field_type) != OKAY {
				return FAILED
			}

			pair := NewNode(ND_TuplePair, 2)
			pair.Children[0] = t_field
			pair.Children[1] = t_val
			pair.LineNum = t_field.LineNum

			t.Add(pair)
		}

	} else {
		count := len(children)

		if tup_type != nil && count > len(tup_type.param) {
			PostError("too many fields in %s literal: wanted %d, got %d",
				what, len(tup_type.param), count)
			return FAILED
		}

		for field_idx, t_val := range children {
			var field_type *Type

			if tup_type != nil {
				field_type = tup_type.param[field_idx].ty
			}

			if ParseElement2(t_val, field_type) != OKAY {
				return FAILED
			}

			t.Add(t_val)
		}
	}

	// not enough fields?
	if tup_type != nil && t.Info.open != 1 {
		count := len(t.Children)

		if count < len(tup_type.param) {
			PostError("not enough fields in %s literal: wanted %d, got %d",
				what, len(tup_type.param), count)
			return FAILED
		}
	}

	return OKAY
}

func ParseMapLiteral(t *Token, map_type *Type) cmError {
	children := t.Children
	t.Children = make([]*Token, 0)

	t.Kind = ND_Map

	var key_type *Type
	var elem_type *Type

	if map_type != nil {
		t.Info.ty = map_type

		if map_type.base != TYP_Map {
			PostError("map literal with non-map type: %s", map_type.String())
			return FAILED
		}

		key_type = map_type.KeyType()
		elem_type = map_type.sub
	}

	if len(children)%2 != 0 {
		PostError("map literal: odd number of elements")
		return FAILED
	}

	seen_keys := make(map[string]bool)

	for i := 0; i < len(children); i += 2 {
		t_key := children[i]
		t_val := children[i+1]

		if ParseElement2(t_key, key_type) != OKAY {
			return FAILED
		}

		if ParseElement2(t_val, elem_type) != OKAY {
			return FAILED
		}

		// detect simple cases of duplicate keys
		if t_key.Kind == ND_Literal {
			hash := t_key.Info.lit.HashString()
			if seen_keys[hash] {
				PostError("duplicate key in map: %s", t_key.Info.lit.DeepString())
				return FAILED
			}
			seen_keys[hash] = true
		}

		pair := NewNode(ND_MapPair, 2)
		pair.Children[0] = t_key
		pair.Children[1] = t_val
		pair.LineNum = t_key.LineNum

		t.Add(pair)
	}

	return OKAY
}

func ParseSetLiteral(t *Token, set_type *Type) cmError {
	children := t.Children
	t.Children = make([]*Token, 0)

	t.Kind = ND_Set

	var key_type *Type

	if set_type != nil {
		t.Info.ty = set_type

		if set_type.base != TYP_Set {
			PostError("set literal with non-set type: %s", set_type.String())
			return FAILED
		}

		key_type = set_type.KeyType()
	}

	seen_keys := make(map[string]bool)

	for _, t_key := range children {
		if ParseElement2(t_key, key_type) != OKAY {
			return FAILED
		}

		// detect simple cases of duplicate keys
		if t_key.Kind == ND_Literal {
			hash := t_key.Info.lit.HashString()
			if seen_keys[hash] {
				PostError("duplicate key in set: %s", t_key.Info.lit.DeepString())
				return FAILED
			}
			seen_keys[hash] = true
		}

		t.Add(t_key)
	}

	return OKAY
}

func ParseUnionLiteral(t *Token, union_type *Type) cmError {
	children := t.Children
	t.Children = make([]*Token, 0)

	if len(children) < 1 {
		PostError("union literal is missing tag")
		return FAILED
	}

	t_tag := children[0]

	if t_tag.Kind != TOK_Name {
		PostError("union literal is missing tag, got %s", t_tag.String())
		return FAILED
	}

	tag_id := union_type.FindParam(t_tag.Str)
	if tag_id < 0 {
		PostError("union literal: unknown tag %s in %s", t_tag.Str, union_type.String())
		return FAILED
	}

	datum_type := union_type.param[tag_id].ty

	t.Kind = ND_Union
	t.Info.ty = union_type
	t.Info.tag_id = tag_id

	if datum_type.base == TYP_Void && len(children) == 1 {
		datum := NewNode(ND_Void, 0)
		datum.Info.ty = void_type
		datum.LineNum = t.LineNum

		t.Add(datum)

	} else {
		if len(children) < 2 {
			PostError("union literal is missing datum value")
			return FAILED
		} else if len(children) > 2 {
			PostError("union literal has extra rubbish at end")
			return FAILED
		}

		datum := children[1]

		if ParseElement2(datum, datum_type) != OKAY {
			return FAILED
		}

		t.Add(datum)
	}

	return OKAY
}

func ParseEnumLiteral(t *Token, enum_type *Type) cmError {
	children := t.Children
	t.Children = make([]*Token, 0)

	if len(children) < 1 {
		PostError("enum literal is missing tag")
		return FAILED
	}

	t_tag := children[0]

	if t_tag.Kind != TOK_Name {
		PostError("enum literal is missing tag, got %s", t_tag.String())
		return FAILED
	}

	tag_id := enum_type.FindParam(t_tag.Str)
	if tag_id < 0 {
		PostError("enum literal: unknown tag %s in %s", t_tag.Str, enum_type.String())
		return FAILED
	}

	t.Kind = ND_Literal
	t.Info.ty = enum_type
	t.Info.lit = new(Value)
	t.Info.lit.MakeEnum(enum_type, int64(tag_id))

	return OKAY
}

func ParseStringBuilder(t *Token) cmError {
	children := t.Children

	t.Kind = ND_String
	t.Info.ty = str_type
	t.Children = make([]*Token, 0)

	append_literal := func(s string) {
		if s == "" {
			return
		}

		if len(t.Children) > 0 {
			last := t.Children[len(t.Children) - 1]
			if last.Kind == ND_Literal && last.Info.ty.base == TYP_String {
				last.Info.lit.MakeString(last.Info.lit.Str + s)
				last.Str = last.Info.lit.Str
				return
			}
		}

		t_str := NewNode(ND_Literal, 0)
		t_str.Str = s
		t_str.Info.ty = str_type
		t_str.Info.lit = new(Value)
		t_str.Info.lit.MakeString(s)

		t.Add(t_str)
	}

	for _, child := range children {
		if ParseElement(child) != OKAY {
			return FAILED
		}

		// handle string and character literals
		if child.Kind == ND_Literal {
			elem_type := child.Info.ty

			if elem_type.base == TYP_String {
				append_literal(child.Info.lit.Str)
				continue
			}
			if elem_type.base == TYP_Int {
				// TODO validate character
				var r [1]rune
				r[0] = rune(child.Info.lit.Int)
				append_literal(string(r[:]))
				continue
			}
		}

		t.Add(child)
	}

	// simplify pure strings to an ND_Literal token
	if len(t.Children) == 0 {
		t.Kind = ND_Literal
		t.Str = ""
		t.Info.lit = new(Value)
		t.Info.lit.MakeString("")
		t.Children = nil

	} else if len(t.Children) == 1 && t.Children[0].Kind == ND_Literal &&
		t.Children[0].Info.ty.base == TYP_String {

		t.Kind = ND_Literal
		t.Info.lit = t.Children[0].Info.lit
		t.Str = t.Info.lit.Str
		t.Children = nil
	}

	return OKAY
}

//----------------------------------------------------------------------

func ParseExpr(t *Token) cmError {
	if len(t.Children) == 0 {
		PostError("empty expression in ()")
		return FAILED
	}

	// check for special forms....
	if len(t.Children) > 0 {
		head := t.Children[0]

		switch {
		case head.Match("cast"):
			return ParseCast(t)
		case head.Match("tag-int"):
			return ParseTagConv(t, int_type)
		case head.Match("tag$"):
			return ParseTagConv(t, str_type)
		case head.Match("class$"):
			return ParseClassName(t)
		case head.Match("unwrap"):
			return ParseUnwrap(t)
		case head.Match("flatten"):
			return ParseFlatten(t)
		case head.Match("is?"):
			return ParseIsTag(t)
		case head.Match("ref-eq?"):
			return ParseRefEqual(t)

		case head.Match("begin"):
			return ParseBegin(t)
		case head.Match("if"):
			return ParseIf(t)
		case head.Match("match"):
			return ParseMatch(t)
		case head.Match("while"):
			return ParseWhile(t)
		case head.Match("skip"), head.Match("junction"):
			// ParseBlock handles proper usage of skip and junction
			PostError("bad context for %s (must be in a block)", head.Str)
			return FAILED

		case head.Match("lam"):
			return ParseLambda(t)
		case head.Match("fmt$"):
			return ParseFmt(t)
		case head.Match("set!"):
			return ParseSetVar(t)

		// Note that "let" within a function is handled elsewhere
		case head.Match("let"):
			return ParseLet(t, ND_DefVar, false)
		case head.Match("let-mut"):
			return ParseLet(t, ND_DefVar, true)
		case head.Match("fun"):
			return ParseDefFunc(t)

		case head.Match("macro"), head.Match("type"), head.Match("generic-type"), head.Match("interface"):
			PostError("cannot define %ss within code", head.Str)
			return FAILED
		}

		// a method call?
		if head.IsField() {
			return ParseMethodCall(t)
		}
	}

	// everything else is a function call
	t.Kind = ND_FunCall
	return ParseAllChildren(t)
}

func ParseMethodCall(t *Token) cmError {
	if len(t.Children) < 2 {
		PostError("method call too short, missing object")
		return FAILED
	}

	t.Kind = ND_MethodCall
	return ParseAllChildren(t)
}

func ParseAccess(t *Token) cmError {
	if len(t.Children) < 1 {
		PostError("access in [] is empty")
		return FAILED
	}
	if len(t.Children) < 2 {
		PostError("access in [] is lacking indexors")
		return FAILED
	}

	head := t.Children[0]

	if head.IsField() {
		PostError("cannot use field/method name in that context")
		return FAILED
	}

	if ParseAllChildren(t) != OKAY {
		return FAILED
	}

	// rejig multiple indexors into a tree of nodes.
	// e.g. [a 1 2 3] --> (.get-elem (.get-elem (.get-elem a 1) 2) 3)
	// also handle the "unwrap" and "flatten" indexors.

	t_object := t.Children[0]
	indexors := t.Children[1:]

	var t_top *Token

	for _, t_index := range indexors {
		t_new := NewNode(ND_MethodCall, 0)
		t_new.LineNum = t_index.LineNum

		want_idx := false

		if t_index.Match("unwrap") {
			t_new.Kind = ND_Unwrap
		} else if t_index.Match("flatten") {
			t_new.Kind = ND_Flatten
		} else if t_index.IsField() {
			t_new.Kind = ND_GetField
			t_new.Str = t_index.Str
		} else {
			t_method := NewNode(TOK_Name, 0)
			t_method.Str = ".get-elem"
			t_method.LineNum = t_index.LineNum

			t_new.Add(t_method)

			want_idx = true
		}

		if t_top != nil {
			t_new.Add(t_top)
		} else {
			t_new.Add(t_object)
		}

		if want_idx {
			t_new.Add(t_index)
		}

		t_top = t_new
	}

	t.Replace(t_top)

	return OKAY
}

func ParseAllChildren(t *Token) cmError {
	for _, child := range t.Children {
		if ParseElement(child) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func ParseName(t *Token, lit_type *Type) cmError {
	// a raw tag can be used as an enum literal
	if t.IsTag() {
		if lit_type == nil {
			// deduce code may know the type...
			t.Kind = ND_RawTag
			return OKAY
		}

		if lit_type.base != TYP_Enum {
			PostError("type mismatch in data: wanted %s, got enum tag",
				lit_type.String())
			return FAILED
		}

		tag_id := lit_type.FindParam(t.Str)
		if tag_id < 0 {
			PostError("enum literal: unknown tag %s in %s",
				t.Str, lit_type.String())
			return FAILED
		}

		t.Kind = ND_Literal
		t.Info.ty = lit_type
		t.Info.lit = new(Value)
		t.Info.lit.MakeEnum(lit_type, int64(tag_id))

		return OKAY
	}

	// names are generally handled by bind code
	return OKAY
}

func ParseNoLiteral(t *Token) cmError {
	children := t.Children

	if len(children) < 2 {
		PostError("the no literal is missing type")
		return FAILED
	}
	if len(children) > 2 {
		PostError("the no literal has extra rubbish at end")
		return FAILED
	}

	target_type, err2 := ParseType(children[1])
	if err2 != OKAY {
		return FAILED
	}

	new_type := NewOptionalType(target_type)

	t.Kind = ND_Union
	t.Info.tag_id = 0 // `NONE tag
	t.Info.ty = new_type
	t.Children = make([]*Token, 0)
	t.Str = ""

	val := NewNode(ND_Void, 0)
	val.Info.ty = void_type
	val.LineNum = t.LineNum

	t.Add(val)

	return OKAY
}

func ParseWrap(t *Token) cmError {
	if len(t.Children) < 2 {
		PostError("missing argument to wrap")
		return FAILED
	}
	if len(t.Children) > 2 {
		PostError("too many arguments in wrap")
		return FAILED
	}

	t_exp := t.Children[1]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	t.Kind = ND_Wrap
	t.Children = make([]*Token, 0)
	t.Add(t_exp)

	return OKAY
}

func ParseUnwrap(t *Token) cmError {
	if len(t.Children) < 2 {
		PostError("missing argument to unwrap")
		return FAILED
	}
	if len(t.Children) > 2 {
		PostError("too many arguments in unwrap")
		return FAILED
	}

	t_exp := t.Children[1]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	t.Kind = ND_Unwrap
	t.Children = make([]*Token, 0)
	t.Add(t_exp)

	return OKAY
}

func ParseFlatten(t *Token) cmError {
	if len(t.Children) < 2 {
		PostError("missing argument to flatten")
		return FAILED
	}
	if len(t.Children) > 2 {
		PostError("too many arguments in flatten")
		return FAILED
	}

	t_exp := t.Children[1]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	t.Kind = ND_Flatten
	t.Children = make([]*Token, 0)
	t.Add(t_exp)

	return OKAY
}

func ParseIsTag(t *Token) cmError {
	children := t.Children

	if len(children) < 2 {
		PostError("missing argument to is?")
		return FAILED
	}
	if len(children) < 3 {
		PostError("missing tag name(s) to is?")
		return FAILED
	}

	t_exp := children[1]
	children = children[2:]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	t.Kind = ND_IsTag
	t.Info.ty = bool_type
	t.Children = make([]*Token, 0)
	t.Add(t_exp)

	for _, t_tag := range children {
		if t_tag.Kind != TOK_Name {
			PostError("illegal tag keyword for is?, got %s", t_tag.String())
			return FAILED
		}

		t.Add(t_tag)
	}

	return OKAY
}

func ParseRefEqual(t *Token) cmError {
	children := t.Children

	if len(children) < 3 {
		PostError("missing argument to ref-eq?")
		return FAILED
	}
	if len(children) > 3 {
		PostError("too many arguments for ref-eq?")
		return FAILED
	}

	t_left := children[1]
	t_right := children[2]

	if ParseElement(t_left) != OKAY {
		return FAILED
	}
	if ParseElement(t_right) != OKAY {
		return FAILED
	}

	t.Kind = ND_RefEqual
	t.Info.ty = bool_type
	t.Children = make([]*Token, 0)
	t.Add(t_left)
	t.Add(t_right)

	return OKAY
}

func ParseSetVar(t *Token) cmError {
	if len(t.Children) != 3 {
		PostError("bad set! syntax")
		return FAILED
	}

	t_name := t.Children[1]

	if t_name.Kind == TOK_Access {
		return ParseSetElem(t)
	}

	if t_name.Kind != TOK_Name {
		PostError("expected var name, got: %s", t_name.String())
		return FAILED
	}

	t.Kind = ND_SetVar
	t.Info.ty = void_type
	t.Children = t.Children[1:]

	return ParseElement(t.Children[1])
}

func ParseSetElem(t *Token) cmError {
	// this will convert TOK_Access into a node tree, with either
	// ND_MethodCall, ND_GetField or ND_Unwrap at the top.
	t_access := t.Children[1]
	if ParseElement(t_access) != OKAY {
		return FAILED
	}

	t_value := t.Children[2]
	if ParseElement(t_value) != OKAY {
		return FAILED
	}

	t.Replace(t_access)

	t.Info.ty = void_type
	t.Add(t_value)

	if t.Kind == ND_MethodCall {
		t.Children[0].Str = ".set-elem"
	} else if t.Kind == ND_GetField {
		t.Kind = ND_SetField
	} else {
		PostError("cannot write to unwrap/flatten indexor")
		return FAILED
	}

	return OKAY
}

func ParseBegin(t *Token) cmError {
	block := t.Children[1:]
	t.Children = make([]*Token, 0)

	// begin with no elements is OK
	if len(block) == 0 {
		t.Kind = ND_Void
		t.Info.ty = void_type
		t.Children = nil
		return OKAY
	}

	if ParseBlock(t, block) != OKAY {
		return FAILED
	}

	return OKAY
}

func ParseSkipOrJunction(t *Token, junc *JunctionInfo) cmError {
	// NOTE: for junctions, we do not keep the node created here

	what := t.Children[0].Str

	if what == "skip" {
		t.Kind = ND_Skip
		t.Info.ty = void_type
	}

	children := t.Children
	t.Children = make([]*Token, 0)

	if len(children) < 2 {
		PostError("%s is missing label name", what)
		return FAILED
	}
	if len(children) > 3 {
		PostError("bad syntax for %s (too many elements)", what)
		return FAILED
	}

	t_name := children[1]
	if !t_name.IsTag() {
		PostError("skip label must be an identifier beginning with `")
		return FAILED
	}

	t.Str = t_name.Str

	if junc != nil {
		junc.name = t.Str
	}

	if len(children) < 3 {
		expr := NewNode(ND_Void, 0)
		expr.Info.ty = void_type
		expr.LineNum = t_name.LineNum

		t.Add(expr)

	} else {
		t_expr := children[2]

		if ParseElement(t_expr) != OKAY {
			return FAILED
		}

		t.Add(t_expr)
	}

	return OKAY
}

func ParseIf(t *Token) cmError {
	children := t.Children
	t.Children = make([]*Token, 0)

	t.Kind = ND_If

	// divide the tokens into clauses, where each clause
	// begins with an "if", "elif" or "else" keyword.

	clauses := make([][]*Token, 0)
	clauses = append(clauses, children[0:1])

	cur_start := 0

	for i := 1; i < len(children); i++ {
		tok := children[i]

		if tok.Match("elif") || tok.Match("else") {
			// start a new clause
			cur_start = i
			clauses = append(clauses, children[i:i+1])

		} else {
			// extend the current clause
			L := len(clauses) - 1
			clauses[L] = children[cur_start : i+1]
		}
	}

	/* DEBUG
	Print("NUM CLAUSES: %d\n", len(clauses))
	for i, cl := range clauses {
		Print("   %d: len %d keyword %q\n", i, len(cl), cl[0].Str)
	} */

	var last *Token

	for idx, grp := range clauses {
		// check stuff
		is_last := (idx == len(clauses)-1)
		is_else := (grp[0].Str == "else")

		ErrorSetToken(grp[0])

		if is_else && !is_last {
			PostError("else clause must be last in if")
			return FAILED
		}

		if len(grp) < 2 || (!is_else && len(grp) < 3) {
			PostError("%s clause is lacking block", grp[0].Str)
			return FAILED
		}

		if is_else {
			else_tok := new(Token)
			else_tok.LineNum = grp[0].LineNum

			if ParseBlock(else_tok, grp[1:]) != OKAY {
				return FAILED
			}

			last.Add(else_tok)
			continue
		}

		t.LineNum = grp[0].LineNum

		// get the condition
		cond_tok := grp[1]
		if ParseElement(cond_tok) != OKAY {
			return FAILED
		}

		// get the code block
		then_tok := new(Token)
		then_tok.LineNum = grp[1].LineNum

		if ParseBlock(then_tok, grp[2:]) != OKAY {
			return FAILED
		}

		t.Add(cond_tok)
		t.Add(then_tok)

		if last != nil {
			last.Add(t)
		}

		last = t
		t = NewNode(ND_If, 0)
	}

	if len(last.Children) == 2 {
		else_body := NewNode(ND_Void, 0)
		else_body.Info.ty = void_type
		else_body.LineNum = last.LineNum

		last.Add(else_body)
	}

	return OKAY
}

func ParseWhile(t *Token) cmError {
	if len(t.Children) < 3 {
		PostError("while is missing condition or body")
		return FAILED
	}

	t.Kind = ND_While
	t.Info.ty = void_type

	children := t.Children
	t.Children = make([]*Token, 0)

	t_cond := children[1]
	if ParseElement(t_cond) != OKAY {
		return FAILED
	}

	t_body := new(Token)
	t_body.LineNum = children[2].LineNum

	if ParseBlock(t_body, children[2:]) != OKAY {
		return FAILED
	}

	t.Add(t_cond)
	t.Add(t_body)

	return OKAY
}

func ParseBlock(dest *Token, children []*Token) cmError {
	// NOTE: the input token 't' is overwritten with result.
	//       input token should have location info.

	ErrorSetToken(dest)

	total := len(children)

	if total == 0 {
		PostError("missing statement (block is empty)")
		return FAILED
	}

	var seq *Token
	var junc *JunctionInfo

	for i := total - 1; i >= 0; i-- {
		sub := children[i]

		// the "let" forms are restricted to use inside a block,
		// so we must handle it here....

		if sub.Kind == TOK_Expr {
			is_let := sub.Children[0].Match("let")
			is_var := sub.Children[0].Match("let-mut")

			if is_let || is_var {
				if seq == nil {
					PostError("last element of block cannot be 'let'")
					return FAILED
				}

				if ParseLet(sub, ND_Let, is_var) != OKAY {
					return FAILED
				}
				sub.Add(seq)

				if junc != nil {
					MarkJunctions(sub, junc)
				}

				seq = sub
				continue
			}

			// the "junction" forms is restricted to use as the last
			// element of a block, and "skip" must also be in a block,
			// so handle them here....
			is_skip := sub.Children[0].Match("skip")
			is_junc := sub.Children[0].Match("junction")

			if is_skip {
				if ParseSkipOrJunction(sub, nil) != OKAY {
					return FAILED
				}
				goto mark_it
			}

			if is_junc {
				junc = new(JunctionInfo)
				junc.jump_pcs = make([]int, 0)

				if i != (total - 1) {
					PostError("junction must be last thing in a block")
					return FAILED
				}
				if ParseSkipOrJunction(sub, junc) != OKAY {
					return FAILED
				}

				// we only want the child expression
				junc.expr = sub.Children[0]
				sub = junc.expr
				goto add_it
			}
		}

		if ParseElement(sub) != OKAY {
			return FAILED
		}

	mark_it:
		// resolve junction references in other nodes in the block.
		// NOTE: assumes we are going backwards (so see junction first).
		if junc != nil {
			MarkJunctions(sub, junc)
		}

	add_it:
		if seq == nil {
			seq = NewNode(ND_Block, 0)
			seq.LineNum = sub.LineNum

		} else if seq.Kind != ND_Block {
			other := seq

			seq = NewNode(ND_Block, 0)
			seq.LineNum = other.LineNum
			seq.Add(other)
		}

		seq.Prepend(sub)
	}

	// ensure result is an ND_Block
	if seq.Kind != ND_Block {
		other := seq

		seq = NewNode(ND_Block, 0)
		seq.LineNum = other.LineNum
		seq.Add(other)
	}

	if junc != nil {
		junc.seq = seq
		seq.Info.junction = junc
	}

	dest.Replace(seq)
	return OKAY
}

func ParseLet(t *Token, kind TokenKind, mutable bool) cmError {
	children := t.Children

	if len(children) < 2 {
		PostError("missing var name in let")
		return FAILED
	}
	if len(children) < 3 {
		PostError("missing value in let")
		return FAILED
	}

	t_var := children[1]
	t_exp := children[2]

	// validate binding name
	if t_var.Kind != TOK_Name {
		PostError("bad var name: wanted identifier, got: %s", t_var.String())
		return FAILED
	}
	if ValidateDefName(t_var, "var", 0) != OKAY {
		return FAILED
	}

	if len(children) > 3 {
		PostError("bad let statement, too many values")
		return FAILED
	}

	// check for defining a global variable to itself
	if kind == ND_DefVar && t_exp.Kind == TOK_Name && t_exp.Str == t_var.Str {
		PostError("cannot define '%s' as itself", t_var.Str)
		return FAILED
	}

	// process expression
	if ParseElement2(t_exp, t_var.Info.ty) != OKAY {
		return FAILED
	}

	t.Kind = kind
	t.Info.mutable = mutable

	t.Children = make([]*Token, 0)
	t.Add(t_var)
	t.Add(t_exp)

	return OKAY
}

func ParseCast(t *Token) cmError {
	var err2 cmError

	children := t.Children

	// the (cast v) form is for going from custom type to base.
	// the (cast T v) form is the more general operation.
	var user_type *Type

	if len(children) >= 3 {
		t_type := children[1]

		user_type, err2 = ParseType(t_type)
		if err2 != OKAY {
			return FAILED
		}

		children = children[1:]
	}

	if len(children) < 2 {
		PostError("missing argument to type cast")
		return FAILED
	}
	if len(children) > 2 {
		PostError("too many arguments in type cast")
		return FAILED
	}

	t_exp := children[1]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	t.Kind = ND_Cast
	t.Info.ty = user_type
	t.Children = make([]*Token, 0)
	t.Add(t_exp)

	return OKAY
}

func ParseTagConv(t *Token, dest_type *Type) cmError {
	children := t.Children

	if len(children) < 2 {
		PostError("missing argument to %s", children[0].Str)
		return FAILED
	}
	if len(children) > 2 {
		PostError("too many arguments for %s", children[0].Str)
		return FAILED
	}

	t_exp := children[1]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	t.Kind = ND_TagConv
	t.Info.ty = dest_type
	t.Children = make([]*Token, 0)
	t.Add(t_exp)

	return OKAY
}

func ParseClassName(t *Token) cmError {
	children := t.Children

	if len(children) < 2 {
		PostError("missing argument to %s", children[0].Str)
		return FAILED
	}
	if len(children) > 2 {
		PostError("too many arguments for %s", children[0].Str)
		return FAILED
	}

	t_exp := children[1]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	t.Kind = ND_ClassName
	t.Info.ty = str_type
	t.Children = make([]*Token, 0)
	t.Add(t_exp)

	return OKAY
}

func ParseDefFunc(t *Token) cmError {
	children := t.Children

	if len(children) < 2 {
		PostError("bad function def: missing name")
		return FAILED
	}
	if len(children) < 4 {
		PostError("bad function def: missing params or body")
		return FAILED
	}

	t_name := children[1]

	if t_name.Kind != TOK_Name {
		PostError("bad function def: name is not an identifier")
		return FAILED
	}

	// check for reserved keywords
	if ValidateDefName(t_name, "function", ALLOW_FIELDS) != OKAY {
		return FAILED
	}

	t.Kind = ND_DefFunc
	t.Children = make([]*Token, 0)
	t.Add(t_name)

	// a method?
	if t_name.IsField() {
		t.Kind = ND_DefMethod

		// Note: we allow method names that are the same as field names.
		//       it is not an issue since method-call and obj-access use
		//       different syntax, former in (), latter in [].
	}

	return ParseFunctionParts(t, children[2:], false)
}

func ParseLambda(t *Token) cmError {
	children := t.Children

	if len(children) < 3 {
		PostError("bad lambda: missing params or body")
		return FAILED
	}

	t.Kind = ND_Lambda
	t.Children = make([]*Token, 0)

	return ParseFunctionParts(t, children[1:], false)
}

func ParseFunctionParts(dest *Token, input []*Token, IntFace bool) cmError {
	// this parses the parameters, result type and body of the
	// three function forms ("fun", "method" and "lam").
	// the input token sequence begins at the parameters in ().
	//
	// an ND_Params token containing TOK_Name nodes is added to the
	// 'dest' token, followed by a token representing the code body.
	// each TOK_Name parameter node will also get a type.
	//
	// dest.Info.result_ty will hold the specified result type of
	// the function, or the void type if absent.

	if len(input) < 1 {
		PostError("missing parameters")
		return FAILED
	}
	if len(input) < 2 {
		PostError("missing body in fun")
		return FAILED
	}

	pars := input[0]
	input = input[1:]

	if pars.Kind != TOK_Expr {
		PostError("bad parameter list, names must be in ()")
		return FAILED
	}

	plist := pars.Children

	// notify type system to collect generic types like "@T".
	// [ lambdas are not supported, only global funcs and methods ]
	if dest.Kind != ND_Lambda && !IntFace {
		GenericTypeBegin()
	}

	// need to parse a method's type early
	var object_type *Type

	if dest.Kind == ND_DefMethod {
		if len(plist) == 0 || !plist[0].Match("self") {
			PostError("bad method def: missing self parameter")
			return FAILED
		}

		if len(plist) < 2 || plist[1].Match("->") {
			PostError("bad method def: missing type for self")
			return FAILED
		}

		t_type := plist[1]

		// for interfaces, require the type to be "_"
		if IntFace {
			if !t_type.Match("_") {
				PostError("bad interface method, self type should be '_'")
				return FAILED
			}

			object_type = void_type

			goto DID_IT
		}

		// the prelude needs to use syntax like: (__array @T)
		if t_type.Kind == TOK_Expr && doing_prelude {
			if len(t_type.Children) < 2 {
				PostError("bad generic method receiver in prelude")
				return FAILED
			}

			object_type = MethodParentType(t_type.Children[0].Str)
			if object_type == nil {
				PostError("bad method def: unknown generic receiver '%s'", t_type.Children[0].Str)
				return FAILED
			}

			dest.Info.ty = object_type

			var new_ty *Type

			// support the fake (class @T) syntax
			if object_type == class_parent {
				new_ty = NewType(TYP_Mystery)
				new_ty.custom = t_type.Children[1].Str

				gen_type_state.types[new_ty.custom] = new_ty

			} else {
				var err2 cmError

				new_ty, err2 = ParseType(t_type)
				if err2 != OKAY {
					return FAILED
				}
			}

			object_type = new_ty

			goto DID_IT
		}

		if t_type.Kind != TOK_Name {
			PostError("bad method def: bad type for self: %s",
				t_type.String())
			return FAILED
		}

		object_type = N_GetType(t_type.Str, t_type.Module)
		if object_type == nil {
			object_type = MethodParentType(t_type.Str)
		}
		if object_type == nil {
			PostError("bad method def: unknown type '%s'", t_type.Str)
			return FAILED
		}

		if object_type.base == TYP_Interface {
			PostError("bad method def: receiver '%s' is an interface", t_type.Str)
			return FAILED
		}

		dest.Info.ty = object_type

	DID_IT:
	}

	if dest.Kind != ND_Lambda && !IntFace {
		// for methods on a generic type, supply its type parameters
		if object_type != nil && object_type.base == TYP_Generic {
			for _, par := range object_type.param {
				gen_type_state.types[par.name] = par.ty
			}
		}
	}

	/* handle parameters */

	// firstly, find result part (but handle it later)

	var result *Token

	// TODO check usage of "->" better
	/*
		if len(input) < 3 {
			PostError("missing return type or body")
			return FAILED
		} */
	if len(plist) >= 2 && plist[len(plist)-2].Match("->") {
		result = plist[len(plist)-1]
		plist = plist[0 : len(plist)-2]
	}

	pars.Kind = ND_Params
	pars.Children = make([]*Token, 0)

	for len(plist) > 0 {
		t_par := plist[0]
		plist = plist[1:]

		if t_par.Kind != TOK_Name {
			PostError("bad parameter name: %s", t_par.String())
			return FAILED
		}

		validate := ALLOW_UNDERSCORE

		if object_type != nil {
			validate |= ALLOW_SELF
		}

		if ValidateDefName(t_par, "parameter", validate) != OKAY {
			return FAILED
		}

		// check if duplicate
		for _, old := range pars.Children {
			if old.Str == t_par.Str && !t_par.Match("_") {
				PostError("duplicate parameter name '%s'", t_par.Str)
				return FAILED
			}
		}

		t_par.Children = nil

		pars.Add(t_par)

		if true {
			if len(plist) == 0 {
				PostError("missing type spec for parameter '%s'", t_par.Str)
				return FAILED
			}

			// we already know the type of 'self' parameter
			if len(pars.Children) == 1 && object_type != nil {
				t_par.Info.ty = object_type
			} else {
				par_type, err2 := ParseType(plist[0])
				if err2 != OKAY {
					return FAILED
				}
				if par_type.base == TYP_Void {
					PostError("parameters cannot be void")
					return FAILED
				}

				t_par.Info.ty = par_type
			}

			plist = plist[1:]
		}
	}

	dest.Add(pars)

	// only parameters can be generic, disallow *new* ones now
	if dest.Kind != ND_Lambda && !IntFace {
		dest.Info.gen_types = GenericTypeCollect()
	}

	/* parse return type */

	if result == nil {
		dest.Info.return_ty = void_type
	} else {
		ret_type, err2 := ParseType(result)
		if err2 != OKAY {
			return FAILED
		}

		dest.Info.return_ty = ret_type
	}

	/* handle body */

	t_body := NewNode(ND_Void, 0)
	t_body.LineNum = pars.LineNum

	// handle the "#native" form
	if len(input) > 0 && input[0].Kind == TOK_Expr &&
		len(input[0].Children) > 0 &&
		input[0].Children[0].Match("#native") {

		t_native := input[0]

		if dest.Kind == ND_Lambda {
			PostError("lambda functions cannot be #native")
			return FAILED
		}
		if len(t_native.Children) != 2 || t_native.Children[1].Kind != TOK_String {
			PostError("malformed #native syntax")
			return FAILED
		}

		// ok
		t_body.Kind = ND_Native
		t_body.Str = t_native.Children[1].Str

	} else if ParseBlock(t_body, input) != OKAY {
		return FAILED
	}

	dest.Add(t_body)

	GenericTypeFinish()

	return OKAY
}

func ParseFmt(t *Token) cmError {
	children := t.Children[1:]

	// no arguments --> empty string
	if len(children) == 0 {
		t.Kind = ND_Literal
		t.Str = ""
		t.Info.ty = str_type
		t.Info.lit = new(Value)
		t.Info.lit.MakeString(t.Str)
		t.Children = nil

		return OKAY
	}

	// we construct a string-builder node...
	t.Kind = ND_String
	t.Info.ty = str_type
	t.Children = make([]*Token, 0)

	add_string := func(s string, LineNum int) {
		t_str := NewNode(ND_Literal, 0)
		t_str.Str = s
		t_str.Info.ty = str_type
		t_str.Info.lit = new(Value)
		t_str.Info.lit.MakeString(s)
		t_str.LineNum = LineNum

		t.Add(t_str)
	}

	add_format := func(f *Formatter, t_expr *Token) {
		if f.before != "" {
			add_string(f.before, t_expr.LineNum)
		}

		t_fmt := NewNode(ND_Fmt, 0)
		t_fmt.Info.ty = str_type
		t_fmt.Info.fmt_spec = f
		t_fmt.LineNum = t_expr.LineNum
		t_fmt.Add(t_expr)

		t.Add(t_fmt)

		if f.after != "" {
			add_string(f.after, t_expr.LineNum)
		}
	}

	for len(children) >= 2 {
		t_fmt := children[0]
		t_expr := children[1]
		children = children[2:]

		if t_fmt.Kind != TOK_String {
			PostError("format string must be string literal, got %s",
				t_fmt.String())
			return FAILED
		}

		f, err := FormatDecode(t_fmt.Str)
		if err != nil {
			PostError("%s", err.Error())
			return FAILED
		}

		if f.verb == 0 {
			PostError("format string lacks a %% directive")
			return FAILED
		}

		if ParseElement(t_expr) != OKAY {
			return FAILED
		}

		add_format(f, t_expr)
	}

	if len(children) > 0 {
		t_lone := children[0]

		if t_lone.Kind != TOK_String {
			PostError("format string must be string literal, got %s",
				t_lone.String())
			return FAILED
		}

		// we only decode it to ensure it has NO verb at all
		f, err := FormatDecode(t_lone.Str)
		if err != nil {
			PostError("%s", err.Error())
			return FAILED
		}

		if f.verb != 0 {
			PostError("missing value after %%%c format directive", f.verb)
			return FAILED
		}

		// convert TOK_String to ND_Literal
		if ParseElement(t_lone) != OKAY {
			return FAILED
		}

		t.Add(t_lone)
	}

	return OKAY
}

//----------------------------------------------------------------------

func ParseMatch(t *Token) cmError {
	children := t.Children
	if len(children) < 2 {
		PostError("match statement without expression")
		return FAILED
	}
	if len(children) < 3 {
		PostError("match statement is lacking rules")
		return FAILED
	}

	t_exp := children[1]

	if ParseElement(t_exp) != OKAY {
		return FAILED
	}

	t.Kind = ND_Match
	t.Children = make([]*Token, 0)

	t.Add(t_exp)

	for i := 2; i < len(children); i++ {
		t_rule := children[i]

		if ParseMatchRule(t, t_rule) != OKAY {
			return FAILED
		}

		t.Add(t_rule)
	}

	return OKAY
}

func ParseMatchRule(parent, t *Token) cmError {
	ErrorSetToken(t)

	if t.Kind != TOK_Expr {
		PostError("malformed match rule, must be in ()")
		return FAILED
	}

	// find the '=>' symbol
	mid_pos := -1
	for i, child := range t.Children {
		if child.Match("=>") {
			if mid_pos >= 0 {
				PostError("bad match rule, multiple => found")
				return FAILED
			}
			mid_pos = i
		}
	}
	if mid_pos < 0 {
		PostError("bad match rule, no => found")
		return FAILED
	}

	terms := t.Children[0:mid_pos]

	// find the 'where' clause, if any
	where_pos := -1
	for i := 0; i < mid_pos; i++ {
		if t.Children[i].Match("where") {
			if where_pos >= 0 {
				PostError("bad match rule, multiple wheres")
				return FAILED
			}
			where_pos = i
			terms = terms[0:i]
		}
	}

	/* handle pattern */

	t_pattern := NewNode(TOK_Data, 0)

	if ParseMatchPattern(t_pattern, terms, true) != OKAY {
		return FAILED
	}

	/* handle where clause */

	var t_where *Token

	if where_pos >= 0 {
		size := mid_pos - where_pos - 1
		if size < 1 {
			PostError("bad match rule, no expression after where")
			return FAILED
		} else if size > 1 {
			PostError("bad match rule, where clause is too long")
			return FAILED
		}

		t_where = t.Children[where_pos+1]

		if ParseElement(t_where) != OKAY {
			return FAILED
		}
	}

	/* handle body */

	t_body := new(Token)
	t_body.LineNum = t.Children[mid_pos].LineNum

	if ParseBlock(t_body, t.Children[mid_pos+1:]) != OKAY {
		return FAILED
	}

	t.Kind = ND_MatchRule
	t.Children = make([]*Token, 0)

	t.Add(t_pattern)
	t.Add(t_body)

	if t_where != nil {
		t.Add(t_where)
	}

	return OKAY
}

func ParseMatchPattern(pat *Token, input []*Token, is_top bool) cmError {
	// looks at the input token(s) and produces one of the following
	// node types to represent the pattern:
	//    TOK_Name        -- term is a non-special identifier
	//    TOK_Data        -- everything in {} brackets
	//    ND_TuplePair    -- used when field-names appear
	//    ND_MatchConsts  -- terms are literals or global constants
	//    ND_MatchTags    -- terms are tag names
	//    ND_MatchUnion   -- term is an union tag and datum match
	//    ND_MatchAll     -- term is "_"

	if len(input) == 0 {
		PostError("bad match rule, missing pattern")
		return FAILED
	}

	head := input[0]

	pat.LineNum = head.LineNum

	// does top-level pattern begin with a tag?
	if is_top && head.IsTag() {
		return ParsePattern_TagSoup(pat, input)
	}

	// a single identifier?
	if len(input) == 1 && head.Kind == TOK_Name {
		return ParsePattern_Name(pat, head)
	}

	// a group of literals or global constants?
	if len(input) > 1 || head.IsMatchConstant() {
		return ParsePattern_Consts(pat, input)
	}

	// handle (ANY ...)
	if head.Kind == TOK_Expr && len(head.Children) > 0 &&
		head.Children[0].Match("any") {
		return ParsePattern_Any(pat, head.Children)
	}

	switch head.Kind {
	case TOK_Data:
		return ParsePattern_Data(pat, head)

	case TOK_Expr, TOK_Access:
		PostError("bad match pattern, cannot be an expression")
		return FAILED

	default:
		PostError("cannot parse match pattern: %s", head.String())
		return FAILED
	}
}

func ParsePattern_TagSoup(pat *Token, input []*Token) cmError {
	// does it look like TAG + DATUM?
	if len(input) == 2 && !input[1].IsTag() {
		t_tag := input[0]
		t_datum := NewNode(TOK_Data, 0)

		pat.Kind = ND_MatchUnion
		pat.Add(t_tag)
		pat.Add(t_datum)

		return ParseMatchPattern(t_datum, input[1:2], false)
	}

	// it must be a group of tags to match
	pat.Kind = ND_MatchTags

	for _, t_tag := range input {
		if !t_tag.IsTag() {
			PostError("malformed union/enum pattern")
			return FAILED
		}

		pat.Add(t_tag)
	}

	return OKAY
}

func ParsePattern_Data(pat, t *Token) cmError {
	// check for '...' at beginning or end
	input := t.Children

	if len(input) > 0 {
		if input[0].Match("...") {
			pat.Info.open = -1
			input = input[1:]
		} else if input[len(input)-1].Match("...") {
			pat.Info.open = 1
			input = input[0 : len(input)-1]
		}
	}

	// check for erroneous type specs
	if len(input) > 0 {
		head := input[0]

		if IsTypeSpec(head) || head.Match("array") ||
			head.Match("map") || head.Match("set") {

			PostError("bad pattern: cannot use type specs")
			return FAILED
		}
	}

	for _, elem := range input {
		if elem.Match("...") {
			PostError("bad pattern: extra/weird '...' found")
			return FAILED
		}
	}

	// check for explicit fields
	has_fields := false
	for _, child := range input {
		if child.IsNamedField() {
			has_fields = true
			break
		}
	}

	for _, child := range input {
		if child.Match("...") {
			PostError("bad pattern: bad/extra '...' found")
			return FAILED
		}
	}

	if has_fields {
		count := len(input)
		if count%2 != 0 {
			PostError("bad pattern: missing/malformed fields")
			return FAILED
		}

		seen_fields := make(map[string]bool)

		for i := 0; i < len(input); i += 2 {
			t_field := input[i]
			t_val := input[i+1]

			if !t_field.IsNamedField() {
				PostError("bad pattern: expected field name, got %s", t_field.String())
				return FAILED
			}
			if t_val.IsNamedField() {
				PostError("bad pattern: missing value for %s", t_field.String())
				return FAILED
			}

			// ensure field names are unique
			if seen_fields[t_field.Str] {
				PostError("bad pattern: repeated field name: %s", t_field.Str)
				return FAILED
			}
			seen_fields[t_field.Str] = true

			t_pat2 := NewNode(TOK_Data, 0)

			if ParseMatchPattern(t_pat2, input[i+1:i+2], false) != OKAY {
				return FAILED
			}

			pair := NewNode(ND_TuplePair, 2)
			pair.Children[0] = t_field
			pair.Children[1] = t_pat2
			pair.LineNum = t_field.LineNum

			pat.Add(pair)
		}

	} else {
		for i := 0; i < len(input); i++ {
			t_pat2 := NewNode(TOK_Data, 0)

			if ParseMatchPattern(t_pat2, input[i:i+1], false) != OKAY {
				return FAILED
			}

			pat.Add(t_pat2)
		}
	}

	return OKAY
}

func ParsePattern_Any(pat *Token, input []*Token) cmError {
	input = input[1:]

	// if there are no constants to match, it matches everything
	if len(input) == 0 {
		PostError("missing elements in the any form")
		return FAILED
	}

	// a list of enum tags?
	if input[0].IsTag() {
		pat.Kind = ND_MatchTags

		for _, t_tag := range input {
			if !t_tag.IsTag() {
				PostError("multiple tag pattern with non-tag: %s",
					t_tag.String())
				return FAILED
			}

			pat.Add(t_tag)
		}

		return OKAY
	}

	return ParsePattern_Consts(pat, input[1:])
}

func ParsePattern_Consts(pat *Token, input []*Token) cmError {
	pat.Kind = ND_MatchConsts

	for _, child := range input {
		if child.Match("_") || child.Match("::") {
			PostError("bad use of %s in match pattern", child.Str)
			return FAILED
		}
		if !child.IsMatchConstant() {
			PostError("multiple match terms must all be constants")
			return FAILED
		}
		if ParseElement(child) != OKAY {
			return FAILED
		}

		pat.Add(child)
	}

	return OKAY
}

func ParsePattern_Name(pat, t *Token) cmError {
	switch {
	case t.Match("_"):
		pat.Kind = ND_MatchAll
		return OKAY
	}

	// Note: global defs are not checked here

	if IsLanguageKeyword(t.Str) {
		PostError("bad binding name: '%s' is a reserved word", t.Str)
		return FAILED
	}

	pat.Replace(t)
	return OKAY
}

func (nd *Token) IsMatchConstant() bool {
	switch nd.Kind {
	// TOK_Name should be a global constant with a simple type
	// [ this is checked later on ]
	case TOK_Int, TOK_Float, TOK_Char, TOK_String, TOK_Name:
		return true
	}

	return false
}

//----------------------------------------------------------------------

func MarkJunctions(t *Token, junction *JunctionInfo) {
	if t.Kind == ND_Skip && t.Str == junction.name &&
		t.Info.junction == nil {

		t.Info.junction = junction

/* DEBUG
Print("Marked Junction at: %s\n", junction.name, t.String())
*/
	}

	if t.Children != nil {
		for _, child := range t.Children {
			MarkJunctions(child, junction)
		}
	}
}
