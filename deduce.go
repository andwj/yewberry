// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"

// DeduceClosure visits everything in a closure's code tree and
// assigns a type to as many nodes as possible.  Nodes which
// already have a type are not changed (except for auto-casting
// of literal values).
//
// For each type of node in a code tree there is usually a
// corresponding DeduceXXX() method to handle the type deduction.
// Many of these take an 'uptype' parameter, which is the type
// which a parent node thinks the child could or should be, though
// the child is free to ignore it (the parent will catch any type
// mismatch, sooner or later).
//
// When "full" is true, then full type-checking is done, and all
// nodes get a type -- the DeduceXXX() methods are guaranteed to
// give their node a type (or fail with a compile error).
func DeduceClosure(cl *Closure, full bool) cmError {
	cl.type_check = full

	// repeat a partial deduction until no more changes
	for {
		cl.deduce_count = 0

		if cl.DeduceNode(cl.t_body, cl.ret_type) != OKAY {
			return FAILED
		}

		if cl.deduce_count == 0 {
			break
		}
	}

	return OKAY
}

func DeduceFunctionType(cl *Closure) cmError {
	// perform partial deduction, to help determine parameter and
	// return types.
	if DeduceClosure(cl, false) != OKAY {
		return FAILED
	}

	fun_type := NewType(TYP_Function)

	// for generic functions, grab list of generic types
	// which was created at parse time.
	fun_type.gen_types = cl.saved_gen_types

	/* determine parameter types */

	for i, par := range cl.t_parameters.Children {
		lvar := cl.parameters[i]
		if lvar == nil { panic("lost parameter") }

		type1 := par.Info.ty // one explicitly specified (it overrides)
		type2 := lvar.ty     // the deduced one

///  fmt.Printf("parameter %s deduced: %s  given: %s\n", lvar.name, type2.String(), type1.String())

		// NOTE: we don't check if type1 == type2 here
		// [ it happens elsewhere, either DeduceClosure or CompileClosure ]

		if type1 != nil {
			lvar.ty = type1
		} else if type2 != nil {
			if type2.HasGeneric() {
				PostError("parameter '%s' deduced as generic, must be explicit",
					par.Str)
				return FAILED
			}
			lvar.ty = type2
		} else {
			PostError("could not determine type of parameter '%s' of function '%s'",
				par.Str, cl.debug_name)
			return FAILED
		}

		fun_type.AddParam(par.Str, lvar.ty)
	}

	/* determine return type */

	type1 := cl.ret_type       // explicit one (it overrides)
	type2 := cl.t_body.Info.ty // the deduced one

///	fmt.Printf("ret-type deduced: %s  given: %s\n", type2.String(), type1.String())

	// NOTE: we don't check if type1 == type2 here
	// [ it happens elsewhere, either DeduceClosure or CompileClosure ]

	if type1 != nil {
		fun_type.sub = type1
	} else if type2 != nil {
		fun_type.sub = type2
	} else {
		// if no idea at all, choose VOID
		fun_type.sub = void_type
	}

	cl.ty = fun_type
	cl.ret_type = fun_type.sub

	return OKAY
}

func DeduceStaticValue(t *Token, uptype *Type, full bool) cmError {
	// create a dummy closure
	cl := NewClosure("#StaticValue#")
	cl.t_body = t
	cl.type_check = full

	// also bind identifiers in full mode
	if full {
		if cl.BindNode(t) != OKAY {
			return FAILED
		}
	}

	for {
		cl.deduce_count = 0

		if cl.DeduceNode(cl.t_body, uptype) != OKAY {
			return FAILED
		}

		if cl.deduce_count == 0 {
			break
		}
	}

	return OKAY
}

func (cl *Closure) DeduceNode(t *Token, uptype *Type) cmError {
	ErrorSetToken(t)

	switch t.Kind {
	case ND_Local, ND_Upvar:
		return cl.DeduceLocal(t, uptype)

	case ND_Global:
		return cl.DeduceGlobal(t, uptype)

	case TOK_Name:
		if cl.type_check {
			if t.IsField() {
				PostError("unexpected field name: %s", t.Str)
			} else {
				PostError("unexpected identifier: %s", t.Str)
			}
			return FAILED
		}
		return OKAY

	case ND_RawTag:
		return cl.DeduceRawTag(t, uptype)

	case ND_FunCall:
		return cl.DeduceFunCall(t)

	case ND_MethodCall:
		return cl.DeduceMethodCall(t)

	case ND_Void:
		// already has a type
		return OKAY

	case ND_Literal:
		return cl.DeduceLiteral(t, uptype)

	case ND_Array:
		return cl.DeduceArrayLiteral(t, uptype)

	case ND_Set:
		return cl.DeduceSetLiteral(t, uptype)

	case ND_Map:
		return cl.DeduceMapLiteral(t, uptype)

	case ND_Tuple:
		return cl.DeduceTupleLiteral(t, uptype)

	case ND_Object:
		// type is known, no need to pass uptype
		return cl.DeduceObjectLiteral(t)

	case ND_Union:
		// type is known, no need to pass uptype
		return cl.DeduceUnionLiteral(t)

	case ND_String:
		return cl.DeduceStringBuilder(t)

	case ND_Block:
		return cl.DeduceBlock(t, uptype)

	case ND_If:
		return cl.DeduceIf(t, uptype)

	case ND_While:
		return cl.DeduceWhile(t)

	case ND_Skip:
		return cl.DeduceSkip(t)

	case ND_Cast:
		return cl.DeduceCast(t, uptype)

	case ND_Wrap:
		return cl.DeduceWrap(t, uptype)

	case ND_Unwrap, ND_Flatten:
		return cl.DeduceUnwrap(t, uptype)

	case ND_ClassName:
		return cl.DeduceClassName(t, uptype)

	case ND_IsTag:
		return cl.DeduceIsTag(t, uptype)

	case ND_TagConv:
		return cl.DeduceTagConv(t, uptype)

	case ND_RefEqual:
		return cl.DeduceRefEqual(t, uptype)

	case ND_Fmt:
		return cl.DeduceFmt(t)

	case ND_Match:
		return cl.DeduceMatch(t, uptype)

	case ND_Let:
		return cl.DeduceLet(t, uptype)

	case ND_SetVar:
		// result is always void, no need to pass uptype
		return cl.DeduceSetVar(t)

	case ND_SetField:
		// result is always void, no need to pass uptype
		return cl.DeduceSetField(t)

	case ND_GetField:
		return cl.DeduceGetField(t)

	case ND_Lambda:
		return cl.DeduceLambda(t, uptype)

	case ND_Native:
		// only used to declare a function as native code
		return OKAY

	default:
		panic("DeduceNode: unexpected node: " + t.String())
	}

	return FAILED
}

func (cl *Closure) Deduct(t *Token, new_type *Type) {
	if new_type != nil {
		if t.Info.ty != new_type {
			t.Info.ty = new_type
			cl.deduce_count += 1
		}
	}
}

//----------------------------------------------------------------------

func (cl *Closure) DeduceChildren(children []*Token) cmError {
	for _, sub := range children {
		if cl.DeduceNode(sub, nil) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceBlock(t *Token, uptype *Type) cmError {
	last_node := t.Children[len(t.Children)-1]

	for _, sub := range t.Children {
		var downtype *Type

		if sub == last_node {
			downtype = uptype
		}

		if cl.DeduceNode(sub, downtype) != OKAY {
			return FAILED
		}
	}

	cl.Deduct(t, last_node.Info.ty)

	// type checking between skip nodes and the junction
	if t.Info.junction != nil {
		junc := t.Info.junction

		if cl.DeduceSkipOrJunction(junc.expr, junc) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceGlobal(t *Token, uptype *Type) cmError {
	// we can ignore uptype here.
	// [ global vars never get deduced from function bodies ]

	def := t.Info.gdef

	// node type always mirrors the variable
	cl.Deduct(t, def.ty)

	if cl.type_check {
		// ignore failed-to-compile functions
		if def.loc.Clos != nil && def.loc.Clos.failed {
			return FAILED
		}

		// TODO : this can happen -- review when/how/why
		// [ it's probably when other code failed to compiled ]
		if def.ty == nil {
			PostError("GLOBAL WITH NO TYPE: " + def.name)
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceLocal(t *Token, uptype *Type) cmError {
	lvar := t.Info.lvar

	if uptype != nil {
		if lvar.ty == nil {
			lvar.ty = uptype
		} else if uptype.AssignableTo(lvar.ty) {
			// ok
		} else if lvar.ty.AssignableTo(uptype) {
			// use the more general type
			lvar.ty = uptype
		} else {
			PostError("incompatible types for local '%s', got %s and %s",
				lvar.name, lvar.ty.String(), uptype.String())
			return FAILED
		}
	}

	// node type always mirrors the variable
	cl.Deduct(t, lvar.ty)

	if cl.type_check {
		if lvar.ty == nil {
			PostError("could not determine type of local '%s'", lvar.name)
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceRawTag(t *Token, uptype *Type) cmError {
	// raw tags never have a type yet, and we can wait until the
	// type checking phase...
	if !cl.type_check {
		return OKAY
	}

	if uptype == nil {
		PostError("unknown type for raw tag: %s", t.Str)
		return FAILED
	}

	// this logic mirrors that in ParseName...

	if uptype.base != TYP_Enum {
		PostError("type mismatch: wanted %s, got enum tag %s",
			uptype.String(), t.Str)
		return FAILED
	}

	tag_id := uptype.FindParam(t.Str)
	if tag_id < 0 {
		PostError("enum literal: unknown tag %s in %s",
			t.Str, uptype.String())
		return FAILED
	}

	t.Kind = ND_Literal
	t.Info.ty = uptype
	t.Info.lit = new(Value)
	t.Info.lit.MakeEnum(uptype, int64(tag_id))

	return OKAY
}

func (cl *Closure) DeduceLiteral(t *Token, uptype *Type) cmError {
	if uptype != nil {
		// auto-cast plain literals to a custom type
		if  t.Info.ty.custom == "" && uptype.custom != "" &&
			t.Info.ty.CastableTo(uptype) {

			t.Info.lit.ty = uptype
			cl.Deduct(t, uptype)
		}
	}

	return OKAY
}

func (cl *Closure) DeduceArrayLiteral(t *Token, uptype *Type) cmError {
	// some array literals already have a type
	arr_type := t.Info.ty

	if arr_type == nil && uptype != nil && uptype.base == TYP_Array {
		arr_type = uptype
		cl.Deduct(t, arr_type)
	}

	var downtype *Type
	if arr_type != nil {
		if arr_type.base == TYP_ByteVec {
			downtype = int_type
		} else {
			downtype = arr_type.sub
		}
	}

	// deduce the elements
	for _, t_elem := range t.Children {
		if cl.DeduceNode(t_elem, downtype) != OKAY {
			return FAILED
		}
	}

	// if array type is unknown, deduce it from elements
	if arr_type == nil && len(t.Children) > 0 {
		var elem_type *Type

		for _, t_elem := range t.Children {
			child_type := t_elem.Info.ty

			if child_type == nil {
				continue
			}

			if child_type.base == TYP_Void {
				PostError("array elements cannot be void")
				return FAILED
			}

			if elem_type == nil {
				elem_type = child_type
			} else if elem_type.AssignableTo(child_type) {
				elem_type = child_type
			} else {
				// type may be incompatible, it gets checked later
			}
		}

		if elem_type != nil {
			arr_type = NewType(TYP_Array)
			arr_type.sub = elem_type
			arr_type.parent = array_parent

			// cannot use Deduct() since we arr_type is always new
			t.Info.ty = arr_type
		}
	}

	if cl.type_check {
		if arr_type == nil {
			if len(t.Children) == 0 {
				PostError("empty array literal without type spec")
			} else {
				PostError("could not determine type of array literal")
			}
			return FAILED
		}

		// verify all children
		var sub_type *Type
		if arr_type.base == TYP_ByteVec {
			sub_type = int_type
		} else {
			sub_type = arr_type.sub
		}

		for _, t_elem := range t.Children {
			if !t_elem.Info.ty.AssignableTo(sub_type) {
				PostError("type mismatch on array element: wanted %s, got %s",
					sub_type.String(), t_elem.Info.ty.String())
				return FAILED
			}

			if arr_type.base == TYP_ByteVec && t_elem.Kind == ND_Literal {
				val := t_elem.Info.lit.Int
				if val < 0 || val > 255 {
					PostError("constant is out-of-range for byte: %d\n", val)
					return FAILED
				}
			}
		}
	}

	return OKAY
}

func (cl *Closure) DeduceTupleLiteral(t *Token, uptype *Type) cmError {
	tup_type := t.Info.ty

	// upgrade to ND_Object if untyped and uptype is a class
	if tup_type == nil && uptype != nil && uptype.base == TYP_Class {
		t.Kind = ND_Object
		cl.Deduct(t, uptype)

		return cl.DeduceObjectLiteral(t)
	}

	// use the uptype?
	if tup_type == nil && uptype != nil && uptype.base == TYP_Tuple {
		tup_type = uptype
		cl.Deduct(t, tup_type)
	}

	named_fields := false
	if len(t.Children) > 0 && t.Children[0].Kind == ND_TuplePair {
		named_fields = true
	}

	if tup_type != nil {
		if t.Info.open > 0 {
			if len(t.Children) > len(tup_type.param) {
				PostError("too many tuple elements: wanted %d or less, got %d",
					len(tup_type.param), len(t.Children))
				return FAILED
			}
		} else {
			if len(t.Children) != len(tup_type.param) {
				PostError("wrong number of tuple elements: wanted %d, got %d",
					len(tup_type.param), len(t.Children))
				return FAILED
			}
		}

		if named_fields && !tup_type.named_fields {
			PostError("tuple type %s does not have named fields",
				tup_type.String())
			return FAILED
		}
	}

	// deduce the tuple elements
	for i, child := range t.Children {
		field_name := ""
		t_elem := child

		if child.Kind == ND_TuplePair {
			field_name = child.Children[0].Str
			t_elem = child.Children[1]
		}

		var field_type *Type

		if tup_type != nil {
			// when field names are used, they must occur in same order
			if field_name != "" && tup_type.named_fields &&
				field_name != tup_type.param[i].name {

				PostError("wrong tuple field: expected %s, got %s",
					tup_type.param[i].name, field_name)
				return FAILED
			}

			field_type = tup_type.param[i].ty
		}

		if cl.DeduceNode(t_elem, field_type) != OKAY {
			return FAILED
		}
	}

	// if tuple type is not yet known, deduce from children
	if tup_type == nil {
		tup_type = NewType(TYP_Tuple)
		tup_type.named_fields = (t.Children[0].Kind == ND_TuplePair)
		tup_type.parent = tuple_parent

		failed := false

		for _, child := range t.Children {
			field_name := ""
			t_elem := child

			if child.Kind == ND_TuplePair {
				field_name = child.Children[0].Str
				t_elem = child.Children[1]
			}

			child_type := t_elem.Info.ty

			if child_type == nil {
				failed = true
				break
			}

			if child_type.base == TYP_Void {
				PostError("tuple elements cannot be void")
				return FAILED
			}

			tup_type.AddParam(field_name, child_type)
		}

		if failed {
			tup_type = nil
		} else {
			// cannot use Deduct() since we tup_type is always new
			t.Info.ty = tup_type
		}
	}

	if cl.type_check {
		if tup_type == nil {
			PostError("could not determine type of tuple literal")
			return FAILED
		}

		// verify all children
		for i, child := range t.Children {
			field_name := fmt.Sprintf(".%d", i)
			t_elem := child

			if child.Kind == ND_TuplePair {
				field_name = child.Children[0].Str
				t_elem = child.Children[1]
			}

			field_type := tup_type.param[i].ty

			if !t_elem.Info.ty.AssignableTo(field_type) {
				PostError("type mismatch on tuple field %s: wanted %s, got %s",
					field_name, field_type.String(), t_elem.Info.ty.String())
				return FAILED
			}
		}
	}

	return OKAY
}

func (cl *Closure) DeduceObjectLiteral(t *Token) cmError {
	// the class type is already known
	class_type := t.Info.ty

	if t.Info.open > 0 {
		if len(t.Children) > len(class_type.param) {
			PostError("too many class fields: wanted %d or less, got %d",
				len(class_type.param), len(t.Children))
			return FAILED
		}
	} else {
		if len(t.Children) != len(class_type.param) {
			PostError("wrong number of class fields: wanted %d, got %d",
				len(class_type.param), len(t.Children))
			return FAILED
		}
	}

	for i, child := range t.Children {
		field_name := class_type.param[i].name
		field_idx := i
		t_elem := child

		if child.Kind == ND_TuplePair {
			t_field := child.Children[0]
			t_elem = child.Children[1]

			field_name = child.Children[0].Str
			field_idx = class_type.FindParam(field_name)

			if field_idx < 0 {
				PostError("unknown class field %s in %s",
					field_name, class_type.String())
				return FAILED
			}

			t_field.Info.tag_id = field_idx
		}

		field_type := class_type.param[field_idx].ty

		if cl.DeduceNode(t_elem, field_type) != OKAY {
			return FAILED
		}

		if cl.type_check {
			if !t_elem.Info.ty.AssignableTo(field_type) {
				PostError("type mismatch on class field %s: wanted, %s got %s",
					field_name, field_type.String(), t_elem.Info.ty.String())
				return FAILED
			}
		}
	}

	return OKAY
}

func (cl *Closure) DeduceMapLiteral(t *Token, uptype *Type) cmError {
	map_type := t.Info.ty

	if map_type == nil && uptype != nil && uptype.base == TYP_Map {
		map_type = uptype
		cl.Deduct(t, map_type)
	}

	var down_key *Type
	var down_elem *Type

	if map_type != nil {
		down_key = map_type.KeyType()
		down_elem = map_type.sub
	}

	// deduce the elements
	for _, pair := range t.Children {
		t_key := pair.Children[0]
		t_elem := pair.Children[1]

		if cl.DeduceNode(t_key, down_key) != OKAY {
			return FAILED
		}
		if cl.DeduceNode(t_elem, down_elem) != OKAY {
			return FAILED
		}
	}

	// if map type is not yet known, deduce it from children
	if map_type == nil && len(t.Children) > 0 {
		var key_type *Type
		var elem_type *Type

		for _, pair := range t.Children {
			t_key := pair.Children[0]
			t_elem := pair.Children[1]

			child_key := t_key.Info.ty
			child_elem := t_elem.Info.ty

			if child_key != nil {
				if child_key.base == TYP_Void {
					PostError("map keys cannot be void")
					return FAILED
				}
				if key_type == nil {
					key_type = child_key
				} else if key_type.AssignableTo(child_key) {
					key_type = child_key
				} else {
					// type may be incompatible, it gets checked later
				}
			}

			if child_elem != nil {
				if child_elem.base == TYP_Void {
					PostError("map elements cannot be void")
					return FAILED
				}

				if elem_type == nil {
					elem_type = child_elem
				} else if elem_type.AssignableTo(child_elem) {
					elem_type = child_elem
				} else {
					// type may be incompatible, it gets checked later
				}
			}
		}

		if key_type != nil && elem_type != nil {
			map_type = NewType(TYP_Map)
			map_type.sub = elem_type
			map_type.parent = map_parent
			map_type.AddParam("", key_type)

			// cannot use Deduct() since map_type is always new
			t.Info.ty = map_type
		}
	}

	if cl.type_check {
		if map_type == nil {
			if len(t.Children) == 0 {
				PostError("empty map literal without type spec")
			} else {
				PostError("could not determine type of map literal")
			}
			return FAILED
		}

		// verify all children
		key_type := map_type.KeyType()
		elem_type := map_type.sub

		for _, pair := range t.Children {
			t_key := pair.Children[0]
			t_elem := pair.Children[1]

			if !t_key.Info.ty.AssignableTo(key_type) {
				PostError("type mismatch on map key: wanted %s, got %s",
					key_type.String(), t_key.Info.ty.String())
				return FAILED
			}
			if !t_elem.Info.ty.AssignableTo(elem_type) {
				PostError("type mismatch on map element: wanted %s, got %s",
					elem_type.String(), t_elem.Info.ty.String())
				return FAILED
			}
		}
	}

	return OKAY
}

func (cl *Closure) DeduceSetLiteral(t *Token, uptype *Type) cmError {
	// some set literals already have a type
	set_type := t.Info.ty

	if set_type == nil && uptype != nil && uptype.base == TYP_Set {
		set_type = uptype
		cl.Deduct(t, set_type)
	}

	var downtype *Type
	if set_type != nil {
		downtype = set_type.KeyType()
	}

	// deduce the elements
	for _, t_key := range t.Children {
		if cl.DeduceNode(t_key, downtype) != OKAY {
			return FAILED
		}
	}

	// if set type is not yet known, deduce from elements
	if set_type == nil && len(t.Children) > 0 {
		var key_type *Type

		for _, t_key := range t.Children {
			child_type := t_key.Info.ty

			if child_type == nil {
				continue
			}

			if child_type.base == TYP_Void {
				PostError("set keys cannot be void")
				return FAILED
			}

			if key_type == nil {
				key_type = child_type
			} else if key_type.AssignableTo(child_type) {
				key_type = child_type
			} else {
				// type may be incompatible, it gets checked later
			}
		}

		if key_type != nil {
			set_type = NewType(TYP_Set)
			set_type.sub = key_type
			set_type.parent = set_parent

			// cannot use Deduct() since we set_type is always new
			t.Info.ty = set_type
		}
	}

	if cl.type_check {
		if set_type == nil {
			if len(t.Children) == 0 {
				PostError("empty set literal without type spec")
			} else {
				PostError("could not determine type of set literal")
			}
			return FAILED
		}

		key_type := set_type.sub

		// verify all children
		for _, t_key := range t.Children {
			if !t_key.Info.ty.AssignableTo(key_type) {
				PostError("type mismatch on set element: wanted %s, got %s",
					key_type.String(), t_key.Info.ty.String())
				return FAILED
			}
		}
	}

	return OKAY
}

func (cl *Closure) DeduceUnionLiteral(t *Token) cmError {
	// the union type is already known
	union_type := t.Info.ty
	tag_id := t.Info.tag_id
	datum_type := union_type.param[tag_id].ty

	t_datum := t.Children[0]

	if cl.DeduceNode(t_datum, datum_type) != OKAY {
		return FAILED
	}

	if cl.type_check {
		if !t_datum.Info.ty.AssignableTo(datum_type) {
			PostError("type mismatch on union datum: wanted %s, got %s",
				datum_type.String(), t_datum.Info.ty.String())
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceStringBuilder(t *Token) cmError {
	// node type is already known (string)

	// deduce the elements
	for _, t_elem := range t.Children {
		if cl.DeduceNode(t_elem, nil) != OKAY {
			return FAILED
		}

		if cl.type_check {
			if !(t_elem.Info.ty.base == TYP_String || t_elem.Info.ty.base == TYP_Int) {
				PostError("expected a char or string, got %s", t_elem.Info.ty.String())
				return FAILED
			}
		}
	}

	return OKAY
}

//----------------------------------------------------------------------

func (cl *Closure) DeduceFunCall(t *Token) cmError {
	// no uptype here, there's not much we could do with it

	t_func := t.Children[0]
	params := t.Children[1:]

	if cl.DeduceNode(t_func, nil) != OKAY {
		return FAILED
	}
	fun_type := t_func.Info.ty

	if fun_type == nil {
		// we don't know what we're dealing with yet, so handle the
		// parameters in a very basic way.
		return cl.DeduceChildren(params)
	}

	if fun_type.base != TYP_Function {
		PostError("cannot call non-function, got %s", fun_type.String())
		return FAILED
	}

	return cl.DeduceParamsAndResult(t, params, fun_type, false)
}

func (cl *Closure) DeduceMethodCall(t *Token) cmError {
	t_method := t.Children[0]
	t_obj := t.Children[1]
	params := t.Children[1:] // include object itself

	if cl.DeduceNode(t_obj, nil) != OKAY {
		return FAILED
	}
	obj_type := t_obj.Info.ty

	if obj_type == nil {
		// we don't know what we're dealing with yet, so handle the
		// parameters in a very basic way.
		params = t.Children[2:]
		return cl.DeduceChildren(params)
	}

	var fun_loc *Value
	var fun_type *Type

	if obj_type.base == TYP_Interface {
		idx := obj_type.FindParam(t_method.Str)
		if idx < 0 {
			PostError("unknown method %s in type %s", t_method.Str, obj_type.String())
			return FAILED
		}

		fun_type = obj_type.param[idx].ty

		t.Info.is_operator = false

	} else {
		// find method in current type or a parent
		cur := obj_type

		for {
			fun_loc = cur.GetMethod(t_method.Str)
			if fun_loc != nil {
				break
			}
			cur = cur.parent
			if cur == nil {
				PostError("unknown method %s in type %s", t_method.Str, obj_type.String())
				return FAILED
			}
		}

		t.Info.lit = fun_loc

		// this can happen with failed-to-compile methods
		if fun_loc.Clos == nil {
			return FAILED
		}

		fun_type = fun_loc.Clos.ty
	}

	// TODO review if/when/how/why this can happen (and above Clos == nil)
	if fun_type == nil {
		if cl.type_check {
			PostError("METHOD WITHOUT TYPE")
			return FAILED
		}

		// handle parameters is a very basic way
		params = t.Children[2:]
		return cl.DeduceChildren(params)
	}

	IntFace := (obj_type.base == TYP_Interface)

	if cl.DeduceParamsAndResult(t, params, fun_type, IntFace) != OKAY {
		return FAILED
	}

	// a little bit of magic: disallow operators with two different
	// custom types...
	if cl.type_check && t.Info.is_operator {
		L := t.Children[1]
		R := t.Children[2]

		if L.Info.ty.custom != "" && R.Info.ty.custom != "" {
			if L.Info.ty.custom != R.Info.ty.custom {
				PostError("type mismatch on binary operator, got %s and %s",
					L.Info.ty.custom, R.Info.ty.custom)
				return FAILED
			}
		}
	}

	return OKAY
}

func (cl *Closure) DeduceParamsAndResult(t *Token, params []*Token,
	fun_type *Type, IntFace bool) cmError {

	if len(params) != len(fun_type.param) {
		PostError("wrong number of parameters: wanted %d, got %d",
			len(fun_type.param), len(params))
		return FAILED
	}

	// handle calls to a globally defined generic function
	if fun_type.gen_types != nil {
		if IntFace {
			panic("interface with generic method??")
		}

		return cl.DeduceGenericParams(t, params, fun_type)
	}

	for i, t_par := range params {
		par_info := fun_type.param[i]
		downtype := par_info.ty

		// for interfaces we cannot check the "self" parameter
		if IntFace && i == 0 {
			downtype = nil
		}

		if cl.DeduceNode(t_par, downtype) != OKAY {
			return FAILED
		}

		if cl.type_check && !(IntFace && i == 0) {
			if !t_par.Info.ty.AssignableTo(par_info.ty) {
				if (par_info.name == "") {
					PostError("type mismatch on parameter #%d: wanted %s, got %s",
						i+1, par_info.ty.String(), t_par.Info.ty.String())
				} else {
					PostError("type mismatch on parameter '%s': wanted %s, got %s",
						par_info.name, par_info.ty.String(), t_par.Info.ty.String())
				}
				return FAILED
			}
		}
	}

	// handle return type
	cl.Deduct(t, fun_type.sub)

	return OKAY
}

func (cl *Closure) DeduceGenericParams(t *Token, params []*Token, fun_type *Type) cmError {
	// when formal parameters have any generic types, we must
	// construct a mapping between the generic type and the
	// actual type being passed in.  this is needed to detect
	// type mismatches.

	if !cl.type_check {
		if !fun_type.sub.HasGeneric() {
			cl.Deduct(t, fun_type.sub)
		}
		return OKAY
	}

	// TODO review for recursive functions

	gen_map := make(map[string]*Type)

	for i, t_par := range params {
		par_info := fun_type.param[i]
		downtype := par_info.ty

		// for generic parameters, we must not deduce actual parameters
		// as being something generic-ish (it is ACTUALLY the other-way
		// round: we need to know actual parameter so we can match it
		// against generic one)
		if downtype.HasGeneric() {
			downtype = nil
		}

		if cl.DeduceNode(t_par, downtype) != OKAY {
			return FAILED
		}

		if par_info.ty.HasGeneric() {
			if t_par.Info.ty.base == TYP_Void {
				PostError("parameter '%s' has generic type, cannot be void",
					par_info.name)
				return FAILED
			}

			if GenericAssignableTo(t_par.Info.ty, par_info.ty, par_info.name, gen_map) != OKAY {
				return FAILED
			}
		} else {
			// for non-generic parameters, do normal type check
			if !t_par.Info.ty.AssignableTo(par_info.ty) {
				if (par_info.name == "") {
					PostError("type mismatch on parameter #%d: wanted %s, got %s",
						i+1, par_info.ty.String(), t_par.Info.ty.String())
				} else {
					PostError("type mismatch on parameter '%s': wanted %s, got %s",
						par_info.name, par_info.ty.String(), t_par.Info.ty.String())
				}
				return FAILED
			}
		}
	}

	// sanity check that each generic type got an actual one
	for name := range fun_type.gen_types {
		if gen_map[name] == nil {
			PostError("generic type %s not bound in function call", name)
			return FAILED
		}
	}

	// need this to handle compound types like: (array @T)
	ret_type, err2 := fun_type.sub.GenericReconstruct(gen_map)
	if err2 != OKAY {
		return FAILED
	}

/* DEBUG
	Print("GenericReconstruct from %s\n", fun_type.sub.String())
	Print("----------------------> %s\n", ret_type.String())
*/

	// cannot use Deduct() since our ret_type may be newly created
	// (leading to infinite looping)
	if t.Info.ty == nil {
		cl.deduce_count += 1
	}
	t.Info.ty = ret_type

	return OKAY
}

func (cl *Closure) DeduceCast(t *Token, uptype *Type) cmError {
	// when destination type is unset, it is the simple (cast v)
	// form which is for going from custom type to base type.
	if t.Info.ty == nil {
		return cl.DeduceBaseCast(t)
	}

	// destination type is always known.
	dest_type := t.Info.ty

	t_child := t.Children[0]

	// if child is an untyped literal, deduce as given type
	var downtype *Type

	switch t_child.Kind {
	case ND_Literal, ND_Array, ND_Map, ND_Set, ND_Tuple:
		downtype = uptype
	}

	if cl.DeduceNode(t_child, downtype) != OKAY {
		return FAILED
	}

	src_type := t_child.Info.ty

	if cl.type_check {
		if !src_type.CastableTo(dest_type) {
			PostError("cannot cast from %s to %s",
				src_type.String(), dest_type.String())
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceBaseCast(t *Token) cmError {
	t_child := t.Children[0]

	if cl.DeduceNode(t_child, nil) != OKAY {
		return FAILED
	}

	custom_ty := t_child.Info.ty

	if custom_ty != nil {
		if custom_ty.base == TYP_Mystery ||
			custom_ty.base == TYP_Interface ||
			custom_ty.HasGeneric() {

			PostError("cannot cast to base type: %s", custom_ty.String())
			return FAILED
		}

		cl.Deduct(t, custom_ty.GetBaseForm())
	}

	return OKAY
}

func (cl *Closure) DeduceWrap(t *Token, uptype *Type) cmError {
	t_child := t.Children[0]

	var downtype *Type

	if uptype != nil && uptype.CanUnwrap() {
		downtype = uptype.param[1].ty
	}

	if cl.DeduceNode(t_child, downtype) != OKAY {
		return FAILED
	}

	new_type := t.Info.ty
	plain_type := t_child.Info.ty

	if new_type == nil && plain_type != nil {
		new_type = NewOptionalType(plain_type)

		cl.Deduct(t, new_type)
	}

	return OKAY
}

func (cl *Closure) DeduceUnwrap(t *Token, uptype *Type) cmError {
	t_child := t.Children[0]

	if cl.DeduceNode(t_child, nil) != OKAY {
		return FAILED
	}

	src_type := t_child.Info.ty

	if src_type != nil {
		if !src_type.CanUnwrap() {
			PostError("wanted Opt/Result type for unwrap/flatten, got %s", src_type.String())
			return FAILED
		}

		dest_type := src_type.param[1].ty

		if t.Kind == ND_Flatten {
			if dest_type.base > TYP_Enum || !dest_type.Defaultable() {
				PostError("unsupported subtype for flatten: %s", dest_type.String())
				return FAILED
			}
		}

		cl.Deduct(t, dest_type)
	}

	return OKAY
}

func (cl *Closure) DeduceClassName(t *Token, uptype *Type) cmError {
	t_child := t.Children[0]

	if cl.DeduceNode(t_child, nil) != OKAY {
		return FAILED
	}

	class_type := t_child.Info.ty

	if class_type != nil {
		if !(class_type.base == TYP_Class || class_type.base == TYP_Interface) {
			PostError("class$ requires a class or interface, got %s",
				class_type.String())
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceIsTag(t *Token, uptype *Type) cmError {
	t_enum := t.Children[0]

	if cl.DeduceNode(t_enum, uptype) != OKAY {
		return FAILED
	}

	enum_type := t_enum.Info.ty

	if enum_type != nil {
		if !(enum_type.base == TYP_Union || enum_type.base == TYP_Enum) {
			PostError("is? can only be used with unions or enums, got %s",
				enum_type.String())
			return FAILED
		}

		for i := 1; i < len(t.Children); i++ {
			t_tag := t.Children[i]
			t_tag.Info.tag_id = enum_type.FindParam(t_tag.Str)

			if t_tag.Info.tag_id < 0 {
				PostError("unknown tag %s in type %s", t_tag.Str,
					enum_type.String())
				return FAILED
			}
		}
	}

	return OKAY
}

func (cl *Closure) DeduceTagConv(t *Token, uptype *Type) cmError {
	t_child := t.Children[0]

	// TODO: review 'uptype' here (can it ever be helpful?)
	if cl.DeduceNode(t_child, uptype) != OKAY {
		return FAILED
	}

	enum_type := t_child.Info.ty

	if enum_type != nil {
		if !(enum_type.base == TYP_Union || enum_type.base == TYP_Enum) {
			PostError("tag-int and tag$ require a union or enum, got %s",
				enum_type.String())
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceRefEqual(t *Token, uptype *Type) cmError {
	t_left := t.Children[0]
	t_right := t.Children[1]

	// TODO see if we can use uptype

	if cl.DeduceNode(t_left, t_right.Info.ty) != OKAY {
		return FAILED
	}
	if cl.DeduceNode(t_right, t_left.Info.ty) != OKAY {
		return FAILED
	}

	L_type := t_left.Info.ty
	R_type := t_right.Info.ty

	if L_type == nil || R_type == nil {
		return OKAY
	}

	if !(L_type.AssignableTo(R_type) || R_type.AssignableTo(L_type)) {
		PostError("ref-eq? with incompatible types: %s and %s",
			L_type.String(), R_type.String())
		return FAILED
	}

	switch L_type.base {
	case TYP_Interface, TYP_Array, TYP_ByteVec, TYP_Map, TYP_Set, TYP_Class, TYP_Function:
		return OKAY

	default:
		PostError("ref-eq? with non-reference type: %s", L_type.String())
		return FAILED
	}
}

func (cl *Closure) DeduceLet(t *Token, uptype *Type) cmError {
	t_var := t.Children[0]
	t_exp := t.Children[1]
	t_body := t.Children[2]

	lvar := t_var.Info.lvar
	if lvar == nil {
		panic("let form Failed to find variable")
	}

	// visit expr first, pass lvar type (in case it was explicitly set)
	if cl.DeduceNode(t_exp, lvar.ty) != OKAY {
		return FAILED
	}

	// handle variable next, when type is implicit this should set it
	if cl.DeduceNode(t_var, t_exp.Info.ty) != OKAY {
		return FAILED
	}

	// do body last (though order for it does not matter)
	if cl.DeduceNode(t_body, uptype) != OKAY {
		return FAILED
	}

	cl.Deduct(t, t_body.Info.ty)

	if cl.type_check {
		if lvar.ty.base == TYP_Void {
			PostError("local variables cannot be void")
			return FAILED
		}

		if !t_exp.Info.ty.AssignableTo(lvar.ty) {
			PostError("type mismatch in assignment: wanted %s, got %s",
				lvar.ty.String(), t_exp.Info.ty.String())
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceSetVar(t *Token) cmError {
	t_var := t.Children[0]
	t_exp := t.Children[1]

	var var_type *Type

	switch t_var.Kind {
	case ND_Local, ND_Upvar:
		lvar := t_var.Info.lvar

		if !lvar.mutable {
			PostError("variable '%s' is read-only", lvar.name)
			return FAILED
		}

		var_type = lvar.ty

	case ND_Global:
		def := t_var.Info.gdef

		if !def.mutable {
			PostError("variable '%s' is read-only", def.name)
			return FAILED
		}

		var_type = def.ty

	default:
		PostError("unknown variable: %s", t_var.String())
		return FAILED
	}

	// visit expr first, pass lvar type (in case it was explicitly set)
	if cl.DeduceNode(t_exp, var_type) != OKAY {
		return FAILED
	}

	// handle variable next, when type is implicit this should set it
	if cl.DeduceNode(t_var, t_exp.Info.ty) != OKAY {
		return FAILED
	}

	if cl.type_check {
		if !t_exp.Info.ty.AssignableTo(t_var.Info.ty) {
			PostError("type mismatch in assignment: wanted %s, got %s",
				t_var.Info.ty.String(), t_exp.Info.ty.String())
			return FAILED
		}
	}

	// node itself is always void_type, so nothing needed

	return OKAY
}

func (cl *Closure) DeduceSetField(t *Token) cmError {
	t_arr := t.Children[0]
	t_exp := t.Children[1]
	_ = t_exp

	// there is no way of inferring the container type
	if cl.DeduceNode(t_arr, nil) != OKAY {
		return FAILED
	}

	arr_type := t_arr.Info.ty

	if arr_type == nil {
		// we don't know what we're dealing with yet, so handle the
		// children in a very basic way.
		return cl.DeduceChildren(t.Children)
	}

	if arr_type.base == TYP_Tuple {
		PostError("bad assignment, cannot write tuple elements")
		return FAILED
	}

	// handle the indexor
	field_idx := cl.DeduceFieldIndex(arr_type, t.Str)
	if field_idx < 0 {
		return FAILED
	}

	// remember index for the compiler
	t.Info.tag_id = field_idx

	elem_type := arr_type.param[field_idx].ty

	// handle the value
	if cl.DeduceNode(t_exp, elem_type) != OKAY {
		return FAILED
	}

	if cl.type_check {
		if !t_exp.Info.ty.AssignableTo(elem_type) {
			PostError("type mismatch in assignment: wanted %s, got %s",
				elem_type.String(), t_exp.Info.ty.String())
			return FAILED
		}
	}

	// node itself is always void_type, so nothing needed

	return OKAY
}

func (cl *Closure) DeduceGetField(t *Token) cmError {
	// no uptype here, not much we could do with it

	t_arr := t.Children[0]

	// there is no way of inferring the container type
	if cl.DeduceNode(t_arr, nil) != OKAY {
		return FAILED
	}

	arr_type := t_arr.Info.ty

	if arr_type == nil {
		// we don't know what we're dealing with yet, so handle the
		// children in a very basic way.
		return cl.DeduceChildren(t.Children)
	}

	// handle the indexor
	field_idx := cl.DeduceFieldIndex(arr_type, t.Str)
	if field_idx < 0 {
		return FAILED
	}

	// remember index for the compiler
	t.Info.tag_id = field_idx

	elem_type := arr_type.param[field_idx].ty

	cl.Deduct(t, elem_type)

	return OKAY
}

func (cl *Closure) DeduceFieldIndex(arr_type *Type, name string) int {
	var field_idx int

	switch arr_type.base {
	case TYP_Class:
		field_idx = arr_type.FindParam(name)
		if field_idx < 0 {
			PostError("unknown class field %s in %s", name,
				arr_type.String())
			return -1
		}

	case TYP_Tuple:
		// see if part after dot is a number or a name
		r := []rune(name)

		if len(r) >= 2 && '0' <= r[1] && r[1] <= '9' {
			n, _ := fmt.Sscanf(name[1:], "%d", &field_idx)
			if n != 1 || field_idx < 0 {
				PostError("bad or unknown tuple field: %s", name)
				return -1
			}
			if field_idx >= len(arr_type.param) {
				PostError("tuple field is out-of-range (.%d > %d)",
					field_idx, len(arr_type.param)-1)
				return -1
			}
		} else {
			field_idx = arr_type.FindParam(name)
			if field_idx < 0 {
				PostError("unknown tuple field %s in %s", name,
					arr_type.String())
				return -1
			}
		}

	default:
		PostError("cannot access that type: %s", arr_type.String())
		return -1
	}

	return field_idx
}

func (cl *Closure) DeduceFmt(t *Token) cmError {
	// node type is already known (str)

	t_child := t.Children[0]
	if cl.DeduceNode(t_child, nil) != OKAY {
		return FAILED
	}

	if cl.type_check {
		f := t.Info.fmt_spec

		// handle the special %Z directive
		if f.verb == 'Z' {
			if f.AutoFromType(t_child.Info.ty) != OKAY {
				return FAILED
			}
		}

		// check compatibility
		if f.CheckType(t_child.Info.ty) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceLambda(t *Token, uptype *Type) cmError {
	// NOTE: this template was created in BindLambda()
	template := t.Info.lit.Clos

	// already done?
	// [ we can only perform DeduceFunctionType once, on the first
	//   entry into this function, so there is no point to repeat
	//   the code below (and it could even do the wrong thing). ]
	if template.ty != nil {
		return OKAY
	}

	// deduce missing parts of template's signature from uptype
	if uptype != nil {
		if uptype.base == TYP_Function {
			num_pars := len(template.t_parameters.Children)

			if len(uptype.param) != num_pars {
				PostError("wrong number of params for lambda: wanted %d, got %d",
					len(uptype.param), num_pars)
				template.failed = true
				return FAILED
			}

			// deduce parameter types
			for i, t_par := range template.t_parameters.Children {
				if t_par.Info.ty == nil {
					t_par.Info.ty = uptype.param[i].ty
				}
			}

			// deduce return type
			if template.ret_type == nil {
				template.ret_type = uptype.sub
			}
		}
	}

	// the BindLambda() code has already done BindClosure() on the
	// template lambda, and also called MarkTailCalls().  now we
	// just need to determine the lambda's type (function signature).

	if DeduceFunctionType(template) != OKAY {
		template.failed = true
		return FAILED
	}

	// we now have a full type in template.ty

	cl.Deduct(t, template.ty)
	return OKAY
}

func (cl *Closure) DeduceIf(t *Token, uptype *Type) cmError {
	t_cond := t.Children[0]
	t_then := t.Children[1]
	t_else := t.Children[2]

	// handle condition
	if cl.DeduceNode(t_cond, bool_type) != OKAY {
		return FAILED
	}
	if cl.type_check {
		if t_cond.Info.ty.custom != "bool" {
			PostError("if condition must be a bool, got %s", t_cond.Info.ty.String())
			return FAILED
		}
	}

	// determine the down types for the children
	var down_type *Type

	if t_then.Info.ty != nil {
		down_type = t_then.Info.ty
	} else if t_else.Info.ty != nil {
		down_type = t_else.Info.ty
	} else {
		down_type = uptype
	}

	if cl.DeduceNode(t_then, down_type) != OKAY {
		return FAILED
	}
	if cl.DeduceNode(t_else, down_type) != OKAY {
		return FAILED
	}

	// verify then and else clauses have compatible types
	then_type := t_then.Info.ty
	else_type := t_else.Info.ty

	if then_type != nil && else_type != nil {
		if else_type.AssignableTo(then_type) {
			// ok
		} else if then_type.AssignableTo(else_type) {
			// ok
			then_type = else_type
		} else {
			PostError("type mismatch with 'if' results: %s and %s",
				then_type.String(), else_type.String())
			return FAILED
		}
	}

	// deduce type for node itself
	if then_type != nil {
		cl.Deduct(t, then_type)
	} else if else_type != nil {
		cl.Deduct(t, else_type)
	}

	return OKAY
}

func (cl *Closure) DeduceWhile(t *Token) cmError {
	t_cond := t.Children[0]
	t_body := t.Children[1]

	if cl.DeduceNode(t_cond, bool_type) != OKAY {
		return FAILED
	}

	if cl.DeduceNode(t_body, nil) != OKAY {
		return FAILED
	}

	if cl.type_check {
		if t_cond.Info.ty.custom != "bool" {
			PostError("while condition must be a bool, got %s",
				t_cond.Info.ty.String())
			return FAILED
		}

		// ignore type of body (it does not matter)
	}

	// node itself is always void_type, so nothing needed

	return OKAY
}

func (cl *Closure) DeduceSkip(t *Token) cmError {
	junc := t.Info.junction

	if junc == nil {
		PostError("junction '%s' not found", t.Str)
		return FAILED
	}

	t_exp := t.Children[0]

	// if the target junction is in tail position, so is t_exp
	if junc.tail {
		MarkTailCalls(t_exp)
	}

	if cl.DeduceNode(t_exp, junc.ty) != OKAY {
		return FAILED
	}

	// node itself is already set as void, nothing to do

	return cl.DeduceSkipOrJunction(t_exp, junc)
}

func (cl *Closure) DeduceSkipOrJunction(t_exp *Token, junc *JunctionInfo) cmError {
	// idea here is that junc.ty becomes the "superset" of all
	// expressions in skip statements and the junction itself.

	exp_type := t_exp.Info.ty

	if exp_type != nil {
		if junc.ty == nil {
			junc.ty = exp_type
		} else if exp_type.AssignableTo(junc.ty) {
			// ok
		} else if junc.ty.AssignableTo(exp_type) {
			junc.ty = exp_type
		} else if cl.type_check {
			PostError("type mismatch in skip or junction: %s and %s",
				exp_type.String(), junc.ty.String())
			return FAILED
		}
	}

	return OKAY
}

//----------------------------------------------------------------------

// MatchSeenRecord keeps track of which enum tags have
// been seen (handled) by match rules.  It also keeps
// track of the "_" catch-all pattern.
type MatchSeenRecord struct {
	catch_all bool
	tags      map[string]bool
}

func (cl *Closure) DeduceMatch(t *Token, uptype *Type) cmError {
	t_expr := t.Children[0]

	// we don't try to infer the expression type, since patterns
	// are too "loose" (less specific than the expression).
	if cl.DeduceNode(t_expr, nil) != OKAY {
		return FAILED
	}

	var seen MatchSeenRecord
	seen.tags = make(map[string]bool)

	// for match rules, pass type of expression (if known) as well as
	// the uptype for the bodies of rules.
	//
	// NOTE: each DeducePattern_XXX() method is responsible for
	//       type-checking against the expression type.  This is
	//       opposite to other Deduce methods, but patterns are
	//       not real code / data.
	expr_type := t_expr.Info.ty

	for i := 1; i < len(t.Children); i++ {
		t_rule := t.Children[i]

		if cl.DeduceMatchRule(t_rule, expr_type, uptype) != OKAY {
			return FAILED
		}

		if cl.TrackMatchPatterns(t_rule, expr_type, &seen) != OKAY {
			return FAILED
		}
	}

	// check all bodies have compatible types
	var common_type *Type

	for i := 1; i < len(t.Children); i++ {
		t_rule := t.Children[i]
		body_type := t_rule.Children[1].Info.ty

		if body_type == nil {
			// skip it
		} else if common_type == nil {
			common_type = body_type
		} else if body_type.AssignableTo(common_type) {
			// ok
		} else if common_type.AssignableTo(body_type) {
			common_type = body_type
		} else {
			PostError("type mismatch in match results: %s and %s",
				common_type.String(), body_type.String())
			return FAILED
		}
	}

	cl.Deduct(t, common_type)

	// show an error if (a) expression is an enum and not all values
	// were handled, or (b) rule bodies return results and it is
	// possible for a match to fail.

	if cl.type_check {
		if (expr_type.base == TYP_Union || expr_type.base == TYP_Enum) && !seen.catch_all {
			PostError("union/enum match is not exhaustive, %s not fully handled",
				cl.TrackUnseenEnum(expr_type, &seen))
			return FAILED
		}

		first_body := t.Children[1].Children[1]
		if first_body.Info.ty.base != TYP_Void && !seen.catch_all {
			PostError("match produces a result, but rules are not exhaustive")
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeduceMatchRule(t *Token, expr_type, body_type *Type) cmError {
	ErrorSetToken(t)

	t_pattern := t.Children[0]
	t_body := t.Children[1]

	// nothing to do for pattern until we know the expression type
	if expr_type != nil {
		if cl.DeduceMatchPattern(t_pattern, expr_type) != OKAY {
			return FAILED
		}
	}

	// where clause?
	if len(t.Children) >= 3 {
		t_where := t.Children[2]

		if cl.DeduceNode(t_where, bool_type) != OKAY {
			return FAILED
		}
		if cl.type_check {
			if t_where.Info.ty.custom != "bool" {
				PostError("where condition must be a bool, got %s", t_where.Info.ty.String())
				return FAILED
			}
		}
	}

	if cl.DeduceNode(t_body, body_type) != OKAY {
		return FAILED
	}

	return OKAY
}

func (cl *Closure) TrackMatchPatterns(t_rule *Token, expr_type *Type, seen *MatchSeenRecord) cmError {
	if expr_type == nil {
		return OKAY
	}

	// validate the current pattern
	if seen.catch_all {
		PostError("unneeded match rule (previous were exhaustive)")
		return FAILED
	}

	// ignore rules with a where clause
	if len(t_rule.Children) >= 3 {
		return OKAY
	}

	t_pat := t_rule.Children[0]

	if t_pat.Kind == ND_MatchAll || t_pat.Kind == ND_Local {
		seen.catch_all = true
		return OKAY
	}

	// handle unions/enums from here on
	if !(expr_type.base == TYP_Union || expr_type.base == TYP_Enum) {
		return OKAY
	}

	switch t_pat.Kind {
	case ND_MatchTags:
		for _, t_tag := range t_pat.Children {
			if seen.tags[t_tag.Str] {
				PostError("bad match: tag %s already handled", t_tag.Str)
				return FAILED
			}
			seen.tags[t_tag.Str] = true
		}

	case ND_MatchUnion:
		if expr_type.base != TYP_Union {
			return OKAY
		}

		t_tag := t_pat.Children[0]
		child := t_pat.Children[1]

		if child.Kind == ND_MatchAll || child.Kind == ND_Local {
			if seen.tags[t_tag.Str] {
				PostError("bad match: tag %s already handled", t_tag.Str)
				return FAILED
			}
			seen.tags[t_tag.Str] = true
		}

	case ND_MatchConsts:
		for _, t_const := range t_pat.Children {
			if t_const.Kind == ND_Global {
				def := t_const.Info.gdef
				tag_id := def.loc.Union.tag_id

				if def.ty == nil || def.ty.base != expr_type.base {
					panic("weird global in union/enum match")
				}

				// only consider the tag handled if it has no datum
				if def.ty.param[tag_id].ty.base == TYP_Void {
					tag_name := def.ty.param[tag_id].name

					if seen.tags[tag_name] {
						PostError("bad match: tag %s already handled", tag_name)
						return FAILED
					}
					seen.tags[tag_name] = true
				}
			}
		}
	}

	if cl.TrackUnseenEnum(expr_type, seen) == "" {
		seen.catch_all = true
	}

	return OKAY
}

func (cl *Closure) TrackUnseenEnum(enum_type *Type, seen *MatchSeenRecord) string {
	for _, par := range enum_type.param {
		if !seen.tags[par.name] {
			return par.name
		}
	}
	return "" // all seen
}

func (cl *Closure) DeduceMatchPattern(pat *Token, expr_type *Type) cmError {
	// a goal here is that all TOK_Data nodes get turned into more
	// specialized nodes, such as ND_Array, ND_Map, ND_Tuple, etc...

	switch pat.Kind {
	case ND_MatchAll:
		return OKAY

	case ND_Local:
		return cl.DeduceLocal(pat, expr_type)

	case ND_MatchConsts:
		return cl.DeducePattern_Consts(pat, expr_type)

	case ND_MatchTags:
		return cl.DeducePattern_Tags(pat, expr_type)

	case ND_MatchUnion:
		return cl.DeducePattern_Union(pat, expr_type)

	case TOK_Data:
		return cl.DeducePattern_Data(pat, expr_type)

	case TOK_Name:
		return cl.DeducePattern_TagName(pat, expr_type)

	case ND_Array, ND_Map, ND_Set, ND_Tuple:
		// since those forms are *only* created here, they don't need to
		// be processed again.
		return OKAY

	default:
		PostError("cannot deduce match pattern: %s", pat.String())
		return FAILED
	}
}

func (cl *Closure) DeducePattern_Consts(pat *Token, expr_type *Type) cmError {
	// children are either ND_Literal or ND_Global
	for _, child := range pat.Children {
		switch child.Kind {
		case ND_Literal:
			if cl.DeduceLiteral(child, expr_type) != OKAY {
				return FAILED
			}
		case ND_Global:
			if cl.DeduceGlobal(child, expr_type) != OKAY {
				return FAILED
			}
		default:
			panic("weird node in ND_MatchConsts")
		}

		child_type := child.Info.ty

		switch child_type.base {
		case TYP_Int, TYP_Float, TYP_String, TYP_Enum, TYP_Function:
			// ok
		default:
			PostError("match constant must be a simple type, got: %s",
				child_type.String())
			return FAILED
		}

		if !child_type.AssignableTo(expr_type) {
			PostError("type mismatch on match constant: wanted %s, got %s",
				expr_type.String(), child_type.String())
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeducePattern_TagName(pat *Token, expr_type *Type) cmError {
	if !pat.IsTag() {
		PostError("unexpected identifier in pattern: %s", pat.Str)
		return FAILED
	}

	if !(expr_type.base == TYP_Union || expr_type.base == TYP_Enum) {
		PostError("expected union/enum type for tag match, got %s", expr_type.String())
		return FAILED
	}

	t_tag := NewNode(pat.Kind, 0)
	t_tag.Replace(pat)

	t_tag.Info.tag_id = expr_type.FindParam(t_tag.Str)
	if t_tag.Info.tag_id < 0 {
		PostError("unknown tag %s in type %s", t_tag.Str, expr_type.String())
		return FAILED
	}

	pat.Kind = ND_MatchTags
	pat.Children = make([]*Token, 0)
	pat.Add(t_tag)

	return OKAY
}

func (cl *Closure) DeducePattern_Tags(pat *Token, expr_type *Type) cmError {
	if !(expr_type.base == TYP_Union || expr_type.base == TYP_Enum) {
		PostError("expected union/enum type for tag match, got %s", expr_type.String())
		return FAILED
	}

	for _, t_tag := range pat.Children {
		t_tag.Info.tag_id = expr_type.FindParam(t_tag.Str)
		if t_tag.Info.tag_id < 0 {
			PostError("unknown tag %s in type %s", t_tag.Str, expr_type.String())
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeducePattern_Data(pat *Token, expr_type *Type) cmError {
	// how we interpret stuff in {} is based on the expression type
	switch expr_type.base {
	case TYP_Array:
		return cl.DeducePattern_Array(pat, expr_type)

	case TYP_Map:
		return cl.DeducePattern_Map(pat, expr_type)

	case TYP_Set:
		return cl.DeducePattern_Set(pat, expr_type)

	case TYP_Tuple:
		return cl.DeducePattern_Tuple(pat, "tuple", expr_type)

	case TYP_Class:
		return cl.DeducePattern_Tuple(pat, "class", expr_type)

	case TYP_Union:
		return cl.DeducePattern_Union(pat, expr_type)

	default:
		PostError("bad match against data pattern in {}, value is %s",
			expr_type.String())
		return FAILED
	}
}

func (cl *Closure) DeducePattern_Array(pat *Token, arr_type *Type) cmError {
	pat.Kind = ND_Array

	elem_type := arr_type.sub

	for _, child := range pat.Children {
		if cl.DeduceMatchPattern(child, elem_type) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeducePattern_Set(pat *Token, set_type *Type) cmError {
	pat.Kind = ND_Set

	key_type := set_type.KeyType()

	for _, child := range pat.Children {
		if child.Kind == ND_Local {
			PostError("bad pattern: cannot bind variables to a set key")
			return FAILED
		}

		if cl.DeduceMatchPattern(child, key_type) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeducePattern_Map(pat *Token, map_type *Type) cmError {
	pat.Kind = ND_Map

	key_type := map_type.KeyType()
	elem_type := map_type.sub

	// we need to convert the children to ND_MapPair nodes
	children := pat.Children
	pat.Children = make([]*Token, 0)

	if len(children)%2 != 0 {
		PostError("bad map pattern: odd number of elements")
		return FAILED
	}

	for i := 0; i < len(children); i += 2 {
		t_key := children[i]
		t_elem := children[i+1]

		if t_key.Kind == ND_Local {
			PostError("bad pattern: cannot bind variables to a map key")
			return FAILED
		}
		if t_key.Kind == ND_MatchAll && t_elem.Kind != ND_MatchAll {
			PostError("bad map pattern: key is '_' but value isn't")
			return FAILED
		}

		if cl.DeduceMatchPattern(t_key, key_type) != OKAY {
			return FAILED
		}
		if cl.DeduceMatchPattern(t_elem, elem_type) != OKAY {
			return FAILED
		}

		t_pair := NewNode(ND_MapPair, 0)
		t_pair.LineNum = t_key.LineNum
		t_pair.Add(t_key)
		t_pair.Add(t_elem)

		pat.Add(t_pair)
	}

	return OKAY
}

func (cl *Closure) DeducePattern_Tuple(pat *Token, what string, tup_type *Type) cmError {
	// Note: this code handles class objects too

	// tuples and classes are never empty
	if len(pat.Children) == 0 {
		PostError("empty pattern in {} for %s match", what)
		return FAILED
	}

	pat.Kind = ND_Tuple

	if tup_type.base == TYP_Class {
		// ok
	} else if tup_type.base == TYP_Tuple {
		// ok
	} else {
		PostError("cannot match %s against a %s", tup_type.base.String(), what)
		return FAILED
	}

	// size check
	total := len(pat.Children)

	if pat.Info.open == 0 {
		if total != len(tup_type.param) {
			PostError("wrong size for %s pattern: wanted %d, got %d",
				what, len(tup_type.param), total)
			return FAILED
		}
	} else {
		if total > len(tup_type.param) {
			PostError("wrong size for %s pattern: wanted %d (or less), got %d",
				what, len(tup_type.param), total)
			return FAILED
		}
	}

	for i, sub := range pat.Children {
		var t_field *Token
		var t_value *Token

		if sub.Kind == ND_TuplePair {
			t_field = sub.Children[0]
			t_value = sub.Children[1]
		} else {
			t_value = sub
		}

		// determine index of tuple/class field
		field_idx := i

		if t_field != nil {
			field_idx = tup_type.FindParam(t_field.Str)
			if field_idx < 0 {
				PostError("no such field %s in %s", t_field.Str, tup_type.String())
				return FAILED
			}
			t_field.Info.tag_id = field_idx

		} else if pat.Info.open < 0 {
			field_idx = len(tup_type.param) - total + i
		}

		val_type := tup_type.param[field_idx].ty

		if cl.DeduceMatchPattern(t_value, val_type) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) DeducePattern_Union(pat *Token, union_type *Type) cmError {
	// this code is used for both top-level union patterns (ND_MatchUnion)
	// and for something in {} when we know the type is a union (TOK_Data).

	if union_type.base != TYP_Union {
		PostError("bad match: wanted %s, got union pattern", union_type.String())
		return FAILED
	}

	if pat.Info.open != 0 {
		PostError("bad match: cannot use '...' in union pattern")
		return FAILED
	}

	children := pat.Children
	pat.Children = make([]*Token, 0)

	if len(children) == 0 {
		PostError("empty pattern in {} for union match")
		return FAILED
	}

	t_tag := children[0]
	if !t_tag.IsTag() {
		PostError("bad union pattern: expected tag as first element")
		return FAILED
	}

	// find tag in union type
	t_tag.Info.tag_id = union_type.FindParam(t_tag.Str)
	if t_tag.Info.tag_id < 0 {
		PostError("unknown union tag %s in type %s", t_tag.Str, union_type.String())
		return FAILED
	}

	datum_type := union_type.param[t_tag.Info.tag_id].ty

	// handle {`TAG} as abbreviation of {`TAG {void}}
	if len(children) == 1 && datum_type.base == TYP_Void {
		pat.Kind = ND_MatchTags
		pat.Add(t_tag)
		return OKAY
	}

	if len(children) < 2 {
		PostError("bad union pattern: missing datum after tag")
		return FAILED
	}
	if len(children) > 2 {
		PostError("wrong syntax for union pattern (extra items)")
		return FAILED
	}

	t_datum := children[1]
	if t_datum.IsTag() {
		PostError("bad union pattern: missing datum, got %s", t_datum.Str)
		return FAILED
	}

	pat.Kind = ND_MatchUnion
	pat.Add(t_tag)
	pat.Add(t_datum)

	return cl.DeduceMatchPattern(t_datum, datum_type)
}
