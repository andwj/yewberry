
Yewberry README
===============

by Andrew Apted, 2020.


About
-----

Yewberry is a statically-typed programming language currently
in development.  It has a syntax like Scheme or LISP, but the
semantics are very different.

This repository contains a lot of history, previously there was
an interpreter with a REPL (see the "interpreter" branch), while
the "native_c" branch is developing a compiler which creates
native executables, using a C compiler for the low-level code
generation.

Once this compiler is fairly well cooked, development will move
to my "yewberry-3" repository and continue there.


Documentation
-------------

The [Overview](OVERVIEW.md) document contains a good overview
of all the features of the Yewberry language.  There is no
full specification of the language yet.


Legalese
--------

Yewberry is Copyright &copy; 2020 Andrew Apted.

Yewberry is Free Software, under the terms of the GNU General
Public License, version 3 or (at your option) any later version.
See the [LICENSE.txt](LICENSE.txt) file for the complete text.

Yewberry comes with NO WARRANTY of any kind, express or implied.
Please read the license for full details.

