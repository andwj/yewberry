// Copyright 2020 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

// import "fmt"

type Module struct {
	// a simple name for this module (without any slash).
	// it may be different from the ".ymod" file, due to several
	// modules having the same name.  main purpose is to be a prefix
	// to distinguish identifiers in compiled code.
	name string

	ns *Namespace
}

// modules is the list of modules which are being compiled.
// this list is provided by the driver program.
// the first module is always the prelude, and the last one is
// always the main program.
var modules []*Module

func InitModules() {
	modules = make([]*Module, 0)
}

func AddModule(name string) *Module {
	mod := new(Module)
	mod.name = name
	mod.ns = NewNamespace()

	modules = append(modules, mod)

	return mod
}

//----------------------------------------------------------------------

type Namespace struct {
	modules map[string]*Module
	macros  map[string]*Macro
	types   map[string]*Type
	defs    map[string]*GlobalDef
}

// context represents the environment of the code currently being
// compiled.  that code will use this to lookup global identifiers.
// nothing outside of this namespace (and any parents) will be
// accessible.
var context *Namespace
var prelude *Namespace

func NewNamespace() *Namespace {
	ns := new(Namespace)
	ns.macros = make(map[string]*Macro)
	ns.types = make(map[string]*Type)
	ns.defs = make(map[string]*GlobalDef)
	ns.modules = make(map[string]*Module)

	return ns
}

func (ns *Namespace) HasModule(name string) bool {
	_, ok := ns.modules[name]
	return ok
}

func (ns *Namespace) GetModule(name string) *Module {
	return ns.modules[name]
}

func (ns *Namespace) AddModule(name string, mod *Module) {
	ns.modules[name] = mod
}

func (ns *Namespace) RemoveModule(name string) {
	delete(ns.modules, name)
}

func (ns *Namespace) HasMacro(name string) bool {
	_, ok := ns.macros[name]
	return ok
}

func (ns *Namespace) GetMacro(name string) *Macro {
	return ns.macros[name]
}

func (ns *Namespace) AddMacro(name string, mac *Macro) {
	ns.macros[name] = mac
}

func (ns *Namespace) RemoveMacro(name string) {
	delete(ns.macros, name)
}

func (ns *Namespace) HasType(name string) bool {
	_, ok := ns.types[name]
	return ok
}

func (ns *Namespace) GetType(name string) *Type {
	return ns.types[name]
}

func (ns *Namespace) AddType(name string, mac *Type) {
	ns.types[name] = mac
}

func (ns *Namespace) RemoveType(name string) {
	delete(ns.types, name)
}

func (ns *Namespace) HasDef(name string) bool {
	_, ok := ns.defs[name]
	return ok
}

func (ns *Namespace) GetDef(name string) *GlobalDef {
	return ns.defs[name]
}

func (ns *Namespace) AddDef(name string, mac *GlobalDef) {
	ns.defs[name] = mac
}

func (ns *Namespace) RemoveDef(name string) {
	delete(ns.defs, name)
}

//----------------------------------------------------------------------

func N_HasMacro(name, mod_name string) bool {
	return N_GetMacro(name, mod_name) != nil
}

func N_GetMacro(name, mod_name string) *Macro {
	if mod_name == "" {
		mac := context.GetMacro(name)
		if mac == nil {
			mac = prelude.GetMacro(name)
		}
		return mac

	} else {
		mod := context.GetModule(mod_name)

		if mod == nil {
			return nil
		} else {
			return mod.ns.GetMacro(name)
		}
	}
}

func N_HasType(name, mod_name string) bool {
	return N_GetType(name, mod_name) != nil
}

func N_GetType(name, mod_name string) *Type {
	if mod_name == "" {
		mac := context.GetType(name)
		if mac == nil {
			mac = prelude.GetType(name)
		}
		return mac

	} else {
		mod := context.GetModule(mod_name)

		if mod == nil {
			return nil
		} else {
			return mod.ns.GetType(name)
		}
	}
}

func N_HasDef(name, mod_name string) bool {
	return N_GetDef(name, mod_name) != nil
}

func N_GetDef(name, mod_name string) *GlobalDef {
	if mod_name == "" {
		mac := context.GetDef(name)
		if mac == nil {
			mac = prelude.GetDef(name)
		}
		return mac

	} else {
		mod := context.GetModule(mod_name)

		if mod == nil {
			return nil
		} else {
			return mod.ns.GetDef(name)
		}
	}
}
