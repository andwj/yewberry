// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "io"
import "os"
import "strings"
import "math"
import "math/rand"
import "unicode"

import "github.com/peterh/liner"

type Builtin struct {
	name string
	code func()
}

var builtins map[string]*Builtin

// the result of frexp builtin: a (tuple float int)
var frexp_type *Type

// results of parse-int and parse-float
var parse_int_type *Type
var parse_float_type *Type

func SetupBuiltins() {
	frexp_type = NewType(TYP_Tuple)
	frexp_type.parent = tuple_parent
	frexp_type.AddParam("", float_type)
	frexp_type.AddParam("", int_type)

	parse_int_type = NewOptionalType(int_type)
	parse_float_type = NewOptionalType(float_type)

	builtins = make(map[string]*Builtin)

	/* ---- functions ---- */

	// string manipulation / conversion
	RegisterBuiltin("parse-int", BI_ParseInt)
	RegisterBuiltin("parse-float", BI_ParseFloat)

	// floating point math library
	RegisterBuiltin("floor", BI_Floor)
	RegisterBuiltin("ceil", BI_Ceil)
	RegisterBuiltin("round", BI_Round)
	RegisterBuiltin("trunc", BI_Trunc)

	RegisterBuiltin("nan?", BI_IsNan)
	RegisterBuiltin("inf?", BI_IsInfinity)

	RegisterBuiltin("hypot", BI_Hypot)
	RegisterBuiltin("log", BI_Log)
	RegisterBuiltin("log10", BI_Log10)
	RegisterBuiltin("frexp", BI_Frexp)
	RegisterBuiltin("ldexp", BI_Ldexp)

	RegisterBuiltin("sin", BI_Sin)
	RegisterBuiltin("cos", BI_Cos)
	RegisterBuiltin("tan", BI_Tan)
	RegisterBuiltin("asin", BI_ArcSin)
	RegisterBuiltin("acos", BI_ArcCos)
	RegisterBuiltin("atan", BI_ArcTan)
	RegisterBuiltin("atan2", BI_ArcTan2)

	// random numbers
	RegisterBuiltin("rand-seed", BI_RandSeed)
	RegisterBuiltin("rand-int", BI_RandInt)
	RegisterBuiltin("rand-float", BI_RandFloat)

	// very basic I/O and OS library
	RegisterBuiltin("print", BI_Print)
	RegisterBuiltin("input-line", BI_InputLine)
	RegisterBuiltin("exit-program", BI_ExitProgram)
	RegisterBuiltin("abort-program", BI_AbortProgram)

	/* ---- methods ---- */

	// boolean logic
	RegisterMethod("bool.not", BI_BoolNot)

	// comparisons
	RegisterMethod("int.eq?", BI_IntEqual)
	RegisterMethod("int.lt?", BI_IntLess)
	RegisterMethod("int.gt?", BI_IntGreater)
	RegisterMethod("int.ne?", BI_IntNotEqual)
	RegisterMethod("int.le?", BI_IntLessEq)
	RegisterMethod("int.ge?", BI_IntGreaterEq)

	RegisterMethod("float.eq?", BI_FloatEqual)
	RegisterMethod("float.lt?", BI_FloatLess)
	RegisterMethod("float.gt?", BI_FloatGreater)
	RegisterMethod("float.ne?", BI_FloatNotEqual)
	RegisterMethod("float.le?", BI_FloatLessEq)
	RegisterMethod("float.ge?", BI_FloatGreaterEq)

	RegisterMethod("str.eq?", BI_StrEqual)
	RegisterMethod("str.lt?", BI_StrLess)
	RegisterMethod("str.gt?", BI_StrGreater)
	RegisterMethod("str.ne?", BI_StrNotEqual)
	RegisterMethod("str.le?", BI_StrLessEq)
	RegisterMethod("str.ge?", BI_StrGreaterEq)

	RegisterMethod("bool.eq?", BI_BoolEqual)
	RegisterMethod("bool.ne?", BI_BoolNotEqual)

	// integer arithmetic
	RegisterMethod("int.add", BI_IntAdd)
	RegisterMethod("int.sub", BI_IntSubtract)
	RegisterMethod("int.mul", BI_IntMultiply)
	RegisterMethod("int.div", BI_IntDivide)
	RegisterMethod("int.mod", BI_IntModulo)
	RegisterMethod("int.rem", BI_IntRemainder)
	RegisterMethod("int.pow", BI_IntPower)

	RegisterMethod("int.neg", BI_IntNeg)
	RegisterMethod("int.abs", BI_IntAbs)
	RegisterMethod("int.to-float", BI_ToFloat)

	// floating point arithmetic
	RegisterMethod("float.add", BI_FloatAdd)
	RegisterMethod("float.sub", BI_FloatSubtract)
	RegisterMethod("float.mul", BI_FloatMultiply)
	RegisterMethod("float.div", BI_FloatDivide)
	RegisterMethod("float.mod", BI_FloatModulo)
	RegisterMethod("float.rem", BI_FloatRemainder)
	RegisterMethod("float.pow", BI_FloatPower)

	RegisterMethod("float.neg", BI_FloatNeg)
	RegisterMethod("float.abs", BI_FloatAbs)
	RegisterMethod("float.sqrt", BI_FloatSqrt)
	RegisterMethod("float.to-int", BI_ToInt)

	// bitwise operations
	RegisterMethod("int.or", BI_BitOr)
	RegisterMethod("int.and", BI_BitAnd)
	RegisterMethod("int.xor", BI_BitXor)
	RegisterMethod("int.flip", BI_BitFlip)

	RegisterMethod("int.shift-left", BI_ShiftLeft)
	RegisterMethod("int.shift-right", BI_ShiftRight)

	// character tests
	RegisterMethod("char.letter?", BI_CharLetter)
	RegisterMethod("char.digit?", BI_CharDigit)
	RegisterMethod("char.symbol?", BI_CharSymbol)
	RegisterMethod("char.mark?", BI_CharMark)
	RegisterMethod("char.space?", BI_CharSpace)
	RegisterMethod("char.control?", BI_CharControl)

	RegisterMethod("char.tolower", BI_CharLower)
	RegisterMethod("char.toupper", BI_CharUpper)

	// strings
	RegisterMethod("str.add", BI_StrAdd)
	RegisterMethod("str.len", BI_StrLen)
	RegisterMethod("str.get-elem", BI_StrGetElem)

	RegisterMethod("str.tolower", BI_StrLower)
	RegisterMethod("str.toupper", BI_StrUpper)
	RegisterMethod("str.subseq", BI_StrSubseq)

	// arrays
	RegisterMethod("array.len", BI_ArrayLen)
	RegisterMethod("array.copy", BI_ArrayCopy)
	RegisterMethod("array.subseq", BI_ArraySubseq)
	RegisterMethod("array.add", BI_ArrayConcat)

	RegisterMethod("array.get-elem", BI_ArrayGetElem)
	RegisterMethod("array.set-elem", BI_ArraySetElem)
	RegisterMethod("array.append-elem", BI_ArrayAppend)
	RegisterMethod("array.insert-elem", BI_ArrayInsert)
	RegisterMethod("array.remove-elem", BI_ArrayRemove)
	RegisterMethod("array.remove-seq", BI_ArrayRemoveSeq)
	RegisterMethod("array.clear-all", BI_ArrayClear)

	// byte vectors
	// [ we re-use array methods for some things ]
	RegisterMethod("byte-vec.copy", BI_BytevecCopy)
	RegisterMethod("byte-vec.subseq", BI_BytevecSubseq)
	RegisterMethod("byte-vec.add", BI_BytevecConcat)

	RegisterMethod("byte-vec.set-elem", BI_BytevecSetElem)
	RegisterMethod("byte-vec.append-elem", BI_BytevecAppend)
	RegisterMethod("byte-vec.insert-elem", BI_BytevecInsert)

	RegisterMethod("byte-vec.len", BI_ArrayLen)
	RegisterMethod("byte-vec.get-elem", BI_ArrayGetElem)
	RegisterMethod("byte-vec.remove-elem", BI_ArrayRemove)
	RegisterMethod("byte-vec.remove-seq", BI_ArrayRemoveSeq)
	RegisterMethod("byte-vec.clear-all", BI_ArrayClear)

	// maps
	RegisterMethod("map.len", BI_MapLen)
	RegisterMethod("map.copy", BI_MapCopy)
	RegisterMethod("map.add", BI_MapMerge)
	RegisterMethod("map.collect-keys", BI_MapCollectKeys)

	RegisterMethod("map.get-elem", BI_MapGetElem)
	RegisterMethod("map.set-elem", BI_MapSetElem)
	RegisterMethod("map.remove-elem", BI_MapRemoveElem)
	RegisterMethod("map.clear-all", BI_MapClear)

	// set handling
	RegisterMethod("set.len", BI_SetLen)
	RegisterMethod("set.copy", BI_SetCopy)
	RegisterMethod("set.clear-all", BI_SetClear)
	RegisterMethod("set.collect-keys", BI_SetCollectKeys)

	RegisterMethod("set.get-elem", BI_SetGetElem)
	RegisterMethod("set.set-elem", BI_SetSetElem)
	RegisterMethod("set.remove-elem", BI_SetRemoveElem)

	RegisterMethod("set.add", BI_SetUnion)
	RegisterMethod("set.sub", BI_SetDifference)
	RegisterMethod("set.mul", BI_SetIntersect)

	// classes
	RegisterMethod("class.copy", BI_ObjCopy)
}

func RegisterBuiltin(name string, code func()) {
	builtins[name] = &Builtin{name, code}
}

func RegisterMethod(name string, code func()) {
	builtins[name] = &Builtin{name, code}
}

//----------------------------------------------------------------------

func BI_BoolNot() {
	v := data_stack[stack_frame+0]

	SetResult_Bool(!v.IsTrue())
}

//----------------------------------------------------------------------

func BI_IntEqual() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "eq? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "eq? primitive")

	SetResult_Bool(a.Int == b.Int)
}

func BI_IntNotEqual() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "ne? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "ne? primitive")

	SetResult_Bool(a.Int != b.Int)
}

func BI_IntLess() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "lt? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "lt? primitive")

	SetResult_Bool(a.Int < b.Int)
}

func BI_IntLessEq() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "le? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "le? primitive")

	SetResult_Bool(a.Int <= b.Int)
}

func BI_IntGreater() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "gt? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "gt? primitive")

	SetResult_Bool(a.Int > b.Int)
}

func BI_IntGreaterEq() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "ge? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "ge? primitive")

	SetResult_Bool(a.Int >= b.Int)
}

func BI_StrEqual() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_String, "str-eq? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_String, "str-eq? primitive")

	SetResult_Bool(a.Str == b.Str)
}

func BI_StrNotEqual() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_String, "str-ne? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_String, "str-ne? primitive")

	SetResult_Bool(a.Str != b.Str)
}

func BI_StrLess() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_String, "str-lt? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_String, "str-lt? primitive")

	SetResult_Bool(a.Str < b.Str)
}

func BI_StrLessEq() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_String, "str-le? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_String, "str-le? primitive")

	SetResult_Bool(a.Str <= b.Str)
}

func BI_StrGreater() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_String, "str-gt? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_String, "str-gt? primitive")

	SetResult_Bool(a.Str > b.Str)
}

func BI_StrGreaterEq() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_String, "str-ge? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_String, "str-ge? primitive")

	SetResult_Bool(a.Str >= b.Str)
}

func BI_FloatEqual() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-eq? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-eq? primitive")

	SetResult_Bool(a.Float == b.Float)
}

func BI_FloatNotEqual() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-ne? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-ne? primitive")

	SetResult_Bool(a.Float != b.Float)
}

func BI_FloatLess() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-lt? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-lt? primitive")

	SetResult_Bool(a.Float < b.Float)
}

func BI_FloatLessEq() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-le? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-le? primitive")

	SetResult_Bool(a.Float <= b.Float)
}

func BI_FloatGreater() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-gt? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-gt? primitive")

	SetResult_Bool(a.Float > b.Float)
}

func BI_FloatGreaterEq() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-ge? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-ge? primitive")

	SetResult_Bool(a.Float >= b.Float)
}

func BI_BoolEqual() {
	a := data_stack[stack_frame+0]
	a.VerifyType(BTYP_Bool, "bool-eq? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(BTYP_Bool, "bool-eq? primitive")

	SetResult_Bool(a.Int == b.Int)
}

func BI_BoolNotEqual() {
	a := data_stack[stack_frame+0]
	a.VerifyType(BTYP_Bool, "bool-ne? primitive")

	b := data_stack[stack_frame+1]
	b.VerifyType(BTYP_Bool, "bool-ne? primitive")

	SetResult_Bool(a.Int != b.Int)
}

//----------------------------------------------------------------------

func BI_ParseInt() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_String, "parse-int")

	var res Value

	lex := NewLexer(strings.NewReader(v.Str))
	tok := lex.Scan()

	if tok.Kind == TOK_EOF || tok.Kind == TOK_ERROR {
		res.MakeNone(parse_int_type)
		SetResult(res)
		return
	}

	// since "INAN" is produced by fmt$, we should handle it here
	if tok.Match("INAN") {
		res.Copy(glob_INAN.loc)
		res.Wrap(parse_int_type)
		SetResult(res)
		return
	}

	if !(tok.Kind == TOK_Int || tok.Kind == TOK_Char) {
		res.MakeNone(parse_int_type)
		SetResult(res)
		return
	}

	err := res.DecodeInt(tok.Str)
	if err != nil {
		res.MakeNone(parse_int_type)
		SetResult(res)
		return
	}

	res.Wrap(parse_int_type)
	SetResult(res)
}

func BI_ParseFloat() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_String, "parse-float")

	var res Value

	lex := NewLexer(strings.NewReader(v.Str))
	tok := lex.Scan()

	if tok.Kind == TOK_EOF || tok.Kind == TOK_ERROR {
		res.MakeNone(parse_float_type)
		SetResult(res)
		return
	}

	// since "NAN", "+INF" and "-INF" is produced by fmt$, we should handle them
	if tok.Match("NAN") {
		res.Copy(glob_NAN.loc)
		res.Wrap(parse_float_type)
		SetResult(res)
		return

	} else if tok.Match("+INF") {
		res.Copy(glob_PINF.loc)
		res.Wrap(parse_float_type)
		SetResult(res)
		return

	} else if tok.Match("-INF") {
		res.Copy(glob_NINF.loc)
		res.Wrap(parse_float_type)
		SetResult(res)
		return
	}

	if !(tok.Kind == TOK_Int || tok.Kind == TOK_Float) {
		res.MakeNone(parse_float_type)
		SetResult(res)
		return
	}

	err := res.DecodeFloat(tok.Str)
	if err != nil {
		res.MakeNone(parse_float_type)
		SetResult(res)
		return
	}

	res.Wrap(parse_float_type)
	SetResult(res)
}

func BI_StrLower() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_String, "str-lower")

	res := Value{}
	res.MakeString(strings.ToLower(v.Str))

	SetResult(res)
}

func BI_StrUpper() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_String, "str-upper")

	res := Value{}
	res.MakeString(strings.ToUpper(v.Str))

	SetResult(res)
}

func BI_StrSubseq() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_String, "substring")

	r := []rune(v.Str)
	length := len(r)

	// determine the start and end indices
	v1 := data_stack[stack_frame+1]
	v1.VerifyType(TYP_Int, "substring")

	v2 := data_stack[stack_frame+2]
	v2.VerifyType(TYP_Int, "substring")

	start := int(v1.Int)
	end := int(v2.Int)

	// clamp the start to 0, end to the length
	if start < 0 {
		start = 0
	}
	if end > length {
		end = length
	}

	res := Value{}

	// result will be empty?
	// [ this handles all weird cases, e.g. start >= length ]
	if start >= end {
		res.MakeString("")
	} else {
		// produce a shallow copy of the given range
		r = r[start:end]
		res.MakeString(string(r))
	}

	SetResult(res)
}

//----------------------------------------------------------------------

func BI_CharLetter() {
	v := data_stack[stack_frame+0]
	v.VerifyType(BTYP_Char, "char-letter?")

	res := unicode.IsLetter(rune(v.Int))

	SetResult_Bool(res)
}

func BI_CharDigit() {
	v := data_stack[stack_frame+0]
	v.VerifyType(BTYP_Char, "char-digit?")

	// Note: IsNumber() is another possibility here, but I am not
	// sure whether that would be correct or not.

	res := unicode.IsDigit(rune(v.Int))

	SetResult_Bool(res)
}

func BI_CharSymbol() {
	v := data_stack[stack_frame+0]
	v.VerifyType(BTYP_Char, "char-symbol?")

	// NOTE: we treat P-class ("Punctuation") and S-class "Symbols"
	// the same here.  It is not really clear what the difference is.
	// e.g. Go considers '*' to be punctuation and '+' a symbol.
	// Also a single quote is punctuation, back-quote is a symbol.

	ch := rune(v.Int)

	res := unicode.IsSymbol(ch) || unicode.IsPunct(ch)

	SetResult_Bool(res)
}

func BI_CharMark() {
	// Note: a "mark" is a combining character, like diacritics.
	// The unicode range U0300 to U036F are the main ones (but
	// there are others).

	v := data_stack[stack_frame+0]
	v.VerifyType(BTYP_Char, "char-mark?")

	res := unicode.IsMark(rune(v.Int))

	SetResult_Bool(res)
}

func BI_CharSpace() {
	v := data_stack[stack_frame+0]
	v.VerifyType(BTYP_Char, "char-space?")

	res := unicode.IsSpace(rune(v.Int))

	SetResult_Bool(res)
}

func BI_CharControl() {
	v := data_stack[stack_frame+0]
	v.VerifyType(BTYP_Char, "char-control?")

	res := unicode.IsControl(rune(v.Int))

	SetResult_Bool(res)
}

func BI_CharLower() {
	v := data_stack[stack_frame+0]
	v.VerifyType(BTYP_Char, "char-lower")

	ch := unicode.ToLower(rune(v.Int))

	res := Value{}
	res.MakeChar(ch)

	SetResult(res)
}

func BI_CharUpper() {
	v := data_stack[stack_frame+0]
	v.VerifyType(BTYP_Char, "char-upper")

	ch := unicode.ToUpper(rune(v.Int))

	res := Value{}
	res.MakeChar(ch)

	SetResult(res)
}

//----------------------------------------------------------------------

func BI_IntNeg() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Int, "int-neg")

	SetResult_Int(-v.Int)
}

func BI_IntAbs() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Int, "int-abs")

	if v.Int < 0 {
		SetResult_Int(-v.Int)
	} else {
		SetResult_Int(v.Int)
	}
}

func BI_IntAdd() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "int-add")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "int-add")

	SetResult_Int(a.Int + b.Int)
}

func BI_IntSubtract() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "int-sub")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "int-sub")

	SetResult_Int(a.Int - b.Int)
}

func BI_IntMultiply() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "int-mul")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "int-mul")

	SetResult_Int(a.Int * b.Int)
}

func BI_IntDivide() {
	// this implements "truncated division" which means it is
	// like the division is done with infinite precision and
	// then everything after the decimal point is thrown away.
	//
	// examples:  5 /  3 =  1
	//           -5 /  3 = -1
	//            5 / -3 = -1
	//           -5 / -3 =  1

	num := data_stack[stack_frame+0]
	num.VerifyType(TYP_Int, "int-div")

	div := data_stack[stack_frame+1]
	div.VerifyType(TYP_Int, "int-div")

	if div.Int == 0 {
		SetResult(*glob_INAN.loc)
		return
	}

	SetResult_Int(num.Int / div.Int)
}

func BI_IntModulo() {
	// this implements a modulo which uses the *floor* of the
	// integer division i.e. as if the division was done with
	// infinite precision, then rounded down toward -infinity.
	//
	// examples:  5 mod  3 =  2
	//           -5 mod  3 =  1
	//            5 mod -3 = -1
	//           -5 mod -3 = -2

	num := data_stack[stack_frame+0]
	num.VerifyType(TYP_Int, "int-mod")

	div := data_stack[stack_frame+1]
	div.VerifyType(TYP_Int, "int-mod")

	if div.Int == 0 {
		SetResult(*glob_INAN.loc)
		return
	}

	// WISH: a purely integer way to calculate this
	n := float64(num.Int)
	d := float64(div.Int)

	tmp := n - math.Floor(n/d)*d

	SetResult_Int(int64(tmp))
}

func BI_IntRemainder() {
	// this implements a remainder: if N is the numerator and
	// D is the divisor, then compute (N - (N / D) * D) using
	// integer division (as above) and multiplication.
	//
	// examples:  5 %  3 =  2
	//           -5 %  3 = -2
	//            5 % -3 =  2
	//           -5 % -3 = -2

	num := data_stack[stack_frame+0]
	num.VerifyType(TYP_Int, "int-rem")

	div := data_stack[stack_frame+1]
	div.VerifyType(TYP_Int, "int-rem")

	if div.Int == 0 {
		SetResult(*glob_INAN.loc)
		return
	}

	SetResult_Int(num.Int % div.Int)
}

func BI_IntPower() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "int-pow")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "int-pow")

	// handle some special cases...
	switch {
	case b.Int < 0:
		SetResult(*glob_INAN.loc)
		return

	case b.Int == 0:
		SetResult_Int(1)
		return

	case b.Int == 1:
		SetResult_Int(a.Int)
		return

	case a.Int == 0 || a.Int == 1:
		SetResult_Int(a.Int)
		return

	case a.Int == -1:
		if (b.Int % 2) == 0 {
			SetResult_Int(1)
		} else {
			SetResult_Int(-1)
		}
	}

	// here we have abs(a) >= 2 && b >= 2.
	// hence any b >= 64 will definitely overflow an int64.

	if b.Int >= 64 {
		SetResult(*glob_INAN.loc)
		return
	}

	res := int64(a.Int)

	for i := b.Int; i > 1; i-- {
		newval := res * int64(a.Int)

		// check for overflow
		if newval / int64(a.Int) != res {
			SetResult(*glob_INAN.loc)
			return
		}

		res = newval
	}

	SetResult_Int(res)
}

func BI_FloatAdd() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-add")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-add")

	SetResult_Float(a.Float + b.Float)
}

func BI_FloatSubtract() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-sub")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-sub")

	SetResult_Float(a.Float - b.Float)
}

func BI_FloatMultiply() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-mul")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-mul")

	SetResult_Float(a.Float * b.Float)
}

func BI_FloatDivide() {
	num := data_stack[stack_frame+0]
	num.VerifyType(TYP_Float, "float-div")

	div := data_stack[stack_frame+1]
	div.VerifyType(TYP_Float, "float-div")

	if div.Float == 0 {
		// emulate usual behavior of floating-point div-by-zero
		// [ the Go spec allows other behaviors, like a panic ]
		switch {
		case num.Float < 0:
			SetResult_Float(math.Inf(-1))
		case num.Float > 0:
			SetResult_Float(math.Inf(1))
		default:
			SetResult_Float(math.NaN())
		}
		return
	}

	SetResult_Float(num.Float / div.Float)
}

func BI_FloatModulo() {
	num := data_stack[stack_frame+0]
	num.VerifyType(TYP_Float, "float-mod")

	div := data_stack[stack_frame+1]
	div.VerifyType(TYP_Float, "float-mod")

	if div.Float == 0 {
		// emulate usual behavior of floating-point div-by-zero
		// [ the Go spec allows other behaviors, like a panic ]
		SetResult_Float(math.NaN())
		return
	}

	res := num.Float - math.Floor(num.Float/div.Float)*div.Float

	SetResult_Float(res)
}

func BI_FloatRemainder() {
	num := data_stack[stack_frame+0]
	num.VerifyType(TYP_Float, "float-rem")

	div := data_stack[stack_frame+1]
	div.VerifyType(TYP_Float, "float-rem")

	if div.Float == 0 {
		// emulate usual behavior of floating-point div-by-zero
		// [ the Go spec allows other behaviors, like a panic ]
		SetResult_Float(math.NaN())
		return
	}

	res := math.Mod(num.Float, div.Float)

	SetResult_Float(res)
}

func BI_FloatPower() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "float-pow")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "float-pow")

	if b.Float < 0 {
		SetResult_Float(math.NaN())
		return
	}

	res := math.Pow(a.Float, b.Float)

	SetResult_Float(res)
}

func BI_FloatNeg() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "float-neg")

	SetResult_Float(-v.Float)
}

func BI_FloatAbs() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "float-abs")

	if v.Float < 0 {
		SetResult_Float(-v.Float)
	} else {
		SetResult_Float(v.Float)
	}
}

func BI_FloatSqrt() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "float.sqrt")

	SetResult_Float(math.Sqrt(v.Float))
}

func BI_StrAdd() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_String, "str-add")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_String, "str-add")

	SetResult_String(a.Str + b.Str)
}

func BI_StrLen() {
	s := data_stack[stack_frame+0]

	SetResult_Int(int64(s.Length()))
}

func BI_StrGetElem() {
	str := data_stack[stack_frame+0]
	str.VerifyType(TYP_String, "str-get-elem")

	idx := data_stack[stack_frame+1]
	idx.VerifyType(TYP_Int, "str-get-elem")

	r := []rune(str.Str)

	var val Value

	// produce NUL char if the index is out of bounds
	if idx.Int < 0 || int(idx.Int) >= len(r) {
		val.MakeChar(0)
	} else {
		val.MakeChar(r[idx.Int])
	}

	SetResult(val)
}

//----------------------------------------------------------------------

func BI_ToInt() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "to-int")

	const limit = 9.223372036854775e+18

	if math.IsNaN(v.Float) || math.IsInf(v.Float, 0) ||
		v.Float > limit || v.Float < -limit {
		SetResult(*glob_INAN.loc)
	} else {
		// this is truncation toward zero
		SetResult_Int(int64(v.Float))
	}
}

func BI_ToFloat() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Int, "to-float")

	if v.Int == glob_INAN.loc.Int {
		SetResult(*glob_NAN.loc)
	} else {
		SetResult_Float(float64(v.Int))
	}
}

func BI_Floor() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "floor")

	SetResult_Float(math.Floor(v.Float))
}

func BI_Ceil() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "ceil")

	SetResult_Float(math.Ceil(v.Float))
}

func BI_Round() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "round")

	// we round halfway cases *away* from zero
	SetResult_Float(math.Round(v.Float))
}

func BI_Trunc() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "trunc")

	SetResult_Float(math.Trunc(v.Float))
}

func BI_IsNan() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "nan?")

	SetResult_Bool(math.IsNaN(v.Float))
}

func BI_IsInfinity() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "inf?")

	SetResult_Bool(math.IsInf(v.Float, 0))
}

func BI_Hypot() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "hypot")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "hypot")

	SetResult_Float(math.Hypot(a.Float, b.Float))
}

func BI_Log() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "log")

	SetResult_Float(math.Log(v.Float))
}

func BI_Log10() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "log10")

	SetResult_Float(math.Log10(v.Float))
}

func BI_Frexp() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "frexp")

	frac, exp := math.Frexp(v.Float)

	f_val := Value{}
	f_val.MakeFloat(frac)

	e_val := Value{}
	e_val.MakeInt(int64(exp))

	tup := Value{}
	tup.MakeTuple(frexp_type)

	tup.Array.data[0] = f_val
	tup.Array.data[1] = e_val

	SetResult(tup)
}

func BI_Ldexp() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "ldexp")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "ldexp")

	SetResult_Float(math.Ldexp(a.Float, int(b.Int)))
}

func BI_Sin() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "sin")

	SetResult_Float(math.Sin(v.Float))
}

func BI_Cos() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "cos")

	SetResult_Float(math.Cos(v.Float))
}

func BI_Tan() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "tan")

	SetResult_Float(math.Tan(v.Float))
}

func BI_ArcSin() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "asin")

	SetResult_Float(math.Asin(v.Float))
}

func BI_ArcCos() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "acos")

	SetResult_Float(math.Acos(v.Float))
}

func BI_ArcTan() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Float, "atan")

	SetResult_Float(math.Atan(v.Float))
}

func BI_ArcTan2() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Float, "atan2")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Float, "atan2")

	SetResult_Float(math.Atan2(a.Float, b.Float))
}

//----------------------------------------------------------------------

func BI_BitAnd() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "bit-and")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "bit-and")

	SetResult_Int(a.Int & b.Int)
}

func BI_BitOr() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "bit-or")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "bit-or")

	SetResult_Int(a.Int | b.Int)
}

func BI_BitXor() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Int, "bit-xor")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Int, "bit-xor")

	SetResult_Int(a.Int ^ b.Int)
}

func BI_BitFlip() {
	v := data_stack[stack_frame+0]
	v.VerifyType(TYP_Int, "bit-flip")

	SetResult_Int(^v.Int)
}

func BI_ShiftLeft() {
	num := data_stack[stack_frame+0]
	num.VerifyType(TYP_Int, "shift-left")

	count := data_stack[stack_frame+1]
	count.VerifyType(TYP_Int, "shift-left")

	res := num.Int

	if count.Int > 0 {
		res = res << uint(count.Int)
	}
	if count.Int < 0 {
		res = res >> uint(-count.Int)
	}

	SetResult_Int(res)
}

func BI_ShiftRight() {
	num := data_stack[stack_frame+0]
	num.VerifyType(TYP_Int, "shift-right")

	count := data_stack[stack_frame+1]
	count.VerifyType(TYP_Int, "shift-right")

	res := num.Int

	if count.Int > 0 {
		res = res >> uint(count.Int)
	}
	if count.Int < 0 {
		res = res << uint(-count.Int)
	}

	SetResult_Int(res)
}

//----------------------------------------------------------------------

func BI_ArrayLen() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Array, "array.len")

	SetResult_Int(int64(a.Length()))
}

func BI_ArrayCopy() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Array, "array.copy")

	SetResult(a.CopyArray())
}

func BI_BytevecCopy() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_ByteVec, "byte-vec.copy")

	SetResult(a.CopyByteVec())
}

func BI_ArrayClear() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Array, "array-clear")

	// Wish: optimize this
	for end := a.Length(); end > 0; end-- {
		a.DeleteElem(end - 1)
	}

	SetResult_Void()
}

func BI_ArrayGetElem() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Array, "array-get-elem")

	idx := data_stack[stack_frame+1]
	idx.VerifyType(TYP_Int, "array-get-elem")

	if !arr.ValidIndex(int(idx.Int)) {
		Die("read array element: index out of bounds")
		return
	}

	SetResult(arr.GetElem(int(idx.Int)))
}

func BI_ArraySetElem() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Array, "array-set-elem")

	idx := data_stack[stack_frame+1]
	idx.VerifyType(TYP_Int, "array-set-elem")

	val := data_stack[stack_frame+2]

	if !arr.ValidIndex(int(idx.Int)) {
		Die("write array element: index out of bounds")
		return
	}

	arr.SetElem(int(idx.Int), val)

	SetResult_Void()
}

func BI_BytevecSetElem() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_ByteVec, "byte-vec.set-elem")

	idx := data_stack[stack_frame+1]
	idx.VerifyType(TYP_Int, "byte-vec.set-elem")

	val := data_stack[stack_frame+2]
	val.VerifyType(TYP_Int, "byte-vec.set-elem")

	if !arr.ValidIndex(int(idx.Int)) {
		Die("write array element: index out of bounds")
		return
	}

	var new_val Value
	new_val.MakeInt(val.Int & 255)

	arr.SetElem(int(idx.Int), new_val)

	SetResult_Void()
}

func BI_ArrayConcat() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Array, "array-concat")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Array, "array-concat")

	c := a.CopyArray()

	for i := 0; i < b.Length(); i++ {
		c.AppendElem(b.GetElem(i))
	}

	SetResult(c)
}

func BI_BytevecConcat() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Array, "byte-vec.concat")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Array, "byte-vec.concat")

	c := a.CopyByteVec()

	for i := 0; i < b.Length(); i++ {
		c.AppendElem(b.GetElem(i))
	}

	SetResult(c)
}

func BI_ArrayAppend() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Array, "append!")

	val := data_stack[stack_frame+1]

	arr.AppendElem(val)

	SetResult_Void()
}

func BI_BytevecAppend() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_ByteVec, "append!")

	val := data_stack[stack_frame+1]
	val.VerifyType(TYP_Int, "insert!")

	var new_val Value
	new_val.MakeInt(val.Int & 255)

	arr.AppendElem(new_val)

	SetResult_Void()
}

func BI_ArrayInsert() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Array, "insert!")

	idx := data_stack[stack_frame+1]
	idx.VerifyType(TYP_Int, "insert!")

	val := data_stack[stack_frame+2]

	arr.InsertElem(int(idx.Int), val)

	SetResult_Void()
}

func BI_BytevecInsert() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_ByteVec, "insert!")

	idx := data_stack[stack_frame+1]
	idx.VerifyType(TYP_Int, "insert!")

	val := data_stack[stack_frame+2]
	val.VerifyType(TYP_Int, "insert!")

	var new_val Value
	new_val.MakeInt(val.Int & 255)

	arr.InsertElem(int(idx.Int), new_val)

	SetResult_Void()
}

func BI_ArrayRemove() {
	arr := data_stack[stack_frame+0]
	idx := data_stack[stack_frame+1]

	arr.VerifyType(TYP_Array, "array-remove-elem")
	idx.VerifyType(TYP_Int, "array-remove-elem")

	arr.DeleteElem(int(idx.Int))

	SetResult_Void()
}

func BI_ArrayRemoveSeq() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Array, "remove-seq!")

	length := len(arr.Array.data)

	// determine the start and end indices
	v1 := data_stack[stack_frame+1]
	v1.VerifyType(TYP_Int, "remove-seq!")

	v2 := data_stack[stack_frame+2]
	v2.VerifyType(TYP_Int, "remove-seq!")

	start := int(v1.Int)
	end := int(v2.Int)

	// clamp the start to 0, end to the length
	if start < 0 {
		start = 0
	}
	if end > length {
		end = length
	}

	// TODO: optimize this
	for start < end {
		arr.DeleteElem(end - 1)
		end -= 1
	}

	SetResult_Void()
}

func BI_ArraySubseq() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Array, "array-subseq")

	length := len(arr.Array.data)

	// determine the start and end indices
	v1 := data_stack[stack_frame+1]
	v1.VerifyType(TYP_Int, "array-subseq")

	v2 := data_stack[stack_frame+2]
	v2.VerifyType(TYP_Int, "array-subseq")

	start := int(v1.Int)
	end := int(v2.Int)

	// clamp the start to 0, end to the length
	if start < 0 {
		start = 0
	}
	if end > length {
		end = length
	}

	res := Value{}

	// result will be empty?
	// [ this handles all weird cases, e.g. start >= length ]
	if start >= end {
		res.MakeArray(0, arr.ty)
	} else {
		// produce a shallow copy of the given range
		res.MakeArray(end-start, arr.ty)

		copy(res.Array.data, arr.Array.data[start:end])
	}

	SetResult(res)
}

func BI_BytevecSubseq() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_ByteVec, "byte-vec.subseq")

	length := len(arr.Array.data)

	// determine the start and end indices
	v1 := data_stack[stack_frame+1]
	v1.VerifyType(TYP_Int, "byte-vec.subseq")

	v2 := data_stack[stack_frame+2]
	v2.VerifyType(TYP_Int, "byte-vec.subseq")

	start := int(v1.Int)
	end := int(v2.Int)

	// clamp the start to 0, end to the length
	if start < 0 {
		start = 0
	}
	if end > length {
		end = length
	}

	res := Value{}

	// result will be empty?
	// [ this handles all weird cases, e.g. start >= length ]
	if start >= end {
		res.MakeByteVec(0)
	} else {
		// produce a shallow copy of the given range
		res.MakeByteVec(end-start)

		copy(res.Array.data, arr.Array.data[start:end])
	}

	SetResult(res)
}

//----------------------------------------------------------------------

func BI_MapLen() {
	m := data_stack[stack_frame+0]
	m.VerifyType(TYP_Map, "map-len")

	SetResult_Int(int64(m.Length()))
}

func BI_MapCopy() {
	m := data_stack[stack_frame+0]
	m.VerifyType(TYP_Map, "map-copy")

	SetResult(m.CopyMap())
}

func BI_MapClear() {
	m := data_stack[stack_frame+0]
	m.VerifyType(TYP_Map, "map-clear!")

	m.ClearMap()

	SetResult_Void()
}

func BI_MapCollectKeys() {
	m := data_stack[stack_frame+0]
	m.VerifyType(TYP_Map, "map.collect-keys")

	SetResult(m.CollectKeys())
}

func BI_MapGetElem() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Map, "map-get-elem")

	key_type := arr.ty.KeyType()

	key := data_stack[stack_frame+1]
	key.VerifyKeyType(key_type, "map-get-elem")

	access_ty := arr.ty.MapAccessType()

	var val Value

	if arr.KeyExists(key) {
		val = arr.KeyGet(key)
		val.Wrap(access_ty)
	} else {
		val.MakeNone(access_ty)
	}

	SetResult(val)
}

func BI_MapSetElem() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Map, "map-set-elem")

	key_type := arr.ty.KeyType()

	key := data_stack[stack_frame+1]
	key.VerifyKeyType(key_type, "map-set-elem")

	val := data_stack[stack_frame+2]

	arr.KeySet(key, val)

	SetResult_Void()
}

func BI_MapRemoveElem() {
	m := data_stack[stack_frame+0]
	key := data_stack[stack_frame+1]

	m.VerifyType(TYP_Map, "map-remove-elem")

	m.KeyDelete(key)

	SetResult_Void()
}

func BI_MapMerge() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Map, "map-merge")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Map, "map-merge")

	c := a.CopyMap()
	c.JoinMap(&b)

	SetResult(c)
}


//----------------------------------------------------------------------

func BI_SetLen() {
	m := data_stack[stack_frame+0]
	m.VerifyType(TYP_Set, "set-len")

	SetResult_Int(int64(m.Length()))
}

func BI_SetCopy() {
	m := data_stack[stack_frame+0]
	m.VerifyType(TYP_Set, "set-copy")

	SetResult(m.CopyMap())
}

func BI_SetClear() {
	m := data_stack[stack_frame+0]
	m.VerifyType(TYP_Set, "set-clear!")

	m.ClearMap()

	SetResult_Void()
}

func BI_SetCollectKeys() {
	m := data_stack[stack_frame+0]
	m.VerifyType(TYP_Set, "set.collect-keys")

	SetResult(m.CollectKeys())
}

func BI_SetGetElem() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Set, "set-get-elem")

	key_type := arr.ty.sub

	key := data_stack[stack_frame+1]
	key.VerifyKeyType(key_type, "set-set-elem")

	SetResult_Bool(arr.KeyExists(key))
}

func BI_SetSetElem() {
	arr := data_stack[stack_frame+0]
	arr.VerifyType(TYP_Set, "set-set-elem")

	key_type := arr.ty.sub

	key := data_stack[stack_frame+1]
	key.VerifyKeyType(key_type, "set-set-elem")

	val := data_stack[stack_frame+2]
	val.VerifyType(BTYP_Bool, "set-set-elem")

	arr.KeySet(key, val)

	SetResult_Void()
}

func BI_SetRemoveElem() {
	arr := data_stack[stack_frame+0]
	key := data_stack[stack_frame+1]

	arr.VerifyType(TYP_Set, "set-remove-elem")

	arr.KeyDelete(key)

	SetResult_Void()
}

func BI_SetUnion() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Set, "set-union")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Set, "set-union")

	c := a.CopyMap()
	c.JoinMap(&b)

	SetResult(c)
}

func BI_SetDifference() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Set, "set-difference")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Set, "set-difference")

	c := a.CopyMap()

	var F Value
	F.MakeBool(false)

	keys := b.CollectKeys()
	for i := 0; i < keys.Length(); i++ {
		c.KeySet(keys.GetElem(i), F)
	}

	SetResult(c)
}

func BI_SetIntersect() {
	a := data_stack[stack_frame+0]
	a.VerifyType(TYP_Set, "set-difference")

	b := data_stack[stack_frame+1]
	b.VerifyType(TYP_Set, "set-difference")

	var c Value
	c.MakeMap(a.ty)

	var T Value
	T.MakeBool(true)

	keys := a.CollectKeys()
	for i := 0; i < keys.Length(); i++ {
		if b.KeyExists(keys.GetElem(i)) {
			c.KeySet(keys.GetElem(i), T)
		}
	}

	SetResult(c)
}

func BI_ObjCopy() {
	a := data_stack[stack_frame+0]

	SetResult(a.CopyObject())
}

//----------------------------------------------------------------------

func BI_RandSeed() {
	seed := data_stack[stack_frame+0]
	seed.VerifyType(TYP_Int, "rand-seed")

	rand.Seed(seed.Int)

	SetResult_Void()
}

func BI_RandInt() {
	// result will satisfy: low <= x <= high

	low := data_stack[stack_frame+0]
	low.VerifyType(TYP_Int, "rand-int")

	high := data_stack[stack_frame+1]
	high.VerifyType(TYP_Int, "rand-int")

	if high.Int <= low.Int {
		SetResult_Int(low.Int)
		return
	}

	var val int64

	// check whether (high - low + 1) is too large for int64
	diff := high.Int/4 - low.Int/4 + 4

	if diff > (math.MaxInt64/4) {
		// this is crude, but at least it is 100% correct
		for {
			val = int64(rand.Uint64())

			if low.Int <= val && val <= high.Int {
				SetResult_Int(val)
				return
			}
		}
	}

	val = low.Int + rand.Int63n(high.Int - low.Int + 1)

	SetResult_Int(val)
}

func BI_RandFloat() {
	// result will satisfy: 0.0 <= x < 1.0
	SetResult_Float(rand.Float64())
}

//----------------------------------------------------------------------

func BI_ExitProgram() {
	// this exits the REPL too!

	status := data_stack[stack_frame+0]
	status.VerifyType(TYP_Int, "exit primitive")

	if editor != nil {
		editor.Close()
	}

	os.Exit(int(status.Int))
}

func BI_AbortProgram() {
	msg := data_stack[stack_frame+0]
	msg.VerifyType(TYP_String, "abort primitive")

	Die("program aborted: %s", msg.Str)
}

func BI_Print() {
	msg := data_stack[stack_frame+0]
	msg.VerifyType(TYP_String, "print primitive")

	Print("%s\n", msg.Str)

	SetResult_Void()
}

func BI_InputLine() {
	prompt := data_stack[stack_frame+0]
	prompt.VerifyType(TYP_String, "input primitive")

	var s string
	var err error

	if editor == nil {
		EnableEditor()
	}

	// use the line editor
	s, err = editor.Prompt(prompt.Str)

	if err == io.EOF {
		// NOTE: displaying "EOF" does not makes sense if Stdin is a pipe
		SetResult(*glob_EOF.loc)
		return
	}

	if err == liner.ErrPromptAborted {
		Die("aborted\n")
		return
	}

	// assume other errors are unrecoverable
	if err != nil {
		Die("%s", err.Error())
		return
	}

	// remember history (line editor only)
	if editor != nil {
		editor.AppendHistory(s)
	}

	SetResult_String(s)
}

func InputFromStdin(prompt string) (string, error) {
	// read directly from Stdin
	var sb strings.Builder

	Print("%s", prompt)

	// with C, we would need a fflush(stdout) here, but with Go
	// I think it is unnecessary (since there is no flush-like
	// method on File objects to call).

	for {
		var buf [1]byte
		var n int
		var err error

		n, err = os.Stdin.Read(buf[:])

		if n == 0 {
			if err == nil {
				// this should never happen
				err = fmt.Errorf("bad read on Stdin")
				return "", err
			}

			// allow unterminated line before EOF
			if err == io.EOF && sb.Len() > 0 {
				err = nil
				break
			}

			return "", err
		}

		if buf[0] == '\r' || buf[0] == 0 {
			// skip CRs and NULs
			continue
		}

		if buf[0] == '\n' {
			// we have reached end-of-line
			err = nil
			break
		}

		sb.Write(buf[:])
	}

	return sb.String(), nil
}
