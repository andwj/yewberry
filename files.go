// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "os"
// import "fmt"
import "bytes"
import "strings"
import "path/filepath"

type lexProvider interface {
	Open() (*Lexer, error)
	Close()
}

type CodeSource struct {
	sources []lexProvider
	current lexProvider
	along   int // next one to open, EOF when >= len(sources)
}

func NewCodeSource() *CodeSource {
	var cs CodeSource
	cs.sources = make([]lexProvider, 0)
	cs.along = 0
	return &cs
}

func (cs *CodeSource) Add(src lexProvider) {
	cs.sources = append(cs.sources, src)
}

func (cs *CodeSource) OpenNext() (*Lexer, error) {
	if cs.current != nil {
		cs.current.Close()
		cs.current = nil
		cs.along += 1
	}

	if cs.along >= len(cs.sources) {
		// that's all folks!
		return nil, Ok
	}

	cs.current = cs.sources[cs.along]

	lex, err := cs.current.Open()
	if err != nil {
		cs.current = nil
	}
	return lex, err
}

func (cs *CodeSource) Finish() {
	if cs.current != nil {
		cs.current.Close()
	}
	// move position to EOF
	cs.along = len(cs.sources)
}

//----------------------------------------------------------------------

type stringSource struct {
	code string
}

func (ss *stringSource) Open() (*Lexer, error) {
	ErrorSetModule("")

	lex := NewLexer(strings.NewReader(ss.code))
	return lex, Ok
}

func (ss *stringSource) Close() {
	// nothing needed
}

func NewStringSource(code string) lexProvider {
	ss := new(stringSource)
	ss.code = code
	return ss
}

//----------------------------------------------------------------------

type fileSource struct {
	filename string
	handle   *os.File
}

func (fs *fileSource) Open() (*Lexer, error) {
	var err error

	fs.handle, err = os.Open(fs.filename)
	if err != nil {
		return nil, err
	}

	ErrorSetModule("")
	ErrorSetFile(fs.filename)

	lex := NewLexer(fs.handle)

	// using the sweet syntax?
	if strings.ToLower(filepath.Ext(fs.filename)) == ".ys" {
		lex.sweet = true
	}

	return lex, Ok
}

func (fs *fileSource) Close() {
	fs.handle.Close()
	fs.handle = nil
}

func NewFileSource(filename string) lexProvider {
	fs := new(fileSource)
	fs.filename = filename
	return fs
}

//----------------------------------------------------------------------

type assetSource struct {
	filename string
}

func (a *assetSource) Open() (*Lexer, error) {
	data, err := Asset(a.filename)
	if err != nil {
		return nil, err
	}

	ErrorSetModule("")
	ErrorSetFile(a.filename)

	lex := NewLexer(bytes.NewReader(data))
	return lex, Ok
}

func (a *assetSource) Close() {
	// nothing needed
}

func NewAssetSource(filename string) lexProvider {
	a := new(assetSource)
	a.filename = filename
	return a
}
