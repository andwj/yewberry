
Yewberry Overview
=================

Basic syntax
------------

Yewberry code is like Scheme and Lisp, using S-expressions
between `(` and `)` parentheses.  There are also math-like
expressions with infix and unary operators.

Comments are introduced by the `;` character and extend to the
end of the line.  There are no block comments, and no system
for quoting code.

Identifiers can contain not only alpha-numeric characters, but
also various symbols and punctuation marks.  Hence something
like `foo%7` which is considered to be three tokens in languages
like C, C++ and Go, is just a single identifier in Yewberry.


Types
-----

The fundamental types are:

- Void : represents the lack of any value
- Int : 64-bit signed integers
- Float : 64-bit floating point numbers
- String : UTF-8 encoded strings
- Boolean : is either FALSE or TRUE
- Char : a unicode code point

- Array : growable zero-indexed arrays
- Map : associative dictionaries
- Set : a map where elements are either in or out
- Union : enumeration of keywords + associated value
- Enum : a dataless union -- just a set of keywords
- Tuple : an immutable group of elements
- Class : a mutable group of elements
- Function : anonymous function with N parameters

Ints, floats, strings, booleans, chars, unions, enums and tuples
are value types.  Their components (if any) are immutable -- they
may be read but cannot be modified in isolation.

Arrays, maps, sets, classes and functions are reference types,
their value is really a pointer to the object in memory.
Apart from functions, the elements of these types are mutable.

In addition to these, custom types can be defined.  See the
section below for more information.


Type specifiers
---------------

Type specifiers are used to specify the types of function
parameters and the return type of a function.  The return
type places the specifier after a `->` (at the end of the
parameters), but this is optional and defaults to `void` if
absent.  Each parameter must have a type spec directly after
its name.

For simple types, the type specifier is just a word, like
`int` or `str`, but for complex types the specifier occurs
within `(` and `)` parentheses.

Specifier syntax for the simple types:
```
void
int
float
str
bool
char
```

Specifier syntax for the compound types:
```
(tuple TYPE TYPE...)
(union TAGNAME TYPE TAGNAME TYPE...)
(enum TAGNAME1 TAGNAME2...)

(array TYPE)
(map TYPE TYPE)
(set TYPE)

(class .field TYPE .field TYPE...)
(function TYPE TYPE... RETURN-TYPE)
```


Primary forms
-------------

In the following list, elements between `[` and `]` are optional,
and elements between `{` and `}` can be repeated zero or more
times.  `BLOCK` is a sequence of one or more expressions or let
forms.  Let forms can be used at top level to define global
variables, otherwise they must occur in a BLOCK and define a
local variable whose scope is the rest of the block.

```
(let name value)
(let-mut name value)
(fun name "(" { param TYPE } [ "->" TYPE ] ")" BLOCK)
(lam "(" { param TYPE } [ "->" TYPE ] ")" BLOCK)
(if cond BLOCK { "elif" cond BLOCK } [ "else" BLOCK ] )
(match exp { (PATTERN => BLOCK) } )
(begin { expr })
(set! var value)
(set! "[" obj { indexor } "]" value)
```


Loop forms
----------

In the following, `I`, `K` and `V` represent user-supplied
variable names which take each index, key or value.  None
of these forms produce a final result (it is always void).
Breaking out of a loop can be done using the `skip` form.

```
(loop BLOCK)
(while cond BLOCK)
(for I start end BLOCK)
(for-step I start end step BLOCK)
(for-each K V container BLOCK)
```


Flow control
------------

The `skip` form allows jumping to a future point in the current
function, essentially skipping the execution of code in-between.
The target is specified with a `junction` form, which can only
be used as the final element of a block.  Junctions have a label
which is just an identifier beginning with a back-quote.
By convention label names are fully uppercase.  The `skip` form
specifies which label to find, and the corresponding junction
MUST exist either at the end of the block containing the `skip`
form, or at the end of a parent block.

The skip form and the corresponding junction may also contain
an expression.  An absent expression is equivalent to void.  The
type of the expression in a skip form must match the one in the
junction.  If control naturally reaches the junction form, its
expression is evaluated and that is the result of the block.
When a skip form is reached, its value is evaluated, control
moves to the end of the block with the matching junction, and
the result of that block is the skip value.

If a junction is in tail-call position, then its expression is
also in tail-call position, moreover any skip form which targets
that junction also has its expression in tail-call position.

```
(skip `LABEL [ value ])
(junction `LABEL [ value ])
```


Math expressions
----------------

An expression within `(` and `)` may be a math expression,
which is detected when it contains at least one operator.
Math expressions consist of terms, like numbers or normal
function calls, and operators, either unary or binary (infix)
operators.  The precedence of binary operators determines
the order of evaluation, as you would expect.

For example:
```
(1 + 2 * 3)  ; result is 7
```

Such expressions are handled similar to (and earlier than)
macros, and get expanded to method calls.  Each operator is
mapped to a particular method.  The above example will expand
to the following:
```
(.add 1 (.mul 2 3))
```

Using methods allows user-defined types to be used in math
expressions too, e.g. you could define the `.add` method for
a custom "List" type.

See the Numbers section below for a full list of operators.
Most math operators also have a built-in function which can
be used instead, such as `int-lt?` for the `<` operator on
integers, which can be useful as predicates.


Built-in constants
------------------

```
FALSE  ; negative value for bool type
TRUE   ; positive value for bool type

NUL    ; the zero character
EOF    ; "\x1A", returned from `input-line` builtin

+INF   ; positive infinity for floats
-INF   ; negative infinity for floats
NAN    ; NaN (Not a Number) for floats

INAN    ; Integer Not a Number, for division by zero
MAX_INT ; maximum integer value
MIN_INT ; minimum integer value (excluding INAN)

E      ; base of the natural logarithm
PI     ; pi
PHI    ; the golden ratio
SQRT2  ; the square-root of 2
```


Arrays
------

Arrays can be constructed using the syntax:
```
{array elem1 elem2 }      ; an array with implicit element type
{ARRAY-TYPE elems... }  ; an array with an explicit overall type
```

Arrays can be read and written using syntax within plain `[]`
square brackets.  Multiple dimensions are supported by using
two or more index values.  Out-of-bound indices will raise an
error.
```
(let A {array 1 2 3})
(print ($ [A 2]))  ; displays "3"
(set! [A 1] 22)    ; modifies A to [1 22 3]
```

The following builtins for arrays are available:
```
(len A)
(empty? A)
(non-empty? A)
(copy A)
(subseq A start end)
(ref-eq? A1 A2)

(append! A value)
(insert! A index value)
(remove! A index)
(remove-seq! A start end)
(clear! A)

(find A predicate)
(filter! A predicate)
(sort! A predicate)
```

The following operators work on arrays:
```
(A1 + A2)
```


Maps
----

Maps are an unordered group of key/value pairs which provide an
ability to quickly lookup keys to get the associated value.
The keys of a map are generally value types like ints, floats,
strings, tuples and enums.  However reference types are also
supported as keys, but only their reference is used -- their
actual contents has no effect on lookup.

The element type of a map can be anything except void.

Maps can be constructed with the following syntax:
```
{map key1 value1 key2 value2 }  ; a map with implicit key/value types
{MAP-TYPE key1 value1 }         ; a map with an explicit overall type
```

Maps are read and written using the same syntax as arrays
(using plain square brackets).  The result of a read operation
is the element type wrapped in the "Opt" (optional) type, and
produces the value `NONE` when the key is not in the map.
It requires a match, unwrap or flatten expression to access
the actual value (if any).
```
(let M {map "name" "Jane" "loc" "Sydney"})
(print [M "name" unwrap])   ; displays: Jane
(val? [M "sister"])         ; checks if "sister" key exists
(set! [M "loc"] "Paris")    ; modifies the "loc" key
```

The following builtins for maps are available:
```
(len M)        ; total number of key/value pairs
(empty? M)
(non-empty? M)
(copy M)
(clear! M)
(remove! M key)
(collect-keys M)
(ref-eq? M1 M2)
```

The following operators work on maps:
```
(M1 + M2)      ; merge two maps into a new one
               ; keys of the second map take priority
```


Sets
----

Sets are similar to maps, however sets only allow a binary
choice for each key: in the set, or out of it.  Accessing a
set with `[]` produces a boolean value, and setting a key
to `FALSE` is equivalent to removing that key from the set.

Sets are potentially implemented in a way which uses a lot
less memory than normal maps, especially for small positive
integers.  Sets also has a nicer literal syntax, since only
the keys in the set need to be specified.

For example:
```
{set "gold" "silver" "lead"}  ; a set with implicit key type
{SET-TYPE 2 3 5 7 11 13}      ; a set with an explicit overall type
```

The following builtins for sets are available:
```
(len S)      ; total number of keys in the set
(empty? S)
(non-empty? S)
(copy S)
(clear! S)
(collect-keys S)
(ref-eq? S1 S2)
```

The following operators work on sets:
```
(S1 + S2)    ; creates a set as union of S1 and S2
(S1 * S2)    ; creates a set as intersection of S1 and S2
(S1 - S2)    ; creates a set as S1 without elements in S2
```


Tuples
------

A tuple is a group of elements, whose size and type of elements
are fixed.  For example, one tuple could be two integers, another
could be three strings and an array.  Tuples are never empty.

Tuples have value semantics, hence the elements of a tuple cannot
be modified using the `set!` form.  Tuples only have a default
value if all the elements have a default value.

Tuples can be constructed with the following syntax.  The `...`
keyword can be used to indicate that all remaining fields should
be set to their default value.
```
{ value1 value2 }
{ .field value1 .field value2 }
{TUPLE-TYPE value1 value2 }
{TUPLE-TYPE .field value1 .field value2 }
```

Tuples are read using a syntax similar to arrays, but the access
key must be a number or identifier starting with a period, and
cannot be a variable.  For example:
```
(let T {"Fred" 35 "London"})
(print [T .0])   ; displays: Fred
(print [T .2])   ; displays: London
```

Tuple types may define identifiers for accessing fields.
Unlike classes, these field names are optional, but either all
fields are given a name or none are.  When creating tuples for
a custom type, fields must always occur in the same order as
defined in the type, even when using field names.


Classes
-------

Class types are used quite similarly to tuples, but there are
several major differences.  Firstly, all fields of a class
must be given a field name, and constructors which use field
names allow the names in any order.  Secondly, class objects are
mutable, any field can be modified.  Thirdly, class objects are
reference types.  Lastly, class objects support interfaces.

Like tuples, the `...` keyword at the end of a constructor in
`{}` curly brackets can be used to indicate that all remaining
fields should be set to their default value.

Example:
```
(type Person (class .name str .age int))
(let p {Person .name "Fred" .age 35})
(print [p .name])
(set! [p .age] 44)
```

The following builtins for classes are available:
```
(copy OB)
(class$ OB)
(ref-eq? OB1 OB2)
```


Unions and Enums
----------------

A union type is a set of keywords, where only a single keyword
is the current value at any one time.  Each keyword in the union
has an associated data value, which may be `void` for the lack
of a value.  These keywords are called "tags", they must begin
with a backquote, and the coding convention is to use uppercase
letters in them.

An enum type is like a union type where every datum is void,
hence an enum type definition only needs to list the tag names.
Enums are also represented in memory in a more efficient way.

Unions and enums have value semantics, the datum of a union value
cannot be modified by the `set!` form.  A union only has a default
value when one of its fields is void -- this must exist in order to
use the union type as a map element type.  The default value of an
enum is simply its first tag.

Example type definition:
```
(type Shape (union
   `BLANK void
   `CIRCLE (tuple .r float)
   `RECTANGLE (tuple .w float .h float)
))
```

Examples of constructing a value:
```
{Shape `BLANK}
{Shape `RECTANGLE {300.0 160.0}}
```

Checking the current tag can be done with the `is?` form,
or a `match` expression.  Match expressions are required for
reading the data value of a particular tag.
```
(let S {Shape `CIRCLE {50.0}})
(is? S `CIRCLE)
(match S (`CIRCLE {r} => (print ($ "circle radius is" r)) ))
```

The following builtins for unions and enums are available:
```
(tag$ U)
(tag-int U)
```


Characters
----------

Characters are integer values which represent Unicode code
points.  The builtin `char` type is used for this purpose.
They can be used to construct or examine strings.

The following six builtins are for testing the general class of
a character.  The term "mark" refers to combining characters,
ones used to modify a previous character to add a small shape.
Note that there are some characters which do not appear in any
of these classes.
```
(.letter? C)
(.digit? C)
(.symbol? C)
(.mark? C)
(.space? C)
(.control? C)
```

A few other builtins are available for characters:
```
(to-lower C)
(to-upper C)

(lowercase? C)
(uppercase? C)
```


Strings
-------

Strings are read-only sequences of Unicode characters.
Strings can be accessed as if they were arrays, producing
a `char` value for the character.

Strings can be constructed from characters or other strings
using the builder syntax.  This can be useful for converting
a single character to a string, or for splitting a very long
string across multiple lines.
```
{str 'x' 'y' 'z'}      ; result is "xyz"
{str 65 66 67}         ; result is "ABC"
{str "Hello" "World"}  ; result is "HelloWorld"
```

The `fmt$` form is used to convert simple values into strings
with control over the formatting (see Formatted Strings section
below), and the `$` form is an even simpler mechanism which
concatenates all of its arguments using a single space as the
separator.  For example:
```
($ "Sum is:" (1 + 2))   ; result is "Sum is: 3"
```

Strings can be compared using the comparison operators
(see the Numbers section below), and can be joined using
the `+` operator.

The following builtins are available for strings:
```
(len S)
(empty? S)
(non-empty? S)
(subseq S start end)

(min S1 S2 ...)
(max S1 S2 ...)

(to-lower S)
(to-upper S)

(parse-int S)    ; try to convert S to an integer
(parse-float S)  ; try to convert S to a float
```


Numbers
-------

There are two types of numbers: 64-bit signed integers
and 64-bit floating point.  Conversion between the two
types is not automatic, a builtin function must be used.

The basic math operations are:
```
(L + R)   ; (int-add L R)  ; (float-add L R)  ; (str-add L R)
(L - R)   ; (int-sub L R)  ; (float-sub L R)
(L * R)   ; (int-mul L R)  ; (float-mul L R)
(L / R)   ; (int-div L R)  ; (float-div L R)
(L % R)   ; (int-rem L R)  ; (float-rem L R)
          ; (int-mod L R)  ; (float-mod L R)
(L ** R)  ; (int-pow L R)  ; (float-pow L R)

(abs N)
(min N1 N2 ...)
(max N1 N2 ...)
```

Conversions:
```
(int F)
(float I)
```

Bit-wise operations:
```
(L & R)   ; (bit-and L R)
(L | R)   ; (bit-or  L R)
(L ^ R)   ; (bit-xor L R)
(~ N)     ; (bit-not N)

(N << shift)  ; (shift-left  N shift)
(N >> shift)  ; (shift-right N shift)
```

Comparison operations:
```
(L == R)   ; (int-eq? L R)  ; (float-eq? L R)  ; (str-eq? L R)  ; (bool-eq? L R)
(L != R)   ; (int-ne? L R)  ; (float-ne? L R)  ; (str-ne? L R)  ; (bool-ne? L R)
(L <  R)   ; (int-lt? L R)  ; (float-lt? L R)  ; (str-lt? L R)
(L <= R)   ; (int-le? L R)  ; (float-le? L R)  ; (str-le? L R)
(L >  R)   ; (int-gt? L R)  ; (float-gt? L R)  ; (str-gt? L R)
(L >= R)   ; (int-ge? L R)  ; (float-ge? L R)  ; (str-ge? L R)
```

Boolean logic:
```
(L && R)   ; (and L R ...)   ; Note: short-circuiting
(L || R)   ; (or  L R ...)   ; Note: short-circuiting
(! B)      ; (not B)
```

Rounding:
```
(floor F)
(ceil F)
(round F)
(trunc F)
```

Transcendental functions:
```
(log F)
(log10 F)
(exp F)
(exp F)

(sin F)
(cos F)
(tan F)
(asin F)
(acos F)
(atan F)
(atan2 Y X)
```

Miscellaneous:
```
(sqrt F)
(hypot X Y)

(frexp F)           ; result is a tuple: {frac exp}
(ldexp frac exp)    ; exp here is an integer exponent

(nan? F)   ; TRUE if float F is any kind of NaN
(inf? F)   ; TRUE if float F is an infinity

(to-radians DEG)
(to-degrees RAD)
```


Byte vectors
------------

The `byte-vec` type is a special type of array whose elements
are restricted to be 8-bit unsigned values ("bytes").  Reading
an element always returns an integer in the range 0-255, and
writing an element will only keep the lowest 8 bits.  This type
is much more memory efficient than an `(array int)`, and is
particularly useful for file I/O tasks.


Pattern matching
----------------

The `match` form allows matching a value against a series of
patterns, as well as deconstructing compound data types.  Each
rule in the match is tried in turn, and the one which matches
will execute its body.  For union and enum matches, the compiler
issues an error if a tag was not handled.  If the bodies have a
result, then a catch-all rule is generally required at the end.

Simple example:
```
(let a 3)

(match a
   (1 => "one")
   (2 => "two")
   (3 => "three")
   (_ => "UNKNOWN")
)
```

The above example also shows how a plain underscore `_` can be
used to match anything.  It is often used in the last rule of
the match to catch anything not handled by previous rules.

When the expression is a simple type, then multiple values of
that type may be given in a match rule.  The match succeeds if
any of the values is equal to the expression.  The syntax
`(any 1 2 3)` can be used to check multiple values of an
element of a data structure.
```
(match b (1 2 3 => TRUE) (_ => FALSE))

(match A ({3 (any 4 5 6)} => TRUE))
```

Identifiers in a pattern are either global constants, such as
TRUE, PI or any user-defined constant, or are new variables to
be created and bound to the corresponding element's value.
However there are a few exceptions to that general rule:
-  `_` matches anything and does not create a binding
-  for enum types, an identifier can be a tag name

When the expression is a compound type, the pattern can be
used to perform a partial match.  It may also bind new vars
to elements, a process called "destructuring", and these vars
can be used in the body of a match rule and where clauses.

Array examples:
```
; match against a fixed-size array
(match A ({2 3 4} => "yes") (_ => "no"))

; match against beginning elements of an array
(match A ({2 3 ...} => "yes") (_ => "no"))

; match against final elements of an array
(match A ({... 4 5} => "yes") (_ => "no"))

; bind a variable x from first element of an array
(match A ({x ...} => (x + 1)) (_ => -1))
```

Matches against a map or set literal require the specified keys
to be present, and these can be given in any order.  Like arrays,
the length must also match unless "..." is used to indicate an
open pattern.

Matches against tuples and classes may use field names, or may not.
All fields must be present unless "..." is used to signify that
there are unspecified fields whose value we don't care about.

```
; match against a map
(match M ({"foo" 3 "bar" x} => (x * 2)) (_ => -1))

; match against a set
(match S ({ 1 2 4 } => "124") (_ => "bad"))

; match against a tuple, all elements
(match T ({ "foo" _ 123 } => "yes") (_ => "no"))

; match the first element of a tuple
(match T ({ "foo" ... } => "yes") (_ => "no"))

; match some fields of a class object
(match obj ({ .name "Fred" .age x ... } => x) (_ => -1))
```

For union expressions, a match is either just the tag name
(and datum is ignored), or against both tag and datum.  When
both tag and datum is given, they must be surrounded by `{ }`
curly brackets, except a top-level match can omit brackets.
For the top-level expression, the compiler will produce an
error unless every union tag is handled by one of the rules.

Enum examples:
```
; test a single tag
(match U (`FOO => "yes") (_ => "no"))

; test for multiple tags
(match U (`FOO `BAR `MIP => "yes") (_ => "no"))

; test both tag and datum
(match U (`FOO 123 => "yes") (_ => "no"))

; test an union within an array
(match A ( { {`FOO 456} ... } => "yes") (_ => "no"))
```

Match rules can further restrict what is matched via a `where`
clause.  This is an expression which must evaluate to TRUE
for the match to be accepted.  Naturally it can use any vars
which were bound in the pattern.

Example of a where clause:
```
(match A ({x ...} where (1 <= x && x <= 9) => "ok"))
```


Custom types
------------

Custom types can be defined using the `type` form, which takes
the name of the type followed by the type specifier.  If the
new type is a class or tuple, then the type specifier can also
include field names for each element (compulsory for classes).
Field names must begin with a period, followed by a non-digit,
and can be used to access the elements.

The coding convention is for custom type names to begin with
an uppercase letter (i.e. they are CamelCase), and for field
names to begin with a lowercase letter after the dot.

Some examples:
```
(type Duration int)
(type StrList (array str))
(type Person (class .name str .age int))
```

Custom types are generally compatible with their base type, but
are considered distinct from any other custom type.  For example,
a `Duration` type and `Width` type cannot be added or multiplied,
and one cannot be passed to a function when the other is wanted
(except via an explicit cast).

The way to create values of a custom type is via the `{}` curly
bracket constructor syntax, where the first element is the type
name and the second element is the value.  Some examples:
```
{Duration 60}
{StrList "a" "b" "c"}
{Person .name "Jane" .age 36}
```

Values of a custom type can be converted to their base type by
using a type cast.  For example:
```
(cast {Duration 10})   ; result has type "int"
```

Literals (numbers, strings, etc) will be treated as "untyped" in
certain situations, i.e. automatically casted to the required
type.  Some common places where this occurs:
- a parameter in a function call
- the value returned from a function
- the value in an assignment
- a constructor with explicit type


Methods
-------

Methods are functions which can be defined on any custom type.
The definition syntax is similar to normal functions but with
two important differences: the first parameter must be called
`self`, and the name of the method must begin with a `.` period
which is followed by a non-digit.  The `self` parameter is the
*receiver* of the method, and it must have a custom type.

Calling a method is similar to calling a function: the first
element in a parenthesis expression is the method name, the
second element is the receiver (a value of the custom type),
and then the remaining parameters of the method.

An example:
```
(fun .young? (self Person -> bool)
  ([self .age] < 13)
)

(if (.young? p)
  (print "this person is young.")
)
```


Formatted strings
-----------------

The special `fmt$` form is used to convert simple values like
integers and floats into strings, and control their formatting.
How it works is similar to the `printf` family of functions in
the C language, and the `fmt` package in Go.

The form takes pairs of values, where the first value must be
a literal string with a SINGLE format directive in it, and the
second value is an arbitrary expression with a type compatible
with the format directive.  The compiler will guarantee type
safety, which is why the format string must be a literal.
The final element of the `fmt$` form can be another string
which does NOT contain any format directive.  Unlike the `$`
form, no spaces are inserted between the elements.

Format directives begin with a percent sign ('%') character.
If the next character is also '%', then it is just an escape
for the percent sign and not a true directive.  After the '%'
there must be a "verb", a single alphabetic character that
describes what kind of conversion to perform.  There can be
additional characters between the '%' and the verb which
control the conversion more precisely.

List of verbs, with required type and meaning:
```
   %d  int    format as base 10 (decimal) integer
   %b  int    format as base 2 (binary) integer
   %o  int    format as base 8 (octal) integer
   %x  int    format as base 16 (hexadecimal) integer
   %X  int    like %x but use uppercase letters

   %f  float  format as normal floating point
   %e  float  format as scientific notation
   %c  char   format as a (raw) character
   %s  str    format as a (raw) string
   %t  union  format as tag name of the union or enum
   %p  ref    format as a hexadecimal pointer
```

The following special values are shown as a word instead of
a number: +INF, -INF, NAN, INAN.

Each format directive may have a field width (series of digits)
and/or a precision (digits following a dot '.').  If the field
width begins with a `0` zero digit, it means to pad with digits
instead of spaces.  There may also be some characters which acts
as flags, enabling a particular behavior.  The following flags
are available:

```
   +    force a sign before all numbers, even zero
   -    have a space before numbers >= 0, minus sign otherwise
   <    left justify in the field width
   >    right justify in the field width (is the default)

   '    enclose chars/strings in quotes and escape unprintables.
        for unions/enums, include the backquote.
        for non-base-10 numbers, include a prefix like "0x" (etc)
```


Interfaces
----------

An interface is a group of methods given a particular name.
A definition can list the methods explicitly, or embed (copy)
the methods from an existing interface, or do both.  The same
method may appear more than once provided its signature is
exactly the same.  The `self` parameter of interface methods
cannot have a real type, hence an `_` underscore is used.

By convention, interface names begin with `I-`.

A parameter which is an interface is similar to a generic
parameter, since the exact type of the value will be unknown
to the function.  The only thing that function can do is pass
the value around or call the interface methods.  The real
type is limited to class objects, and any class which contains
the full set of methods of an interface is said to implement
that interface (naturally it can implement others too).
This is called "duck typing".

An interface type can be used in most places where a custom
class type can be used, such as elements of tuples, arrays,
maps (etc), and the parameters and result of functions.

Example of defining interfaces:
```
(interface I-Read
   (fun .read (self _ b byte-vec len int -> error)
)
(interface I-Write
   (fun .write (self _ b byte-vec len int -> error)
)
(interface I-ReadWrite
   (embed I-Read I-Write)
)
```


Generics
--------

Yewberry has a limited form of generics which I call "black
box generics".  This means that code which has a generic
parameter (etc) has no idea what the real type is, within
that code the type is a complete mystery.  Naturally this
severely limits what you can do with such a value, you cannot
even compare two of them for equality!  All you can do is
pass it around to other code or data structures.

This kind of black box is represented as an identifier which
begins with `@` (the at symbol).  It can be used in many
places where a normal type name is expected, such as the
parameters or result of a function or method.  A more useful
case is when arrays, maps, tuples (etc) contain a generic
type -- these are called "generic-ish", and allow operations
like read/write/insert/remove elements and querying the
size of the container.

A basic example:
```
(fun double-up (v @T -> (tuple @T @T)) {v v})

(double-up 3)    ; produces a tuple with two ints
(double-up "a")  ; produces a tuple with two strings
```

Custom generic types can be created using the `generic-type`
form, which takes the name of the new type, a list of type
parameters, and the type definition.  Each parameter must
be a generic name beginning with `@`, typically followed by
a single uppercase letter.  These parameters represent types
which get replaced in a concrete instance of the new type.

Example:
```
(generic-type Pair (@T) (tuple @T @T))

(let my-pair {(Pair str) "one" "two"})

(fun pair-equal? (ip (Pair int) -> bool)
  ([ip .0] == [ip .1])
)
```


Optional types
--------------

The optional type represents a value which can either be an
actual value, or nothing.  It is implemented as a union type
with two tags, `VAL` for a value and `NONE` for a nothing.
The type of the value can be anything, even another optional
type (though that is not very useful).  When used with a
reference type, the native code representation is more
efficient since `NONE` is represented as a null pointer.

Example:
```
(fun show-it (i (Opt int))
   (match i
      (`VAL i2 => (print ($ i2)))
      (`NONE => (print "Nada"))
   )
)
(show-it {wrap 3})
(show-it {no int})
```

Values of an optional type can be constructed like a union,
but the following shortcuts are available:
```
{no TYPE}    ; makes a `NONE value
{wrap EXPR}  ; makes a `VAL using type of the expression
```

Typically the value is accessed via a `match` form, as in the
example above, since that is the safest way.  But the "unwrap"
forms (in round or square brackets) can also be used, however
these will abort the program if the given value is a `NONE`.
```
(unwrap EXPR)          ; unwrap using the normal syntax
[MAP-EXPR KEY unwrap]  ; unwrap within the access syntax
```

Another option for simple types is the `flatten` form, which
returns the default value for the type when the optional is
`NONE`.  Like unwrap, this can be used in access syntax too.

The following builtins are also available:
```
(val?  EXPR)
(none? EXPR)
```

The following macros can make matching against an optional
value a bit easier.  The else clauses can be omitted.
```
(if-val  VAR EXPR ... else ...)
(if-none VAR EXPR ... else ...)
```


Macros
------

Macros allow creating new syntactical structures.  When a user
defined macro is encountered, it is replaced with the body of
the macro and any arguments given to the macro are inserted
where they appear in the body.

Macros in Yewberry are safer than mere textual substitution.
Like Scheme they are "hygienic" -- which means all identifiers
in the macro body are given special treatment to prevent some
possible problems with name clashes that can occur with purely
textual substitution.  It also means some limits on what can be
done with macros, e.g. language keywords cannot be passed as
parameters.

Each macro contains one or more rules for matching the macro
against the actual arguments in its usage.  The first matching
rule is used.  It is an error if none of the rules match.

Example:
```
(macro swap!
  (locals tmp)
  (rule (a b)
    (begin
      (let tmp a)
      (set! a b)
      (set! b tmp)
    )
  )
)

; usage
(swap! x y)
```

One of the biggest problems with non-hygenic macro systems, like
the `#define` macros of C and C++, is that a local variable which
is defined inside the macro might clash with a name passed as a
macro parameter.  Yewberry tackles this problem by requiring any
local variable defined in a macro to be specified in a `locals`
form at the very beginning of the macro definition.  For example,
any `let` form with a variable NOT mentioned there is an error.
These local variables get renamed when the macro is expanded and
it is impossible for them to clash with any normal identifier.

Each rule of a macro is introduced via the `rule` keyword,
followed by the rule's pattern in `()` parentheses and then
the body.  The body must be a single token, since the place
where the macro is invoked may be a place where multiple tokens
is not allowed (e.g. the value in a `let` form).  A rule using
`_` instead of a pattern in `()` is a catch-all rule, and is
mainly useful with the `macro-error` form to provide a custom
error message for a failure to match.

The pattern of a macro rule often looks like the parameters of
a function, but it is actually a lot more powerful.  Each
element may be a name, which is a parameter to the macro and
gets expanded to the actual parameter, or it can be a string,
which represents a keyword which must be matched exactly.
Elements may also be a list in `()`, `[]` or `{}` brackets,
these require the actual parameter to be the same kind of list
and matching will recurse to elements of that list.

Macro patterns also support a name which ends in `...` (three
dots), including `...` itself.  These names can match multiple
tokens in the macro call, or even none at all, but usually as
many as possible.  Since they may match nothing, you often need
another binding parameter before it to force matching at least
one input token, e.g. for macros that support a BLOCK of code.


Other builtins
--------------

Random numbers:
```
(rand-seed seed)
(rand-int low high)
(rand-float)  ; result is >= 0.0 and < 1.0
```

Input and output:
```
(input-line prompt)
(print str)
```

Miscellaneous:
```
(exit-program status)
(abort-program msg)
```


Example program
---------------

```
;;
;; Prime number example
;;

(for p 0 100
   (if (prime? p)
      (print ($ p))
   )
)

(fun prime? (p int -> bool)
   (if (p < 2)
      FALSE
   else
      (let-mut i 2)
      (let-mut result TRUE)
      (while (result && (i * i) <= p)
         (if (p % i == 0)
            (set! result FALSE)
          else
            (set! i (i + 1))
         )
      )
      result
   )
)
```
