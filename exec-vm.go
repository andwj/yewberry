// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "math"

type Operation struct {
	T OpCode // Type
	S int32  // S field
	D int32  // D field

	LineNum int32
}

const NO_PART int32 = -0x112234

type OpCode int16

const (
	OP_INVALID OpCode = iota

	/* Control Flow */

	OP_NOP       //  does nothing
	OP_JUMP      //  PC := D
	OP_IF_FALSE  //  if A == FALSE then PC := D
	OP_IF_TRUE   //  if A == TRUE  then PC := D

	OP_FRAME     //  prepare to call function,  S = num params  [ bytecode only ]
	OP_FUN_CALL  //  call function,  func on stack, S = num params
	OP_TAIL_CALL //  tail-call func, func on stack, S = num params
	OP_RETURN    //  return from current function

	/* Register and Stack Stuff */

	OP_CONST   //  A := cl.constants[S]
	OP_INT_A   //  A := op.S
	OP_INT_B   //  B := op.S
	OP_PUSH_A  //  A is pushed onto the stack
	OP_POP_A   //  A is popped off the stack
	OP_DROP    //  drop S elements off the stack

	OP_MOVE_B  //  B := A
	OP_MOVE_C  //  C := A
	OP_GET_B   //  A := B
	OP_GET_C   //  A := C

	/* Miscellaneous Stuff */

	OP_FMT     //  S is constant for fmt_str, D is type, A is value
	OP_TOBYTE  //  A := (A & 255)
	OP_WRAP    //  A := {opt A}, union type in S
	OP_UNWRAP  //  A := (unwrap A), raise error if `NONE
	OP_FLATTEN //  A := (flatten A), result type in S

	OP_STR_CHAR  //  A := convert A from char to string
	OP_STR_READ  //  A := A[C]
	OP_STR_ADD   //  A := A + B

	OP_BOOL_AND  //  A := (A && B)
	OP_BOOL_OR   //  A := (A || B)
	OP_BOOL_NOT  //  A := (! A)

	OP_TRUE    //  A := TRUE
	OP_REF_EQ  //  A := (ref-eq? A B)
	OP_IS_LEN  //  A := ((len A) == S)
	OP_MIN_LEN //  A := ((len A) >= S)

	/* Memory Access */

	OP_UPVAR      //  A := cl.upvars[S]
	OP_GLOB_READ  //  A := cl.globals[S]
	OP_GLOB_WRITE //  cl.globals[S] := A
	OP_EXTV_READ  //  A := external var in A
	OP_EXTV_WRITE //  external var in B := A
	OP_LOC_READ   //  A := local var S   [ used for temporaries too ]
	OP_LOC_WRITE  //  local var S := A

	OP_ARR_READ   //  A := A[C]
	OP_ARR_GET    //  A := A[S] if S >= 0, A[len(A)+S] if S < 0
	OP_ARR_WRITE  //  A[C] := B
	OP_ARR_APPEND //  append B onto array A
	OP_MAP_HAS    //  A := TRUE if C is element in A, FALSE if not
	OP_MAP_READ   //  A := A[C], optional type in S
	OP_MAP_WRITE  //  A[C] := B
	OP_SET_HAS    //  A := TRUE if C is element in A, FALSE if not
	OP_SET_WRITE  //  A[C] := B

	OP_TUPL_READ  //  A := A[S], S is the field index
	OP_TUPL_WRITE //  A[S] := B, S is the field index
	OP_OBJ_READ   //  A := A[S], S is the field index
	OP_OBJ_WRITE  //  A[S] := B, S is the field index
	OP_OBJ_METHOD //  A := method function, S is method name
	OP_OBJ_CLASS  //  A := (class$ A)
	OP_UNION_READ //  A := datum of A
	OP_UNION_TAG  //  A := (tag-int A)
	OP_ENUM_TAG   //  A := (tag-int A)
	OP_TAG_STR    //  A := S.tag_names[A], S is union/enum type
	OP_TAG_EQ     //  A := (tag-int A == tag-int B)

	OP_MAKE_VOID    //  A := the void value
	OP_MAKE_DEFAULT //  A := default value for type in S
	OP_MAKE_FUNC    //  A := new closure from template S
	OP_MAKE_VAR     //  A := new on-heap variable (value in A)
	OP_MAKE_BYTEVEC //  A := a freshly allocated byte-vec

	OP_MAKE_ARRAY //  A := a freshly allocated array, type in S
	OP_MAKE_MAP   //  A := a freshly allocated map, type in S
	OP_MAKE_SET   //  A := a freshly allocated set, type in S
	OP_MAKE_TUPLE //  A := a freshly allocated tuple, type in S
	OP_MAKE_OBJ   //  A := a freshly allocated object, type in S
	OP_MAKE_UNION //  A := a fresh union, type in S, tag in D, datum in A
)

//----------------------------------------------------------------------

const MAX_DATA_STACK = 250000
const MAX_CALL_STACK = 25000

var data_stack [MAX_DATA_STACK]Value
var call_stack [MAX_CALL_STACK]FuncCallState

// stack_p is top of stack (nothing >= stack_p is in use)
var stack_p int

// stack_frame is base of current function's frame
var stack_frame int

// call_p is top of call stack, 0 if empty.
// the currently executing function is at (call_p - 1).
var call_p int

// after a function returns we erase the used portion of the
// stack, to prevent the garbage collector from keeping some
// objects alive too long (potentially indefinitely).
// To improve performance, we only do this occasionally,
// and 'data_highwater' keeps track of highest used slot.
var data_erase_time int
var data_highwater  int

// general data registers
var register_A Value
var register_B Value
var register_C Value

// when this is true, an run-time error has occurred
var run_error bool

type FuncCallState struct {
	// the function being run
	cl *Closure

	// the code pointer for this function
	op_ptr int

	// the stack_frame value for caller func
	old_frame int

	// this is used to detect run-away loops
	iterations int64
}

//----------------------------------------------------------------------

func Die(msg string, a ...interface{}) {
	run_error = true

	// determine location of the error
	var lno int
	var funcname string

	if call_p >= 1 {
		csf := &call_stack[call_p-1]

		if call_p >= 2 && csf.cl.builtin != nil {
			csf = &call_stack[call_p-2]
		}

		lno = csf.DetermineLine()
		funcname = csf.cl.debug_name
	}

	if lno > 0 {
		ErrorSetLine(lno)
	}
	if funcname != "" {
		ErrorSetFunc(funcname)
	}

	PostError(msg, a...)

	// show a stack trace
	trace_size := int(glob_STACK_TRACE.loc.Int)

	ShowStackTrace(trace_size)
}

func SetResult(v Value) {
	register_A = v
}

func SetResult_Bool(b bool) {
	register_A.MakeBool(b)
}

func SetResult_Int(i int64) {
	register_A.MakeInt(i)
}

func SetResult_Float(f float64) {
	register_A.MakeFloat(f)
}

func SetResult_String(s string) {
	register_A.MakeString(s)
}

func SetResult_Void() {
	register_A.MakeVoid()
}

//----------------------------------------------------------------------

func RunTop(cl *Closure) (Value, cmError) {
	// reset data stack
	stack_p = 0

	// reset call stack
	call_p = 0

	// reset error condition
	run_error = false

	// clear registers
	register_A.MakeVoid()
	register_B.MakeVoid()
	register_C.MakeVoid()

	// dummy function slot
	StackPush(register_A)

	if !RunClosure(cl, 0) {
		// an error occurred
		return Value{}, FAILED
	}

	return register_A, OKAY
}

// RunClosure executes the compiled code in the closure.
// A function and its parameters must be setup earlier
// (pushed onto the data stack).
// Returns true if ok, false if an error was raised.
func RunClosure(cl *Closure, num_param int) bool {
	start_p := call_p

	if !CallStackPush(cl, stack_p - num_param) {
		return false
	}

	if cl.builtin != nil {
		cl.builtin.code()
		CallStackPop(true)
		return !run_error
	}

	max_loop := glob_MAX_LOOP.loc.Int

	for call_p > start_p {
		call_cur := &call_stack[call_p-1]

		for call_cur.op_ptr < len(call_cur.cl.vm_ops) {
			op := &call_cur.cl.vm_ops[call_cur.op_ptr]
			call_cur.op_ptr += 1

			if op.T == OP_RETURN {
				CallStackPop(true)
				break
			}

			PerformOp(op, call_cur.cl)

			if run_error {
				return false
			}

			call_cur = &call_stack[call_p-1]

			call_cur.iterations++

			if call_cur.iterations >= max_loop {
				Die("run-away loop detected")
				return false
			}
		}
	}

	return true
}

func CallStackPush(cl *Closure, new_frame int) bool {
	if stack_p < new_frame {
		panic("CallStackPush with stack_p < new_frame")
	}

	// check for stack overflow
	if call_p+2 >= len(call_stack) {
		Die("overflow of call stack")
		return false
	}

	d_top := stack_p+int(cl.highest_water)

	// use a high number, since above calc may not work for builtins/special funcs
	if d_top+16 >= len(data_stack) {
		Die("overflow of data stack")
		return false
	}

	if data_highwater < d_top {
		data_highwater = d_top
	}

	call_p++
	call_cur := &call_stack[call_p - 1]

	call_cur.cl = cl
	call_cur.old_frame = stack_frame
	call_cur.op_ptr = 0
	call_cur.iterations = 0

	stack_frame = new_frame

	return true
}

func CallStackPop(erase bool) {
	if call_p == 0 {
		panic("underflow of call stack")
	}

	call_cur := &call_stack[call_p - 1];
	call_cur.cl = nil

	data_erase_time++

	if erase && data_erase_time > 20 {
		StackClear(stack_frame, data_highwater+4)

		data_highwater = stack_frame
		data_erase_time = 0
	}

	// pop everything off the current frame
	stack_p = stack_frame - 1
	stack_frame = call_cur.old_frame

	call_p--
}

func (csf *FuncCallState) DetermineLine() int {
	if csf.cl.builtin != nil {
		return 0
	}

	op_ptr := csf.op_ptr - 1

	if op_ptr >= 0 && op_ptr < len(csf.cl.vm_ops) {
		return int(csf.cl.vm_ops[op_ptr].LineNum)
	}

	return 0
}

func ShowStackTrace(size int) {
	if size <= 0 {
		return
	}

	Print("Stack trace:\n")

	last_p := call_p - 1 - size
	if last_p < 0 {
		last_p = 0
	}

	for i := call_p - 1; i >= last_p; i-- {
		csf := &call_stack[i]
		lno := csf.DetermineLine()

		if lno <= 0 {
			Print("   [%03d] %s\n", i, csf.cl.debug_name)
		} else {
			Print("   [%03d] %s (line %d)\n", i, csf.cl.debug_name, lno)
		}
	}
}

func StackPush(v Value) {
	if stack_p+2 >= len(data_stack) {
		Die("overflow of data stack")
		return
	}
	data_stack[stack_p] = v
	stack_p += 1
}

func StackPop() Value {
	if stack_p < 1 {
		panic("stack underflow with StackPop")
	}
	stack_p -= 1
	return data_stack[stack_p]
}

func StackDrop(count int) {
	if stack_p < count {
		panic("stack underflow with StackDrop")
	}
	stack_p -= count
}

func StackClear(start, end int) {
	if end > MAX_DATA_STACK - 1 {
		end = MAX_DATA_STACK - 1
	}

	for i := start; i <= end; i++ {
		data_stack[i].MakeVoid()
	}
}

//----------------------------------------------------------------------

func PerformOp(op *Operation, cl *Closure) {
	switch op.T {
	case OP_NOP:
		// nothing to do

	case OP_JUMP:
		perform_Jump(op, cl)

	case OP_IF_FALSE:
		perform_IfFalse(op, cl)

	case OP_IF_TRUE:
		perform_IfTrue(op, cl)

	case OP_FUN_CALL:
		perform_FunCall(op, cl)

	case OP_TAIL_CALL:
		perform_TailCall(op, cl)

	case OP_RETURN:
		// NOTE: handled by caller

	/* Stack Stuff */

	case OP_CONST:
		perform_Const(op, cl)

	case OP_INT_A:
		register_A.MakeInt(int64(op.S))

	case OP_INT_B:
		register_B.MakeInt(int64(op.S))

	case OP_PUSH_A:
		StackPush(register_A)

	case OP_POP_A:
		register_A = StackPop()

	case OP_DROP:
		StackDrop(int(op.S))

	case OP_MOVE_B:
		register_B = register_A

	case OP_MOVE_C:
		register_C = register_A

	case OP_GET_B:
		register_A = register_B

	case OP_GET_C:
		register_A = register_C

	case OP_FMT:
		perform_Fmt(op, cl)

	case OP_TOBYTE:
		perform_ToByte(op, cl)

	/* Matching Stuff */

	case OP_BOOL_AND:
		perform_And(op, cl)

	case OP_BOOL_OR:
		perform_Or(op, cl)

	case OP_BOOL_NOT:
		perform_Not(op, cl)

	case OP_TRUE:
		register_A = *glob_TRUE.loc

	case OP_REF_EQ:
		perform_RefEqual(op, cl)

	case OP_IS_LEN:
		perform_IsLen(op, cl)

	case OP_MIN_LEN:
		perform_MinLen(op, cl)

	/* Memory Access */

	case OP_UPVAR:
		perform_Upvar(op, cl)

	case OP_GLOB_READ:
		perform_GlobalRead(op, cl)

	case OP_GLOB_WRITE:
		perform_GlobalWrite(op, cl)

	case OP_EXTV_READ:
		perform_ExternalRead(op, cl)

	case OP_EXTV_WRITE:
		perform_ExternalWrite(op, cl)

	case OP_LOC_READ:
		perform_LocalRead(op, cl)

	case OP_LOC_WRITE:
		perform_LocalWrite(op, cl)

	case OP_ARR_READ:
		perform_ArrayRead(op, cl)

	case OP_ARR_GET:
		perform_ArrayGet(op, cl)

	case OP_ARR_WRITE:
		perform_ArrayWrite(op, cl)

	case OP_ARR_APPEND:
		perform_ArrayAppend(op, cl)

	case OP_MAP_HAS:
		perform_MapHas(op, cl)

	case OP_MAP_READ:
		perform_MapRead(op, cl)

	case OP_MAP_WRITE:
		perform_MapWrite(op, cl)

	case OP_SET_HAS:
		perform_SetHas(op, cl)

	case OP_SET_WRITE:
		perform_SetWrite(op, cl)

	case OP_TUPL_READ:
		perform_TupleRead(op, cl)

	case OP_TUPL_WRITE:
		perform_TupleWrite(op, cl)

	case OP_OBJ_READ:
		perform_ObjectRead(op, cl)

	case OP_OBJ_WRITE:
		perform_ObjectWrite(op, cl)

	case OP_OBJ_METHOD:
		perform_ObjectMethod(op, cl)

	case OP_OBJ_CLASS:
		perform_ObjectClassName(op, cl)

	case OP_UNION_READ:
		perform_UnionRead(op, cl)

	case OP_UNION_TAG:
		perform_UnionTag(op, cl)

	case OP_ENUM_TAG:
		perform_EnumTag(op, cl)

	case OP_TAG_STR:
		perform_TagStr(op, cl)

	case OP_TAG_EQ:
		register_A.MakeBool(register_A.Int == register_B.Int)

	case OP_WRAP:
		perform_Wrap(op, cl)

	case OP_UNWRAP:
		perform_Unwrap(op, cl)

	case OP_FLATTEN:
		perform_Flatten(op, cl)

	case OP_STR_CHAR:
		perform_StringFromChar(op, cl)

	case OP_STR_READ:
		perform_StringRead(op, cl)

	case OP_STR_ADD:
		perform_StringAdd(op, cl)

	case OP_MAKE_VOID:
		perform_MakeVoid(op, cl)

	case OP_MAKE_DEFAULT:
		perform_MakeDefault(op, cl)

	case OP_MAKE_FUNC:
		perform_MakeFunc(op, cl)

	case OP_MAKE_VAR:
		perform_MakeVar(op, cl)

	case OP_MAKE_BYTEVEC:
		perform_MakeByteVec(op, cl)

	case OP_MAKE_ARRAY:
		perform_MakeArray(op, cl)

	case OP_MAKE_MAP:
		perform_MakeMap(op, cl)

	case OP_MAKE_SET:
		perform_MakeSet(op, cl)

	case OP_MAKE_TUPLE:
		perform_MakeTuple(op, cl)

	case OP_MAKE_OBJ:
		perform_MakeObject(op, cl)

	case OP_MAKE_UNION:
		perform_MakeUnion(op, cl)

	/* Math-expr operations */

	case OP2_BOOL_EQ:
		register_A.MakeBool(register_A.IsTrue() == register_B.IsTrue())

	case OP2_BOOL_NE:
		register_A.MakeBool(register_A.IsTrue() != register_B.IsTrue())

	case OP2_INT_EQ:
		register_A.MakeBool(register_A.Int == register_B.Int)

	case OP2_INT_NE:
		register_A.MakeBool(register_A.Int != register_B.Int)

	case OP2_INT_LT:
		register_A.MakeBool(register_A.Int < register_B.Int)

	case OP2_INT_LE:
		register_A.MakeBool(register_A.Int <= register_B.Int)

	case OP2_INT_GT:
		register_A.MakeBool(register_A.Int > register_B.Int)

	case OP2_INT_GE:
		register_A.MakeBool(register_A.Int >= register_B.Int)

	case OP2_FLOAT_EQ:
		register_A.MakeBool(register_A.Float == register_B.Float)

	case OP2_STR_EQ:
		register_A.MakeBool(register_A.Str == register_B.Str)

	default:
		panic("UNKNOWN OPCODE FOUND")
	}
}

func perform_Const(op *Operation, cl *Closure) {
	if op.S < 0 || int(op.S) >= len(cl.constants) {
		panic("BAD CONSTANT INDEX")
	}
	register_A = cl.constants[op.S]
}

func perform_Fmt(op *Operation, cl *Closure) {
	fmt_str := cl.constants[op.S].Str
	ty := cl.types[op.D]

	var s string

	switch ty.base {
	case TYP_Int:
		// handle special values
		if register_A.Int == glob_INAN.loc.Int {
			s = "INAN"
		} else {
			// this handles char too (%c)
			s = fmt.Sprintf(fmt_str, int(register_A.Int))
		}

	case TYP_Float:
		// handle special values
		if math.IsNaN(register_A.Float) {
			s = "NAN"
		} else if math.IsInf(register_A.Float, -1) {
			s = "-INF"
		} else if math.IsInf(register_A.Float, 1) {
			s = "+INF"
		} else {
			s = fmt.Sprintf(fmt_str, register_A.Float)
		}

	case TYP_String:
		s = fmt.Sprintf(fmt_str, register_A.Str)

	case TYP_Union, TYP_Enum:
		tag_id := int(register_A.Int)
		s = ty.param[tag_id].name

	case TYP_Array, TYP_ByteVec, TYP_Class:
		// the verb in fmt_str is always "%p"
		s = fmt.Sprintf(fmt_str, register_A.Array)

	case TYP_Map, TYP_Set:
		// the verb in fmt_str is always "%p"
		s = fmt.Sprintf(fmt_str, register_A.Map)

	case TYP_Function:
		s = fmt.Sprintf(fmt_str, register_A.Clos)

	default:
		panic("Bad type in OP_FMT")
	}

	register_A.MakeString(s)
}

func perform_ToByte(op *Operation, cl *Closure) {
	register_A.VerifyType(TYP_Int, "OP_TOBYTE")
	register_A.Int &= 255
}

func perform_And(op *Operation, cl *Closure) {
	register_A.VerifyType(BTYP_Bool, "OP_BOOL_AND")
	register_B.VerifyType(BTYP_Bool, "OP_BOOL_AND")

	register_A.MakeBool(register_A.IsTrue() && register_B.IsTrue())
}

func perform_Or(op *Operation, cl *Closure) {
	register_A.VerifyType(BTYP_Bool, "OP_BOOL_OR")
	register_B.VerifyType(BTYP_Bool, "OP_BOOL_OR")

	register_A.MakeBool(register_A.IsTrue() || register_B.IsTrue())
}

func perform_Not(op *Operation, cl *Closure) {
	register_A.VerifyType(BTYP_Bool, "OP_BOOL_NOT")
	register_A.MakeBool(register_A.IsFalse())
}

func perform_RefEqual(op *Operation, cl *Closure) {
	register_A.MakeBool(register_A.RefEqual(&register_B))
}

func perform_IsLen(op *Operation, cl *Closure) {
	register_A.MakeBool(register_A.Length() == int(op.S))
}

func perform_MinLen(op *Operation, cl *Closure) {
	register_A.MakeBool(register_A.Length() >= int(op.S))
}

func perform_UnionTag(op *Operation, cl *Closure) {
	v := register_A
	register_A.MakeInt(int64(v.Union.tag_id))
}

func perform_EnumTag(op *Operation, cl *Closure) {
	register_A.MakeInt(register_A.Int)
}

func perform_TagStr(op *Operation, cl *Closure) {
	tag_id := int(register_A.Int)
	enum_type := cl.types[op.S]
	tag_str := enum_type.param[tag_id].name

	register_A.MakeString(tag_str)
}

func perform_ArrayGet(op *Operation, cl *Closure) {
	arr := register_A
	arr.VerifyType(TYP_Array, "OP_ARR_GET")

	idx := int(op.S)
	if idx < 0 {
		idx += arr.Length()
	}

	if !arr.ValidIndex(idx) {
		panic("BAD RAW ARRAY INDEX IN MATCH")
	}

	register_A = arr.GetElem(idx)
}

func perform_GlobalRead(op *Operation, cl *Closure) {
	if op.S < 0 || int(op.S) >= len(cl.globals) {
		panic("BAD GLOBAL-DEF INDEX")
	}

	S := cl.globals[op.S].loc
	register_A = *S
}

func perform_GlobalWrite(op *Operation, cl *Closure) {
	if op.S < 0 || int(op.S) >= len(cl.globals) {
		panic("BAD GLOBAL-DEF INDEX for WRITE")
	}

	def := cl.globals[op.S]

	def.loc.Copy(&register_A)
	def.loc.ty = def.ty
}

func perform_LocalRead(op *Operation, cl *Closure) {
	if op.S < 0 {
		panic("BAD LOCAL INDEX")
	}
	register_A = data_stack[stack_frame+int(op.S)]
}

func perform_LocalWrite(op *Operation, cl *Closure) {
	if op.S < 0 {
		panic("BAD LOCAL INDEX")
	}
	data_stack[stack_frame+int(op.S)] = register_A
}

func perform_Upvar(op *Operation, cl *Closure) {
	if op.S < 0 || int(op.S) >= len(cl.upvars) {
		panic("BAD UPVAR INDEX")
	}
	register_A.Copy(&cl.upvars[op.S])
}

func perform_ExternalRead(op *Operation, cl *Closure) {
	S := register_A

	if S.ty == nil || S.ty.base != TYP_Void {
		panic("BAD EXTERNAL VAR for READ")
	}

	register_A = S.Array.data[0]
}

func perform_ExternalWrite(op *Operation, cl *Closure) {
	D := register_B

	if D.ty == nil || D.ty.base != TYP_Void {
		panic("BAD EXTERNAL VAR for WRITE")
	}

	D.Array.data[0] = register_A
}

func perform_Jump(op *Operation, cl *Closure) {
	if op.D < 0 || int(op.D) >= len(cl.vm_ops) {
		panic("BAD DESTINATION FOR OP_JUMP or OP_IF_XXX")
	}

	call_stack[call_p - 1].op_ptr = int(op.D)
}

func perform_IfFalse(op *Operation, cl *Closure) {
	register_A.VerifyType(BTYP_Bool, "OP_IF_FALSE")

	if !register_A.IsTrue() {
		perform_Jump(op, cl)
	}
}

func perform_IfTrue(op *Operation, cl *Closure) {
	register_A.VerifyType(BTYP_Bool, "OP_IF_TRUE")

	if register_A.IsTrue() {
		perform_Jump(op, cl)
	}
}

func perform_FunCall(op *Operation, cl *Closure) {
	num_param := int(op.S)
	param_p := stack_p - num_param

	F := data_stack[param_p - 1]
	F.VerifyType(TYP_Function, "OP_FUN_CALL")

	if F.Clos.failed {
		Die("call to deleted function")
		return
	}

	if !CallStackPush(F.Clos, param_p) {
		// stack overflow error
		return
	}

	// handle built-in primitives
	if F.Clos.builtin != nil {
		F.Clos.builtin.code()
		CallStackPop(true)
		return
	}

	// run the code normally, see RunClosure() above
}

func perform_TailCall(op *Operation, cl *Closure) {
	num_param := int(op.S)
	param_p := stack_p - num_param

	F := data_stack[param_p - 1]
	F.VerifyType(TYP_Function, "OP_TAIL_CALL")

	if F.Clos.failed {
		Die("call to deleted function")
		return
	}

	// for builtins, treat as a normal call
	if F.Clos.builtin != nil {
		perform_FunCall(op, cl)
		return
	}

	save_iter := call_stack[call_p - 1].iterations

	CallStackPop(false)

	// add back the function (CallStackPop popped old one)
	StackPush(F)

	// rejig data stack: copy down the parameters
	new_frame := stack_p

	if param_p > new_frame {
		for i := 0; i < num_param; i++ {
			data_stack[new_frame+i] = data_stack[param_p+i]
		}
	}
	stack_p += num_param

	CallStackPush(F.Clos, new_frame)

	// restore the iteration counter
	call_stack[call_p - 1].iterations = save_iter
}

func perform_ArrayRead(op *Operation, cl *Closure) {
	arr := register_A
	arr.VerifyType(TYP_Array, "OP_ARR_READ")

	idx := register_C
	idx.VerifyType(TYP_Int, "OP_ARR_READ")

	// raise error if *any* dimension is out of bounds
	if !arr.ValidIndex(int(idx.Int)) {
		Die("read array element: index out of bounds")
		return
	}

	register_A = arr.GetElem(int(idx.Int))
}

func perform_ArrayWrite(op *Operation, cl *Closure) {
	val := register_B

	idx := register_C
	idx.VerifyType(TYP_Int, "OP_ARR_WRITE")

	arr := register_A
	arr.VerifyType(TYP_Array, "OP_ARR_WRITE")

	// raise error if *any* dimension is out of bounds
	if !arr.ValidIndex(int(idx.Int)) {
		Die("write array element: index out of bounds")
		return
	}

	arr.SetElem(int(idx.Int), val)
}

func perform_ArrayAppend(op *Operation, cl *Closure) {
	arr := register_A
	arr.VerifyType(TYP_Array, "OP_ARR_APPEND")

	arr.AppendElem(register_B)
}

func perform_MapHas(op *Operation, cl *Closure) {
	arr := register_A
	arr.VerifyType(TYP_Map, "OP_MAP_HAS")

	key_type := arr.ty.KeyType()

	key := register_C
	key.VerifyKeyType(key_type, "OP_MAP_HAS")

	register_A.MakeBool(arr.KeyExists(key))
}

func perform_MapRead(op *Operation, cl *Closure) {
	arr := register_A
	arr.VerifyType(TYP_Map, "OP_MAP_READ")

	key_type := arr.ty.KeyType()

	key := register_C
	key.VerifyKeyType(key_type, "OP_MAP_READ")

	opt_ty := cl.types[op.S]

	if arr.KeyExists(key) {
		register_A = arr.KeyGet(key)
		register_A.Wrap(opt_ty)
	} else {
		register_A.MakeNone(opt_ty)
	}
}

func perform_MapWrite(op *Operation, cl *Closure) {
	val := register_B

	arr := register_A
	arr.VerifyType(TYP_Map, "OP_MAP_WRITE")

	key_type := arr.ty.KeyType()

	key := register_C
	key.VerifyKeyType(key_type, "OP_MAP_WRITE")

	arr.KeySet(key, val)
}

func perform_SetHas(op *Operation, cl *Closure) {
	arr := register_A
	arr.VerifyType(TYP_Set, "OP_SET_HAS")

	key_type := arr.ty.sub

	key := register_C
	key.VerifyKeyType(key_type, "OP_SET_HAS")

	register_A.MakeBool(arr.KeyExists(key))
}

func perform_SetWrite(op *Operation, cl *Closure) {
	arr := register_A
	arr.VerifyType(TYP_Set, "OP_SET_WRITE")

	key_type := arr.ty.sub

	key := register_C
	key.VerifyKeyType(key_type, "OP_SET_WRITE")

	val := register_B
	val.VerifyType(BTYP_Bool, "OP_SET_WRITE")

	arr.KeySet(key, val)
}

func perform_TupleRead(op *Operation, cl *Closure) {
	tup := register_A
	tup.VerifyType(TYP_Tuple, "OP_TUPL_READ")

	idx := int(op.S)
	if idx < 0 || idx >= len(tup.Array.data) {
		panic("OP_TUPL_READ: index out-of-bounds")
	}

	register_A = tup.Array.data[idx]
}

func perform_TupleWrite(op *Operation, cl *Closure) {
	val := register_B

	tup := register_A
	tup.VerifyType(TYP_Tuple, "OP_TUPL_WRITE")

	idx := int(op.S)
	if idx < 0 || idx >= len(tup.Array.data) {
		panic("OP_TUPL_WRITE: index out-of-bounds")
	}

	tup.Array.data[idx] = val
}

func perform_ObjectRead(op *Operation, cl *Closure) {
	obj := register_A
	obj.VerifyType(TYP_Class, "OP_OBJ_READ")

	idx := int(op.S)
	if idx < 0 || idx >= len(obj.Array.data) {
		panic("OP_OBJ_READ: index out-of-bounds")
	}

	register_A = obj.Array.data[idx]
}

func perform_ObjectWrite(op *Operation, cl *Closure) {
	val := register_B

	obj := register_A
	obj.VerifyType(TYP_Class, "OP_OBJ_WRITE")

	idx := int(op.S)
	if idx < 0 || idx >= len(obj.Array.data) {
		panic("OP_OBJ_WRITE: index out-of-bounds")
	}

	obj.Array.data[idx] = val
}

func perform_ObjectMethod(op *Operation, cl *Closure) {
	name := cl.constants[op.S].Str

	obj := register_A
	obj.VerifyType(TYP_Class, "OP_OBJ_METHOD")

	// in native code version, each object stores a pointer to
	// an information table, and we access the method via it.
	// here we take advantage of the interpreter's structs.
	class_ty := obj.ty

	loc := class_ty.methods[name]
	if loc == nil || loc.Clos == nil {
		panic("OP_OBJ_METHOD: no such method: " + name)
	}

	register_A = *loc
}

func perform_ObjectClassName(op *Operation, cl *Closure) {
	obj := register_A
	obj.VerifyType(TYP_Class, "OP_OBJ_CLASS")

	// in native code version, each object stores a pointer to
	// an information table, and we can read the name from it.
	// here we take advantage of the interpreter's structs.
	class_ty := obj.ty

	register_A.MakeString(class_ty.custom)
}

func perform_UnionRead(op *Operation, cl *Closure) {
	register_A.VerifyType(TYP_Union, "OP_UNION_READ")
	register_A = register_A.Union.datum
}

func perform_StringFromChar(op *Operation, cl *Closure) {
	ch := register_A
	ch.VerifyType(TYP_Int, "OP_STR_CHAR")

	var r [1]rune
	r[0] = rune(ch.Int)

	register_A.MakeString(string(r[:]))
}

func perform_StringRead(op *Operation, cl *Closure) {
	str := register_A
	str.VerifyType(TYP_String, "OP_STR_READ")

	idx := register_C
	idx.VerifyType(TYP_Int, "OP_STR_READ")

	r := []rune(str.Str)

	// produce NUL char if the index is out of bounds
	if idx.Int < 0 || int(idx.Int) >= len(r) {
		register_A.MakeChar(0)
	} else {
		register_A.MakeChar(r[idx.Int])
	}
}

func perform_Unwrap(op *Operation, cl *Closure) {
	register_A.VerifyType(TYP_Union, "OP_UNWRAP")

	if register_A.Union.tag_id == 0 {
		Die("attempt to unwrap a `NONE or `ERR value")
		return
	}

	register_A.Unwrap()
}

func perform_Flatten(op *Operation, cl *Closure) {
	register_A.VerifyType(TYP_Union, "OP_FLATTEN")

	if register_A.Union.tag_id == 0 {
		perform_MakeDefault(op, cl)
	} else {
		register_A.Unwrap()
	}
}

func perform_Wrap(op *Operation, cl *Closure) {
	register_A.Wrap(cl.types[op.S])
}

func perform_StringAdd(op *Operation, cl *Closure) {
	register_A.VerifyType(TYP_String, "OP_STR_ADD")
	register_B.VerifyType(TYP_String, "OP_STR_ADD")

	register_A.MakeString(register_A.Str + register_B.Str)
}

func perform_MakeVoid(op *Operation, cl *Closure) {
	register_A.MakeVoid()
}

func perform_MakeDefault(op *Operation, cl *Closure) {
	register_A.MakeDefault(cl.types[op.S])
}

func perform_MakeFunc(op *Operation, cl *Closure) {
	S := cl.constants[op.S]
	template := S.Clos

	newcl := new(Closure)
	newcl.vm_ops = template.vm_ops
	newcl.ty = template.ty
	newcl.ret_type = template.ret_type
	newcl.constants = template.constants
	newcl.types = template.types
	newcl.globals = template.globals
	newcl.debug_name = template.debug_name + fmt.Sprintf("@%p", newcl)

	// create external vars
	newcl.upvars = make([]Value, len(template.upvar_templates))

	for i, et := range template.upvar_templates {
		ref := data_stack[stack_frame+int(et.offset)]
		newcl.upvars[i] = ref
	}

	register_A.MakeFunction(newcl)
}

func perform_MakeVar(op *Operation, cl *Closure) {
	val := register_A

	// hack alert!!
	// type is void, but we use 'Array' for variable's storage
	register_A.MakeVoid()
	register_A.Array = new(ArrayImpl)
	register_A.Array.data = make([]Value, 1)
	register_A.Array.data[0] = val
}

func perform_MakeArray(op *Operation, cl *Closure) {
	arr_type := cl.types[op.S]
	register_A.MakeArray(0, arr_type)
}

func perform_MakeByteVec(op *Operation, cl *Closure) {
	register_A.MakeByteVec(0)
}

func perform_MakeMap(op *Operation, cl *Closure) {
	map_type := cl.types[op.S]
	register_A.MakeMap(map_type)
}

func perform_MakeSet(op *Operation, cl *Closure) {
	set_type := cl.types[op.S]
	register_A.MakeMap(set_type)  // not a typo
}

func perform_MakeTuple(op *Operation, cl *Closure) {
	tup_type := cl.types[op.S]
	register_A.MakeTuple(tup_type)
}

func perform_MakeObject(op *Operation, cl *Closure) {
	class_type := cl.types[op.S]
	register_A.MakeObject(class_type)
}

func perform_MakeUnion(op *Operation, cl *Closure) {
	datum := register_A

	union_type := cl.types[op.S]

	register_A.MakeUnion(union_type)
	register_A.Union.tag_id = int(op.D)
	register_A.Union.datum = datum
}

//----------------------------------------------------------------------

func DumpOps(cl *Closure) {
	Print("vm_ops for %s\n", cl.debug_name)
	for idx, op := range cl.vm_ops {
		Print("   [%03d] %s\n", idx, op.String(cl))
	}
}

func (op *Operation) String(cl *Closure) string {
	S := op.PartString(cl, "S", op.S)
	D := op.PartString(cl, "D", op.D)

	return fmt.Sprintf("%-12s  %-7s %-7s", op.OpString(), S, D)
}

func (op *Operation) OpString() string {
	switch op.T {
	case OP_NOP:
		return "OP_NOP"
	case OP_JUMP:
		return "OP_JUMP"
	case OP_IF_FALSE:
		return "OP_IF_FALSE"
	case OP_IF_TRUE:
		return "OP_IF_TRUE"

	case OP_FRAME:
		return "OP_FRAME"
	case OP_FUN_CALL:
		return "OP_FUN_CALL"
	case OP_TAIL_CALL:
		return "OP_TAIL_CALL"
	case OP_RETURN:
		return "OP_RETURN"

	case OP_CONST:
		return "OP_CONST"
	case OP_INT_A:
		return "OP_INT_A"
	case OP_INT_B:
		return "OP_INT_B"
	case OP_PUSH_A:
		return "OP_PUSH_A"
	case OP_POP_A:
		return "OP_POP_A"
	case OP_DROP:
		return "OP_DROP"
	case OP_MOVE_B:
		return "OP_MOVE_B"
	case OP_MOVE_C:
		return "OP_MOVE_C"
	case OP_GET_B:
		return "OP_GET_B"
	case OP_GET_C:
		return "OP_GET_C"
	case OP_FMT:
		return "OP_FMT"
	case OP_TOBYTE:
		return "OP_TOBYTE"
	case OP_WRAP:
		return "OP_WRAP"
	case OP_UNWRAP:
		return "OP_UNWRAP"
	case OP_FLATTEN:
		return "OP_FLATTEN"

	case OP_BOOL_AND:
		return "OP_BOOL_AND"
	case OP_BOOL_OR:
		return "OP_BOOL_OR"
	case OP_BOOL_NOT:
		return "OP_BOOL_NOT"

	case OP_TRUE:
		return "OP_TRUE"
	case OP_REF_EQ:
		return "OP_REF_EQ"
	case OP_IS_LEN:
		return "OP_IS_LEN"
	case OP_MIN_LEN:
		return "OP_MIN_LEN"

	case OP_UPVAR:
		return "OP_UPVAR"
	case OP_GLOB_READ:
		return "OP_GLOB_READ"
	case OP_GLOB_WRITE:
		return "OP_GLOB_WRITE"
	case OP_EXTV_READ:
		return "OP_EXTV_READ"
	case OP_EXTV_WRITE:
		return "OP_EXTV_WRITE"
	case OP_LOC_READ:
		return "OP_LOC_READ"
	case OP_LOC_WRITE:
		return "OP_LOC_WRITE"

	case OP_ARR_READ:
		return "OP_ARR_READ"
	case OP_ARR_GET:
		return "OP_ARR_GET"
	case OP_ARR_WRITE:
		return "OP_ARR_WRITE"
	case OP_ARR_APPEND:
		return "OP_ARR_APPEND"
	case OP_MAP_HAS:
		return "OP_MAP_HAS"
	case OP_MAP_READ:
		return "OP_MAP_READ"
	case OP_MAP_WRITE:
		return "OP_MAP_WRITE"
	case OP_SET_HAS:
		return "OP_SET_HAS"
	case OP_SET_WRITE:
		return "OP_SET_WRITE"

	case OP_TUPL_READ:
		return "OP_TUPL_READ"
	case OP_TUPL_WRITE:
		return "OP_TUPL_WRITE"
	case OP_OBJ_READ:
		return "OP_OBJ_READ"
	case OP_OBJ_WRITE:
		return "OP_OBJ_WRITE"
	case OP_OBJ_METHOD:
		return "OP_OBJ_METHOD"
	case OP_OBJ_CLASS:
		return "OP_OBJ_CLASS"
	case OP_UNION_READ:
		return "OP_UNION_READ"
	case OP_UNION_TAG:
		return "OP_UNION_TAG"
	case OP_ENUM_TAG:
		return "OP_ENUM_TAG"
	case OP_TAG_STR:
		return "OP_TAG_STR"
	case OP_TAG_EQ:
		return "OP_TAG_EQ"
	case OP_STR_CHAR:
		return "OP_STR_CHAR"
	case OP_STR_READ:
		return "OP_STR_READ"
	case OP_STR_ADD:
		return "OP_STR_ADD"

	case OP_MAKE_VOID:
		return "OP_MAKE_VOID"
	case OP_MAKE_DEFAULT:
		return "OP_MAKE_DEFAULT"
	case OP_MAKE_FUNC:
		return "OP_MAKE_FUNC"
	case OP_MAKE_VAR:
		return "OP_MAKE_VAR"
	case OP_MAKE_BYTEVEC:
		return "OP_MAKE_BYTEVEC"
	case OP_MAKE_ARRAY:
		return "OP_MAKE_ARRAY"
	case OP_MAKE_MAP:
		return "OP_MAKE_MAP"
	case OP_MAKE_SET:
		return "OP_MAKE_SET"
	case OP_MAKE_TUPLE:
		return "OP_MAKE_TUPLE"
	case OP_MAKE_OBJ:
		return "OP_MAKE_OBJ"
	case OP_MAKE_UNION:
		return "OP_MAKE_UNION"

	default:
		return "?????"
	}
}

func (op *Operation) PartString(cl *Closure, name string, val int32) string {
	if val == NO_PART {
		return ""
	}

	// handle the constants block
	if op.T == OP_CONST {
		v := cl.constants[val]
		if v.ty.base == TYP_Int || v.ty.base == TYP_Float || v.ty.base == TYP_String {
			return fmt.Sprintf("%s=%v (%s)", name, val, v.DeepString())
		}
	}

	// handle global reads
	if op.T == OP_GLOB_READ && name == "S" {
		def := cl.globals[op.S]
		return fmt.Sprintf("%s=%v (%s)", name, val, def.name)
	}

	return fmt.Sprintf("%s=%v", name, val)
}

func FloatToInt(v float64) int64 {
	const limit = 9.223372036854775e+18

	if v > limit {
		return math.MaxInt64
	}
	if v < -limit {
		return math.MinInt64
	}

	return int64(v)
}
