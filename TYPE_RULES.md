
DEFINITIONS
-----------

*Simple types* are types which are immutable and do not contain
fields which can be read individually.  Strings are also considered
to be simple because its element type is always `char` and they
are immutable.  The following are the available simple types:
-  `void`
-  `int`
-  `float`
-  `string`
-  `char`
-  `bool`

*Compound types* are types which contain other types, and those
types can be chosen by the programmer.  The following are the
available compound types:
-  `array`
-  `map`
-  `set`
-  `union`
-  `enum`
-  `tuple`
-  `class`
-  `function`

*Fundamental type* is the basic structure of a type, ignoring
any components of the type.  For example, all map types have a
key type and a value type, but the fundamental type is just `map`.

*Custom type* is a type given a name by the programmer.
In general, two different custom types are incompatible with
each other, since they could represent two distinct units of
measurement or two distinct object classes.  However custom
types with the same base type can be casted between each other
or the base type.

*Plain type* is a type which is not a custom type.

*Plain literal* is a number or string literal in the code which
does not have a custom type.

*Reference type* is a type which is really a pointer to the
object in memory, and which is usually a mutable object.  When
used as keys in maps and sets, the reference (pointer) is the
actual key and the contents of the object is ignored.
While strings, unions and tuples may be implemented as pointers,
semantically they are immutable value types.  Functions are
also immutable but the ability to compare them is useful.
The following are the reference types:
-  `array`
-  `map`
-  `set`
-  `class`
-  `function`

*Optional type* is a type formed by the "opt" syntax, or via
certain literal syntax.  It represents either the presence or
absence of a value, and is implemented as a union type with
two tags: `NONE` and `VAL`.

*Untyped literal* is an array, map, set or tuple literal in the
code which does not explicitly specify the type of its elements.
If the array, map or set literal is empty, the type is initially
unknown and will be a compile error if it cannot be deduced.
Note that union, enum and class literals cannot be written in
code without an explicit type, so they are never untyped.

*Mutually assignable* means that S is assignable to D while
D is also assignable to S.

*Weakly castable* means castable but only if no additional
actions occur, such as conversions or unwrapping.  For example,
an array of ints cannot be casted to an array of floats since
that would require creating a new array in memory and performing
the conversion (from int to float) on each element.



TYPE CHECKING
-------------

-  if E is an actual parameter of a function call and P is the
   formal parameter, then T(E) must be *assignable* to T(P).

-  if E is in tail position of function F, then T(E) must be
   *assignable* to the result type of the function F.

-  if E is used in the form: (set! VAR E), then T(E) must be
   *assignable* to the type of the variable VAR.

-  if E is used in the form: (cast TYPE E), then T(E) must be
   *castable* to the specified type.

-  some special magic for operators is used to disallow an
   operation when LHS is a custom type and RHS is a different
   custom type.



TYPE DEDUCTION
--------------

Type deduction is a process used to determine the type of
something in the code which lacks an explicit type.
The following things can use type deduction:

-  global variables and constants

-  local variables (let forms, match patterns, for-each)

-  untyped literals (arrays, maps, sets, tuples)

The current algorithm is simplistic and will not make every
possible deduction, hence the programmer is required to add
more type annotations than is ideal.  See below for a detailed
description of the algorithm.



ASSIGNABLE RULES
----------------

*(S is the source type, D is the destination type)*

-  if D is an interface type, assignment is permitted if and only if
   S has a class type which implements the interface, or S is an
   interface type which is a superset of D.

-  if S and D are both custom types, assignment is permitted if and
   only if they are the exact same type (unless a previous rule applies).

*(from here on, don't care if S and D are custom types)*

-  if S and D are the same simple type, assignment is permitted.

-  if S and D are both arrays, check whether the element type of S
   is assignable to the element type of D.

-  if S and D are both maps or both sets, check whether the key and
   value types of S are assignable to the key and value types of D.

-  if S and D are both tuples or both classes, check whether S and D
   have the same number of elements, and whether each element of S
   is assignable to the corresponding element of D.

-  if S and D are both unions, check whether D has same number of tags
   as S, and each tag of S has the same name PLUS the datum is assignable
   to the corresponding tag of D.

-  if S and D are both enums, check whether D has same number of tags as
   S, and each tag of S has the same name as the corresponding tag of D.

-  if S and D are both functions, check whether S and D have the same
   number of parameters, the types of each parameter are *mutually*
   assignable, and return types of S and D are *mutually* assignable.

-  otherwise NO assignment is permitted.



CASTABLE RULES
--------------

*(S is the source type, D is the destination type)*

*(these rules ignore whether S and D are plain or custom types)*

-  if S and D are the same simple type, casting is permitted.

-  if S and D are both arrays, check whether the element type of S
   is weakly castable to the element type of D.

-  if S and D are both maps or both sets, check whether the key and
   value types of S are weakly castable to the key and value types of D.

-  if S and D are both tuples or both classes, check whether S and D
   have the same number of elements, and whether each element of S
   is weakly castable to the corresponding element of D.

-  if S and D are both unions, check whether D has the same number of
   tags as S, and each datum S is weakly castable to the corresponding
   datum of D.

-  if S and D are both enums, check whether D has same number of tags
   as S.

-  if S and D are both functions, check whether S is assignable to D.
   This rule is deliberately more restrictive than for other types.

-  otherwise NO casting is permitted.



PROGRAM-WIDE DEDUCTION
----------------------

1. perform type deduction on each module.  Modules deeper in the
   dependency chain are done before modules higher up.  The main
   program's module is always done last.

2. all global definitions in a module are arranged into an order.
   Files specified later in a module occur before files specified
   earlier the same module.  In a file, variables and constants
   occur in definition order (earliest first), followed by all
   functions and methods in reverse order of appearance, followed
   by all executable expressions in order of appearance.

   NOTE: the reverse order for functions and methods is to promote
         a coding style where the most significant ones appear in a
         file before the less significant ones.  In other words, a
         function usually appears after the one(s) which call it.

3. all global definitions (vars, constants, functions, methods) in
   a module are examined and a definition is created for each one.
   When the type is fully known, it is assigned to the definition,
   otherwise the definition is left without a type (for now).

4. visit all global functions and methods and perform a partial
   type deduction, and finalize their return type and the types of
   each parameter (i.e. the type of the function itself).
   If the return type could not be deduced, it is assumed to be `void`.
   If a parameter type could not be deduced, a compiler error occurs.
   After this step, all global functions have a valid type.

5. visit all global variables and constants in the module, fully
   deduce their type, and evaluate their value into something
   which the interpreter or native compiler can directly use.
   After this step, all global variables have a valid type.

6. compile the body of all global functions and methods, deducing any
   types that have not yet been deduced, and perform full type checking
   of all nodes in the code tree.

7. compile the execution expressions with full type deduction.



DEDUCTION IN A FUNCTION
-----------------------

TODO
