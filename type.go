// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

// import "fmt"
import "strings"

type BaseType int

const (
	TYP_Void BaseType = iota

	// TYP_Mystery means the variable/parameter/etc has a type
	// like "@T" (beginning with the at sign).  It signifies
	// that the real type could be anything, like a black box,
	// and there is not much you can do with it except pass it
	// around.
	//
	// Note that a Value should never have this type
	TYP_Mystery

	TYP_Int
	TYP_Float
	TYP_String
	TYP_Tuple
	TYP_Union
	TYP_Enum

	// Note that a Value should never be one of these two
	TYP_Generic
	TYP_Interface

	TYP_Array
	TYP_ByteVec
	TYP_Map
	TYP_Set
	TYP_Class
	TYP_Function

	// TYP_Dummy represents types which are not yet processed
	TYP_Dummy
)

type Type struct {
	base BaseType

	// this is "" for plain types, or a name for custom types
	custom string

	// this is element type for arrays and maps, key type for sets,
	// return type for functions.
	// for TYP_Generic this is the template, like (array @T).
	sub *Type

	// parameters of a function, fields of a tuple, tags of an enum.
	// for interfaces, this is the method signatures, but the "self"
	// parameter in the type is not used (just void).
	// for TYP_Generic, these are the type parameters.
	param []ParamInfo

	// all methods defined for this type.
	// each Value is a TYP_Function which can be called like any other
	// func, though it expects the receiver as the first parameter.
	methods map[string]*Value

	// a nicer name for error messages (mainly for instantiated
	// generic types).
	nice_name string

	// for custom types and generic reconstructed types, this is
	// the base type.  For arrays and maps, it is the fake "array_parent"
	// and "map_parent" type.  This is only used for finding methods.
	parent *Type

	// for TYP_Tuple type, indicates that each field has a name
	named_fields bool

	// this is only used for a globally defined generic function.
	// base will == TYP_Function, and at least one parameter will
	// be (partially) generic.  map keys are the generic type
	// name, like "@T".
	gen_types map[string]*Type

	// private field for HasGeneric() method.
	_has_generic int

	// private field for MapAccessType() method.
	_map_access *Type

	// private field for GetBaseForm() method.
	_base_form *Type

	// the type definition failed to parse
	failed bool
}

type ParamInfo struct {
	// name of parameter or custom tuple field.
	// in type specifiers, like "(fun X Y)", this is not used.
	name string
	ty   *Type
}

const (
	// BTYP_Bool is a fake base-type representing 'bool'
	BTYP_Bool BaseType = 0x83

	// BTYP_Char is a fake base-type representing 'char'
	BTYP_Char BaseType = 0x84
)

var (
	void_type    *Type
	int_type     *Type
	str_type     *Type
	float_type   *Type
	bytevec_type *Type

	// these two are defined in the prelude
	bool_type *Type
	char_type *Type

	// these fake types are ONLY used to hold/lookup methods
	array_parent *Type
	map_parent *Type
	set_parent *Type
	union_parent *Type
	enum_parent *Type
	tuple_parent *Type
	class_parent *Type

	// these UNFINISHED_XXX types are *ONLY* for use in a Value,
	// since a lot of code breaks if Value.ty is nil.
	// elsewhere, such as in Closure and GlobalDef, ty == nil is
	// used to signify that the type is not yet known.
	UNFINISHED_FUNC *Type
	UNFINISHED_VAR  *Type
)

var gen_type_state struct {
	use    bool // referring to one is allowed
	create bool // creation of generic type is allowed

	types map[string]*Type // current generic types
}

//----------------------------------------------------------------------

func SetupTypes() {
	void_type = NewType(TYP_Void)
	int_type = NewType(TYP_Int)
	str_type = NewType(TYP_String)
	float_type = NewType(TYP_Float)
	bytevec_type = NewType(TYP_ByteVec)

	array_parent = NewType(TYP_Array)
	map_parent = NewType(TYP_Map)
	set_parent = NewType(TYP_Set)
	union_parent = NewType(TYP_Union)
	enum_parent = NewType(TYP_Enum)
	tuple_parent = NewType(TYP_Tuple)
	class_parent = NewType(TYP_Class)

	UNFINISHED_FUNC = NewType(TYP_Function)
	UNFINISHED_FUNC.custom = "#UNFINISHED-FUNC#"
	UNFINISHED_FUNC.sub = void_type

	UNFINISHED_VAR = NewType(TYP_Void)
	UNFINISHED_VAR.custom = "#UNFINISHED-VAR#"
}

func NewType(base BaseType) *Type {
	ty := new(Type)
	ty.base = base
	ty.param = make([]ParamInfo, 0)
	ty.methods = make(map[string]*Value)
	return ty
}

func PurgeFailedTypes() {
	for name, ty := range context.types {
		if ty.failed {
			context.RemoveType(name)
		}
	}
}

func PurgeFailedMethods() {
	for _, ty := range context.types {
		for name, loc := range ty.methods {
			if loc.Clos.failed {
				ty.DeleteMethod(name)
			}
		}
	}
}

func (base BaseType) String() string {
	switch base {
	case TYP_Void:
		return "<Void>"
	case TYP_Mystery:
		return "<Mystery>"
	case TYP_Int:
		return "<Int>"
	case TYP_Float:
		return "<Float>"
	case TYP_String:
		return "<Str>"
	case TYP_Tuple:
		return "<Tuple>"
	case TYP_Union:
		return "<Union>"
	case TYP_Enum:
		return "<Enum>"
	case TYP_Generic:
		return "<Generic>"
	case TYP_Interface:
		return "<Interface>"
	case TYP_Array:
		return "<Array>"
	case TYP_ByteVec:
		return "<ByteVec>"
	case TYP_Map:
		return "<Map>"
	case TYP_Set:
		return "<Set>"
	case TYP_Class:
		return "<Class>"
	case TYP_Function:
		return "<Function>"
	case TYP_Dummy:
		return "!UNPARSED!"
	default:
		return "!!!INVALID!!!"
	}
}

func ParseType(t *Token) (*Type, cmError) {
	if t.Match("void") {
		return void_type, OKAY
	}

	if t.Match("int") {
		return int_type, OKAY
	}

	if t.Match("str") {
		return str_type, OKAY
	}

	if t.Match("float") {
		return float_type, OKAY
	}

	if t.Match("byte-vec") {
		return bytevec_type, OKAY
	}

	// check for a generic name
	if t.IsGeneric() {
		if !gen_type_state.use {
			PostError("cannot use generic type '%s' in that context", t.Str)
			return nil, FAILED
		}

		ty := gen_type_state.types[t.Str]
		if ty != nil {
			return ty, OKAY
		}

		if !gen_type_state.create {
			PostError("generic type '%s' does not appear in parameters", t.Str)
			return nil, FAILED
		}

		ty = NewType(TYP_Mystery)
		ty.custom = t.Str

		gen_type_state.types[t.Str] = ty
		return ty, OKAY
	}

	// check for user defined types
	if t.Kind == TOK_Name {
		user := N_GetType(t.Str, t.Module)
		if user == nil {
			PostError("unknown type '%s'", t.Str)
			return nil, FAILED
		}
		if user.base == TYP_Generic {
			PostError("missing parameters for generic type: %s", t.Str)
			return nil, FAILED
		}

		return user, OKAY
	}

	if t.Kind != TOK_Expr || len(t.Children) < 2 {
		PostError("malformed type, got: %s", t.String())
		return nil, FAILED
	}

	return ParseCompoundType(t.Children)
}

func ParseCompoundType(children []*Token) (*Type, cmError) {
	head := children[0]

	// handle user-created generic types
	if head.Kind == TOK_Name {
		user := N_GetType(head.Str, head.Module)
		if user != nil {
			if user.base == TYP_Generic {
				return ParseGenericInstance(user, children)
			} else {
				PostError("strange usage of custom type: %s", head.Str)
				return nil, FAILED
			}
		}
	}

	if head.Match("array") {
		if len(children) != 2 {
			PostError("malformed array type")
			return nil, FAILED
		}

		sub, err2 := ParseType(children[1])
		if err2 != OKAY {
			return nil, FAILED
		}

		if sub.base == TYP_Void {
			PostError("array elements cannot be void")
			return nil, FAILED
		}

		res := NewType(TYP_Array)
		res.sub = sub
		res.parent = array_parent

		return res, OKAY
	}

	if head.Match("map") {
		if len(children) != 3 {
			PostError("malformed map type")
			return nil, FAILED
		}

		key_type, err2 := ParseType(children[1])
		if err2 != OKAY {
			return nil, FAILED
		}
		if key_type.base == TYP_Void {
			PostError("map keys cannot be void")
			return nil, FAILED
		}

		val_type, err2 := ParseType(children[2])
		if err2 != OKAY {
			return nil, FAILED
		}
		if val_type.base == TYP_Void {
			PostError("map elements cannot be void")
			return nil, FAILED
		}

		res := NewType(TYP_Map)
		res.sub = val_type
		res.parent = map_parent
		res.AddParam("", key_type)

		return res, OKAY
	}

	if head.Match("set") {
		if len(children) != 2 {
			PostError("malformed set type")
			return nil, FAILED
		}

		key_type, err2 := ParseType(children[1])
		if err2 != OKAY {
			return nil, FAILED
		}
		if key_type.base == TYP_Void {
			PostError("set keys cannot be void")
			return nil, FAILED
		}

		res := NewType(TYP_Set)
		res.sub = key_type
		res.parent = set_parent

		return res, OKAY
	}

	if head.Match("tuple") {
		children = children[1:]
		count := len(children)

		if count < 1 {
			PostError("malformed tuple type")
			return nil, FAILED
		}

		// when field names are present, there must be a field name
		// before *every* element.
		res := NewType(TYP_Tuple)
		res.named_fields = children[0].IsField()
		res.parent = tuple_parent

		if res.named_fields {
			if count%2 != 0 {
				PostError("malformed tuple type")
				return nil, FAILED
			}

			count /= 2
		}

		// get types for each field
		for i := 0; i < count; i++ {
			var t_type *Token
			field_name := ""

			if res.named_fields {
				t_field := children[i*2]
				t_type = children[i*2+1]

				if !t_field.IsNamedField() {
					PostError("bad field name in tuple type: %s", t_field.Str)
					return nil, FAILED
				}

				field_name = t_field.Str

			} else {
				t_type = children[i]

				if t_type.IsField() {
					PostError("malformed tuple type")
					return nil, FAILED
				}
			}

			sub, err2 := ParseType(t_type)
			if err2 != OKAY {
				return nil, FAILED
			}

			if sub.base == TYP_Void {
				PostError("tuple elements cannot be void")
				return nil, FAILED
			}

			res.AddParam(field_name, sub)
		}

		return res, OKAY
	}

	if head.Match("class") {
		children = children[1:]
		count := len(children)

		if count < 1 || count%2 != 0 {
			PostError("malformed class type")
			return nil, FAILED
		}

		count /= 2

		res := NewType(TYP_Class)
		res.parent = class_parent

		// get types for each field
		for i := 0; i < count; i++ {
			t_field := children[i*2]
			t_type := children[i*2+1]

			if !t_field.IsNamedField() {
				PostError("bad field name in class type: %s", t_field.Str)
				return nil, FAILED
			}

			field_name := t_field.Str

			sub, err2 := ParseType(t_type)
			if err2 != OKAY {
				return nil, FAILED
			}

			if sub.base == TYP_Void {
				PostError("class elements cannot be void")
				return nil, FAILED
			}

			res.AddParam(field_name, sub)
		}

		return res, OKAY
	}

	if head.Match("union") {
		children = children[1:]

		count := len(children)

		if count == 0 || count%2 != 0 {
			PostError("malformed union type")
			return nil, FAILED
		}

		count /= 2

		res := NewType(TYP_Union)
		res.parent = union_parent

		// get each tag keyword and associated data type
		for i := 0; i < count; i++ {
			t_tag := children[i*2]
			t_datum := children[i*2+1]

			if !t_tag.IsTag() {
				PostError("union tag must be identifier beginning with `")
				return nil, FAILED
			}

			sub, err2 := ParseType(t_datum)
			if err2 != OKAY {
				return nil, FAILED
			}

			res.AddParam(t_tag.Str, sub)
		}

		// if we have a void datum, make sure it is the first.
		// [ Value.MakeDefault relies on this ]
		for i := range res.param {
			if res.param[i].ty.base == TYP_Void {
				if i > 0 {
					tmp := res.param[0]
					res.param[0] = res.param[i]
					res.param[i] = tmp
				}
				break
			}
		}

		return res, OKAY
	}

	if head.Match("enum") {
		children = children[1:]

		count := len(children)

		if count == 0 {
			PostError("enum type is missing tags")
			return nil, FAILED
		}

		res := NewType(TYP_Enum)
		res.parent = enum_parent

		// get each tag keyword
		for i := 0; i < count; i++ {
			t_tag := children[i]

			if !t_tag.IsTag() {
				PostError("enum tag must be identifier beginning with `")
				return nil, FAILED
			}

			res.AddParam(t_tag.Str, void_type)
		}

		return res, OKAY
	}

	if head.Match("function") {
		num := len(children)

		if num < 2 {
			PostError("malformed function type")
			return nil, FAILED
		}

		// the return type is last
		sub, err2 := ParseType(children[num-1])
		if err2 != OKAY {
			return nil, FAILED
		}

		res := NewType(TYP_Function)
		res.sub = sub

		// get types for each parameter
		for i := 1; i < num-1; i++ {
			par_type, err2 := ParseType(children[i])
			if err2 != OKAY {
				return nil, FAILED
			}

			if par_type.base == TYP_Void {
				PostError("parameters cannot be void")
				return nil, FAILED
			}

			res.AddParam("", par_type)
		}

		return res, OKAY
	}

	if head.Kind == TOK_Name {
		PostError("unknown type: %s", head.Str)
		return nil, FAILED
	} else {
		PostError("malformed type, got: %s", head.String())
		return nil, FAILED
	}
}

func ParseGenericInstance(user *Type, children []*Token) (*Type, cmError) {
	want_num := len(user.param)

	if len(children)-1 != want_num {
		PostError("wrong number of type parameters for %s, wanted %d, got %d",
			user.custom, want_num, len(children)-1)
		return nil, FAILED
	}

	// get the actual types
	actual_types := make(map[string]*Type)

	for i := 0; i < want_num; i++ {
		name := user.param[i].name

		sub, err := ParseType(children[1+i])
		if err != OKAY {
			return nil, FAILED
		}
		actual_types[name] = sub
	}

	res, err2 := user.sub.GenericReconstruct(actual_types)
	if err2 != OKAY {
		return nil, FAILED
	}

/* DEBUG
Print("GenericReconstruct TYPE %s\n", user.String())
Print("----------------------> %s\n", res.String())
*/
	res.parent = user

	// construct a nicer name for error messages
	res.nice_name = user.sub.GenericNiceName(actual_types)

	return res, OKAY
}

func MethodParentType(name string) *Type {
	// NOTE: this is only used for methods in the prelude
	if !doing_prelude {
		return nil
	}

	switch name {
	case "int": return int_type
	case "str": return str_type
	case "float": return float_type
	case "byte-vec": return bytevec_type

	case "array": return array_parent
	case "map": return map_parent
	case "set": return set_parent
	case "class": return class_parent
	case "union": return union_parent
	case "enum": return enum_parent
	case "tuple": return tuple_parent

	default: return nil
	}
}

func IsTypeSpec(t *Token) bool {
	if t.Module == "" {
		if t.Match("void") || t.Match("int") ||
			t.Match("float") || t.Match("str") ||
			t.Match("byte-vec") {
			return true
		}
	}

	if t.Kind == TOK_Name {
		return N_HasType(t.Str, t.Module)
	}

	if t.Kind == TOK_Expr && len(t.Children) >= 1 {
		head := t.Children[0]

		if head.Module == "" {
			if  head.Match("array") || head.Match("map") || head.Match("set") ||
				head.Match("tuple") || head.Match("class") ||
				head.Match("union") || head.Match("enum") ||
				head.Match("function") {

				return true
			}
		}

		if head.Kind == TOK_Name && N_HasType(head.Str, head.Module) {
			return true
		}
	}

	return false
}

func (ty *Type) String() string {
	if ty == nil {
		return "(nil)"
		// panic("nil given to Type.String()")
	}

	if ty.nice_name != "" {
		return ty.nice_name
	}

	// this handles TYP_Interface and TYP_Generic too
	if ty.custom != "" {
		return ty.custom
	}

	switch ty.base {
	case TYP_Void:
		return "void"

	case TYP_Int:
		return "int"

	case TYP_String:
		return "str"

	case TYP_Float:
		return "float"

	case TYP_Array:
		return "(array " + ty.sub.String() + ")"

	case TYP_ByteVec:
		return "byte-vec"

	case TYP_Map:
		return "(map " + ty.KeyType().String() + " " + ty.sub.String() + ")"

	case TYP_Set:
		return "(set " + ty.sub.String() + ")"

	case TYP_Class:
		s := "(class"
		for _, par := range ty.param {
			s = s + " " + par.ty.String()
		}
		return s + ")"

	case TYP_Tuple:
		s := "(tuple"
		for _, par := range ty.param {
			s = s + " " + par.ty.String()
		}
		return s + ")"

	case TYP_Union:
		s := "(union"
		for _, par := range ty.param {
			s = s + " " + par.name
			s = s + " " + par.ty.String()
		}
		return s + ")"

	case TYP_Enum:
		s := "(enum"
		for _, par := range ty.param {
			s = s + " " + par.name
		}
		return s + ")"

	case TYP_Function:
		s := "(function "

		for _, par := range ty.param {
			s = s + par.ty.String() + " "
		}

		if ty.sub == nil {
			return s + "!!!UNKNOWN-RETURN!!! )"
		}

		return s + ty.sub.String() + ")"

	default:
		return "!!BAD-TYPE!!"
	}
}

func (ty *Type) Replace(other *Type) {
	// NOTE: we do not overwrite the custom name

	ty.base = other.base
	ty.sub = other.sub
	ty.named_fields = other.named_fields
	ty.param = other.param

// THIS IS NO GOOD
//--	ty.methods = other.methods
}

func (ty *Type) AddParam(name string, par_ty *Type) {
	ty.param = append(ty.param, ParamInfo{name: name, ty: par_ty})
}

func (ty *Type) FindParam(name string) int {
	for i := range ty.param {
		if ty.param[i].name == name {
			return i
		}
	}

	// not found
	return -1
}

func (ty *Type) KeyType() *Type {
	if ty.base == TYP_Set {
		return ty.sub
	}
	return ty.param[0].ty
}

func (ty *Type) MapAccessType() *Type {
	if ty._map_access == nil {
		ty._map_access = NewOptionalType(ty.sub)
	}

	return ty._map_access
}

func (ty *Type) GetBaseForm() *Type {
	if ty.custom == "" {
		return ty
	}

	// NOTE: could possibly use ty.parent here, but I'm not sure
	//       if that would sometimes muck up (e.g. get one of the
	//       fake xxx_parent types).

	if ty._base_form == nil {
		base := NewType(ty.base)

		base.sub = ty.sub
		base.named_fields = ty.named_fields

		for i := range ty.param {
			base.AddParam(ty.param[i].name, ty.param[i].ty)
		}

		ty._base_form = base
	}

	return ty._base_form
}

func (ty *Type) CanUnwrap() bool {
	if ty.base != TYP_Union {
		return false
	}
	if len(ty.param) != 2 {
		return false
	}
	if ty.param[1].ty.base == TYP_Void {
		return false
	}

	// check for Opt and Result
	switch ty.param[1].name {
	case "`VAL", "`OK":
		return true

	default:
		return false
	}
}

//----------------------------------------------------------------------

func GenericTypeBegin() {
	gen_type_state.use = true
	gen_type_state.create = true

	gen_type_state.types = make(map[string]*Type)
}

func GenericTypeCollect() map[string]*Type {
	// cannot create new ones after this call
	gen_type_state.create = false

	if len(gen_type_state.types) > 0 {
		return gen_type_state.types
	}
	return nil
}

func GenericTypeFinish() {
	gen_type_state.use = false
	gen_type_state.create = false
	gen_type_state.types = nil
}

func (ty *Type) HasGeneric() bool {
	if ty._has_generic < 0 {
		return false
	}
	if ty._has_generic > 1 {
		return true
	}

	// perform slow test
	seen := make(map[*Type]bool)

	result := ty.HasGenericWorker(seen)

	if result {
		ty._has_generic = 1
	} else {
		ty._has_generic = -1
	}

	return result
}


func (ty *Type) HasGenericWorker(seen map[*Type]bool) bool {
	if ty.base == TYP_Mystery {
		return true
	}

	// don't visit types we already have visited.
	// returning false here does not mean the final result
	// is false -- another part may still have a generic.
	if seen[ty] {
		return false
	}

	seen[ty] = true

	if ty.sub != nil && ty.sub.HasGenericWorker(seen) {
		return true
	}

	for _, par := range ty.param {
		if par.ty.HasGenericWorker(seen) {
			return true
		}
	}

	return false
}

//----------------------------------------------------------------------

func (ty *Type) MethodExists(name string) bool {
	_, ok := ty.methods[name]
	return ok
}

func (ty *Type) CreateMethod(name string) *Value {
	loc := ty.GetMethod(name)

	if loc == nil {
		// create a new memory location for it
		loc = new(Value)
		loc.MakeVoid()

		ty.SetMethod(name, loc)
	}

	return loc
}

func (ty *Type) GetMethod(name string) *Value {
	return ty.methods[name]
}

func (ty *Type) SetMethod(name string, loc *Value) {
	ty.methods[name] = loc
}

func (ty *Type) DeleteMethod(name string) {
	delete(ty.methods, name)
}

func (ty *Type) AddInterfaceMethod(name string, sig *Type) cmError {
	// allow a duplicate name ONLY if signature is the same
	idx := ty.FindParam(name)
	if idx >= 0 {
		if !sig.Equal(ty.param[idx].ty) {
			PostError("bad interface method: '%s' exists with different type", name)
			return FAILED
		}
	}

	ty.AddParam(name, sig)
	return OKAY
}

func (ty *Type) HasInterfaceMethod(name string, sig *Type) bool {
	if ty.base == TYP_Class {
		loc := ty.methods[name]
		if loc == nil {
			return false
		}
		cur := loc.ty

		if cur == nil { panic("nil method type") }
		if cur.base != TYP_Function { panic("weird method type") }

		// a true method signature has a real type for "self", but
		// interface methods lack it, so we must do our own check.

		if !sig.sub.Equal(cur.sub) {
			return false
		}

		if len(sig.param) != len(cur.param) {
			return false
		}

		for i := 1; i < len(sig.param); i++ {
			if !sig.param[i].ty.Equal(cur.param[i].ty) {
				return false
			}
		}

		return true

	} else if ty.base == TYP_Interface {
		idx := ty.FindParam(name)
		if idx < 0 {
			return false
		}
		return sig.Equal(ty.param[idx].ty)

	} else {
		return false
	}
}

func (ty *Type) ImplementsInterface(ifc *Type) bool {
	// Note: HasInterfaceMethod will check that ty is a class or interface

	for _, par := range ifc.param {
		if !ty.HasInterfaceMethod(par.name, par.ty) {
			return false
		}
	}

	return true
}

func NewOptionalType(sub_type *Type) *Type {
	ty := NewType(TYP_Union)

	ty.AddParam("`NONE", void_type)
	ty.AddParam("`VAL", sub_type)
	ty.parent = union_parent

	return ty
}

//----------------------------------------------------------------------

func (ty *Type) Equal(other *Type) bool {
	// TYPE_RULES.md calls this "mutually assignable".

	return ty.AssignableTo(other) && other.AssignableTo(ty)
}

func (src *Type) AssignableTo(dest *Type) bool {
	if src == nil || dest == nil {
		panic("AssignableTo with nil type")
	}

	if src == dest {
		return true
	}

	// interface handling
	if dest.base == TYP_Interface {
		return src.ImplementsInterface(dest)
	}

	if src.base != dest.base {
		return false
	}

	// if both types are custom types, they must be the exact same one
	if src.custom != "" && dest.custom != "" {
		return src.custom == dest.custom
	}

	// functions are deliberately strict
	if src.base == TYP_Function {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if !src.param[i].ty.Equal(dest.param[i].ty) {
				return false
			}
		}

		// check return type
		return src.sub.Equal(dest.sub)
	}

	// this checks array/map elements and set keys
	if src.sub != nil {
		if !src.sub.AssignableTo(dest.sub) {
			return false
		}
	}

	// for maps, check that the key type is assignable
	if src.base == TYP_Map {
		if !src.KeyType().AssignableTo(dest.KeyType()) {
			return false
		}
	}

	// for tuples and classes, check all fields are assignable
	if src.base == TYP_Tuple || src.base == TYP_Class {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if !src.param[i].ty.AssignableTo(dest.param[i].ty) {
				return false
			}
		}
	}

	if src.base == TYP_Union {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if src.param[i].name != dest.param[i].name {
				return false
			}
			if !src.param[i].ty.AssignableTo(dest.param[i].ty) {
				return false
			}
		}
	}

	if src.base == TYP_Enum {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if src.param[i].name != dest.param[i].name {
				return false
			}
		}
	}

	return true
}

func (src *Type) CastableTo(dest *Type) bool {
	if src == nil || dest == nil {
		panic("CastableTo with nil type")
	}

	// allow casting anything to void
	if dest.base == TYP_Void {
		return true
	}

	return src.WeaklyCastableTo(dest)
}

func (src *Type) WeaklyCastableTo(dest *Type) bool {
	if src == dest {
		return true
	}

	// interface handling
	if dest.base == TYP_Interface {
		return src.ImplementsInterface(dest)
	}

	if src.base != dest.base {
		return false
	}

	// functions are deliberately more restrictive than other casts
	if src.base == TYP_Function {
		return src.AssignableTo(dest)
	}

	// this checks array/map elements and set keys
	if src.sub != nil {
		if !src.sub.WeaklyCastableTo(dest.sub) {
			return false
		}
	}

	// for maps, check that the key type is castable
	if src.base == TYP_Map {
		if !src.KeyType().WeaklyCastableTo(dest.KeyType()) {
			return false
		}
	}

	// for tuples and classes, check all fields are castable
	if src.base == TYP_Tuple || src.base == TYP_Class {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if !src.param[i].ty.WeaklyCastableTo(dest.param[i].ty) {
				return false
			}
		}
	}

	if src.base == TYP_Union {
		if len(src.param) != len(dest.param) {
			return false
		}

		for i := range src.param {
			if !src.param[i].ty.WeaklyCastableTo(dest.param[i].ty) {
				return false
			}
		}
	}

	if src.base == TYP_Enum {
		if len(src.param) != len(dest.param) {
			return false
		}
	}

	return true
}

func GenericAssignableTo(src, dest *Type, par_name string, gen_map map[string]*Type) cmError {
	if src == nil || dest == nil {
		panic("GenericAssignableTo with nil type")
	}

	// primary logic for generic types -- allow matching any source type,
	// but check that multiple sources are consistent.
	if dest.base == TYP_Mystery {
		d_actual := gen_map[dest.custom]

		if d_actual != nil && d_actual != src {
			PostError("mismatch on generic type '%s': got %s and %s",
				dest.custom, d_actual.String(), src.String())
			return FAILED
		}

		gen_map[dest.custom] = src
		return OKAY
	}

	if src.base != dest.base {
		goto mismatch
	}

	// if both types are custom types, they must be the exact same one
	if src.custom != "" && dest.custom != "" {
		if src.custom == dest.custom {
			return OKAY
		}
		goto mismatch
	}

	// this checks array/map elements and set keys
	// for functions, check result type is generically assignable.
	if src.sub != nil {
		if GenericAssignableTo(src.sub, dest.sub, par_name, gen_map) != OKAY {
			return FAILED
		}
	}

	// for maps, check key type is generically assignable.
	if src.base == TYP_Map {
		if GenericAssignableTo(src.KeyType(), dest.KeyType(), par_name, gen_map) != OKAY {
			return FAILED
		}
	}

	// for tuples and classes, check all fields are assignable.
	// for functions, check parameters are generically assignable.
	if src.base == TYP_Tuple || src.base == TYP_Class || src.base == TYP_Function {
		if len(src.param) != len(dest.param) {
			goto mismatch
		}

		for i := range src.param {
			if GenericAssignableTo(src.param[i].ty, dest.param[i].ty, par_name, gen_map) != OKAY {
				return FAILED
			}
		}
	}

	if src.base == TYP_Union {
		if len(src.param) != len(dest.param) {
			goto mismatch
		}

		for i := range src.param {
			if src.param[i].name != dest.param[i].name {
				goto mismatch
			}
			if GenericAssignableTo(src.param[i].ty, dest.param[i].ty, par_name, gen_map) != OKAY {
				return FAILED
			}
		}
	}

	if src.base == TYP_Enum {
		if len(src.param) != len(dest.param) {
			goto mismatch
		}

		for i := range src.param {
			if src.param[i].name != dest.param[i].name {
				goto mismatch
			}
		}
	}

	return OKAY

mismatch:
	PostError("type mismatch on parameter '%s': wanted %s, got %s",
				par_name, dest.String(), src.String())
	return FAILED
}

// GenericReconstruct takes a generic-ish type, like (array @T), and
// constructs a concrete type which replaces the generic place-holders
// with the actual types in 'gen_map'.
func (ty *Type) GenericReconstruct(gen_map map[string]*Type) (*Type, cmError) {
	var err2 cmError

	if !ty.HasGeneric() {
		return ty, OKAY
	}

	if ty.base == TYP_Mystery {
		if gen_map[ty.custom] == nil {
			PostError("could not reconstruct generic type '%s'", ty.custom)
			return nil, FAILED
		}
		return gen_map[ty.custom], OKAY
	}

	new_ty := NewType(ty.base)
	new_ty.named_fields = ty.named_fields

	// !! FIXME review this, is it really correct?
	new_ty.parent = ty

	if ty.sub != nil {
		new_ty.sub, err2 = ty.sub.GenericReconstruct(gen_map)
		if err2 != OKAY {
			return nil, FAILED
		}
	}

	for _, par := range ty.param {
		var new_par_ty *Type
		new_par_ty, err2 = par.ty.GenericReconstruct(gen_map)
		if err2 != OKAY {
			return nil, FAILED
		}
		new_ty.AddParam(par.name, new_par_ty)
	}

	return new_ty, OKAY
}

func (ty *Type) GenericNiceName(gen_map map[string]*Type) string {
	s := "(" + ty.custom

	for _, par := range ty.param {
		sub := gen_map[par.name]
		sub_name := "???"

		// create a simplified (shallow) view of the types
		if sub != nil {
			if sub.custom != "" {
				sub_name = sub.custom
			} else {
				sub_name = sub.base.String()
				sub_name = strings.ToLower(sub_name)
				sub_name = strings.Map(func (r rune) rune {
					if r == '<' || r == '>' {
						return -1 // remove it
					} else {
						return r
					}
				}, sub_name)
			}
		}

		s = s + " " + sub_name
	}

	return s + ")"
}

//----------------------------------------------------------------------

// Defaultable tests whether the given type has a default value.
// The simple types (like int and str) always do, unions only if
// they have a void datum, tuples only if all fields have a default.
// Functions and class objects never have default values.  For
// arrays, maps sets, the default is an new empty container.
func (ty *Type) Defaultable() bool {
	switch ty.base {
	case TYP_Void, TYP_Int, TYP_Float, TYP_String, TYP_Enum:
		return true

	case TYP_Array, TYP_ByteVec, TYP_Map, TYP_Set:
		return true

	case TYP_Union:
		for _, par := range ty.param {
			if par.ty.base == TYP_Void {
				return true
			}
		}
		return false

	case TYP_Tuple:
		for _, par := range ty.param {
			if !par.ty.Defaultable() {
				return false
			}
		}
		return true

	default:
		return false
	}
}

// Unconstructable detects whether this type cannot ever be
// constructed (due to an unbreakable cyclic reference).
func (ty *Type) Unconstructable() bool {
	seen := make(map[*Type]bool)

	return !ty.canConstruct(seen)
}

func (T *Type) canConstruct(seen map[*Type]bool) bool {
	// only tuples and unions can form "unusable" cyclic refs.
	// for example, new arrays and maps can be empty.
	if !(T.base == TYP_Tuple || T.base == TYP_Union) {
		return true
	}

	// if we reach an already visited type, it means the current
	// "path" from original type to this one has a cycle in it
	// and hence is unusable.
	if seen[T] {
		return false
	}

	// recreate the seen map with new type in it
	new_seen := make(map[*Type]bool)
	for S, _ := range seen {
		new_seen[S] = true
	}
	new_seen[T] = true

	// for unions we only need ONE of its tags to be constructable.
	if T.base == TYP_Union {
		for _, par := range T.param {
			if par.ty.canConstruct(new_seen) {
				return true
			}
		}
		return false
	}

	// for tuples we require ALL fields to be constructable.
	if T.base == TYP_Tuple {
		for _, par := range T.param {
			if !par.ty.canConstruct(new_seen) {
				return false
			}
		}
	}

	return true
}
