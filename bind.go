// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

//import "fmt"

type LocalVar struct {
	name     string
	ty       *Type
	offset   int32
	external bool
	mutable  bool
	owner    *Closure
}

type LocalGroup struct {
	vars map[string]*LocalVar

	parent *LocalGroup
}

func (group *LocalGroup) Size() int {
	return len(group.vars)
}

//----------------------------------------------------------------------

// BindClosure analyses a code tree and creates LocalVars for each
// local binding which is created.  It also converts bare TOK_Name
// nodes to ND_Local and ND_Global nodes, adorning them with a
// pointer to the corresponding LocalVar or GlobalDef.
func BindClosure(cl *Closure) cmError {
	cl.locals = nil

	cl.PushLocalGroup()
	defer cl.PopLocalGroup()

	if !cl.is_expr {
		for _, t_par := range cl.t_parameters.Children {
			lvar := cl.AddLocal(t_par.Str, false /* mutable */)
			lvar.ty = t_par.Info.ty

			cl.parameters = append(cl.parameters, lvar)
		}
	}

	if cl.BindNode(cl.t_body) != OKAY {
		return FAILED
	}

	MarkTailCalls(cl.t_body)
	return OKAY
}

func (cl *Closure) BindNode(t *Token) cmError {
	ErrorSetToken(t)

	switch t.Kind {
	case TOK_Name:
		return cl.BindName(t)

	case ND_Literal, ND_Void:
		return OKAY

	case ND_Tuple, ND_Object:
		return cl.BindTupleLiteral(t)

	case ND_Let:
		return cl.BindLet(t)

	case ND_IsTag:
		return cl.BindNode(t.Children[0])

	case ND_Match:
		return cl.BindMatch(t)

	case ND_SetVar:
		// NOTE: we need a local variable to be found and become a
		//       ND_Local node, hence recurse into both nodes.
		return cl.BindChildren(t)

	case ND_Lambda:
		return cl.BindLambda(t)

	case ND_DefVar:
		PostError("cannot use 'let' in that context")

	case ND_DefFunc, ND_DefMethod:
		PostError("cannot define globals within code")

	default:
		// everything else uses a basic pass
		return cl.BindChildren(t)
	}

	return FAILED
}

func (cl *Closure) BindChildren(t *Token) cmError {
	for _, sub := range t.Children {
		if cl.BindNode(sub) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) BindName(t *Token) cmError {
	// ignore field/method names, they are resolved in deduce code
	if t.IsField() {
		return OKAY
	}

	if t.Match("_") {
		PostError("cannot use '%s' in that context", t.Str)
		return FAILED
	}

	name := t.Str

	var lvar *LocalVar

	// find scope of the name.
	// some identifiers from macros need to ignore local scopes.
	if t.Cat != IDC_GlobalDef {
		lvar = cl.LookupLocal(name)
	}

	// a global?
	if lvar == nil {
		gdef := N_GetDef(name, t.Module)
		if gdef == nil {
			if t.Cat == IDC_GlobalDef {
				PostError("unknown global: %s", name)
			} else {
				PostError("unknown identifier: %s", name)
			}
			return FAILED
		}

		t.Kind = ND_Global
		t.Info.gdef = gdef

		if gdef.ty != nil {
			t.Info.ty = gdef.ty
		}
		return OKAY
	}

	// a local
	t.Kind = ND_Local
	t.Info.lvar = lvar

	// if in a parent closure, create an upvar template for it.
	// for sub-sub-lambdas (etc), multiple closures get a template.
	if lvar.owner != cl {
		t.Kind = ND_Upvar

		// since the owner *must* be higher up the chain, we know
		// 'p' *will* eventually reach the owner in this loop.
		for p := cl; p != lvar.owner; p = p.parent {
			if p == nil { panic("weird upvar scope") }
			p.CaptureLocal(lvar)
		}
	}

	return OKAY
}

func (cl *Closure) BindTupleLiteral(t *Token) cmError {
	for _, child := range t.Children {
		t_elem := child
		if child.Kind == ND_TuplePair {
			t_elem = child.Children[1]
		}

		if cl.BindNode(t_elem) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

//----------------------------------------------------------------------

func (cl *Closure) BindLambda(t *Token) cmError {
	t_pars := t.Children[0]
	t_body := t.Children[1]

	debug_name := cl.debug_name+".lam"

	template := CreateClosureStuff(t, t_pars, t_body, debug_name)
	template.parent = cl

	t.Info.lit = new(Value)
	t.Info.lit.MakeUnfinishedFunction(template)

	if BindClosure(template) != OKAY {
		template.failed = true
		return FAILED
	}

	return OKAY
}

func (cl *Closure) BindLet(t *Token) cmError {
	cl.PushLocalGroup()
	defer cl.PopLocalGroup()

	// create a local variable binding and slot
	t_var := t.Children[0]
	name := t_var.Str

	if cl.locals.vars[name] != nil {
		PostError("duplicate var name '%s'", name)
		return FAILED
	}

	// must do expression BEFORE creating the local,
	// so that forms like (let verb verb) work properly.
	if cl.BindNode(t.Children[1]) != OKAY {
		return FAILED
	}

	t_var.Kind = ND_Local
	t_var.Info.lvar = cl.AddLocal(name, t.Info.mutable)

	// use an explicitly given type
	t_var.Info.lvar.ty = t_var.Info.ty

	// do body
	if cl.BindNode(t.Children[2]) != OKAY {
		return FAILED
	}

	return OKAY
}

//----------------------------------------------------------------------

func (cl *Closure) BindMatch(t *Token) cmError {
	t_expr := t.Children[0]
	if cl.BindNode(t_expr) != OKAY {
		return FAILED
	}

	for i := 1; i < len(t.Children); i++ {
		rule := t.Children[i]
		if cl.BindMatchRule(rule) != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func (cl *Closure) BindMatchRule(t *Token) cmError {
	// save this group in the ND_MatchRule token, since
	// CompileMatchRule needs to know all the bindings.
	t.Info.group = cl.PushLocalGroup()
	defer cl.PopLocalGroup()

	if cl.BindMatchPattern(t.Children[0]) != OKAY {
		return FAILED
	}

	for i := 1; i < len(t.Children); i++ {
		if cl.BindNode(t.Children[i]) != OKAY {
			return FAILED
		}
	}
	return OKAY
}

func (cl *Closure) BindMatchPattern(pat *Token) cmError {
	switch pat.Kind {
	case TOK_Name:
		return cl.BindPattern_Name(pat)

	case ND_MatchAll, ND_MatchTags:
		// nothing needed
		return OKAY

	case ND_MatchConsts:
		return cl.BindPattern_Consts(pat)

	case TOK_Data, ND_MatchUnion:
		return cl.BindPattern_common(pat)

	default:
		PostError("cannot bind match pattern: %s", pat.String())
		return FAILED
	}
}

func (cl *Closure) BindPattern_Name(pat *Token) cmError {
	name := pat.Str

	// is the name a global constant?
	def := N_GetDef(name, pat.Module)
	if def != nil {
		if def.mutable {
			PostError("match must be constant, but %s is mutable", name)
			return FAILED
		}

		// create a new node for ND_MatchConsts
		t_child := NewNode(ND_Global, 0)
		t_child.Replace(pat)
		t_child.Kind = ND_Global
		t_child.Info.gdef = def

		if def.ty != nil {
			t_child.Info.ty = def.ty
		}

		pat.Kind = ND_MatchConsts
		pat.Children = make([]*Token, 0)
		pat.Add(t_child)

		return OKAY
	}

	/* name is a new binding */

	if cl.locals.vars[name] != nil {
		PostError("duplicate binding name '%s'", name)
		return FAILED
	}

	lvar := cl.AddLocal(name, false) // not mutable

	pat.Kind = ND_Local
	pat.Info.lvar = lvar

	return OKAY
}

func (cl *Closure) BindPattern_Consts(pat *Token) cmError {
	for _, child := range pat.Children {
		if child.Kind == TOK_Name {
			name := child.Str

			// is the name a global constant?
			def := N_GetDef(name, child.Module)
			if def == nil {
				PostError("unknown global in match pattern: %s", name)
				return FAILED
			}

			if def.mutable {
				PostError("match must be constant, but %s is mutable", name)
				return FAILED
			}

			child.Kind = ND_Global
			child.Info.gdef = def
		}
	}

	return OKAY
}

func (cl *Closure) BindPattern_common(pat *Token) cmError {
	var err2 cmError

	for _, t_sub := range pat.Children {
		if t_sub.IsTag() {
			continue
		}

		if t_sub.Kind == ND_TuplePair {
			err2 = cl.BindMatchPattern(t_sub.Children[1])
		} else {
			err2 = cl.BindMatchPattern(t_sub)
		}

		if err2 != OKAY {
			return FAILED
		}
	}
	return OKAY
}

//----------------------------------------------------------------------

func (cl *Closure) PushLocalGroup() *LocalGroup {
	group := new(LocalGroup)
	group.vars = make(map[string]*LocalVar)
	group.parent = cl.locals

	cl.locals = group

	return group
}

func (cl *Closure) PopLocalGroup() {
	if cl.locals == nil {
		panic("PopLocalGroup without push")
	}

	cl.locals = cl.locals.parent
}

func (cl *Closure) AddLocal(name string, mutable bool) *LocalVar {
	if cl.locals == nil {
		panic("AddLocal when closure has no LocalGroup")
	}

	lvar := new(LocalVar)
	lvar.name = name
	lvar.offset = -77 // determined by compile code
	lvar.mutable = mutable
	lvar.owner = cl

	// the special '_' match-all name is never stored in the group,
	// though its LocalVar will get used in various places.
	if name != "_" {
		cl.locals.vars[name] = lvar
	}

	return lvar
}

func (cl *Closure) LookupLocal(name string) *LocalVar {
	for p := cl; p != nil; p = p.parent {
		for group := p.locals; group != nil; group = group.parent {
			lvar := group.vars[name]

			if lvar != nil {
				// found it.
				return lvar
			}
		}
	}

	// not found
	return nil
}

func (cl *Closure) CaptureLocal(lvar *LocalVar) {
	// when variable is read-only, we don't need it to place it
	// on the heap, instead a newly created closure will capture
	// its VALUE instead.
	if lvar.mutable {
		lvar.external = true
	}

	// already captured here?
	for _, et := range cl.upvar_templates {
		if et.lvar == lvar {
			return
		}
	}

	et := new(UpvarTemplate)

	et.lvar = lvar
	et.index = int32(len(cl.upvar_templates))
	et.offset = -55 // determined in CompileLambda()

	if lvar.owner != cl.parent {
		et.far_up = true
	}

	cl.upvar_templates = append(cl.upvar_templates, et)
}

//----------------------------------------------------------------------

func MarkTailCalls(t *Token) {
	switch t.Kind {
	case ND_FunCall, ND_MethodCall:
		t.Info.tail = true

	case ND_Cast:
		// type casts don't perform any real action, they just tell
		// the compiler that something has a new type, hence we can
		// allow their sub-expression to be a tail call.
		MarkTailCalls(t.Children[0])

	case ND_DefFunc, ND_DefMethod:
		// body of function definition
		MarkTailCalls(t.Children[2])

	case ND_Block:
		// only last element of a sequence can be a tail call
		MarkTailCalls(t.Children[len(t.Children)-1])

		// this is needed in order to support ND_Skip nodes, which
		// can be tail calls when the corresponding junction is.
		// they get marked as tail calls elsewhere.
		if t.Info.junction != nil {
			t.Info.junction.tail = true
		}

	case ND_Let:
		// body of a let expression
		MarkTailCalls(t.Children[2])

	case ND_If:
		// both the then and else bodies can be tail calls
		MarkTailCalls(t.Children[1])
		MarkTailCalls(t.Children[2])

	case ND_Match:
		// bodies of match rules can be tail calls
		for i := 1; i < len(t.Children); i++ {
			rule := t.Children[i]
			body := rule.Children[1]
			MarkTailCalls(body)
		}

	default:
		// everything else: no tail calls are possible.
		//
		// for example, an ND_Array element cannot be a tail call
		// since there will be code AFTER the code computing the
		// element which stores it in the array.
	}
}
