// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

// import "fmt"
// import "math"

type OperatorInfo struct {
	name   string
	method string

	unary       bool
	precedence  int
	right_assoc bool

	// this true for operators like '+' where the result is the
	// same type as the operands, and false for comparison ops.
	// TODO: this is not used ATM, see if it should be....
	same_result bool
}

// these op-codes are only usable in bytecode files
const (
	OP2_INVALID OpCode = 128 + iota

	// boolean
	OP2_BOOL_EQ
	OP2_BOOL_NE

	// integer
	OP2_INT_NEG

	OP2_INT_EQ
	OP2_INT_NE
	OP2_INT_LT
	OP2_INT_LE
	OP2_INT_GT
	OP2_INT_GE

	OP2_INT_ADD
	OP2_INT_SUB
	OP2_INT_MUL
	OP2_INT_DIV
	OP2_INT_REM

	OP2_BIT_NOT
	OP2_BIT_OR
	OP2_BIT_XOR
	OP2_BIT_AND

	OP2_SHIFT_LEFT
	OP2_SHIFT_RIGHT

	// floating point
	OP2_FLOAT_NEG

	OP2_FLOAT_EQ
	OP2_FLOAT_NE
	OP2_FLOAT_LT
	OP2_FLOAT_LE
	OP2_FLOAT_GT
	OP2_FLOAT_GE

	OP2_FLOAT_ADD
	OP2_FLOAT_SUB
	OP2_FLOAT_MUL
	OP2_FLOAT_DIV
	OP2_FLOAT_REM

	// string
	OP2_STR_EQ
	OP2_STR_NE
	OP2_STR_LT
	OP2_STR_LE
	OP2_STR_GT
	OP2_STR_GE
)

// NOTE: to implement unary negation, would need a separate map
var (
	unary_ops  map[string]*OperatorInfo
	binary_ops map[string]*OperatorInfo
)

func SetupOperators() {
	unary_ops = make(map[string]*OperatorInfo)
	binary_ops = make(map[string]*OperatorInfo)

	/* unary operators */

	RegisterOperator(0, "!", ".not")
	RegisterOperator(0, "-", ".neg")
	RegisterOperator(0, "~", ".flip")

	/* binary infix operators */

	// Note t"&&" and "||" are special, they are short-circuiting,
	// and their method name is really a macro in the prelude.
	RegisterOperator(1, "||", "or")
	RegisterOperator(2, "&&", "and")

	RegisterOperator(3, "==", ".eq?")
	RegisterOperator(3, "!=", ".ne?")
	RegisterOperator(4, "<",  ".lt?")
	RegisterOperator(4, "<=", ".le?")
	RegisterOperator(4, ">",  ".gt?")
	RegisterOperator(4, ">=", ".ge?")

	RegisterOperator(5, "+",  ".add")
	RegisterOperator(5, "-",  ".sub")
	RegisterOperator(6, "*",  ".mul")
	RegisterOperator(6, "/",  ".div")
	RegisterOperator(6, "%",  ".rem")
	RegisterOperator(7, "**", ".pow")

	RegisterOperator(5, "|", ".or")
	RegisterOperator(5, "^", ".xor")
	RegisterOperator(6, "&", ".and")

	RegisterOperator(6, "<<", ".shift-left")
	RegisterOperator(6, ">>", ".shift-right")
}

func RegisterOperator(prec int, name, method string) {
	info := new(OperatorInfo)
	info.name = name
	info.method = method

	info.unary = (prec == 0)
	info.precedence = prec
	info.right_assoc = (name == "**")
	info.same_result = (prec < 3 || prec > 4)

	if prec == 0 {
		unary_ops[name] = info
	} else {
		binary_ops[name] = info
	}
}

func IsOperator(s string) bool {
	return binary_ops[s] != nil || unary_ops[s] != nil
}

func GetOperator(s string, unary bool) *OperatorInfo {
	if unary {
		return unary_ops[s]
	} else {
		return binary_ops[s]
	}
}

func IsOperatorToken(t *Token) bool {
	return t.Kind == TOK_Name && IsOperator(t.Str)
}

func HasOperator(t *Token) bool {
	for _, child := range t.Children {
		if child.Kind == TOK_Name && IsOperator(child.Str) {
			return true
		}
	}
	return false
}

func (op *OperatorInfo) IsComparison() bool {
	return op.precedence == 3 || op.precedence == 4
}


//----------------------------------------------------------------------

// OperatorExpand checks if the given token or any child in the
// tree is a math expression, is if so it parses that expression
// into a new one where the operators are replaced by method calls.
//
// [ essentially this is a kind of macro expansion ]
//
// The result is either the input token (possibly modified), or a
// newly created token tree.
func OperatorExpand(tok *Token) (*Token, cmError) {
	// recursively handle children
	if tok.Kind >= TOK_Expr {
		for i := 0; i < len(tok.Children); i++ {
			child := tok.Children[i]

			newchild, err2 := OperatorExpand(child)
			if err2 != OKAY {
				return nil, FAILED
			}

			tok.Children[i] = newchild
		}
	}

	if tok.Kind == TOK_Expr && HasOperator(tok) {
		return OperatorParseExpr(tok)
	} else {
		return tok, OKAY
	}
}

func OperatorParseExpr(t *Token) (*Token, cmError) {
	/* first pass : handle multi-token function calls */

	children := make([]*Token, 0)

	for len(t.Children) > 0 {
		elem := t.Children[0]
		t.Children = t.Children[1:]

		// check for two or more terms in a row, convert to an S-expr
		if !IsOperatorToken(elem) &&
			len(t.Children) > 0 &&
			!IsOperatorToken(t.Children[0]) {

			new_exp := NewNode(TOK_Expr, 0)
			new_exp.LineNum = elem.LineNum
			new_exp.Add(elem)

			// for an identifier followed by `()`, drop the `()`.
			// this mimics the sweet syntax for no-arg function calls.
			if elem.Kind == TOK_Name &&
				(len(t.Children) == 1 ||
				 (len(t.Children) > 1 && IsOperatorToken(t.Children[1]))) &&
				t.Children[0].Kind == TOK_Expr &&
				len(t.Children[0].Children) == 0 {

				t.Children = t.Children[1:]
			}

			for len(t.Children) > 0 && !IsOperatorToken(t.Children[0]) {
				new_exp.Add(t.Children[0])
				t.Children = t.Children[1:]
			}

			elem = new_exp
		}

		children = append(children, elem)
	}

	/* this is Dijkstra's shunting-yard algorithm */

	op_stack := make([]*OperatorInfo, 0)
	term_stack := make([]*Token, 0)

	shunt := func() cmError {
		// the op_stack is never empty when this is called
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0 : len(op_stack)-1]

		// possible??
		if len(term_stack) < 2 {
			PostError("FAILURE AT THE SHUNTING YARD")
			return FAILED
		}

		L := term_stack[len(term_stack)-2]
		R := term_stack[len(term_stack)-1]
		term_stack = term_stack[0 : len(term_stack)-2]

		t_method := NewNode(TOK_Name, 0)
		t_method.Str = op.method
		t_method.LineNum = L.LineNum

		node := NewNode(TOK_Expr, 3)
		node.Children[0] = t_method
		node.Children[1] = L
		node.Children[2] = R
		node.LineNum = L.LineNum

		// mark node as a binary operator
		node.Info.is_operator = true

		term_stack = append(term_stack, node)
		return OKAY
	}

	// saved unary ops for the next (unseen) term
	un_ops := make([]*OperatorInfo, 0)

	seen_term := false

	for _, tok := range children {
		if tok.Kind == TOK_Name && IsOperator(tok.Str) {
			// operator is binary after a term, unary otherwise
			unary := !seen_term

			op := GetOperator(tok.Str, unary)

			if op == nil {
				PostError("bad math expression: unexpected %s operator", tok.Str)
				return nil, FAILED
			}

			if unary {
				un_ops = append(un_ops, op)
				continue
			}

			if len(un_ops) > 0 {
				panic("OOPSIE IN MATH PARSE")
			}

			// shunt existing operators if they have greater precedence
			for len(op_stack) > 0 {
				top := op_stack[len(op_stack)-1]

				if top.precedence > op.precedence ||
					(top.precedence == op.precedence && !top.right_assoc) {

					if shunt() != OKAY {
						return nil, FAILED
					}
					continue
				}

				break
			}

			op_stack = append(op_stack, op)
			seen_term = false

		} else {
			// if not an operator, it must be a term
			seen_term = true

			term := tok

			// apply any saved unary operators
			for len(un_ops) > 0 {
				op := un_ops[len(un_ops)-1]
				un_ops = un_ops[0 : len(un_ops)-1]

				t_method := NewNode(TOK_Name, 0)
				t_method.Str = op.method
				t_method.LineNum = term.LineNum

				node := NewNode(TOK_Expr, 2)
				node.Children[0] = t_method
				node.Children[1] = term
				node.LineNum = term.LineNum

				term = node
			}

			term_stack = append(term_stack, term)
		}
	}

	if !seen_term || len(un_ops) > 0 {
		PostError("bad math expression, missing term after last operator")
		return nil, FAILED
	}

	// handle the remaining operators on stack
	for len(op_stack) > 0 {
		if shunt() != OKAY {
			return nil, FAILED
		}
	}

	if len(term_stack) != 1 {
		PostError("PARSE MATH FAIlURE")
		return nil, FAILED
	}

	// replace input token with root of the expression tree
	return term_stack[0], OKAY
}


//----------------------------------------------------------------------

/* TODO ??

// Compute will perform an operator on the given values,
// storing the result in 'L'.  If it detects an error, like
// divison by zero, then FAILED is returned.
func (op *OperatorInfo) Compute(L, R *Value) cmError {
	code := op.GetOpcode(L.ty)

	switch code {
	case OP_BOOL_NOT:
		L.MakeBool(! L.IsTrue())
		return OKAY

	case OP_BOOL_OR:
		L.MakeBool(L.IsTrue() || R.IsTrue())
		return OKAY

	case OP_BOOL_AND:
		L.MakeBool(L.IsTrue() && R.IsTrue())
		return OKAY

	case OP2_BOOL_EQ:
		L.MakeBool(L.IsTrue() == R.IsTrue())
		return OKAY

	case OP2_BOOL_NE:
		L.MakeBool(L.IsTrue() != R.IsTrue())
		return OKAY

	case OP2_INT_EQ:
		L.MakeBool(L.Int == R.Int)
		return OKAY

	case OP2_INT_NE:
		L.MakeBool(L.Int != R.Int)
		return OKAY

	case OP2_INT_LT:
		L.MakeBool(L.Int < R.Int)
		return OKAY

	case OP2_INT_LE:
		L.MakeBool(L.Int <= R.Int)
		return OKAY

	case OP2_INT_GT:
		L.MakeBool(L.Int > R.Int)
		return OKAY

	case OP2_INT_GE:
		L.MakeBool(L.Int >= R.Int)
		return OKAY

	case OP2_INT_NEG:
		L.MakeInt(0 - L.Int)
		return OKAY

	case OP2_INT_ADD:
		L.MakeInt(L.Int + R.Int)
		return OKAY

	case OP2_INT_SUB:
		L.MakeInt(L.Int - R.Int)
		return OKAY

	case OP2_INT_MUL:
		L.MakeInt(L.Int * R.Int)
		return OKAY

	case OP2_INT_DIV:
		if R.Int == 0 {
			PostError("division by zero in constant expression")
			return FAILED
		}
		L.MakeInt(L.Int / R.Int)
		return OKAY

	case OP2_INT_REM:
		if R.Int == 0 {
			PostError("division by zero in constant expression")
			return FAILED
		}
		L.MakeInt(L.Int % R.Int)
		return OKAY

	case OP2_BIT_NOT:
		L.MakeInt(^ L.Int)
		return OKAY

	case OP2_BIT_OR:
		L.MakeInt(L.Int | R.Int)
		return OKAY

	case OP2_BIT_XOR:
		L.MakeInt(L.Int ^ R.Int)
		return OKAY

	case OP2_BIT_AND:
		L.MakeInt(L.Int & R.Int)
		return OKAY

	case OP2_SHIFT_LEFT:
		if R.Int > 0 {
			L.MakeInt(L.Int << uint(R.Int))
		}
		if R.Int < 0 {
			L.MakeInt(L.Int >> uint(-R.Int))
		}
		return OKAY

	case OP2_SHIFT_RIGHT:
		if R.Int > 0 {
			L.MakeInt(L.Int >> uint(R.Int))
		}
		if R.Int < 0 {
			L.MakeInt(L.Int << uint(-R.Int))
		}
		return OKAY

	case OP2_FLOAT_EQ:
		L.MakeBool(L.Float == R.Float)
		return OKAY

	case OP2_FLOAT_NE:
		L.MakeBool(L.Float != R.Float)
		return OKAY

	case OP2_FLOAT_LT:
		L.MakeBool(L.Float < R.Float)
		return OKAY

	case OP2_FLOAT_LE:
		L.MakeBool(L.Float <= R.Float)
		return OKAY

	case OP2_FLOAT_GT:
		L.MakeBool(L.Float > R.Float)
		return OKAY

	case OP2_FLOAT_GE:
		L.MakeBool(L.Float >= R.Float)
		return OKAY

	case OP2_FLOAT_NEG:
		L.MakeFloat(0.0 - L.Float)
		return OKAY

	case OP2_FLOAT_ADD:
		L.MakeFloat(L.Float + R.Float)
		return OKAY

	case OP2_FLOAT_SUB:
		L.MakeFloat(L.Float - R.Float)
		return OKAY

	case OP2_FLOAT_MUL:
		L.MakeFloat(L.Float * R.Float)
		return OKAY

	case OP2_FLOAT_DIV:
		if R.Float == 0 {
			PostError("division by zero in constant expression")
			return FAILED
		}
		L.MakeFloat(L.Float / R.Float)
		return OKAY

	case OP2_FLOAT_REM:
		if R.Float == 0 {
			PostError("division by zero in constant expression")
			return FAILED
		}
		L.MakeFloat(math.Mod(L.Float, R.Float))
		return OKAY

	case OP2_STR_EQ:
		L.MakeBool(L.Str == R.Str)
		return OKAY

	case OP2_STR_NE:
		L.MakeBool(L.Str != R.Str)
		return OKAY

	case OP2_STR_LT:
		L.MakeBool(L.Str < R.Str)
		return OKAY

	case OP2_STR_LE:
		L.MakeBool(L.Str <= R.Str)
		return OKAY

	case OP2_STR_GT:
		L.MakeBool(L.Str > R.Str)
		return OKAY

	case OP2_STR_GE:
		L.MakeBool(L.Str >= R.Str)
		return OKAY

	case OP_STR_ADD:
		L.MakeString(L.Str + R.Str)
		return OKAY
	}

	PostError("cannot evaluate '%s' operator in non-code context", op.name)
	return FAILED
}
*/
