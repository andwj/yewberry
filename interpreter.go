// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

// this creates the "assets.go" file using go-bindata:
//go:generate go-bindata -o assets.go -nocompress y_common/

import "os"
// import "fmt"
// import "strings"

import "time"
import "math"
import "math/rand"

var opt_bytecode bool
var opt_native bool

// certain things are only allowed in the prelude
var doing_prelude bool

type GlobalDef struct {
	loc     *Value // memory location for the value
	ty      *Type  // its type [ loc.ty should always mirror this ]
	name    string // its name (for debugging)
	mutable bool   // can be altered?
}

var (
	glob_FALSE *GlobalDef
	glob_TRUE  *GlobalDef
	glob_INAN  *GlobalDef
	glob_NAN   *GlobalDef
	glob_PINF  *GlobalDef
	glob_NINF  *GlobalDef
	glob_EOF   *GlobalDef

	glob_STACK_TRACE *GlobalDef
	glob_MAX_LOOP    *GlobalDef
)

// the unparsed macros of a program.
var raw_macros []*Token

// the unparsed types of a program.
var raw_typedefs []*Token

// the unparsed definitions of a program.
var raw_definitions []*Token

// the unparsed expressions of a program.
var raw_expressions []*Token

// the unfinished functions of a program.
var unfinished_funcs []*UnfinishedFunc

type UnfinishedFunc struct {
	// for normal functions:
	def *GlobalDef

	// for methods:
	obj_type *Type
	method   string

	// common to both:
	loc *Value
}

//----------------------------------------------------------------------

func main() {
	if len(os.Args) >= 2 && os.Args[1] == "-nat" {
		opt_native = true
	}

	ClearErrors()

	InitModules()

	// create a context for the prelude
	// [ which we also use for the REPL ]
	context = NewNamespace()
	prelude = context

	SetupTypes()
	SetupGlobals()
	SetupOperators()
	SetupBuiltins()

	if LoadPrelude() != OKAY {
		Print("Failed to load standard prelude.yb\n")
		ShowAllErrors()
		os.Exit(2)
	}

	SetupRandom()
	SetupENVS()

	status := 0

	if len(os.Args) < 2 {
		SetupARGS([]string{"REPL"})
		status = RunREPL()

	} else if opt_native {
		if len(os.Args) < 3 {
			Print("Missing filename to compile\n")
			os.Exit(2)
		}

		NativeInit()

		status = RunScriptFile(os.Args[2])

		if status == 0 {
			NativeSave()
		}

	} else {
		// remove interpreter itself from args, have code file at start
		SetupARGS(os.Args[1:])
		status = RunScriptFile(os.Args[1])
	}

	os.Exit(status)
}

func SetupGlobals() {
	MakeConstant("+INF", float_type).loc.MakeFloat(math.Inf(1))
	MakeConstant("-INF", float_type).loc.MakeFloat(math.Inf(-1))
	MakeConstant("NAN", float_type).loc.MakeFloat(math.NaN())
}

func SetupRandom() {
	rand.Seed(time.Now().Unix())
}

func SetupARGS(args []string) {
	ARGS := LookupStandardGlobal("ARGS")
	ARGS.loc.VerifyType(TYP_Array, "ARGS")

	for _, s := range args {
		str_val := Value{}
		str_val.MakeString(s)

		ARGS.loc.AppendElem(str_val)
	}
}

func SetupENVS() {
	ENVS := LookupStandardGlobal("ENVS")
	ENVS.loc.VerifyType(TYP_Map, "ENVS")

	for _, env := range os.Environ() {
		key, val := SplitEnvString(env)

		if key != "" {
			var s_key Value
			var s_val Value

			s_key.MakeString(key)
			s_val.MakeString(val)

			ENVS.loc.KeySet(s_key, s_val)
		}
	}
}

func SplitEnvString(env string) (key, val string) {
	// find the '=' between key and value
	for i := 0; i < len(env); i++ {
		if env[i] == '=' {
			return env[0:i], env[i+1:]
		}
	}

	// something weird, ignore it
	return "", ""
}

//----------------------------------------------------------------------

func LoadPrelude() cmError {
	doing_prelude = true

	code_src := NewCodeSource()
	code_src.Add(NewAssetSource("y_common/prelude.yb"))

	defer code_src.Finish()

	if ProcessCode(code_src) != OKAY {
		return FAILED
	}

	doing_prelude = false

	bool_type = N_GetType("bool", "")
	char_type = N_GetType("char", "")

	if bool_type == nil {
		PostError("bool type is not defined in the prelude")
		return FAILED
	}
	if char_type == nil {
		PostError("char type is not defined in the prelude")
		return FAILED
	}

	glob_FALSE = LookupStandardGlobal("FALSE")
	glob_TRUE = LookupStandardGlobal("TRUE")
	glob_INAN = LookupStandardGlobal("INAN")

	glob_NAN = LookupStandardGlobal("NAN")
	glob_PINF = LookupStandardGlobal("+INF")
	glob_NINF = LookupStandardGlobal("-INF")

	glob_EOF = LookupStandardGlobal("EOF")
	glob_STACK_TRACE = LookupStandardGlobal("STACK-TRACE")
	glob_MAX_LOOP = LookupStandardGlobal("MAX-LOOP")

	return OKAY
}

func RunScriptFile(filename string) (status int) {
	// if the script uses the command line editor, this will reset
	// the terminal into a usable state should a panic occur.
	defer func() {
		if editor != nil {
			editor.Close()
		}
	}()

	ClearErrors()

	code_src := NewCodeSource()
	code_src.Add(NewFileSource(filename))

	defer code_src.Finish()

	// create new context for the code file
	context = NewNamespace()

	if ProcessCode(code_src) == OKAY {
		// status zero means Ok
		return 0
	}

	ShowAllErrors()
	return 1
}

func ProcessCode(csrc *CodeSource) cmError {
	raw_macros = make([]*Token, 0)
	raw_typedefs = make([]*Token, 0)
	raw_definitions = make([]*Token, 0)
	raw_expressions = make([]*Token, 0)
	unfinished_funcs = make([]*UnfinishedFunc, 0)

	if ProcessAllFiles(csrc) != OKAY {
		return FAILED
	}

	HandleAllMacros()
	HandleAllTypes()

	if HaveErrors() {
		return FAILED
	}

	PrepareAllGlobals()   // step 3
	DeduceAllFunctions()  // step 4

	EvaluateAllStatics()   // step 5
	CompileAllFunctions()  // step 6

	if HaveErrors() {
		return FAILED
	}

	RunAllExpressions()

	if HaveErrors() {
		return FAILED
	}
	return OKAY
}

func ProcessAllFiles(csrc *CodeSource) cmError {
	var lex *Lexer
	var err error

	for {
		if lex == nil {
			lex, err = csrc.OpenNext()
			if err != nil {
				PostError("%s", err.Error())
				return FAILED
			}
			if lex == nil {
				panic("No code sources?")
			}
		}

		t := lex.Scan()

		ErrorSetToken(t)

		if t.Kind == TOK_ERROR {
			PostError("%s", t.Str)
			// fall into below code, open next file
		}

		if t.Kind == TOK_EOF || t.Kind == TOK_ERROR {
			lex, err = csrc.OpenNext()
			if err != nil {
				PostError("%s", err.Error())
				return FAILED
			}
			if lex == nil {
				// all done, no more sources
				break
			}
		}

		ProcessToken(t)
	}

	if HaveErrors() {
		return FAILED
	}

	return OKAY
}

func ProcessToken(t *Token) cmError {
	ErrorSetToken(t)

	if t.Kind == TOK_Expr {
		if len(t.Children) == 0 {
			PostError("empty expr in ()")
			return FAILED
		}

		head := t.Children[0]

		if head.Match("macro") {
			raw_macros = append(raw_macros, t)
			return OKAY
		}

		if head.Match("type") || head.Match("interface") ||
			head.Match("generic-type") {

			raw_typedefs = append(raw_typedefs, t)
			return OKAY
		}
	}

	// merely remember the definition or expression, to be handled later
	raw_definitions = append(raw_definitions, t)
	return OKAY
}

//----------------------------------------------------------------------

func HandleAllMacros() {
	// create a macro for each one, but don't parse it yet
	for _, tok := range raw_macros {
		if PreliminaryParseMacro(tok) != OKAY {
			// mark the failure to prevent future processing
			// TODO do it a better way?
			tok.Kind = TOK_ERROR
			tok.Str = "cannot parse macro"
		}
	}

	// parse the actual macro definitions
	for _, tok := range raw_macros {
		if tok.Kind != TOK_ERROR {
			ParseMacro(tok.Children)
		}
	}

	// analyse the rule bodies
	for _, mac := range context.macros {
		if !mac.analysed && !mac.failed {
			AnalyseMacro(mac)
		}
	}
}

func PreliminaryParseMacro(t *Token) cmError {
	ErrorSetToken(t)

	children := t.Children

	if len(children) < 2 {
		PostError("bad macro def: missing name")
		return FAILED
	}

	t_name := children[1]

	// allow macro redefinition in the REPL
	if REPL {
		context.RemoveMacro(t_name.Str)
	}

	if doing_prelude && (t_name.Str == "int" || t_name.Str == "str" || t_name.Str == "float") {
		// HACK: in prelude allow "int", "str" and "float" macros
	} else if ValidateDefName(t_name, "macro", 0) != OKAY {
		return FAILED
	}

	mac := NewMacro(t_name.Str)

	context.AddMacro(t_name.Str, mac)

	return OKAY
}

//----------------------------------------------------------------------

func HandleAllTypes() {
	// create a type for each one, but parse them later
	for _, tok := range raw_typedefs {
		if PreliminaryParseType(tok) != OKAY {
			// mark the failure to prevent future processing
			// TODO do it a better way?
			tok.Kind = TOK_ERROR
			tok.Str = "cannot parse type"
		}
	}

	// parse the actual type definitions
	for _, tok := range raw_typedefs {
		if tok.Kind != TOK_ERROR {
			ParseTypeDef(tok)
		}
	}

	// check for uninstantiable cyclic types
	for _, ty := range context.types {
		if !ty.failed {
			if ty.Unconstructable() {
				PostError("type %s cannot be constructed (cyclic refs)", ty.custom)
				ty.failed = true
			}
		}
	}
}

func PreliminaryParseType(t *Token) cmError {
	ErrorSetToken(t)

	children := t.Children

	if len(children) < 2 {
		PostError("bad type def: missing name")
		return FAILED
	}

//	t_head := children[0]
	t_name := children[1]

	if t_name.Kind != TOK_Name {
		PostError("bad type def: name is not an identifier")
		return FAILED
	}

	// allow types to be redefined in the REPL
	if REPL {
		context.RemoveType(t_name.Str)
	}

	if ValidateDefName(t_name, "type", 0) != OKAY {
		return FAILED
	}

	ty := NewType(TYP_Dummy)
	ty.custom = t_name.Str

	context.AddType(t_name.Str, ty)

	return OKAY
}

func ParseTypeDef(t *Token) cmError {
	ErrorSetToken(t)

	if t.Children[0].Match("generic-type") {
		return ParseGenericTypeDef(t)
	}
	if t.Children[0].Match("interface") {
		return ParseInterfaceDef(t)
	}

	children := t.Children

	if len(children) < 3 {
		PostError("bad type def: missing type spec")
		return FAILED
	}
	if len(children) > 3 {
		PostError("bad type def: extra rubbish at end")
		return FAILED
	}

	t_name := children[1]
	t_spec := children[2]

	user := context.GetType(t_name.Str)

	new_type, err2 := ParseType(t_spec)
	if err2 != OKAY {
		user.failed = true
		return FAILED
	}

	if new_type.base == TYP_Void {
		PostError("custom type '%s' cannot be void", t_name.Str)
		user.failed = true
		return FAILED
	}
	if new_type.custom != "" {
		PostError("custom type '%s' cannot be another custom type", t_name.Str)
		user.failed = true
		return FAILED
	}

	// replace the dummy place-holder with real one
	user.Replace(new_type)

	// FIXME for maps/arrays/etc   [ WHAT???? ]
	user.parent = new_type

	return OKAY
}

func ParseGenericTypeDef(t *Token) cmError {
	children := t.Children

	if len(children) < 3 {
		PostError("bad generic type: missing parameters")
		return FAILED
	}
	if len(children) < 4 {
		PostError("bad generic type: missing type spec")
		return FAILED
	}
	if len(children) > 4 {
		PostError("bad generic type: extra rubbish at end")
		return FAILED
	}

	t_name := children[1]
	t_pars := children[2]
	t_spec := children[3]

	user := context.GetType(t_name.Str)

	/* parse parameters of generic type */

	GenericTypeBegin()

	if t_pars.Kind != TOK_Expr {
		PostError("bad parameter list, names must be in ()")
        return FAILED
	}
	if len(t_pars.Children) == 0 {
		PostError("missing parameters to generic type")
        return FAILED
	}

	for _, p := range t_pars.Children {
		if !p.IsGeneric() {
			PostError("expected parameter like @T, got %s", p.String())
			return FAILED
		}

		_, exist := gen_type_state.types[p.Str]
		if exist {
			PostError("duplicate generic parameter '%s'", p.Str)
			return FAILED
		}

		// add it
		ty := NewType(TYP_Mystery)
		ty.custom = p.Str

		gen_type_state.types[p.Str] = ty

		user.AddParam(p.Str, ty)
	}

	_ = GenericTypeCollect()

	template, err2 := ParseType(t_spec)
	if err2 != OKAY {
		user.failed = true
		return FAILED
	}

	GenericTypeFinish()

	if template.base == TYP_Void {
		PostError("custom type '%s' cannot be void", t_name.Str)
		user.failed = true
		return FAILED
	}
/*
	if template.custom != "" {
		PostError("custom type '%s' cannot be another custom type", t_name.Str)
		user.failed = true
		return FAILED
	}
*/
	user.base = TYP_Generic
	user.sub = template

//??	user.parent = ???

	return OKAY
}

func ParseInterfaceDef(t *Token) cmError {
	t.InitInfo()

	children := t.Children

	t_name := children[1]
	children = children[2:]

	user := context.GetType(t_name.Str)
	user.base = TYP_Interface

	// Note: an empty interface is allowed, but not very useful

	for _, sub := range children {
		if sub.Kind != TOK_Expr {
			PostError("bad interface: expected method in (), got %s",
				sub.String())
			return FAILED
		}
		if len(sub.Children) == 0 {
			PostError("bad interface: empty () found")
			return FAILED
		}

		head := sub.Children[0]
		err2 := FAILED

		if head.Match("fun") {
			err2 = ParseInterfaceMethod(sub, user)
		} else if head.Match("embed") {
			err2 = ParseInterfaceEmbedding(sub, user)
		} else {
			PostError("bad interface: unknown element (%s ...)", head.Str)
		}

		if err2 != OKAY {
			return FAILED
		}
	}

	return OKAY
}

func ParseInterfaceMethod(t *Token, user *Type) cmError {
	children := t.Children

	if len(children) < 2 {
		PostError("bad interface method: missing name")
		return FAILED
	}
	if len(children) < 3 {
		PostError("bad interface method: missing parameters")
		return FAILED
	}
	if len(children) > 3 {
		PostError("bad interface method: extra rubbish at end")
		return FAILED
	}

	t_method := children[1]
	children = children[2:]

	if t_method.Kind != TOK_Name {
		PostError("bad interface method: wanted method name, got %s", t_method.String())
		return FAILED
	}
	if !t_method.IsField() {
		PostError("bad interface method: name must begin with a dot")
		return FAILED
	}

	// hack alert 1: add a dummy body so we can use ParseFunctionParts
	fake_body := new(Token)
	fake_body.Kind = TOK_Name
	fake_body.Str = "FALSE"
	fake_body.LineNum = t.LineNum

	children = append(children, fake_body)

	f := NewNode(ND_DefMethod, 0)
	f.LineNum = t.LineNum

	err2 := ParseFunctionParts(f, children, true)
	if err2 != OKAY {
		return FAILED
	}

	meth_ty, err3 := ExtractTypeSignature(f)
	if err3 != OKAY {
		return FAILED
	}

	return user.AddInterfaceMethod(t_method.Str, meth_ty)
}

func ParseInterfaceEmbedding(t *Token, user *Type) cmError {
	for _, sub := range t.Children[1:] {
		if sub.Kind != TOK_Name {
			PostError("bad interface embed: expected name, got %s",
				sub.String())
			return FAILED
		}

		name := sub.Str
		other := N_GetType(name, sub.Module)

		if other == nil || other.base != TYP_Interface {
			PostError("bad interface embed: no such interface %s", name)
			return FAILED
		}

		// okay, attempt to merge into current interface

		for _, par := range other.param {
			if user.AddInterfaceMethod(par.name, par.ty) != OKAY {
				return FAILED
			}
		}
	}

	return OKAY
}

func ExtractTypeSignature(f *Token) (*Type, cmError) {
	t_pars := f.Children[0]

	ty := NewType(TYP_Function)

	ty.sub = f.Info.return_ty
	if ty.sub == nil {
		ty.sub = void_type
	}

	for _, par := range t_pars.Children {
		if par.Info.ty == nil {
			panic("interface method parameter lacks a type")
		}
		ty.AddParam(par.Str, par.Info.ty)
	}

	return ty, OKAY
}

//----------------------------------------------------------------------

func PrepareAllGlobals() {
	for _, tok := range raw_definitions {
		PrepareDefinition(tok)
	}
}

func PrepareDefinition(t *Token) {
	ErrorSetToken(t)

	// expand macros here
	macro_stamp = 0

	var err2 cmError

	t, err2 = OperatorExpand(t)
	if err2 != OKAY {
		return
	}

	t, err2 = MacroExpand(t)
	if err2 != OKAY {
		return
	}

	if ParseElement(t) != OKAY {
		return
	}

	// is it really a definition?
	switch t.Kind {
	case ND_DefVar:
		if PrepareGlobalVar(t) != OKAY {
			// prevent trying to EvaluateGlobalVar this
			t.Kind = TOK_ERROR
			t.Str  = "failed global var"
		}

	case ND_DefFunc:
		PrepareGlobalFunc(t)

	case ND_DefMethod:
		PrepareGlobalMethod(t)

	default:
		raw_expressions = append(raw_expressions, t)
	}
}

func PrepareGlobalVar(t *Token) cmError {
	t_var := t.Children[0]
	t_exp := t.Children[1]

	// global defs normally cannot be redefined, but the REPL allows it
	if context.HasDef(t_var.Str) && !REPL {
		PostError("global '%s' is already defined", t_var.Str)
		return FAILED
	}

	// perform partial deduction.
	// [ BindNode cannot be done yet, hence this deduction is fairly
	//   limited since it cannot use the type of any globals ]
	if DeduceStaticValue(t_exp, t_var.Info.ty, false) != OKAY {
		return FAILED
	}

	// use explicit type when given, or deduced type if known
	ty := t_var.Info.ty

	if ty == nil {
		ty = t_exp.Info.ty
	}

	MakeGlobal(t_var.Str, ty, t.Info.mutable)

	return OKAY
}

func EvaluateAllStatics() {
	for _, tok := range raw_definitions {
		if tok.Kind == ND_DefVar {
			EvaluateGlobalVar(tok)
		}
	}
}

func EvaluateGlobalVar(t *Token) cmError {
	t_var := t.Children[0]
	t_exp := t.Children[1]

	// we created the global var before, so it must exist
	def := context.GetDef(t_var.Str)

	// perform type inference on expression
	if DeduceStaticValue(t_exp, def.ty, true) != OKAY {
		return FAILED
	}

	if t_exp.Info.ty.base == TYP_Void {
		PostError("global variables cannot be void")
		return FAILED
	}

	// if an explicit type was given, check it agrees with value
	if def.ty != nil {
		if !t_exp.Info.ty.AssignableTo(def.ty) {
			PostError("incompatible types for global '%s': got %s and %s",
				t_var.Str, def.ty.String(), t_exp.Info.ty.String())
			return FAILED
		}
	} else {
		def.ty = t_exp.Info.ty
	}

	if def.loc.EvaluateTree(t_exp) != OKAY {
		return FAILED
	}

	return OKAY
}

func PrepareGlobalFunc(t *Token) cmError {
	t_name := t.Children[0]
	t_pars := t.Children[1]
	t_body := t.Children[2]

	// global defs normally cannot be redefined, but the REPL allows it
	if context.HasDef(t_name.Str) && !REPL {
		PostError("global '%s' is already defined", t_name.Str)
		return FAILED
	}

	debug_name := t_name.Module + t_name.Str

	cl := CreateClosureStuff(t, t_pars, t_body, debug_name)

	def := MakeConstant(t_name.Str, nil)
	def.loc.MakeUnfinishedFunction(cl)

	uf := new(UnfinishedFunc)
	uf.def = def
	uf.loc = def.loc

	unfinished_funcs = append(unfinished_funcs, uf)
	return OKAY
}

func PrepareGlobalMethod(t *Token) cmError {
	t_method := t.Children[0]
	t_pars := t.Children[1]
	t_body := t.Children[2]

	obj_type := t_pars.Children[0].Info.ty

	// methods must normally be unique per type, but the REPL can replace them
	if obj_type.MethodExists(t_method.Str) {
		if REPL {
			obj_type.DeleteMethod(t_method.Str)
		} else {
			PostError("bad method def, '%s' already defined", t_method.Str)
			return FAILED
		}
	}

	debug_name := obj_type.custom + "::" + t_method.Str

	cl := CreateClosureStuff(t, t_pars, t_body, debug_name)

	loc := t.Info.ty.CreateMethod(t_method.Str)
	loc.MakeUnfinishedFunction(cl)

	uf := new(UnfinishedFunc)
	uf.loc = loc
	uf.obj_type = obj_type
	uf.method = t_method.Str

	unfinished_funcs = append(unfinished_funcs, uf)
	return OKAY
}

func CreateClosureStuff(t, t_pars, t_body *Token, debug_name string) *Closure {
	cl := NewClosure(debug_name)

	cl.t_body = t_body
	cl.t_parameters = t_pars
	cl.ret_type = t.Info.return_ty
	cl.saved_gen_types = t.Info.gen_types

	return cl
}

func DeduceAllFunctions() {
	// NOTE: methods are handled here too

	// we go *backwards* through the definitions of a code file
	// [ TODO if multiple code files, should do top of dependency chain last ]

	for i := len(unfinished_funcs)-1; i >= 0; i-- {
		uf := unfinished_funcs[i]
		loc := uf.loc

		if loc.Clos.ty == nil {
			cl := loc.Clos

			// analyse local bindings, handle parameters
			if BindClosure(cl) != OKAY {
				cl.failed = true
				continue
			}

			// figure out type signature of function, or die trying
			if DeduceFunctionType(cl) != OKAY {
				cl.failed = true
				continue
			}

			loc.ty = cl.ty

			if uf.def != nil {
				uf.def.ty = cl.ty
			}
		}
	}
}

func CompileAllFunctions() {
	for _, def := range context.defs {
		loc := def.loc

		if loc.ty.base == TYP_Function &&
			loc.Clos.builtin == nil &&
			!loc.Clos.IsCompiled() &&
			!loc.Clos.failed {

			CompileClosure(loc.Clos)
		}
	}

	/* methods */

	for _, ty := range context.types {
		CompileMethodsForType(ty)
	}

	// FIXME ugh, do this better!  [ AND only once ]
	CompileMethodsForType(int_type)
	CompileMethodsForType(str_type)
	CompileMethodsForType(float_type)
	CompileMethodsForType(bytevec_type)

	CompileMethodsForType(array_parent)
	CompileMethodsForType(map_parent)
	CompileMethodsForType(set_parent)
	CompileMethodsForType(class_parent)
	CompileMethodsForType(union_parent)
	CompileMethodsForType(enum_parent)
	CompileMethodsForType(tuple_parent)
}

func CompileMethodsForType(ty *Type) {
	for _, loc := range ty.methods {
		if loc.ty.base == TYP_Function &&
			loc.Clos.builtin == nil &&
			!loc.Clos.IsCompiled() &&
			!loc.Clos.failed {

			CompileClosure(loc.Clos)
		}
	}
}

//----------------------------------------------------------------------

func RunAllExpressions() {
	for _, tok := range raw_expressions {
		if RunExpression(tok) != OKAY {
			// stop as soon as any expression fails
			// [ subsequent ones may depend on earlier ones ]
			break
		}
	}
}

func RunExpression(t *Token) cmError {
	cl, err2 := CompileExpression(t)
	if err2 != OKAY {
		return FAILED
	}

	ClearErrors()

	if opt_native {
		NativeAddExpr(cl)
		return OKAY
	}

	result, err2 := RunTop(cl)
	if err2 != OKAY {
		return FAILED
	}

	// display the result, unless it is void
	if REPL && cl.ret_type.base != TYP_Void {
		result.ty = cl.ret_type

		s := result.DeepString()
		t := cl.ret_type.String()

		Print("%s : %s\n", s, t)
	}

	return OKAY
}

func CompileExpression(t *Token) (*Closure, cmError) {
	ErrorSetToken(t)

	cl := NewClosure("#Expr#")
	cl.is_expr = true
	cl.t_body = t

	if BindClosure(cl) != OKAY {
		return nil, FAILED
	}

	MarkTailCalls(t)

	// for consistency with compiled functions, do a partial deduction
	// before a full one.
	if DeduceClosure(cl, false) != OKAY {
		return nil, FAILED
	}

	if CompileClosure(cl) != OKAY {
		return nil, FAILED
	}

	return cl, OKAY
}

//----------------------------------------------------------------------

type VN_Flags int

const (
	ALLOW_UNDERSCORE VN_Flags = (1 << iota)
	ALLOW_FIELDS
	ALLOW_SELF
)

func ValidateDefName(t *Token, what string, flags VN_Flags) cmError {
	if t.Module != "" {
		PostError("cannot define a %s in another module", what)
		return FAILED
	}

	name := t.Str

	if t.Str == "_" && (flags & ALLOW_UNDERSCORE) != 0 {
		return OKAY
	}

	if t.Str == "self" && (flags & ALLOW_SELF) != 0 {
		return OKAY
	}

	// disallow language keywords
	if IsLanguageKeyword(name) {
		PostError("bad %s name: '%s' is a reserved keyword", what, name)
		return FAILED
	}

	// disallow field names
	if len(name) >= 2 && name[0] == '.' {
		if (flags & ALLOW_FIELDS) != 0 {
			return OKAY
		} else {
			PostError("bad %s name: cannot begin with a dot", what)
			return FAILED
		}
	}

	// disallow tag names
	if len(name) >= 2 && name[0] == '`' {
		PostError("bad %s name: cannot begin with a backquote", what)
		return FAILED
	}

	// disallow generic type names
	if len(name) >= 2 && name[0] == '@' {
		PostError("bad %s name: cannot begin with '@'", what)
		return FAILED
	}

	// disallow names of the operators
	if IsOperator(name) {
		PostError("bad %s name: '%s' is a math operator", what, name)
		return FAILED
	}

	// disallow macro names
	if N_HasMacro(name, "") {
		PostError("bad %s name: '%s' already defined as a macro", what, name)
		return FAILED
	}

	// disallow names of user types
	if N_HasType(name, "") {
		PostError("bad %s name: '%s' already defined as a type", what, name)
		return FAILED
	}

	// TODO: consider disallowing globals like TRUE and FALSE

	return OKAY
}

func WhatIsGlobalName(name string) string {
	if IsOperator(name) {
		return "operator"
	}

	if N_HasMacro(name, "") {
		return "macro"
	}

	if N_HasType(name, "") {
		return "type"
	}

	if N_HasDef(name, "") {
		return "global"
	}

	if IsLanguageKeyword(name) {
		return "keyword"
	}

	return "UNSET"
}

func IsLanguageKeyword(name string) bool {
	switch name {
	case "let", "let-mut", "fun", "lam":
		return true

	case "macro", "rule", "type", "generic-type", "interface", "embed":
		return true

	case "begin", "set!", "self", "no", "error":
		return true

	case "if", "else", "elif", "while", "match", "where":
		return true

	case "cast", "wrap", "unwrap":
		return true

	// NOTE: "for" forms are done via macros in the prelude
	case "skip", "junction":
		return true

	case "int", "float", "str", "array", "map", "set":
		return true

	case "tuple", "class", "union", "enum", "function":
		return true

	case ":", "::", "_", "->", "=>":
		return true
	}

	if IsOperator(name) {
		return true
	}

	return false
}
