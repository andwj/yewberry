// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"

// Closure represents a piece of compiled code which can be executed
// by the VM.  It is either a top-level expression, or a function or
// method (which may be global, or may be a lambda function value).
type Closure struct {
	// when this is non-nil, this is a built-in function, and ALL the
	// other fields in this struct are meaningless.
	builtin *Builtin

	// true if this is an expression (not a function)
	is_expr bool

	// token for the body of the function, or the expression
	t_body *Token

	// original parameter tokens, inside a TOK_Expr.
	t_parameters *Token

	// the static type of this function (once fully compiled).
	// it also contains the parameter names.  not used for expressions
	ty *Type

	// the return type of the function or expression.
	// normally this mirrors ty.sub -- however this may be set
	// earlier when return type is known but not the parameters.
	ret_type *Type

	// the variables which are the parameters of the function.
	parameters []*LocalVar

	// the compiled form is a sequence of operations
	vm_ops []Operation

	// all the constant values used in the function.  operations
	// within vm_ops[] can refer to these constants.
	constants []Value

	// all the types used by certain vm operations.
	types []*Type

	// all the globals used by certain vm operations.
	globals []*GlobalDef

	// this is created by the OP_MAKE_FUNC operation in a newly
	// instantiated closure, and contains the upvars from the
	// parent (each Value is an external variable which is accessed
	// by OP_EXTV_XXX operations).
	upvars []Value

	// the variables which must be copied by OP_MAKE_FUNC when
	// this *template* closure is instantiated.
	// It may include variables which are not directly used in a
	// lambda but *are* used in a sub-lambda (or sub-sub-lambda, etc).
	upvar_templates []*UpvarTemplate

	// the parent function which contains this one.  Will be nil for
	// global functions.  Used to determine the scope of local vars,
	// and to determine which local vars get captured.
	parent *Closure

	// the local variables which are currently "in scope", including
	// the parameters of the function/method being compiled.  It is a
	// chain via "parent" field (from innermost block out).  Only used
	// during BindClosure(), vars are not looked up after that.
	locals *LocalGroup

	// this is used while compiling, and is the number of locals and
	// temporaries (on the stack) currently in use.  Starts off at zero,
	// increased when locals are added, and decreases when their scope
	// finishes.
	high_water int32

	// the maximum that "high_water" ever reached.  The function
	// *itself* won't use any more of the data stack than this
	// (naturally it does not include calls of other functions).
	highest_water int32

	debug_name string

	// this is used to set LineNum in emitted VM operations
	cur_line int

	// this forces DeduceNode() to ensure all nodes get a type and to
	// perform full type checking.
	type_check bool

	// incremented for every node which gets deduced
	deduce_count int

	// this saves the list of generic types of a ND_DefFunc/Method
	// token, for use by DeduceFunctionType.  not used afterwards.
	saved_gen_types map[string]*Type

	// did the function or method fail to parse/compile?
	failed bool
}

type UpvarTemplate struct {
	lvar *LocalVar

	index  int32 // index into cl.upvars[]
	offset int32 // stack offset containing external var
	far_up bool  // variable is further up than parent
}

type JunctionInfo struct {
	name string
	ty   *Type

	expr *Token // the expression within a (junction ...) form
	seq  *Token // the ND_Block containing this junction
	tail bool   // true if that ND_Block is in tail position

	// position of OP_JUMP instructions for each skip form.
	// these are all updated when ND_Junction is compiled.
	jump_pcs []int
}

func (cl *Closure) IsCompiled() bool {
	return cl.vm_ops != nil
}

func NewClosure(debug_name string) *Closure {
	cl := new(Closure)
	cl.debug_name = debug_name
	cl.upvar_templates = make([]*UpvarTemplate, 0)
	cl.parameters = make([]*LocalVar, 0)

	return cl
}

func CompileClosure(cl *Closure) cmError {
	cl.vm_ops = make([]Operation, 0)

	cl.locals = nil
	cl.constants = nil
	cl.types = nil
	cl.globals = nil
	cl.high_water = 0
	cl.highest_water = 0

	if !cl.is_expr {
		if cl.ty == nil {
			panic("CompileClosure which is unfinished")
		}
		if cl.ty.base != TYP_Function {
			panic("CompileClosure with weird type")
		}
	}

	if cl.t_body == nil {
		panic("CompileClosure with no body")
	}

	// handle #native code
	if cl.t_body.Kind == ND_Native {
		cl.builtin = builtins[cl.t_body.Str]

		if cl.builtin == nil {
			cl.failed = true
			PostError("No such #native function: %s", cl.t_body.Str)
			return FAILED
		}

		return OKAY
	}

	// perform full type deduction
	if DeduceClosure(cl, true) != OKAY {
		cl.failed = true
		return FAILED
	}

	if cl.is_expr {
		cl.ret_type = cl.t_body.Info.ty
		cl.ty = NewType(TYP_Function)
		cl.ty.sub = cl.ret_type
	} else {
		// check the return type
		if !cl.t_body.Info.ty.AssignableTo(cl.ty.sub) {
			PostError("type mismatch on function result: wanted %s, body is %s",
				cl.ty.sub.String(), cl.t_body.Info.ty.String())
			return FAILED
		}
	}

	if !cl.is_expr {
		// allocate stack slot for each parameter
		for i := range cl.parameters {
			lvar := cl.parameters[i]

			if lvar.name != "_" {
				lvar.offset = cl.RawAllocSlot()

				// for captured parameters, move value to external var
				if lvar.external {
					cl.Emit2(OP_LOC_READ, lvar.offset)
					cl.CreateAssignVar(lvar)
				}
			}
		}
	}

	hw_check := cl.high_water

	cl.cur_line = int(cl.t_body.LineNum)

	// compile the body
	if cl.CompileNode(cl.t_body) != OKAY {
		cl.failed = true
		return FAILED
	}

	cl.Emit1(OP_RETURN)

	if hw_check != cl.high_water {
		panic("high_water got out of sync")
	}

	return OKAY
}

//----------------------------------------------------------------------

func (cl *Closure) Emit3(T OpCode, S, D int32) {
	op := Operation{T, S, D, int32(cl.cur_line)}
	cl.vm_ops = append(cl.vm_ops, op)
}

func (cl *Closure) Emit2(T OpCode, S int32) {
	cl.Emit3(T, S, NO_PART)
}

func (cl *Closure) Emit1(T OpCode) {
	cl.Emit3(T, NO_PART, NO_PART)
}

func (cl *Closure) AddConstant(v Value) int32 {
	if cl.constants == nil {
		cl.constants = make([]Value, 0)
	}

	// see if we can re-use an existing constant
	for idx, other := range cl.constants {
		if v.EqualAsConstant(&other) {
			return int32(idx)
		}
	}

	// nope, so add a new one
	res := int32(len(cl.constants))

	cl.constants = append(cl.constants, v)

	return res
}

func (cl *Closure) AddType(ty *Type) int32 {
	if ty == nil {
		panic("AddType with nil")
	}

	if cl.types == nil {
		cl.types = make([]*Type, 0)
	}

	// re-use an existing entry?
	for idx, other := range cl.types {
		if other == ty {
			return int32(idx)
		}
	}

	// nope, so add a new one
	res := int32(len(cl.types))
	cl.types = append(cl.types, ty)

	return res
}

func (cl *Closure) AddGlobal(def *GlobalDef) int32 {
	if def == nil {
		panic("AddGlobal with nil")
	}

	if cl.globals == nil {
		cl.globals = make([]*GlobalDef, 0)
	}

	// re-use an existing entry?
	for idx, other := range cl.globals {
		if other == def {
			return int32(idx)
		}
	}

	// nope, so add a new one
	res := int32(len(cl.globals))
	cl.globals = append(cl.globals, def)

	return res
}

func (cl *Closure) RawAllocSlot() int32 {
	slot := cl.high_water
	cl.high_water += 1

	if cl.highest_water < cl.high_water {
		cl.highest_water = cl.high_water
	}
	return slot
}

func (cl *Closure) LocalPush() int32 {
	cl.Emit1(OP_PUSH_A)
	return cl.RawAllocSlot()
}

func (cl *Closure) LocalPop() {
	cl.Emit1(OP_POP_A)
	cl.high_water -= 1
}

func (cl *Closure) LocalDrop(count int32) {
	cl.Emit2(OP_DROP, count)
	cl.high_water -= count
}

func (cl *Closure) CreateAssignVar(lvar *LocalVar) {
	if lvar.external {
		// created variable has initial value from "A" reg
		cl.Emit1(OP_MAKE_VAR)
	}

	lvar.offset = cl.LocalPush()
}

//----------------------------------------------------------------------

func (cl *Closure) CompileNode(t *Token) cmError {
	ErrorSetToken(t)

	cl.cur_line = int(t.LineNum)

	switch t.Kind {
	case ND_Local:
		return cl.CompileLocal(t)

	case ND_Upvar:
		return cl.CompileUpvar(t)

	case ND_Global:
		return cl.CompileGlobal(t)

	case ND_FunCall:
		return cl.CompileFunCall(t, false)

	case ND_MethodCall:
		return cl.CompileFunCall(t, true)

	case ND_Array:
		return cl.CompileArrayLiteral(t)

	case ND_Tuple:
		return cl.CompileTupleLiteral(t)

	case ND_Object:
		return cl.CompileObjectLiteral(t)

	case ND_Map:
		return cl.CompileMapLiteral(t)

	case ND_Set:
		return cl.CompileSetLiteral(t)

	case ND_Union:
		return cl.CompileUnionLiteral(t)

	case ND_String:
		return cl.CompileStringBuilder(t)

	case ND_Void:
		return cl.CompileVoid(t)

	case ND_Literal:
		return cl.CompileLiteral(t)

	case ND_Block:
		return cl.CompileBlock(t)

	case ND_Let:
		return cl.CompileLet(t)

	case ND_If:
		return cl.CompileIf(t)

	case ND_While:
		return cl.CompileWhile(t)

	case ND_Skip:
		return cl.CompileSkip(t)

	case ND_Cast:
		// casting requires no extra operations
		return cl.CompileNode(t.Children[0])

	case ND_Wrap:
		return cl.CompileWrap(t)

	case ND_Unwrap:
		return cl.CompileUnwrap(t)

	case ND_Flatten:
		return cl.CompileFlatten(t)

	case ND_ClassName:
		return cl.CompileClassName(t)

	case ND_TagConv:
		return cl.CompileTagConv(t)

	case ND_IsTag:
		return cl.CompileIsTag(t)

	case ND_RefEqual:
		return cl.CompileRefEqual(t)

	case ND_Fmt:
		return cl.CompileFmt(t)

	case ND_Match:
		return cl.CompileMatch(t)

	case ND_SetVar:
		return cl.CompileSetVar(t)

	case ND_SetField:
		return cl.CompileSetField(t)

	case ND_GetField:
		return cl.CompileGetField(t)

	case ND_Lambda:
		return cl.CompileLambda(t)

	case TOK_Name:
		PostError("unknown identifier '%s'", t.Str)
		return FAILED

	default:
		PostError("unexpected node: %s", t.String())
		return FAILED
	}
}

func (cl *Closure) CompileBlock(t *Token) cmError {
	// compile each child expression.
	// the last one will provide the final "result" without having
	// to do anything special.

	for _, sub := range t.Children {
		if cl.CompileNode(sub) != OKAY {
			return FAILED
		}
	}

	// handle a (junction ...) form at the end of a block.
	// it needs to fix both OP_JUMP and OP_DROP instructions.

	if t.Info.junction != nil {
		junc := t.Info.junction

		cl.FixEndJumps(junc.jump_pcs)

		for _, pc := range junc.jump_pcs {
			old_hw := cl.vm_ops[pc - 1].D
			new_hw := cl.high_water

			if new_hw > old_hw {
				panic("high_water at junction > skip")
			}

			cl.vm_ops[pc - 1].S = (old_hw - new_hw)
		}
	}

	return OKAY
}

func (cl *Closure) CompileLocal(t *Token) cmError {
	lvar := t.Info.lvar

if lvar.ty == nil { panic("CompileLocal : nil type in " + lvar.name) }

	// local variables exist in current stack frame.
	// external variables exist on the heap, but have an on-stack proxy.

	cl.Emit2(OP_LOC_READ, lvar.offset)

	if lvar.external {
		cl.Emit1(OP_EXTV_READ)
	}

	return OKAY
}

func (cl *Closure) CompileUpvar(t *Token) cmError {
	lvar := t.Info.lvar

if lvar.ty == nil { panic("CompileUpvar : nil type in " + lvar.name) }

	// find the upvar template
	et := cl.FindUpvar(lvar)

	cl.Emit2(OP_UPVAR, et.index)

	if et.lvar.external {
		cl.Emit1(OP_EXTV_READ)
	}

	return OKAY
}

func (cl *Closure) CompileGlobal(t *Token) cmError {
	gdef := t.Info.gdef

	cl.Emit2(OP_GLOB_READ, cl.AddGlobal(gdef))

	return OKAY
}

func (cl *Closure) CompileVoid(t *Token) cmError {
	cl.Emit1(OP_MAKE_VOID)
	return OKAY
}

func (cl *Closure) CompileLiteral(t *Token) cmError {
	cl.Emit2(OP_CONST, cl.AddConstant(*t.Info.lit))
	return OKAY
}

func (cl *Closure) CompileArrayLiteral(t *Token) cmError {
	arr_type := t.Info.ty

	// create a new empty array
	if arr_type.base == TYP_ByteVec {
		cl.Emit1(OP_MAKE_BYTEVEC)
	} else {
		cl.Emit2(OP_MAKE_ARRAY, cl.AddType(arr_type))
	}
	arr_ofs := cl.LocalPush()

	// process each element...
	for _, elem := range t.Children {
		if cl.CompileNode(elem) != OKAY {
			return FAILED
		}
		if arr_type.base == TYP_ByteVec {
			cl.Emit1(OP_TOBYTE)
		}

		cl.Emit1(OP_MOVE_B)

		cl.Emit2(OP_LOC_READ, arr_ofs)
		cl.Emit1(OP_ARR_APPEND)
	}

	// result is new array
	cl.LocalPop()
	return OKAY
}

func (cl *Closure) CompileMapLiteral(t *Token) cmError {
	map_type := t.Info.ty

	// create a new empty map
	cl.Emit2(OP_MAKE_MAP, cl.AddType(map_type))
	map_ofs := cl.LocalPush()

	key_ofs := cl.LocalPush()

	// insert each element in turn.
	for _, pair := range t.Children {
		t_key := pair.Children[0]
		t_elem := pair.Children[1]

		if cl.CompileNode(t_key) != OKAY {
			return FAILED
		}
		cl.Emit2(OP_LOC_WRITE, key_ofs)

		if cl.CompileNode(t_elem) != OKAY {
			return FAILED
		}
		cl.Emit1(OP_MOVE_B)

		cl.Emit2(OP_LOC_READ, key_ofs)
		cl.Emit1(OP_MOVE_C)

		cl.Emit2(OP_LOC_READ, map_ofs)
		cl.Emit1(OP_MAP_WRITE)
	}

	cl.LocalDrop(1)

	// result is new map
	cl.LocalPop()
	return OKAY
}

func (cl *Closure) CompileSetLiteral(t *Token) cmError {
	set_type := t.Info.ty

	// create a new empty set
	cl.Emit2(OP_MAKE_MAP, cl.AddType(set_type))
	set_ofs := cl.LocalPush()

	// insert each element in turn.
	for _, t_key := range t.Children {
		if cl.CompileNode(t_key) != OKAY {
			return FAILED
		}
		cl.Emit1(OP_MOVE_C)

		cl.Emit1(OP_TRUE)
		cl.Emit1(OP_MOVE_B)

		cl.Emit2(OP_LOC_READ, set_ofs)
		cl.Emit1(OP_SET_WRITE)
	}

	// result is new set
	cl.LocalPop()
	return OKAY
}

func (cl *Closure) CompileTupleLiteral(t *Token) cmError {
	tup_type := t.Info.ty

	// create a new empty tuple
	cl.Emit2(OP_MAKE_TUPLE, cl.AddType(tup_type))
	tup_ofs := cl.LocalPush()

	for i, child := range t.Children {
		t_elem := child
		if child.Kind == ND_TuplePair {
			t_elem = child.Children[1]
		}

		if cl.CompileNode(t_elem) != OKAY {
			return FAILED
		}
		cl.Emit1(OP_MOVE_B)

		cl.Emit2(OP_LOC_READ, tup_ofs)
		cl.Emit2(OP_TUPL_WRITE, int32(i))
	}

	// set remaining fields to default values
	if t.Info.open > 0 {
		for i := len(t.Children); i < len(tup_type.param); i++ {
			field_type := tup_type.param[i].ty

			if !field_type.Defaultable() {
				PostError("cannot init tuple field, type %s has no default value",
					field_type.String())
				return FAILED
			}

			cl.Emit2(OP_MAKE_DEFAULT, cl.AddType(field_type))
			cl.Emit1(OP_MOVE_B)

			cl.Emit2(OP_LOC_READ, tup_ofs)
			cl.Emit2(OP_TUPL_WRITE, int32(i))
		}
	}

	// result is new tuple
	cl.LocalPop()
	return OKAY
}

func (cl *Closure) CompileObjectLiteral(t *Token) cmError {
	class_type := t.Info.ty

	// create a new empty object
	cl.Emit2(OP_MAKE_OBJ, cl.AddType(class_type))
	obj_ofs := cl.LocalPush()

	seen_fields := make([]bool, len(class_type.param))

	for i, child := range t.Children {
		field_idx := i
		t_value := child

		if child.Kind == ND_TuplePair {
			t_field := child.Children[0]
			t_value = child.Children[1]

			field_idx = t_field.Info.tag_id
		}

		if cl.CompileNode(t_value) != OKAY {
			return FAILED
		}
		cl.Emit1(OP_MOVE_B)

		cl.Emit2(OP_LOC_READ, obj_ofs)
		cl.Emit2(OP_OBJ_WRITE, int32(field_idx))

		seen_fields[field_idx] = true
	}

	// set unvisited fields to default values
	if t.Info.open > 0 {
		for i, par := range class_type.param {
			if !seen_fields[i] {
				field_type := par.ty

				// FIXME check in deduce code
				if !field_type.Defaultable() {
					PostError("cannot init class field, type %s has no default value",
						field_type.String())
					return FAILED
				}

				cl.Emit2(OP_MAKE_DEFAULT, cl.AddType(field_type))
				cl.Emit1(OP_MOVE_B)

				cl.Emit2(OP_LOC_READ, obj_ofs)
				cl.Emit2(OP_OBJ_WRITE, int32(i))
			}
		}
	}

	// result is new object
	cl.LocalPop()
	return OKAY
}

func (cl *Closure) CompileUnionLiteral(t *Token) cmError {
	union_type := t.Info.ty
	tag_id := t.Info.tag_id

	t_datum := t.Children[0]

	// compile its datum
	if cl.CompileNode(t_datum) != OKAY {
		return FAILED
	}

	// create the union object, datum in "A" register
	cl.Emit3(OP_MAKE_UNION, cl.AddType(union_type), int32(tag_id))
	return OKAY
}

func (cl *Closure) CompileStringBuilder(t *Token) cmError {
	// there will always be at least one element
	if len(t.Children) < 1 {
		panic("string builder is empty")
	}

	for i, t_elem := range t.Children {
		if i > 0 {
			// save previous string
			cl.LocalPush()
		}

		if cl.CompileNode(t_elem) != OKAY {
			return FAILED
		}

		if t_elem.Info.ty.base == TYP_Int {
			cl.Emit1(OP_STR_CHAR)
		}

		if i > 0 {
			// concatenate previous string with current one
			cl.Emit1(OP_MOVE_B)
			cl.LocalPop()
			cl.Emit1(OP_STR_ADD)
		}
	}

	return OKAY
}

//----------------------------------------------------------------------

func (cl *Closure) CompileFunCall(t *Token, is_method bool) cmError {
	params := t.Children[1:]
	num_params := len(params)

	if opt_bytecode && !t.Info.tail {
		cl.Emit2(OP_FRAME, int32(num_params))
		cl.high_water += 3
	}

	// compute function itself
	if is_method {
		t_obj := params[0]

		if t_obj.Info.ty.base == TYP_Interface {
			// we need the actual class object in reg A
			if cl.CompileNode(t_obj) != OKAY {
				return FAILED
			}

			// dynamic lookup of method function
			var meth_name Value
			meth_name.MakeString(t.Children[0].Str)

			cl.Emit1(OP_MOVE_B)
			cl.Emit2(OP_OBJ_METHOD, cl.AddConstant(meth_name))
			cl.LocalPush()

			// now push the object itself (was saved in reg B)
			cl.Emit1(OP_GET_B)
			cl.LocalPush()

			params = params[1:]

		} else {
			cl.Emit2(OP_CONST, cl.AddConstant(*t.Info.lit))
			cl.LocalPush()
		}
	} else {
		// standard function call
		if cl.CompileNode(t.Children[0]) != OKAY {
			return FAILED
		}
		cl.LocalPush()
	}

	// push the actual parameters onto the stack
	if cl.CompileParameters(params) != OKAY {
		return FAILED
	}

	// make a stack-trace show earliest line
	cl.cur_line = int(t.LineNum)

	if t.Info.tail {
		cl.Emit2(OP_TAIL_CALL, int32(num_params))
	} else {
		cl.Emit2(OP_FUN_CALL, int32(num_params))
	}

	// when the new function executes OP_RETURN, it takes care of
	// dropping all the parameters (etc) we pushed on the stack
	// AND the function itself.
	cl.high_water -= int32(num_params) + 1

	// in the yb-run VM, it also drops the call frame
	if opt_bytecode && !t.Info.tail {
		cl.high_water -= 3
	}

	return OKAY
}

func (cl *Closure) CompileParameters(params []*Token) cmError {
	for _, t_par := range params {
		if cl.CompileNode(t_par) != OKAY {
			return FAILED
		}
		cl.LocalPush()
	}

	return OKAY
}

func (cl *Closure) CompileWrap(t *Token) cmError {
	if cl.CompileNode(t.Children[0]) != OKAY {
		return FAILED
	}

	cl.Emit2(OP_WRAP, cl.AddType(t.Info.ty))
	return OKAY
}

func (cl *Closure) CompileUnwrap(t *Token) cmError {
	if cl.CompileNode(t.Children[0]) != OKAY {
		return FAILED
	}

	cl.Emit1(OP_UNWRAP)
	return OKAY
}

func (cl *Closure) CompileFlatten(t *Token) cmError {
	if cl.CompileNode(t.Children[0]) != OKAY {
		return FAILED
	}

	cl.Emit2(OP_FLATTEN, cl.AddType(t.Info.ty))
	return OKAY
}

func (cl *Closure) CompileClassName(t *Token) cmError {
	t_value := t.Children[0]

	if cl.CompileNode(t_value) != OKAY {
		return FAILED
	}

	cl.Emit1(OP_OBJ_CLASS)
	return OKAY
}

func (cl *Closure) CompileTagConv(t *Token) cmError {
	t_value := t.Children[0]

	if cl.CompileNode(t_value) != OKAY {
		return FAILED
	}

	if t_value.Info.ty.base == TYP_Enum {
		cl.Emit1(OP_ENUM_TAG)
	} else {
		cl.Emit1(OP_UNION_TAG)
	}

	if t.Info.ty.base == TYP_String {
		cl.Emit2(OP_TAG_STR, cl.AddType(t_value.Info.ty))
	}

	return OKAY
}

func (cl *Closure) CompileIsTag(t *Token) cmError {
	t_value := t.Children[0]

	end_jump_pcs := make([]int, 0)

	if cl.CompileNode(t.Children[0]) != OKAY {
		return FAILED
	}
	enum_ofs := cl.LocalPush()

	for i := 1; i < len(t.Children); i++ {
		if i > 1 {
			pc := len(cl.vm_ops)
			end_jump_pcs = append(end_jump_pcs, pc)
			cl.Emit3(OP_IF_TRUE, NO_PART, -7 /* dummy value */)
		}

		t_tag := t.Children[i]

		cl.MatchTag(enum_ofs, t_value.Info.ty, t_tag.Info.tag_id)
	}

	cl.FixEndJumps(end_jump_pcs)

	cl.LocalDrop(1)
	return OKAY
}

func (cl *Closure) CompileRefEqual(t *Token) cmError {
	if cl.CompileNode(t.Children[0]) != OKAY {
		return FAILED
	}
	cl.LocalPush()

	if cl.CompileNode(t.Children[1]) != OKAY {
		return FAILED
	}
	cl.Emit1(OP_MOVE_B)

	cl.LocalPop()
	cl.Emit1(OP_REF_EQ)

	return OKAY
}

func (cl *Closure) CompileSetVar(t *Token) cmError {
	t_var := t.Children[0]
	t_exp := t.Children[1]

	if cl.CompileNode(t_exp) != OKAY {
		return FAILED
	}

	switch t_var.Kind {
	case ND_Upvar:
		cl.Emit1(OP_MOVE_C)  // use C register to hold value

		// find the upvar template
		lvar := t_var.Info.lvar
		et := cl.FindUpvar(lvar)

		if !et.lvar.external {
			panic("writable upvar is not external??")
		}

		cl.Emit2(OP_UPVAR, et.index)
		cl.Emit1(OP_MOVE_B)
		cl.Emit1(OP_GET_C)
		cl.Emit1(OP_EXTV_WRITE)  // var in B := A

	case ND_Global:
		cl.Emit2(OP_GLOB_WRITE, cl.AddGlobal(t_var.Info.gdef))

	case ND_Local:
		lvar := t_var.Info.lvar

		if lvar.external {
			cl.Emit1(OP_MOVE_C)  // use C register to hold value

			// external vars exist on the heap, but have an on-stack proxy
			cl.Emit2(OP_LOC_READ, lvar.offset)
			cl.Emit1(OP_MOVE_B)

			cl.Emit1(OP_GET_C)
			cl.Emit1(OP_EXTV_WRITE)  // var in B := A

		} else {
			// local variables exist in current stack frame
			cl.Emit2(OP_LOC_WRITE, lvar.offset)
		}

	default:
		panic("weird var node")
	}

	return OKAY
}

func (cl *Closure) CompileGetField(t *Token) cmError {
	t_arr := t.Children[0]

	if cl.CompileNode(t.Children[0]) != OKAY {
		return FAILED
	}

	arr_type := t_arr.Info.ty
	field_idx := t.Info.tag_id

	if arr_type.base == TYP_Tuple {
		cl.Emit2(OP_TUPL_READ, int32(field_idx))
	} else {
		cl.Emit2(OP_OBJ_READ, int32(field_idx))
	}

	return OKAY
}

func (cl *Closure) CompileSetField(t *Token) cmError {
	t_arr := t.Children[0]
	t_val := t.Children[1]

	// arr_type := t_arr.Info.ty
	field_idx := t.Info.tag_id

	// compile the class object itself
	// [ it cannot be a tuple, since they are immutable ]
	if cl.CompileNode(t_arr) != OKAY {
		return FAILED
	}
	arr_ofs := cl.LocalPush()

	// compile the value
	if cl.CompileNode(t_val) != OKAY {
		return FAILED
	}
	cl.Emit1(OP_MOVE_B)

	cl.Emit2(OP_LOC_READ, arr_ofs)
	cl.Emit2(OP_OBJ_WRITE, int32(field_idx))

	cl.LocalDrop(1)
	return OKAY
}

func (cl *Closure) CompileFmt(t *Token) cmError {
	t_value := t.Children[0]

	if cl.CompileNode(t_value) != OKAY {
		return FAILED
	}

	val_type := t_value.Info.ty

	var lit Value
	lit.MakeString(t.Info.fmt_spec.Encode())

	cl.Emit3(OP_FMT, cl.AddConstant(lit), cl.AddType(val_type))
	return OKAY
}

func (cl *Closure) CompileLambda(t *Token) cmError {
	template := t.Info.lit.Clos

	if CompileClosure(template) != OKAY {
		return FAILED
	}

	saved_hw := cl.high_water

	// handle upvars, setting "offset" in each UpvarTemplate
	// so that OP_MAKE_FUNC can copy them.

	// for up-up-vars, we need to create a temporary local var
	// which contains the external proxy.  these are dropped
	// immediately after performing the OP_MAKE_FUNC.

	for _, et := range template.upvar_templates {
		if et.far_up {
			cl.Emit2(OP_UPVAR, et.index)
			et.offset = cl.LocalPush()

		} else {
			et.offset = et.lvar.offset
		}
	}

	// this operation creates the "real" closure when run
	cl.Emit2(OP_MAKE_FUNC, cl.AddConstant(*t.Info.lit))

	if cl.high_water > saved_hw {
		cl.LocalDrop(cl.high_water - saved_hw)
	}

	return OKAY
}

func (cl *Closure) CompileLet(t *Token) cmError {
	// compile code to generate the value
	t_var := t.Children[0]
	t_exp := t.Children[1]

	if cl.CompileNode(t_exp) != OKAY {
		return FAILED
	}

	// create a slot for local variable binding, assign value
	cl.CreateAssignVar(t_var.Info.lvar)

	// compile the body
	t_body := t.Children[2]

	if cl.CompileNode(t_body) != OKAY {
		return FAILED
	}

	cl.LocalDrop(1)
	return OKAY
}

func (cl *Closure) CompileIf(t *Token) cmError {
	t_cond := t.Children[0]
	t_then := t.Children[1]
	t_else := t.Children[2]

	// compile the condition
	if cl.CompileNode(t_cond) != OKAY {
		return FAILED
	}

	if_false_pc := len(cl.vm_ops)
	cl.Emit3(OP_IF_FALSE, NO_PART, -7 /* dummy value */)

	// compile the then body
	if cl.CompileNode(t_then) != OKAY {
		return FAILED
	}

	// jump to the very end
	end_jump_pc := len(cl.vm_ops)
	cl.Emit3(OP_JUMP, NO_PART, -8 /* dummy value */)

	// fixup the previous OP_IF_FALSE
	next_pc := len(cl.vm_ops)
	cl.vm_ops[if_false_pc].D = int32(next_pc)

	// compile the else body
	if cl.CompileNode(t_else) != OKAY {
		return FAILED
	}

	// fixup the previous OP_JUMP
	end_pc := len(cl.vm_ops)
	cl.vm_ops[end_jump_pc].D = int32(end_pc)

	return OKAY
}

func (cl *Closure) CompileWhile(t *Token) cmError {
	t_cond := t.Children[0]
	t_body := t.Children[1]

	start_pc := len(cl.vm_ops)

	// compile condition expr
	if cl.CompileNode(t_cond) != OKAY {
		return FAILED
	}

	// jump to end if value is not true
	jump_pc := len(cl.vm_ops)
	cl.Emit3(OP_IF_FALSE, NO_PART, -6 /* dummy value */)

	// compile the body
	if cl.CompileNode(t_body) != OKAY {
		return FAILED
	}

	// jump back to beginning
	cl.Emit3(OP_JUMP, NO_PART, int32(start_pc))

	// fix destination of the previous OP_IF_FALSE
	end_pc := len(cl.vm_ops)
	cl.vm_ops[jump_pc].D = int32(end_pc)

	return OKAY
}

func (cl *Closure) CompileSkip(t *Token) cmError {
	t_exp := t.Children[0]

	junc := t.Info.junction

	if cl.CompileNode(t_exp) != OKAY {
		return FAILED
	}

	// we often need to drop elements off the stack, so
	// add an instruction here and remember the high_water.
	// the real amount to drop is updated by CompileBlock.
	cl.Emit3(OP_DROP, 0, cl.high_water)

	pc := len(cl.vm_ops)
	junc.jump_pcs = append(junc.jump_pcs, pc)

	cl.Emit3(OP_JUMP, NO_PART, -14 /* fixed in CompileBlock */)
	return OKAY
}

func (cl *Closure) FindUpvar(lvar *LocalVar) *UpvarTemplate {
	for _, et := range cl.upvar_templates {
		if et.lvar == lvar {
			return et
		}
	}

	panic("upvar not found in upvar_templates??")
	return nil
}

//----------------------------------------------------------------------

// MatchData has information needed to match against an
// expression, which may be some element deep inside a
// data structure.
type MatchData struct {
	rule       *Token
	expr_ofs   int32
	expr_type  *Type
	top_level  bool
}

func (cl *Closure) CompileMatch(t *Token) cmError {
	// operations we need to fix for the end position
	end_jump_pcs := make([]int, 0)

	// stack slot for final result
	res_ofs := cl.LocalPush()

	var data MatchData

	// compile expression and push onto stack
	t_expr := t.Children[0]

	if cl.CompileNode(t_expr) != OKAY {
		return FAILED
	}
	data.expr_ofs = cl.LocalPush()
	data.expr_type = t_expr.Info.ty
	data.top_level = true

	for i := 1; i < len(t.Children); i++ {
		t_rule := t.Children[i]

		data.rule = t_rule

		if cl.CompileMatchRule(res_ofs, t_rule, &data) != OKAY {
			return FAILED
		}

		// jump to the very end if rule succeeded
		pc := len(cl.vm_ops)
		end_jump_pcs = append(end_jump_pcs, pc)
		cl.Emit3(OP_IF_TRUE, NO_PART, -7 /* dummy value */)
	}

	cl.FixEndJumps(end_jump_pcs)

	cl.LocalDrop(1)

	// result is from the 'res_ofs' temporary
	cl.LocalPop()
	return OKAY
}

func (cl *Closure) CompileMatchRule(res_ofs int32, t *Token, data *MatchData) cmError {
	// the compiled code of each match rule is like a (begin) form
	// which returns TRUE if the match succeeded (and the body was
	// executed), or FALSE if the match failed.

	// patterns can create local bindings, which may be used by the
	// where clause or the rule body.  we must allocate them all here,
	// and drop them at the end of the rule.  they are always read-only,
	// hence can never be external vars.
	group := t.Info.group

	if group.Size() > 0 {
		cl.Emit1(OP_MAKE_VOID)

		for _, lvar := range group.vars {
			lvar.offset = cl.LocalPush()
		}
	}

	end_jump_pcs := make([]int, 0)

	t_pattern := t.Children[0]
	t_body := t.Children[1]

	if cl.CompileMatchPattern(t_pattern, data) != OKAY {
		return FAILED
	}

	pc := len(cl.vm_ops)
	end_jump_pcs = append(end_jump_pcs, pc)
	cl.Emit3(OP_IF_FALSE, NO_PART, -7 /* dummy value */)

	// where clause?
	if len(t.Children) >= 3 {
		t_where := t.Children[2]

		if cl.CompileNode(t_where) != OKAY {
			return FAILED
		}

		pc := len(cl.vm_ops)
		end_jump_pcs = append(end_jump_pcs, pc)
		cl.Emit3(OP_IF_FALSE, NO_PART, -8 /* dummy value */)
	}

	// compile the body
	if cl.CompileNode(t_body) != OKAY {
		return FAILED
	}
	cl.Emit2(OP_LOC_WRITE, res_ofs)

	// a successful match *must* have a result of TRUE
	cl.Emit1(OP_TRUE)

	cl.FixEndJumps(end_jump_pcs)

	if group.Size() > 0 {
		cl.LocalDrop(int32(group.Size()))
	}

	return OKAY
}

func (cl *Closure) CompileMatchPattern(pat *Token, data *MatchData) cmError {
	switch pat.Kind {
	case ND_MatchAll:
		// the "_" pattern trivially succeeds
		cl.Emit1(OP_TRUE)
		return OKAY

	case ND_Local:
		return cl.CompilePattern_Local(pat, data)

	case ND_MatchConsts:
		return cl.CompilePattern_Consts(pat, data)

	case ND_MatchTags:
		return cl.CompilePattern_Tags(pat, data)

	case ND_MatchUnion:
		return cl.CompilePattern_Union(pat, data)

	case ND_Tuple:
		// this handles class objects too
		return cl.CompilePattern_Tuple(pat, data)

	case ND_Array:
		return cl.CompilePattern_Array(pat, data)

	case ND_Set:
		return cl.CompilePattern_Set(pat, data)

	case ND_Map:
		return cl.CompilePattern_Map(pat, data)

	default:
		PostError("cannot compile match pattern: %s", pat.String())
		return FAILED
	}
}

func (cl *Closure) CompilePattern_Local(pat *Token, data *MatchData) cmError {
	lvar := pat.Info.lvar

	// all locals in a match rule have been allocated slots already,
	// so this just needs to store the value.
	if lvar.offset < 0 {
		panic("pattern binding was not allocated")
	}

	cl.Emit2(OP_LOC_READ, data.expr_ofs)
	cl.Emit2(OP_LOC_WRITE, lvar.offset)

	// the match always succeeds
	cl.Emit1(OP_TRUE)
	return OKAY
}

func (cl *Closure) CompilePattern_Consts(pat *Token, data *MatchData) cmError {
	end_jump_pcs := make([]int, 0)

	seen_keys := make(map[string]bool)

	for i, child := range pat.Children {
		if i > 0 {
			pc := len(cl.vm_ops)
			end_jump_pcs = append(end_jump_pcs, pc)
			cl.Emit3(OP_IF_TRUE, NO_PART, -9 /* dummy value */)
		}

		var lit Value
		if lit.EvaluateTree(child) != OKAY {
			return FAILED
		}

		hash := lit.HashString()
		if seen_keys[hash] {
			PostError("match pattern with duplicate constant: %s", lit.DeepString())
			return FAILED
		}
		seen_keys[hash] = true

		cl.Emit2(OP_CONST, cl.AddConstant(lit))
		cl.Emit1(OP_MOVE_B)

		cl.Emit2(OP_LOC_READ, data.expr_ofs)

		switch child.Info.ty.base {
		case TYP_Int:
			cl.Emit1(OP2_INT_EQ)
		case TYP_Float:
			cl.Emit1(OP2_FLOAT_EQ)
		case TYP_String:
			cl.Emit1(OP2_STR_EQ)
		case TYP_Enum:
			cl.Emit1(OP_TAG_EQ)
		case TYP_Function:
			cl.Emit1(OP_REF_EQ)
		default:
			panic("bad type in CompilePattern_Consts")
		}
	}

	cl.FixEndJumps(end_jump_pcs)
	return OKAY
}

func (cl *Closure) CompilePattern_Tags(pat *Token, data *MatchData) cmError {
	end_jump_pcs := make([]int, 0)

	for i, t_tag := range pat.Children {
		if i > 0 {
			pc := len(cl.vm_ops)
			end_jump_pcs = append(end_jump_pcs, pc)
			cl.Emit3(OP_IF_TRUE, NO_PART, -9 /* dummy value */)
		}

		// the deduce logic has determined the tag index
		tag_id := t_tag.Info.tag_id

		cl.MatchTag(data.expr_ofs, data.expr_type, tag_id)
	}

	cl.FixEndJumps(end_jump_pcs)
	return OKAY
}

func (cl *Closure) CompilePattern_Union(pat *Token, data *MatchData) cmError {
	end_jump_pcs := make([]int, 0)

	enum_type := data.expr_type

	t_tag := pat.Children[0]
	t_datum := pat.Children[1]

	// the deduce logic has determined the tag index
	tag_id := t_tag.Info.tag_id

	// add code to check if the tag matches
	cl.MatchTag(data.expr_ofs, enum_type, tag_id)

	pc := len(cl.vm_ops)
	end_jump_pcs = append(end_jump_pcs, pc)
	cl.Emit3(OP_IF_FALSE, NO_PART, -8 /* dummy value */)

	// compile code to read the datum and pattern-match it
	var data2 MatchData

	cl.Emit2(OP_LOC_READ, data.expr_ofs)
	cl.Emit1(OP_UNION_READ)

	data2.expr_ofs = cl.LocalPush()
	data2.expr_type = enum_type.param[tag_id].ty
	data2.rule = data.rule

	if cl.CompileMatchPattern(t_datum, &data2) != OKAY {
		return FAILED
	}

	cl.LocalDrop(1)

	cl.FixEndJumps(end_jump_pcs)
	return OKAY
}

func (cl *Closure) CompilePattern_Tuple(pat *Token, data *MatchData) cmError {
	// Note: this code handles class objects too

	end_jump_pcs := make([]int, 0)
	has_a_check := false

	tup_ofs := data.expr_ofs
	tup_type := data.expr_type

	total := len(pat.Children)

	elem_ofs := cl.LocalPush()

	// Note: parsing code has checked that all field are unique

	for i, sub := range pat.Children {
		var t_field *Token
		var t_value *Token

		if sub.Kind == ND_TuplePair {
			t_field = sub.Children[0]
			t_value = sub.Children[1]
		} else {
			t_value = sub
		}

		// can completely skip a match-all pattern
		if t_value.Kind == ND_MatchAll {
			continue
		}

		if has_a_check {
			end_jump_pcs = append(end_jump_pcs, len(cl.vm_ops))
			cl.Emit3(OP_IF_FALSE, NO_PART, -7 /* dummy value */)
		}

		// read the given tuple/class field
		field_idx := i

		if t_field != nil {
			field_idx = t_field.Info.tag_id
		} else if pat.Info.open < 0 {
			field_idx = len(tup_type.param) - total + i
		}

		cl.Emit2(OP_LOC_READ, tup_ofs)

		if tup_type.base == TYP_Class {
			cl.Emit2(OP_OBJ_READ, int32(field_idx))
		} else {
			cl.Emit2(OP_TUPL_READ, int32(field_idx))
		}
		cl.Emit2(OP_LOC_WRITE, elem_ofs)

		// pattern-match against the element
		var data2 MatchData

		data2.rule = data.rule
		data2.expr_ofs = elem_ofs
		data2.expr_type = tup_type.param[field_idx].ty

		if cl.CompileMatchPattern(t_value, &data2) != OKAY {
			return FAILED
		}

		has_a_check = true
	}

	// if all field patterns were "_", force result to be true
	if !has_a_check {
		cl.Emit1(OP_TRUE)
	}

	cl.FixEndJumps(end_jump_pcs)
	cl.LocalDrop(1)

	return OKAY
}

func (cl *Closure) CompilePattern_Array(pat *Token, data *MatchData) cmError {
	// operations we need to fix for the end position
	end_jump_pcs := make([]int, 0)

	arr_ofs := data.expr_ofs
	arr_type := data.expr_type

	elem_ofs := cl.LocalPush()

	// perform check on length, leave a TRUE/FALSE in "A" reg
	total := len(pat.Children)
	cl.MatchCheckLength(arr_ofs, total, pat.Info.open)

	/* second thing we do: attempt to match each element */

	for i, t_sub := range pat.Children {
		// can completely skip a "_" match
		if t_sub.Kind == ND_MatchAll {
			continue
		}

		end_jump_pcs = append(end_jump_pcs, len(cl.vm_ops))
		cl.Emit3(OP_IF_FALSE, NO_PART, -8 /* dummy value */)

		// read current element of the array
		index := i
		if pat.Info.open < 0 {
			index = i - total // will be negative!
		}
		cl.Emit2(OP_LOC_READ, arr_ofs)
		cl.Emit2(OP_ARR_GET, int32(index))
		cl.Emit2(OP_LOC_WRITE, elem_ofs)

		// pattern-match against the element
		var data2 MatchData

		data2.rule = data.rule
		data2.expr_ofs = elem_ofs
		data2.expr_type = arr_type.sub

		if cl.CompileMatchPattern(t_sub, &data2) != OKAY {
			return FAILED
		}
	}

	cl.FixEndJumps(end_jump_pcs)
	cl.LocalDrop(1)

	return OKAY
}

func (cl *Closure) CompilePattern_Set(pat *Token, data *MatchData) cmError {
	end_jump_pcs := make([]int, 0)

	set_ofs := data.expr_ofs
	set_type := data.expr_type
	_ = set_type

	// perform check on length, leave a TRUE/FALSE in "A" reg
	total := len(pat.Children)
	cl.MatchCheckLength(set_ofs, total, pat.Info.open)

	seen_keys := make(map[string]bool)

	for _, t_key := range pat.Children {
		if t_key.Kind == ND_MatchAll {
			continue
		}

		end_jump_pcs = append(end_jump_pcs, len(cl.vm_ops))
		cl.Emit3(OP_IF_FALSE, NO_PART, -9 /* dummy value */)

		if cl.MatchMapKey(t_key, seen_keys, data) != OKAY {
			return FAILED
		}
	}

	cl.FixEndJumps(end_jump_pcs)
	return OKAY
}

func (cl *Closure) CompilePattern_Map(pat *Token, data *MatchData) cmError {
	end_jump_pcs := make([]int, 0)

	map_ofs := data.expr_ofs
	map_type := data.expr_type
	access_type := map_type.MapAccessType()

	elem_ofs := cl.LocalPush()

	// perform check on length, leave a TRUE/FALSE in "A" reg
	total := len(pat.Children)
	cl.MatchCheckLength(map_ofs, total, pat.Info.open)

	// this is used to detect duplicate keys
	seen_keys := make(map[string]bool)

	for _, t_pair := range pat.Children {
		t_key := t_pair.Children[0]
		t_elem := t_pair.Children[1]

		if t_key.Kind == ND_MatchAll && t_elem.Kind == ND_MatchAll {
			continue
		}

		end_jump_pcs = append(end_jump_pcs, len(cl.vm_ops))
		cl.Emit3(OP_IF_FALSE, NO_PART, -9 /* dummy value */)

		if cl.MatchMapKey(t_key, seen_keys, data) != OKAY {
			return FAILED
		}

		// skip value check when value is the match-all pattern
		if t_elem.Kind == ND_MatchAll {
			continue
		}

		// the MatchMapKey() above has confirmed the key exists, but
		// here we support matching or binding the element.

		end_jump_pcs = append(end_jump_pcs, len(cl.vm_ops))
		cl.Emit3(OP_IF_FALSE, NO_PART, -9 /* dummy value */)

		// read the element of the map
		cl.MatchCompileKey(t_key, nil)
		cl.Emit1(OP_MOVE_C)

		cl.Emit2(OP_LOC_READ, map_ofs)
		cl.Emit2(OP_MAP_READ, cl.AddType(access_type))
		cl.Emit1(OP_UNWRAP)

		cl.Emit2(OP_LOC_WRITE, elem_ofs)

		// pattern-match against the element
		var data2 MatchData

		data2.rule = data.rule
		data2.expr_ofs = elem_ofs
		data2.expr_type = map_type.sub

		if cl.CompileMatchPattern(t_elem, &data2) != OKAY {
			return FAILED
		}
	}

	cl.FixEndJumps(end_jump_pcs)
	cl.LocalDrop(1)

	return OKAY
}

func (cl *Closure) MatchCheckLength(S int32, total, open int) {
	cl.Emit2(OP_LOC_READ, S)

	if open == 0 {
		cl.Emit2(OP_IS_LEN, int32(total))
	} else {
		// pattern has '...' at start or end
		cl.Emit2(OP_MIN_LEN, int32(total))
	}
}

func (cl *Closure) MatchMapKey(t_key *Token, seen_keys map[string]bool, data *MatchData) cmError {
	// compile code to check the key exists in the map or set,
	// producing a boolean value in A register.
	// the "seen_keys" param is used to detect duplicate keys.

	if cl.MatchCompileKey(t_key, seen_keys) != OKAY {
		return FAILED
	}

	cl.Emit1(OP_MOVE_C)
	cl.Emit2(OP_LOC_READ, data.expr_ofs /* map */)

	if data.expr_type.base == TYP_Set {
		cl.Emit1(OP_SET_HAS)
	} else {
		cl.Emit1(OP_MAP_HAS)
	}
	return OKAY
}

func (cl *Closure) MatchCompileKey(t_key *Token, seen_keys map[string]bool) cmError {
	if t_key.Kind != ND_MatchConsts {
		PostError("BUG: weird node in map/set pattern: %s", t_key.String())
		return FAILED
	}

	t_key = t_key.Children[0]

	if t_key.Kind == ND_Global {
		if seen_keys != nil {
			hash := fmt.Sprintf("global_%p", t_key.Info.gdef)
			if seen_keys[hash] {
				PostError("map pattern has duplicate key: %s", t_key.String())
				return FAILED
			}
			seen_keys[hash] = true
		}

		return cl.CompileGlobal(t_key)
	}

	switch t_key.Info.ty.base {
	case TYP_Int, TYP_Float, TYP_String, TYP_Enum, TYP_Function:
		// ok
	default:
		PostError("map key has weird type: %s", t_key.Info.ty.String())
		return FAILED
	}

	var lit Value
	if lit.EvaluateTree(t_key) != OKAY {
		return FAILED
	}

	if seen_keys != nil {
		hash := lit.HashString()
		if seen_keys[hash] {
			PostError("map pattern has duplicate key: %s", t_key.String())
			return FAILED
		}
		seen_keys[hash] = true
	}

	cl.Emit2(OP_CONST, cl.AddConstant(lit))
	return OKAY
}

func (cl *Closure) MatchTag(expr_ofs int32, ty *Type, tag_id int) {
	cl.Emit2(OP_LOC_READ, expr_ofs)

	if ty.base == TYP_Enum {
		cl.Emit1(OP_ENUM_TAG)
	} else {
		cl.Emit1(OP_UNION_TAG)
	}

	cl.Emit2(OP_INT_B, int32(tag_id))
	cl.Emit1(OP2_INT_EQ)
}

func (cl *Closure) FixEndJumps(jump_pcs []int) {
	end_pc := len(cl.vm_ops)

	for _, pc := range jump_pcs {
		cl.vm_ops[pc].D = int32(end_pc)
	}
}
