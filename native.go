// Copyright 2020 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "os"
import "strings"
import "unicode"

type NativeProgram struct {
	file *os.File

	top_exprs []*Closure
}

var native *NativeProgram

func NativeInit() {
	native = new(NativeProgram)
	native.top_exprs = make([]*Closure, 0)
}

func NativeAddExpr(expr *Closure) {
	native.top_exprs = append(native.top_exprs, expr)
}

func NativeSave() {
	if len(native.top_exprs) == 0 {
		Print("program has no top level expressions!\n")
		os.Exit(1)
	}

	// TEMPORARY STUFF FOR OUTPUT (FIXME)
	f, err := os.Create("temp_nacl.s")
	if err != nil {
		Print("Failed to create ASM code file\n")
		os.Exit(1)
	}

	native.file = f
	defer native.file.Close()

	// TODO
}

//----------------------------------------------------------------------

func (np *NativeProgram) EncodeName(name string) string {
	// encode identifiers to be compatible with C/ASM code:
	//   1. letters and digits stay the same
	//   2. convert '-' to "__", except at beginning
	//   3. escape rest as "_XX" or "_uXXXX" or "_vXXXXXXXX"
	//   4. if solely ASCII letters and digits, append "_"

	has_escape := false

	var sb strings.Builder

	for pos, ch := range []rune(name) {
		if ch > 0xFFFF {
			fmt.Fprintf(&sb, "_v%08X", ch)
			has_escape = true
		} else if ch > 0xFF {
			fmt.Fprintf(&sb, "_u%04X", ch)
			has_escape = true
		} else if ch == '-' && pos > 0 {
			sb.WriteString("__")
			has_escape = true
		} else if unicode.IsLetter(ch) || unicode.IsDigit(ch) {
			sb.WriteByte(byte(ch))
		} else {
			fmt.Fprintf(&sb, "_%02X", ch)
			has_escape = true
		}
	}

	// prevent words like "goto" or "sin" clashing with C keywords
	if !has_escape {
		sb.WriteByte(byte('_'))
	}

	return sb.String()
}
