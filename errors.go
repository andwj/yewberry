// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "os"
import "fmt"
// import "strings"
import "path/filepath"

var Ok error = nil

// cmError is a very coarse indicator of a compiling error,
// primarily whether or not something succeeded to parse or
// compile.
type cmError int

const (
	OKAY cmError = iota
	FAILED
)

type LocError struct {
	Str  string // error message
	Mod  string // module the error occurred in, or ""
	File string // filename of error, or ""
	Func string // function which was being compiled, or ""
	Line int    // line # where error occurred, or 0
	Col  int    // column # of error, or 0
}

// estate contains the current location of parsing (module, file, line, etc)
var estate LocError

// error_list is the list of all errors posted with PostError()
var error_list []*LocError

func ClearErrors() {
	error_list = make([]*LocError, 0)

	estate.Mod = ""
	estate.File = ""
	estate.Func = ""
	estate.Line = 0
	estate.Col = 0
}

func HaveErrors() bool {
	return len(error_list) > 0
}

func ShowAllErrors() {
	for i, err := range error_list {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Str)

		loc_str := err.Location()
		if loc_str != "" {
			fmt.Fprintf(os.Stderr, "Occurred%s\n", loc_str)
		}

		if i+1 < len(error_list) {
			fmt.Fprintf(os.Stderr, "\n")
		}
	}
}

func PostError(format string, a ...interface{}) {
	// TODO have error_limit var, honor it

	var L LocError

	// copy the current location info
	L = estate

	// set message
	L.Str = fmt.Sprintf(format, a...)

	error_list = append(error_list, &L)
}

func ErrorSetModule(name string) {
	estate.Mod = name

	ErrorSetFile("")
}

func ErrorSetFile(name string) {
	estate.File = name

	ErrorSetFunc("")
	ErrorSetLine(0)
}

func ErrorSetFunc(name string) {
	estate.Func = name
}

func ErrorSetLine(lno int) {
	estate.Line = lno
}

func ErrorSetToken(t *Token) {
	if t.LineNum > 0 {
		estate.Line = t.LineNum
	}
}

//----------------------------------------------------------------------

func (err *LocError) Error() string {
	return err.Str
}

func (err *LocError) Location() string {
	l_part := ""
	if err.Line > 0 {
		l_part = fmt.Sprintf(" near line %d", err.Line)
	}

	f_part := ""
	if err.File != "" {
		f_part = " in " + filepath.Base(err.File)
	}

	g_part := ""
	if err.Func != "" && err.Func[0] == '.' {
		g_part = " in method '" + err.Func + "'"
	} else if err.Func != "" {
		g_part = " in function '" + err.Func + "'"
	}

	return l_part + f_part + g_part
}
