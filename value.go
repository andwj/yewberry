// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "strings"
import "sort"
import "math"

type Value struct {
	ty *Type

	Int   int64
	Float float64
	Str   string

	Array *ArrayImpl
	Union *UnionImpl
	Map   *MapImpl
	Clos  *Closure
}

type ArrayImpl struct {
	data []Value
}

type MapImpl struct {
	// there is one map for each key type (mainly to support
	// iterating over the map with the "map-for-each" syntax).
	//
	// we are forced to use a pointer to Value here, because Go
	// does not allow taking the address of elements in a map.
	i_data map[int64]*Value
	f_data map[float64]*Value
	s_data map[string]*Value

	// hash version used for enums, tuples, etc
	h_data map[string]*KeyDatumPair
}

type UnionImpl struct {
	tag_id int
	datum  Value
}

type KeyDatumPair struct {
	key   Value
	datum Value
}

type Formatter struct {
	before string // NOTE: only used by parser
	after  string //

	verb  rune   // letter, or 0 if string had no format directive
	field string // field width, "" if not given
	prec  string // precision, "" if not given

	plus  bool // '+' flag in YB, Go and C
	minus bool // '-' flag in YB, ' ' in Go/C
	quote bool // ''' flag in YB

	left  bool // '<' flag in YB, '-' in Go/C
	right bool // '>' flag in YB
	zeros bool // '0' at start of field width
}

//----------------------------------------------------------------------

// Copy stores a direct (bit-for-bit) copy of the other value into
// this Value struct.
func (v *Value) Copy(other *Value) {
	*v = *other
}

func (v *Value) CopyArray() Value {
	v.VerifyType(TYP_Array, "CopyArray")

	// this is a shallow copy: the copy (and its elements) will
	// occupy new memory locations, but the elements themselves
	// can alias the values of the original.

	res := Value{}
	res.MakeArray(len(v.Array.data), v.ty)

	copy(res.Array.data, v.Array.data)

	return res
}

func (v *Value) CopyByteVec() Value {
	v.VerifyType(TYP_ByteVec, "CopyByteVec")

	res := Value{}
	res.MakeByteVec(len(v.Array.data))

	copy(res.Array.data, v.Array.data)

	return res
}

func (v *Value) CopyObject() Value {
	v.VerifyType(TYP_Class, "CopyObject")

	// this is a shallow copy, the copy (and its elements) will
	// occupy new memory locations, but the elements themselves
	// can alias the values of the original.

	res := Value{}
	res.MakeTuple(v.ty)

	copy(res.Array.data, v.Array.data)

	return res
}

func (v *Value) CopyMap() Value {
	// NOTE: used for sets too
	///  v.VerifyType(TYP_Map, "CopyMap")

	res := Value{}

	res.MakeMap(v.ty)
	res.JoinMap(v)

	return res
}

func (v *Value) ClearMap() {
	///  v.VerifyType(TYP_Map, "ClearMap")

	key_type := v.ty.KeyType()

	switch key_type.base {
	case TYP_Int:
		v.Map.i_data = make(map[int64]*Value)
	case TYP_Float:
		v.Map.f_data = make(map[float64]*Value)
	case TYP_String:
		v.Map.s_data = make(map[string]*Value)
	default:
		v.Map.h_data = make(map[string]*KeyDatumPair)
	}
}

func (v *Value) JoinMap(other *Value) {
	// NOTE: this assume types are compatible

///  v.VerifyType(TYP_Map, "CopyMap")

///  other.VerifyType(TYP_Map, "CopyMap")

	key_type := v.ty.KeyType()

	var key Value

	switch key_type.base {
	case TYP_Int:
		for ckey, cval := range other.Map.i_data {
			key.MakeInt(ckey)
			v.KeySet(key, *cval)
		}

	case TYP_Float:
		for ckey, cval := range other.Map.f_data {
			key.MakeFloat(ckey)
			v.KeySet(key, *cval)
		}

	case TYP_String:
		for ckey, cval := range other.Map.s_data {
			key.MakeString(ckey)
			v.KeySet(key, *cval)
		}

	default:
		for _, kd := range other.Map.h_data {
			v.KeySet(kd.key, kd.datum)
		}
	}
}

func (v *Value) Reset() {
	v.Int = 0
	v.Float = 0.0
	v.Str = ""

	v.Array = nil
	v.Union = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeInt(num int64) {
	v.Reset()
	v.ty = int_type
	v.Int = num
}

func (v *Value) MakeFloat(num float64) {
	v.Reset()
	v.ty = float_type
	v.Float = num
}

func (v *Value) MakeVoid() {
	v.Reset()
	v.ty = void_type
}

func (v *Value) MakeChar(c rune) {
	v.Reset()
	v.ty = char_type
	v.Int = int64(c)
}

func (v *Value) MakeBool(b bool) {
	if b {
		v.Copy(glob_TRUE.loc)
	} else {
		v.Copy(glob_FALSE.loc)
	}
}

func (v *Value) MakeNone(opt_ty *Type) {
	v.MakeUnion(opt_ty)
	v.Union.datum.MakeVoid()
}

func (v *Value) MakeString(s string) {
	v.Reset()
	v.ty = str_type
	v.Str = s
}

func (v *Value) MakeArray(size int, ty *Type) {
	impl := new(ArrayImpl)
	impl.data = make([]Value, size)

	v.Reset()
	v.ty = ty
	v.Array = impl
}

func (v *Value) MakeByteVec(size int) {
	impl := new(ArrayImpl)
	impl.data = make([]Value, size)

	v.Reset()
	v.ty = bytevec_type
	v.Array = impl
}

func (v *Value) MakeTuple(ty *Type) {
	impl := new(ArrayImpl)
	impl.data = make([]Value, len(ty.param))

	v.Reset()
	v.ty = ty
	v.Array = impl
}

func (v *Value) MakeObject(ty *Type) {
	impl := new(ArrayImpl)
	impl.data = make([]Value, len(ty.param))

	v.Reset()
	v.ty = ty
	v.Array = impl
}

func (v *Value) MakeUnion(ty *Type) {
	impl := new(UnionImpl)

	v.Reset()
	v.ty = ty
	v.Union = impl
}

func (v *Value) MakeEnum(ty *Type, tag int64) {
	v.Reset()
	v.ty = ty
	v.Int = tag
}

func (v *Value) MakeMap(ty *Type) {
	// NOTE: used for sets too

	impl := new(MapImpl)

	key_type := ty.KeyType()

	switch key_type.base {
	case TYP_Int:
		impl.i_data = make(map[int64]*Value)
	case TYP_Float:
		impl.f_data = make(map[float64]*Value)
	case TYP_String:
		impl.s_data = make(map[string]*Value)
	default:
		impl.h_data = make(map[string]*KeyDatumPair)
	}

	v.Reset()
	v.ty = ty
	v.Map = impl
}

func (v *Value) MakeFunction(cl *Closure) {
	v.Reset()
	v.ty = cl.ty
	v.Clos = cl
}

func (v *Value) MakeUnfinishedFunction(cl *Closure) {
	v.Reset()
	v.ty = UNFINISHED_FUNC
	v.Clos = cl
}

func (v *Value) MakeUnfinishedVar() {
	v.Reset()
	v.ty = UNFINISHED_VAR
}

func (v *Value) MakeDefault(ty *Type) {
	if ty.custom == "bool" {
		v.MakeBool(false)
		return
	}

	switch ty.base {
	case TYP_Array:
		v.MakeArray(0, ty)

	case TYP_ByteVec:
		v.MakeByteVec(0)

	case TYP_Map, TYP_Set:
		v.MakeMap(ty)

	case TYP_Union:
		// we rely on the first tag/datum pair being void
		v.MakeUnion(ty)
		v.Union.datum.MakeVoid()

	case TYP_Enum:
		v.MakeEnum(ty, 0)

	case TYP_Tuple:
		// we rely on the tuple not being cyclic here
		v.MakeTuple(ty)
		for i, sub := range ty.param {
			v.Array.data[i].MakeDefault(sub.ty)
		}

	case TYP_Class:
		panic("MakeDefault called with class type")

	case TYP_Function:
		panic("MakeDefault called with function type")

	default:
		// the following suffices for everything else
		v.Reset()
		v.ty = ty
	}
}

//----------------------------------------------------------------------

func (v *Value) Wrap(opt_type *Type) {
	var old Value
	old.Copy(v)

	v.MakeUnion(opt_type)

	v.Union.tag_id = 1
	v.Union.datum.Copy(&old)
}

func (v *Value) Unwrap() {
	if v.Union.tag_id == 0 {
		panic("Value.Unwrap called on a `NONE or `ERR value")
	}
	v.Copy(&v.Union.datum)
}

func (v *Value) Flatten() {
	if v.Union.tag_id == 0 {
		v.MakeDefault(v.ty.param[1].ty)
	} else {
		v.Copy(&v.Union.datum)
	}
}

func (v *Value) IsFalse() bool {
	return (v.ty.base == TYP_Enum && v.Int == 0)
}

func (v *Value) IsTrue() bool {
	return !(v.ty.base == TYP_Enum && v.Int == 0)
}

func (v *Value) IsNone() bool {
	return (v.ty.base == TYP_Union && v.Union.tag_id == 0)
}

func (v *Value) RefEqual(other *Value) bool {
	ref_type := v.ty

	switch ref_type.base {
	case TYP_Array, TYP_ByteVec, TYP_Class:
		return (v.Array == other.Array)

	case TYP_Map, TYP_Set:
		return (v.Map == other.Map)

	case TYP_Function:
		return (v.Clos == other.Clos)

	default:
		panic("Value.RefEqual with non-ref type")
	}
}

func (v *Value) EqualAsConstant(other *Value) bool {
	if v.ty.base != other.ty.base {
		return false
	}

	// NOTE: we ignore custom types here, e.g. (Dur 10) == 10

	switch v.ty.base {
	case TYP_Void:
		return true

	case TYP_Int:
		return v.Int == other.Int

	case TYP_Float:
		return v.Float == other.Float

	case TYP_String:
		return v.Str == other.Str

	case TYP_Function:
		return v.Clos == other.Clos

	case TYP_Tuple:
		if v.ty != other.ty {
			return false
		}
		for i := 0; i < len(v.ty.param); i++ {
			A := &v.Array.data[i]
			B := &other.Array.data[i]

			if !A.EqualAsConstant(B) {
				return false
			}
		}
		return true

	case TYP_Union:
		if v.ty != other.ty {
			return false
		}
		if v.Union.tag_id != other.Union.tag_id {
			return false
		}
		return v.Union.datum.EqualAsConstant(&other.Union.datum)

	case TYP_Enum:
		if v.ty != other.ty {
			return false
		}
		return v.Int == other.Int

	default:
		// no need to compare arrays, maps, or objects
		return false
	}
}

func (v *Value) VerifyType(base BaseType, where string) {
	if v.ty == nil {
		panic(where + ": unset/illegal value found")
	}

	if base == BTYP_Bool {
		if v.ty.custom != "bool" {
			panic(where + ": verify type failed")
		}
		return
	}
	if base == BTYP_Char {
		if v.ty.custom != "char" {
			panic(where + ": verify type failed")
		}
		return
	}

	// some exec ops are used for both arrays and byte-vecs
	if base == TYP_Array && v.ty.base == TYP_ByteVec {
		return
	}

	if v.ty.base != base {
		panic(where + ": verify type failed")
	}
}

func (v *Value) VerifyKeyType(key_type *Type, where string) {
	v.VerifyType(key_type.base, where)
}

//----------------------------------------------------------------------

func (v *Value) GetElem(idx int) Value {
	if v.ty.custom == "buffer" {
		v.Array.data[idx].Int &= 255
	}

	return v.Array.data[idx]
}

func (v *Value) SetElem(idx int, elem Value) {
	v.Array.data[idx] = elem

	if v.ty.custom == "buffer" {
		v.Array.data[idx].Int &= 255
	}
}

func (v *Value) SwapElems(i, k int) {
	A := v.Array.data[i]
	B := v.Array.data[k]

	v.Array.data[i] = B
	v.Array.data[k] = A
}

func (v *Value) AppendElem(elem Value) {
	v.Array.data = append(v.Array.data, elem)
}

func (v *Value) InsertElem(idx int, elem Value) {
	// an index <= zero will insert at the beginning (prepend).
	// an index >= length will insert at the end (append).

	if idx < 0 {
		idx = 0
	}
	if idx >= len(v.Array.data) {
		v.AppendElem(elem)
		return
	}

	// resize the array
	v.Array.data = append(v.Array.data, Value{})

	// shift elements up
	copy(v.Array.data[idx+1:], v.Array.data[idx:])

	// store new element
	v.SetElem(idx, elem)
}

func (v *Value) DeleteElem(idx int) {
	size := len(v.Array.data)

	// trying to remove non-existent elements is a no-op
	if idx < 0 || idx >= size {
		return
	}

	// shift elements down, if necessary
	if idx < size {
		copy(v.Array.data[idx:], v.Array.data[idx+1:])
	}

	// shrink the slice
	v.Array.data = v.Array.data[0 : size-1]
}

func (v *Value) ValidIndex(idx int) bool {
	if idx < 0 {
		return false
	}
	if idx >= len(v.Array.data) {
		return false
	}
	return true
}

func (v *Value) Length() int {
	switch v.ty.base {
	case TYP_Array, TYP_ByteVec:
		return len(v.Array.data)

	case TYP_Map, TYP_Set:
		return len(v.Map.i_data) + len(v.Map.s_data) +
			len(v.Map.f_data) + len(v.Map.h_data)

	case TYP_String:
		return len([]rune(v.Str))

	default:
		return 1
	}
}

//----------------------------------------------------------------------

func (v *Value) KeyExists(key Value) bool {
	// NOTE: used for sets and maps

	var ok bool

	switch key.ty.base {
	case TYP_Int:
		_, ok = v.Map.i_data[key.Int]
	case TYP_Float:
		_, ok = v.Map.f_data[key.Float]
	case TYP_String:
		_, ok = v.Map.s_data[key.Str]
	default:
		hash := key.HashString()
		_, ok = v.Map.h_data[hash]
	}

	return ok
}

func (v *Value) KeyGet(key Value) Value {
	// NOTE: used for both sets and maps.
	// Also, be sure to check if key exists first!

	var ptr *Value
	var kd *KeyDatumPair
	var ok bool

	switch key.ty.base {
	case TYP_Int:
		ptr, ok = v.Map.i_data[key.Int]
	case TYP_Float:
		ptr, ok = v.Map.f_data[key.Float]
	case TYP_String:
		ptr, ok = v.Map.s_data[key.Str]
	default:
		hash := key.HashString()
		kd, ok = v.Map.h_data[hash]
		if ok {
			ptr = &kd.datum
		}
	}

	if !ok {
		panic("KeyGet used with a non-existent key")
	}

	return *ptr
}

func (v *Value) KeySet(key Value, newval Value) {
	// NOTE: used for sets and maps

	if v.ty.base == TYP_Set && newval.IsFalse() {
		v.KeyDelete(key)
		return
	}

	var ptr *Value
	var kd *KeyDatumPair
	var ok bool

	switch key.ty.base {
	case TYP_Int:
		ptr, ok = v.Map.i_data[key.Int]
		if !ok {
			ptr = new(Value)
			v.Map.i_data[key.Int] = ptr
		}

	case TYP_Float:
		ptr, ok = v.Map.f_data[key.Float]
		if !ok {
			ptr = new(Value)
			v.Map.f_data[key.Float] = ptr
		}

	case TYP_String:
		ptr, ok = v.Map.s_data[key.Str]
		if !ok {
			ptr = new(Value)
			v.Map.s_data[key.Str] = ptr
		}

	default:
		hash := key.HashString()
		kd, ok = v.Map.h_data[hash]
		if !ok {
			kd = new(KeyDatumPair)
			v.Map.h_data[hash] = kd
		}
		kd.key = key
		kd.datum = newval
		return
	}

	*ptr = newval
}

func (v *Value) KeyDelete(key Value) {
	// NOTE: used for sets and maps

	switch key.ty.base {
	case TYP_Int:
		delete(v.Map.i_data, key.Int)
	case TYP_Float:
		delete(v.Map.f_data, key.Float)
	case TYP_String:
		delete(v.Map.s_data, key.Str)
	default:
		hash := key.HashString()
		delete(v.Map.h_data, hash)
	}
}

func (v *Value) CollectKeys() Value {
	key_type := v.ty.KeyType()

	arr_type := NewType(TYP_Array)
	arr_type.sub = key_type

	var list Value
	list.MakeArray(0, arr_type)

	switch key_type.base {
	case TYP_Int:
		for key, _ := range v.Map.i_data {
			var v Value
			v.MakeInt(key)
			list.AppendElem(v)
		}

	case TYP_Float:
		for key, _ := range v.Map.f_data {
			var v Value
			v.MakeFloat(key)
			list.AppendElem(v)
		}

	case TYP_String:
		for key, _ := range v.Map.s_data {
			var v Value
			v.MakeString(key)
			list.AppendElem(v)
		}

	default:
		for _, kd := range v.Map.h_data {
			list.AppendElem(kd.key)
		}
	}

	return list
}

// HashString is used to create a string for the value which is
// usable as the key in a Go map.  It is mainly to support using
// enum and tuple values as yewberry map keys.  The hash strings
// are designed to never collide with different types (e.g. a
// float hash is never the same as an integer hash).
func (v *Value) HashString() string {
	switch v.ty.base {
	case TYP_Void, TYP_Int, TYP_String, TYP_Enum:
		return v.DeepString()
	case TYP_Float:
		return fmt.Sprintf("%b", v.Float)
	case TYP_Union:
		return v.UnionHashString()
	case TYP_Tuple:
		return v.TupleHashString()
	case TYP_Array, TYP_ByteVec, TYP_Map, TYP_Set, TYP_Class, TYP_Function:
		return v.ReferenceHash()
	default:
		return "!!BAD-VALUE!!"
	}
}

func (v *Value) UnionHashString() string {
	tag_id := v.Union.tag_id
	if tag_id < 0 || tag_id >= len(v.ty.param) {
		return "!!BAD-ENUM!!"
	}

	var sb strings.Builder

	sb.WriteString(v.ty.param[tag_id].name)
	sb.WriteString("::")
	sb.WriteString(v.Union.datum.HashString())

	return sb.String()
}

func (v *Value) TupleHashString() string {
	var sb strings.Builder

	sb.WriteString("{")

	for i := range v.Array.data {
		if i > 0 {
			sb.WriteString(" ")
		}
		sb.WriteString(v.Array.data[i].HashString())
	}

	sb.WriteString("}")
	return sb.String()
}

// ReferenceHash creates a hash string for reference types only.
// Everything else will produce an empty string.
// Tuples and functions are handled, but enums are not.
// TODO: tuples are conceptually value types -- remove here
func (v *Value) ReferenceHash() string {
	switch v.ty.base {
	case TYP_Array:
		return fmt.Sprintf("arr#%p", v.Array)
	case TYP_ByteVec:
		return fmt.Sprintf("bvc#%p", v.Array)
	case TYP_Tuple:
		return fmt.Sprintf("tup#%p", v.Array)
	case TYP_Class:
		return fmt.Sprintf("obj#%p", v.Array /* not a typo */)
	case TYP_Map:
		return fmt.Sprintf("map#%p", v.Map)
	case TYP_Set:
		return fmt.Sprintf("set#%p", v.Map)
	case TYP_Function:
		return fmt.Sprintf("fun#%p", v.Clos)
	default:
		return ""
	}
}

//----------------------------------------------------------------------

func MakeConstant(name string, ty *Type) *GlobalDef {
	return MakeGlobal(name, ty, false)
}

func MakeGlobal(name string, ty *Type, mutable bool) *GlobalDef {
	def := context.GetDef(name)

	if def == nil {
		def = new(GlobalDef)

		// create a new memory location for the variable
		// [ caller will need to set it up properly ]
		def.loc = new(Value)

		context.AddDef(name, def)
	}

	def.loc.MakeUnfinishedVar()

	def.ty = ty // can be nil
	def.name = name
	def.mutable = mutable

	return def
}

func LookupStandardGlobal(name string) *GlobalDef {
	def := prelude.GetDef(name)
	if def == nil {
		panic("Global not found: " + name)
	}
	return def
}

//----------------------------------------------------------------------

// EvaluateTree takes a code tree and evaluates it into the
// receiver value.  This evaluation is in a "non-code" context
// and hence much more limited than what normal code can do
// (for example, functions cannot be called).  The code tree
// must be processed by ParseElement() and DeduceStaticValue()
// before calling this.
func (v *Value) EvaluateTree(t *Token) cmError {
	ErrorSetToken(t)

	switch t.Kind {
	case ND_Void:
		v.MakeVoid()
		return OKAY

	case ND_Literal:
		v.Copy(t.Info.lit)
		return OKAY

	case TOK_Name, ND_Global:
		return v.EvaluateGlobal(t)

	case ND_Array:
		return v.EvaluateArray(t)

	case ND_Tuple, ND_Object:
		return v.EvaluateTuple(t)

	case ND_Union:
		return v.EvaluateUnion(t)

	case ND_Map:
		return v.EvaluateMap(t)

	case ND_Set:
		return v.EvaluateSet(t)

	case ND_Cast:
		return v.EvaluateCast(t)

	case ND_Wrap:
		return v.EvaluateWrap(t)

	case ND_Unwrap:
		return v.EvaluateUnwrap(t)

	case ND_Flatten:
		return v.EvaluateFlatten(t)

	case TOK_Expr, ND_FunCall, ND_MethodCall:
		PostError("cannot call functions in non-code context")
		return FAILED

	case TOK_Access:
		PostError("cannot access data in non-code context")
		return FAILED

///  case ND_MathOp:
///		return v.EvaluateMathOp(t)

	default:
		PostError("bad literal value, got: %s", t.String())
		return FAILED
	}
}

func (v *Value) EvaluateGlobal(t *Token) cmError {
	name := t.Str

	// look up existing definition
	def := N_GetDef(name, t.Module)
	if def == nil {
		PostError("no such global var '%s'", name)
		return FAILED
	}

	if def.mutable {
		PostError("mutable variable '%s' in constant expression", name)
		return FAILED
	}

	v.Copy(def.loc)
	v.ty = def.ty

	return OKAY
}

func (v *Value) EvaluateCast(t *Token) cmError {
	new_type := t.Info.ty
	if new_type == nil { panic("no type for EvaluateCast") }

	if v.EvaluateTree(t.Children[0]) != OKAY {
		return FAILED
	}

	// conversions between int and float
	if new_type.base == TYP_Int && v.ty.base == TYP_Float {
		v.Int = FloatToInt(v.Float)
	}
	if new_type.base == TYP_Float && v.ty.base == TYP_Int {
		v.Float = float64(v.Int)
	}

	v.ty = new_type
	return OKAY
}

func (v *Value) EvaluateWrap(t *Token) cmError {
	if v.EvaluateTree(t.Children[0]) != OKAY {
		return FAILED
	}

	v.Wrap(t.Info.ty)
	return OKAY
}

func (v *Value) EvaluateUnwrap(t *Token) cmError {
	if v.EvaluateTree(t.Children[0]) != OKAY {
		return FAILED
	}

	if v.Union.tag_id == 0 {
		PostError("attempt to unwrap a `NONE value")
		return FAILED
	}

	v.Unwrap()
	return OKAY
}

func (v *Value) EvaluateFlatten(t *Token) cmError {
	if v.EvaluateTree(t.Children[0]) != OKAY {
		return FAILED
	}

	v.Flatten()
	return OKAY
}

/*
func (v *Value) EvaluateMathOp(t *Token) cmError {
	var R Value

	if v.EvaluateTree(t.Children[0]) != OKAY {
		return FAILED
	}

	if len(t.Children) == 2 {
		itf R.EvaluateTree(t.Children[1]) != OKAY {
			return FAILED
		}
	}

	return t.Info.op.Compute(v, &R)
}
*/

func (v *Value) EvaluateArray(t *Token) cmError {
	arr_type := t.Info.ty
	if arr_type == nil { panic("EvaluateArray with unknown type") }

	if arr_type.base == TYP_ByteVec {
		v.MakeByteVec(0)
	} else {
		v.MakeArray(0, arr_type)
	}

	for _, t := range t.Children {
		var elem Value

		if elem.EvaluateTree(t) != OKAY {
			return FAILED
		}

		if arr_type.base == TYP_ByteVec {
			elem.Int &= 255
		}

		v.AppendElem(elem)
	}

	return OKAY
}

func (v *Value) EvaluateTuple(t *Token) cmError {
	// Note: this handles objects too

	tup_type := t.Info.ty
	if tup_type == nil { panic("EvaluateTuple with unknown type") }

	what := "tuple"

	if t.Kind == ND_Object {
		v.MakeObject(tup_type)
		what = "object"
	} else {
		v.MakeTuple(tup_type)
	}

	seen_fields := make([]bool, len(tup_type.param))

	for i, child := range t.Children {
		field_idx := i
		field_name := tup_type.param[i].name
		t_elem := child

		if child.Kind == ND_TuplePair {
			field_name = child.Children[0].Str
			t_elem = child.Children[1]

			if t.Kind != ND_Tuple {
				field_idx = tup_type.FindParam(field_name)
			}
		}

		if v.Array.data[field_idx].EvaluateTree(t_elem) != OKAY {
			return FAILED
		}

		seen_fields[field_idx] = true
	}

	// set unvisited fields to default values
	if t.Info.open > 0 {
		for i, par := range tup_type.param {
			if !seen_fields[i] {
				field_type := par.ty

				if !field_type.Defaultable() {
					PostError("cannot init %s field, type %s has no default value",
						what, field_type.String())
					return FAILED
				}

				v.Array.data[i].MakeDefault(field_type)
			}
		}
	}

	return OKAY
}

func (v *Value) EvaluateUnion(t *Token) cmError {
	union_type := t.Info.ty
	if union_type == nil { panic("EvaluateUnion with unknown type") }

	v.MakeUnion(union_type)
	v.Union.tag_id = t.Info.tag_id

	t_datum := t.Children[0]

	return v.Union.datum.EvaluateTree(t_datum)
}

func (v *Value) EvaluateMap(t *Token) cmError {
	map_type := t.Info.ty
	if map_type == nil { panic("EvaluateMap with unknown type") }

	v.MakeMap(map_type)

	for _, pair := range t.Children {
		t_key := pair.Children[0]
		t_elem := pair.Children[1]

		key := Value{}
		if key.EvaluateTree(t_key) != OKAY {
			return FAILED
		}

		elem := Value{}
		if elem.EvaluateTree(t_elem) != OKAY {
			return FAILED
		}

		// disallow duplicate keys
		if v.KeyExists(key) {
			PostError("duplicate key in map: %s", t_key.String())
			return FAILED
		}

		v.KeySet(key, elem)
	}

	return OKAY
}

func (v *Value) EvaluateSet(t *Token) cmError {
	set_type := t.Info.ty
	if set_type == nil { panic("EvaluateSet with unknown type") }

	v.MakeMap(set_type)

	for _, t_key := range t.Children {
		key := Value{}
		if key.EvaluateTree(t_key) != OKAY {
			return FAILED
		}

		// disallow duplicate keys
		if v.KeyExists(key) {
			PostError("duplicate key in set: %s", t_key.String())
			return FAILED
		}

		v.KeySet(key, *glob_TRUE.loc)
	}

	return OKAY
}

//----------------------------------------------------------------------

func (v *Value) DecodeInt(s string) error {
	var long_val int64
	n, _ := fmt.Sscanf(s, "%v", &long_val)

	if n != 1 {
		// hack to allow large hexadecimal values
		var ulong_val uint64
		n, _ = fmt.Sscanf(s, "%v", &ulong_val)

		if n == 1 {
			long_val = int64(ulong_val)
		} else {
			return fmt.Errorf("bad integer '%s'", s)
		}
	}

	v.MakeInt(long_val)

	return Ok
}

func (v *Value) DecodeFloat(s string) error {
	var raw float64

	n, _ := fmt.Sscanf(s, "%v", &raw)
	if n != 1 {
		return fmt.Errorf("bad float literal '%s'", s)
	}

	v.MakeFloat(raw)

	return Ok
}

func (v *Value) DecodeChar(s string) error {
	runes := []rune(s)

	if len(runes) == 0 {
		return fmt.Errorf("malformed character literal")
	}

	v.MakeChar(runes[0])

	return Ok
}

func (v *Value) DecodeString(s string) error {
	// too easy!
	v.MakeString(s)

	return Ok
}

//----------------------------------------------------------------------

func (v *Value) DeepString() string {
	// this keeps track of arrays/tuples which have been seen,
	// to prevent infinite recursion.
	seen := make(map[*Value]bool)

	return v.rawDeepString(seen)
}

func (v *Value) rawDeepString(seen map[*Value]bool) string {
	if v.ty == nil {
		// return "!!UNSET-VALUE!!"
		panic("rawDeepString: value with ty == nil")
	}

	base := v.ty.base

	if base >= TYP_Tuple {
		// already seen?
		_, ok := seen[v]
		if ok {
			return "#CyclicData#"
		}
		// mark it now
		seen[v] = true
	}

	if v.ty.custom == "bool" {
		return v.BoolToString()
	}
	if v.ty.custom == "char" {
		return v.CharToString()
	}

	switch base {
	case TYP_Void:
		return "{void}"

	case TYP_Int:
		return v.IntToString()

	case TYP_Float:
		return v.FloatToString()

	case TYP_String:
		return v.StringToString()

	case TYP_Array:
		return v.ArrayToString("array", seen)

	case TYP_ByteVec:
		return v.ArrayToString("byte-vec", seen)

	case TYP_Map:
		return v.MapToString(seen)

	case TYP_Set:
		return v.SetToString(seen)

	case TYP_Class:
		return v.TupleToString(seen, true)

	case TYP_Tuple:
		return v.TupleToString(seen, false)

	case TYP_Union:
		return v.UnionToString(seen)

	case TYP_Enum:
		return v.EnumToString(seen)

	case TYP_Function:
		return v.FunctionToString()

	default:
		return "!!INVALID!!"
	}
}

func (v *Value) IntToString() string {
	if v.Int == glob_INAN.loc.Int {
		return "INAN"
	} else {
		return fmt.Sprintf("%d", v.Int)
	}
}

func (v *Value) FloatToString() string {
	if math.IsNaN(v.Float) {
		return "NAN"

	} else if math.IsInf(v.Float, -1) {
		return "-INF"

	} else if math.IsInf(v.Float, 1) {
		return "+INF"
	
	} else {
		return fmt.Sprintf("%v", v.Float)
	}
}

func (v *Value) BoolToString() string {
	if v.IsTrue() {
		return "TRUE"
	} else {
		return "FALSE"
	}
}

func (v *Value) CharToString() string {
	return fmt.Sprintf("%q", v.Int)
}

func (v *Value) StringToString() string {
	// we need to surround the string with double quotes, and
	// escape any strange characters (including '"' itself).
	// luckily the %q verb in the fmt package does all this.

	return fmt.Sprintf("%q", v.Str)
}

func (v *Value) ArrayToString(kind string, seen map[*Value]bool) string {
	var sb strings.Builder

	sb.WriteString("{")
	sb.WriteString(kind)

	for i := range v.Array.data {
		child := v.GetElem(i)

		sb.WriteString(" ")
		sb.WriteString(child.rawDeepString(seen))
	}

	sb.WriteString("}")
	return sb.String()
}

func (v *Value) MapToString(seen map[*Value]bool) string {
	map_type := v.ty
	key_type := map_type.KeyType()

	var sb strings.Builder

	// collect all the key/value pairs, and sort them
	entries := make([]string, 0)

	switch key_type.base {
	case TYP_Int:
		for k, v := range v.Map.i_data {
			s := fmt.Sprintf("%d", k) + " " +
				v.rawDeepString(seen)
			entries = append(entries, s)
		}

	case TYP_Float:
		for k, v := range v.Map.f_data {
			s := fmt.Sprintf("%v", k) + " " +
				v.rawDeepString(seen)
			entries = append(entries, s)
		}

	case TYP_String:
		for k, v := range v.Map.s_data {
			s := fmt.Sprintf("%q", k) + " " +
				v.rawDeepString(seen)
			entries = append(entries, s)
		}

	default:
		for _, kd := range v.Map.h_data {
			s := kd.key.KeyToString() + " " +
				kd.datum.rawDeepString(seen)
			entries = append(entries, s)
		}
	}

	sort.Slice(entries, func(i, k int) bool {
		return entries[i] < entries[k]
	})

	sb.WriteString("{map")

	for _, name := range entries {
		sb.WriteString(" ")
		sb.WriteString(name)
	}

	sb.WriteString("}")
	return sb.String()
}

func (v *Value) SetToString(seen map[*Value]bool) string {
	map_type := v.ty
	key_type := map_type.KeyType()

	var sb strings.Builder

	// collect all the keys, and sort them
	entries := make([]string, 0)

	switch key_type.base {
	case TYP_Int:
		for k, _ := range v.Map.i_data {
			s := fmt.Sprintf("%d", k)
			entries = append(entries, s)
		}

	case TYP_Float:
		for k, _ := range v.Map.f_data {
			s := fmt.Sprintf("%v", k)
			entries = append(entries, s)
		}

	case TYP_String:
		for k, _ := range v.Map.s_data {
			s := fmt.Sprintf("%q", k)
			entries = append(entries, s)
		}

	default:
		for _, kd := range v.Map.h_data {
			s := kd.key.KeyToString()
			entries = append(entries, s)
		}
	}

	if key_type.base == TYP_Int {
		// attempt to get expected order for integers
		sort.Slice(entries, func(i, k int) bool {
			a := entries[i]
			b := entries[k]
			if (a[0] == '-') != (b[0] == '-') {
				return a[0] == '-'
			}
			if len(a) != len(b) {
				if a[0] == '-' {
					return len(a) > len(b)
				} else {
					return len(a) < len(b)
				}
			}
			return a < b
		})

	} else {
		sort.Slice(entries, func(i, k int) bool {
			return entries[i] < entries[k]
		})
	}

	sb.WriteString("{set")

	for _, name := range entries {
		sb.WriteString(" ")
		sb.WriteString(name)
	}

	sb.WriteString("}")
	return sb.String()
}

func (v *Value) KeyToString() string {
	// NOTE: we display reference types as a raw-ish pointer number,
	//       otherwise multiple arrays of the form "[1 2]" or so
	//       cannot be distinguished and it leads to confusion.
	if v.ty.base >= TYP_Array {
		return v.HashString()
	} else {
		return v.DeepString()
	}
}

func (v *Value) TupleToString(seen map[*Value]bool, is_class bool) string {
	tup_type := v.ty

	var sb strings.Builder
	sb.WriteString("{")

	for i := range v.Array.data {
		if i > 0 {
			sb.WriteString(" ")
		}

		if is_class {
			sb.WriteString(tup_type.param[i].name)
			sb.WriteString(" ")
		}

		child := v.Array.data[i]

		sb.WriteString(child.rawDeepString(seen))
	}

	sb.WriteString("}")
	return sb.String()
}

func (v *Value) UnionToString(seen map[*Value]bool) string {
	tag_id := v.Union.tag_id
	if tag_id < 0 || tag_id >= len(v.ty.param) {
		return "!!BAD-TAG!!"
	}

	var sb strings.Builder

	sb.WriteString("{")
	sb.WriteString(v.ty.param[tag_id].name)
	sb.WriteString(" ")
	sb.WriteString(v.Union.datum.rawDeepString(seen))
	sb.WriteString("}")

	return sb.String()
}

func (v *Value) EnumToString(seen map[*Value]bool) string {
	tag_id := int(v.Int)
	if tag_id < 0 || tag_id >= len(v.ty.param) {
		return "!!BAD-TAG!!"
	}

	return v.ty.param[tag_id].name
}

func (v *Value) FunctionToString() string {
	return "#Function:" + v.Clos.debug_name + "#"
}

//----------------------------------------------------------------------

func FormatDecode(s string) (*Formatter, error) {
	r := []rune(s)
	f := new(Formatter)

	pos := 0

	for pos < len(r) {
		ch := r[pos]
		pos++

		if ch != '%' {
			if f.verb == 0 {
				f.before += string(ch)
			} else {
				f.after += string(ch)
			}
			continue
		}

		if pos >= len(r) {
			return nil, fmt.Errorf("bad format str: directive has no verb")
		}

		// handle percent escape
		if r[pos] == '%' {
			if f.verb == 0 {
				f.before += string(ch)
			} else {
				f.after += string(ch)
			}
			pos++ // skip extra percent
			continue
		}

		if f.verb != 0 {
			return nil, fmt.Errorf("bad format str: multiple directives found")
		}

		/* parse the directive */

		do_field := false
		do_prec := false

		for pos < len(r) {
			ch := r[pos]
			pos++

			digit := ('0' <= ch && ch <= '9')

			if digit {
				if do_field {
					f.field += string(ch)
					continue
				}
				if do_prec {
					f.prec += string(ch)
					continue
				}
			}

			// stop parsing field width or precision
			if do_field && f.field == "" {
				f.field = "0"
			}
			if do_prec && f.prec == "" {
				f.prec = "0"
			}
			do_field = false
			do_prec = false

			// found the verb?
			if ('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z') {
				switch ch {
				case 'b', 'c', 'd', 'e', 'f', 'g' /* REVIEW */:
					// ok
				case 'o', 'p', 's', 't', 'x', 'X', 'Z':
					// ok
				default:
					return nil, fmt.Errorf("bad format str: unknown verb %%%c", ch)
				}

				f.verb = ch
				break
			}

			// begin field width?
			if digit {
				if f.field != "" {
					return nil, fmt.Errorf("bad format str: more than one field width")
				}
				do_field = true

				if ch == '0' {
					f.zeros = true
				} else {
					f.field += string(ch)
				}
				continue
			}

			// begin precision?
			if ch == '.' {
				if f.prec != "" {
					return nil, fmt.Errorf("bad format str: more than one precision")
				}
				do_prec = true
				continue
			}

			// everything else is a flag
			switch ch {
			case '+':
				f.plus = true
			case '-':
				f.minus = true
			case '\'':
				f.quote = true
			case '<':
				f.left = true
			case '>':
				f.right = true
			default:
				return nil, fmt.Errorf("bad format str: unknown flag %q", ch)
			}
		}

		if f.verb == 0 {
			return nil, fmt.Errorf("bad format str: directive has no verb")
		}
	}

	return f, Ok
}

func (f *Formatter) AutoFromType(ty *Type) cmError {
	if ty.custom == "char" {
		f.verb = 'c'
		f.quote = true
		return OKAY

	} else if ty.base == TYP_Int {
		f.verb = 'd'
		return OKAY

	} else if ty.base == TYP_Float {
		f.verb = 'f'
		return OKAY

	} else if ty.base == TYP_String {
		f.verb = 's'
		return OKAY

	} else if ty.base == TYP_Union || ty.base == TYP_Enum {
		f.verb = 't'
		return OKAY

	} else if ty.base >= TYP_Array {
		f.verb = 'p'
		return OKAY

	} else {
		PostError("cannot stringify a value of type: %s", ty.base.String())
		return FAILED
	}
}

func (f *Formatter) CheckType(ty *Type) cmError {
	switch f.verb {
	case 'b', 'c', 'd', 'o', 'x', 'X':
		if ty.base != TYP_Int {
			PostError("incompatible types for %%%c: wanted int, got %s",
				f.verb, ty.base.String())
			return FAILED
		}

	case 'e', 'f', 'g':
		if ty.base != TYP_Float {
			PostError("incompatible types for %%%c: wanted float, got %s",
				f.verb, ty.base.String())
			return FAILED
		}

	case 's':
		if ty.base != TYP_String {
			PostError("incompatible types for %%%c: wanted str, got %s",
				f.verb, ty.base.String())
			return FAILED
		}

	case 't':
		if !(ty.base == TYP_Union || ty.base == TYP_Enum) {
			PostError("incompatible types for %%%c: wanted union, got %s",
				f.verb, ty.base.String())
			return FAILED
		}

	case 'p':
		if ty.base < TYP_Array {
			PostError("incompatible types for %%%c: wanted a ref type, got %s",
				f.verb, ty.base.String())
			return FAILED
		}
	}

	return OKAY
}

func (f *Formatter) Encode() string {
	if f.verb == 'Z' {
		panic("tried to encode the %Z format directive")
	}

	// NOTE: the following is only suitable for Go's fmt package

	verb := string(f.verb)

	if f.quote {
		switch f.verb {
		case 'b': verb = "#b"
		case 'o': verb = "#o"
		case 't': // TODO
		case 'x': verb = "#x"
		case 'X': verb = "#X"
		case 's', 'c': verb = "q"
		}
	}

	res := "%"

	if f.plus {
		res += "+"
	} else if f.minus {
		res += " "
	}
	if f.left {
		res += "-"
	}

	if f.field != "" {
		if f.zeros {
			res += "0"
		}
		res += f.field
	}
	if f.prec != "" {
		res += "." + f.prec
	}

	res += verb

	return res
}
