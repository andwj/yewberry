// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "os"
import "io"
import "fmt"
import "strings"

import "github.com/peterh/liner"

// this is true when the REPL is active (interactive session).
var REPL bool

// the command-line editor for terminal input.
// always available in REPL, must be explicitly enabled in scripts.
var editor *liner.State

var ErrQuit = fmt.Errorf("user quit")
var ErrAbort = fmt.Errorf("user abort")
var ErrInput = fmt.Errorf("input error")

//----------------------------------------------------------------------

func RunREPL() (status int) {
	REPL = true

	EnableEditor()

	// this will reset the terminal into a usable state should
	// a panic occur somewhere.
	// [ unfortunately there is no way to recover from serious
	//   runtime issues like overflowing the Go stack... ]
	defer editor.Close()

	for {
		err := ProcessUserInput()

		switch {
		case err == ErrQuit:
			return 0

		case err == ErrInput:
			return 1

		case err == ErrAbort:
			return 2
		}
	}
}

func EnableEditor() {
	if editor == nil {
		editor = liner.NewLiner()
		editor.SetCtrlCAborts(true)
	}
}

func ProcessUserInput() error {
	code := ""

	ClearErrors()

	for {
		prompt := "> "
		if code != "" {
			prompt = ">> "
		}

		s, err := editor.Prompt(prompt)

		if err == io.EOF {
			// CTRL-D on a continuation line is a cancel
			if prompt == ">> " {
				fmt.Fprintf(os.Stderr, "\nCancelled\n")
				return Ok
			}

			fmt.Fprintf(os.Stderr, "\nEOF\n")
			return ErrQuit
		}

		if err == liner.ErrPromptAborted {
			fmt.Fprintf(os.Stderr, "Aborted\n")
			return ErrAbort
		}

		if err != nil {
			fmt.Fprintf(os.Stderr, "Input Error: %s\n", err.Error())
			return ErrInput
		}

		// ignore blank lines
		if s == "" {
			continue
		}

		if code == "" {
			code = s
		} else {
			code = code + "\n" + s
		}

		if !CheckUnterminated(code) {
			break
		}
	}

	editor.AppendHistory(SanitizeForHistory(code))

	// see if user wants to quit
	switch code {
	case "#q", "#quit", "quit", "QUIT", "#exit", "exit", "EXIT":
		return ErrQuit
	}

	if len(code) > 0 && code[0] == '#' {
		REPL_ExecCommand(code)
		return Ok
	}

	code_src := NewCodeSource()
	code_src.Add(NewStringSource(code))

	defer code_src.Finish()

	if ProcessCode(code_src) != OKAY {
		ShowAllErrors()

		PurgeFailedMacros()
		PurgeFailedTypes()
		PurgeFailedFunctions()
		PurgeFailedMethods()
	}

	// the REPL can ignore compile/runtime errors
	return Ok
}

func SanitizeForHistory(code string) string {
	// convert newlines to spaces
	// WISH: remove all code comments too
	return strings.Map(
		func(r rune) rune {
			if r >= 1 && r <= 31 {
				return ' '
			} else {
				return r
			}
		}, code)
}

func CheckUnterminated(code string) bool {
	// use the lexer to see if there is an unclosed bracket

	lex := NewLexer(strings.NewReader(code))

	for {
		t := lex.Scan()

		if t.Kind == TOK_EOF {
			return false
		}

		if t.Kind == TOK_ERROR {
			if strings.Contains(t.Str, "unterminated expr") {
				return true
			}
			return false
		}
	}
}

func Print(format string, a ...interface{}) {
	format = fmt.Sprintf(format, a...)

	fmt.Printf("%s", format)
}

func PurgeFailedFunctions() {
	for _, def := range context.defs {
		loc := def.loc
		if loc.ty.base == TYP_Function && loc.Clos.failed {
			context.RemoveDef(def.name)
		}
	}
}

//----------------------------------------------------------------------

func REPL_ExecCommand(line string) {
	ClearErrors()

	lex := NewLexer(strings.NewReader(line))

	words := make([]*Token, 0)

	for {
		t := lex.Scan()

		if t.Kind == TOK_EOF {
			break
		}
		if t.Kind == TOK_ERROR {
			Print("ERROR: %s\n", t.Str)
			return
		}
		words = append(words, t)
	}

	switch words[0].Str {
	case "#help":
		REPL_Help(words[1:])
	case "#show":
		REPL_Show(words[1:])
	case "#code":
		REPL_Code(words[1:])
	case "#parse":
		REPL_Parse(words[1:])
	case "#tokenize":
		REPL_TokenizeFile(words[1:])
	case "#hash":
		REPL_Hash(words[1:])
	default:
		Print("Unknown REPL command: %s\n", words[0].Str)
	}

	// show error messages from certain commands (e.g. #parse)
	ShowAllErrors()
}

func REPL_Help(args []*Token) {
	prt := func(line string) {
		Print("%s\n", line)
	}

	prt("Yewberry REPL summary:")
	prt("")
	prt("- enter literals and expressions to evaluate them:")
	prt("    > 123.456")
	prt("    123.456 : float")
	prt("    > (1 + 2 * 3)")
	prt("    7 : int")
	prt("")
	prt("- define global variables, functions, types:")
	prt("    > (let-mut VAR 5)")
	prt("    > (fun foo () (print \"hello!\"))")
	prt("    > (type Person (class .name str))")
	prt("")
	prt("- special commands:")
	prt("    #q  #quit  quit  #exit  exit  ; quit the REPL")
	prt("    #show   <thing>               ; show information about a thing")
	prt("    #parse  <token>...            ; parse tokens and show tree")
	prt("    #tokenize  <filename>         ; dump raw tokens from a file")
}

func REPL_Show(args []*Token) {
	if len(args) < 1 {
		Print("ERROR: missing argument for #show\n")
		return
	} else if len(args) > 1 {
		Print("ERROR: too many arguments for #show\n")
		return
	}

	t := args[0]

	if t.Kind != TOK_Name {
		switch t.Kind {
		case TOK_Int:
			Print("an integer literal.\n")
		case TOK_Float:
			Print("a floating point literal.\n")
		case TOK_Char:
			Print("a character literal.\n")
		case TOK_String:
			Print("a string literal.\n")
		case TOK_Module:
			Print("a module name.\n")

		case TOK_Expr:
			Print("an expression or special form.\n")
		case TOK_Access:
			Print("a data access expression.\n")
		case TOK_Data:
			Print("a compound literal.\n")

		default:
			Print("something weird.\n")
		}

		return
	}

	name := t.Str

	what := WhatIsGlobalName(name)

	switch what {
	case "keyword":
		Print("a language keyword.\n")
	case "operator":
		Print("a math operator.\n")
	case "macro":
		Print("a macro.\n")
	case "type":
		Print("a custom type.\n")
	case "global":
		def := context.GetDef(name)
		if def.ty.base == TYP_Function {
			Print("a global function.\n")
		} else if def.mutable {
			Print("a global variable.\n")
		} else {
			Print("a global constant.\n")
		}
		Print("type:  %s\n", def.ty.String())
		Print("value: %s\n", def.loc.DeepString())
	case "UNSET":
		Print("not defined (free to use).\n")
	default:
		Print("!!UNKNOWN!!\n")
	}
}

func REPL_Code(args []*Token) {
	var err2 cmError

	if len(args) < 1 {
		Print("ERROR: missing function name or expression\n")
		return
	}

	if len(args) > 2 {
		Print("ERROR: too many arguments to #code\n")
		return
	}

	// handle custom methods
	if len(args) == 2 {
		t_type := args[0]
		t_method := args[1]

		var user *Type
		if t_type.Kind == TOK_Name {
			user = N_GetType(t_type.Str, t_type.Module)
		}
		if user == nil {
			Print("ERROR: bad/unknown type: %s\n", t_type.String())
			return
		}

		fun_loc := user.GetMethod(t_method.Str)

		if fun_loc == nil {
			Print("ERROR: bad/unknown method: %s\n", t_method.String())
			return
		}

		DumpOps(fun_loc.Clos)
		return
	}

	t := args[0]

	// handle global functions
	if t.Kind == TOK_Name {
		name := t.Str
		def := N_GetDef(name, t.Module)
		if def == nil {
			if IsLanguageKeyword(name) {
				Print("no vm_ops (%s is a language keyword)\n", name)
				return
			}
			mac := N_GetMacro(name, "")
			if mac != nil {
				Print("no vm_ops (%s is a macro)\n", name)
				return
			}

			Print("ERROR: unknown function: %s\n", name)
			return
		}

		if def.ty.base != TYP_Function {
			Print("ERROR: %s is a variable, not a function\n", name)
			return
		}

		cl := def.loc.Clos
		if cl.builtin != nil {
			Print("no vm_ops (%s is a builtin function)\n", name)
			return
		}

		DumpOps(cl)
		return
	}

	if t.Kind != TOK_Expr {
		Print("ERROR: wanted name or expression, got %s\n", t.String())
		return
	}

	t, err2 = OperatorExpand(t)
	if err2 != OKAY {
		return
	}

	t, err2 = MacroExpand(t)
	if err2 != OKAY {
		return
	}

	if ParseElement(t) != OKAY {
		return
	}

	cl, err2 := CompileExpression(t)
	if err2 != OKAY {
		return
	}

	DumpOps(cl)
}

func REPL_Parse(args []*Token) {
	var err2 cmError

	for i, t := range args {
		if i > 0 {
			Print("\n")
		}

		Print("input: %s\n", t.String())

		if t.Kind == TOK_Expr {
			t, err2 = OperatorExpand(t)
			if err2 != OKAY {
				// error is shown later
				continue
			}

			t, err2 = MacroExpand(t)
			if err2 != OKAY {
				// error is shown later
				continue
			}
		}

		if ParseElement(t) != OKAY {
			// error is shown later
			continue
		}

		Print("output:\n")
		t.Dump(3)
	}
}

func REPL_TokenizeFile(args []*Token) {
	if len(args) < 1 {
		Print("ERROR: Missing file name\n")
		return
	}
	if args[0].Kind != TOK_String {
		Print("ERROR: bad file name, expected a string\n")
		return
	}

	f, err := os.Open(args[0].Str)
	if err != nil {
		Print("FILE ERROR: %s\n", err.Error())
		return
	}
	defer f.Close()

	lex := NewLexer(f)

	DumpTokens(lex)
}

func REPL_Hash(args []*Token) {
	var err2 cmError
	var lit Value

	if len(args) < 1 {
		Print("ERROR: missing argument to #hash\n")
		return
	}
	if len(args) > 1 {
		Print("ERROR: too many arguments to #hash\n")
		return
	}

	t := args[0]

	// Note: errors get shown later, not here

	if t.Kind == TOK_Expr {
		t, err2 = MacroExpand(t)
		if err2 != OKAY {
			return
		}
	}

	if ParseElement(t) != OKAY {
		return
	}
	if DeduceStaticValue(t, nil, true) != OKAY {
		return
	}
	if lit.EvaluateTree(t) != OKAY {
		return
	}

	Print("hash string: %s\n", lit.HashString())
}
